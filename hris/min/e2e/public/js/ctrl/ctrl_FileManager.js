$(document).ready(function() {
   var addParam = "";
   var sysData = "DataLibrary";
   var modName = "DataManager";
   $("#mAgency").click(function(){
      childFile = "modelAgency";
      addParam += "&n=agency";
      addParam += "&hTable=agency";
      addParam += "&hmode=List";
      addParam += "&hSysData=" + sysData;
      addParam += "&hChildFile=" + childFile;
      addParam += "&hGridTblHdr=Code|Name|Remarks";
      addParam += "&hGridTblFld=Code|Name|Remarks";
      addParam += "&hModel=" + childFile;
      gotoscrn(modName,addParam);
   });
   $("#mPayRate").click(function(){
      childFile = "modelPayRate";
      addParam += "&n=payrate";
      addParam += "&hTable=payrate";
      addParam += "&hmode=List";
      addParam += "&hSysData=" + sysData;
      addParam += "&hChildFile=" + childFile;
      addParam += "&hGridTblHdr=Code|Name|Remarks";
      addParam += "&hGridTblFld=Code|Name|Remarks";
      addParam += "&hModel=" + childFile;
      gotoscrn(modName,addParam);
   });
   $("#mPayPeriod").click(function(){
      childFile = "modelPayPeriod";
      addParam += "&n=pay period";
      addParam += "&hTable=payperiod";
      addParam += "&hmode=List";
      addParam += "&hSysData=" + sysData;
      addParam += "&hChildFile=" + childFile;
      addParam += "&hGridTblHdr=Code|Name|Remarks";
      addParam += "&hGridTblFld=Code|Name|Remarks";
      addParam += "&hModel=" + childFile;
      gotoscrn(modName,addParam);
   });
   $("#mBranch").click(function(){
      childFile = "modelBranch";
      addParam += "&n=Branch";
      addParam += "&hTable=Branch";
      addParam += "&hmode=List";
      addParam += "&hSysData=" + sysData;
      addParam += "&hChildFile=" + childFile;
      addParam += "&hGridTblHdr=Code|Name|Remarks";
      addParam += "&hGridTblFld=Code|Name|Remarks";
      addParam += "&hModel=" + childFile;
      gotoscrn(modName,addParam);
   });
   $("#mDept").click(function(){
      childFile = "modelDept";
      addParam += "&n=department";
      addParam += "&hTable=department";
      addParam += "&hmode=List";
      addParam += "&hSysData=" + sysData;
      addParam += "&hChildFile=" + childFile;
      addParam += "&hGridTblHdr=Code|Name|Remarks";
      addParam += "&hGridTblFld=Code|Name|Remarks";
      addParam += "&hModel=" + childFile;
      gotoscrn(modName,addParam);
   });
   $("#mDesign").click(function(){
      childFile = "modelDesignation";
      addParam += "&n=Designation";
      addParam += "&hTable=designation";
      addParam += "&hmode=List";
      addParam += "&hSysData=" + sysData;
      addParam += "&hChildFile=" + childFile;
      addParam += "&hGridTblHdr=Code|Name|Remarks";
      addParam += "&hGridTblFld=Code|Name|Remarks";
      addParam += "&hModel=" + childFile;
      gotoscrn(modName,addParam);

   });
   $("#mOccupations").click(function(){
      childFile = "modelOccupations";
      addParam += "&n=Occupations";
      addParam += "&hTable=occupations";
      addParam += "&hmode=List";
      addParam += "&hSysData=" + sysData;
      addParam += "&hChildFile=" + childFile;
      addParam += "&hGridTblHdr=Code|Name|Description|Remarks";
      addParam += "&hGridTblFld=Code|Name|Description|Remarks";
      addParam += "&hModel=" + childFile;
      gotoscrn(modName,addParam);
   });
   $("#mDiv").click(function(){
      childFile = "modelDivision";
      addParam += "&n=Division";
      addParam += "&hTable=division";
      addParam += "&hmode=List";
      addParam += "&hSysData=" + sysData;
      addParam += "&hChildFile=" + childFile;
      addParam += "&hGridTblHdr=Code|Name|Remarks";
      addParam += "&hGridTblFld=Code|Name|Remarks";
      addParam += "&hModel=" + childFile;
      gotoscrn(modName,addParam);
   });
   $("#mLoc").click(function(){
      childFile = "modelLocations";
      addParam += "&n=Locations";
      addParam += "&hTable=Locations";
      addParam += "&hmode=List";
      addParam += "&hSysData=" + sysData;
      addParam += "&hChildFile=" + childFile;
      addParam += "&hGridTblHdr=Code|Name|Remarks";
      addParam += "&hGridTblFld=Code|Name|Remarks";
      addParam += "&hModel=" + childFile;
      gotoscrn(modName,addParam);
   });
   $("#mPosClass").click(function(){
      childFile = "modelPosClass";
      addParam += "&n=Position classification";
      addParam += "&hTable=positionclassification";
      addParam += "&hmode=List";
      addParam += "&hSysData=" + sysData;
      addParam += "&hChildFile=" + childFile;
      addParam += "&hGridTblHdr=Code|Name|Remarks";
      addParam += "&hGridTblFld=Code|Name|Remarks";
      addParam += "&hModel=" + childFile;
      gotoscrn(modName,addParam);
   });
   $("#mPosLev").click(function(){
      childFile = "modelPosLevel";
      addParam += "&n=Position Level";
      addParam += "&hTable=positionlevel";
      addParam += "&hmode=List";
      addParam += "&hSysData=" + sysData;
      addParam += "&hChildFile=" + childFile;
      addParam += "&hGridTblHdr=Code|Name|Remarks";
      addParam += "&hGridTblFld=Code|Name|Remarks";
      addParam += "&hModel=" + childFile;
      gotoscrn(modName,addParam);

   });
   $("#mPosName").click(function(){
      childFile = "modelPosition";
      addParam += "&n=Position";
      addParam += "&hTable=position";
      addParam += "&hmode=List";
      addParam += "&hSysData=" + sysData;
      addParam += "&hChildFile=" + childFile;
      addParam += "&hGridTblHdr=Code|Name|Private|Remarks";
      addParam += "&hGridTblFld=Code|Name|IsPrivate|Remarks";
      addParam += "&hModel=" + childFile;
      gotoscrn(modName,addParam);
   });
   $("#mPosItem").click(function(){
      childFile = "modelPosItem";
      addParam += "&n=Position Item";
      addParam += "&hTable=positionitem";
      addParam += "&hmode=List";
      addParam += "&hSysData=" + sysData;
      addParam += "&hChildFile=" + childFile;
      addParam += "&hGridTblHdr=Name|Position|Remarks";
      addParam += "&hGridTblFld=Name|PositionRefId|Remarks";
      addParam += "&hModel=" + childFile;
      gotoscrn(modName,addParam);
   });
   $("#mSection").click(function(){
      childFile = "modelSections";
      addParam += "&n=Branch";
      addParam += "&hTable=Sections";
      addParam += "&hmode=List";
      addParam += "&hSysData=" + sysData;
      addParam += "&hChildFile=" + childFile;
      addParam += "&hGridTblHdr=Code|Name|Remarks";
      addParam += "&hGridTblFld=Code|Name|Remarks";
      addParam += "&hModel=" + childFile;
      gotoscrn(modName,addParam);

   });
   $("#mUnit").click(function(){
      childFile = "modelUnits";
      addParam += "&n=Units";
      addParam += "&hTable=Units";
      addParam += "&hmode=List";
      addParam += "&hSysData=" + sysData;
      addParam += "&hChildFile=" + childFile;
      addParam += "&hGridTblHdr=Code|Name|Remarks";
      addParam += "&hGridTblFld=Code|Name|Remarks";
      addParam += "&hModel=" + childFile;
      gotoscrn(modName,addParam);

   });
   $("#mAppnt").click(function(){
      childFile = "modelApptStatus";
      addParam += "&n=Appointment Status";
      addParam += "&hTable=ApptStatus";
      addParam += "&hmode=List";
      addParam += "&hSysData=" + sysData;
      addParam += "&hChildFile=" + childFile;
      addParam += "&hGridTblHdr=Code|Name|Type|Remarks";
      addParam += "&hGridTblFld=Code|Name|ApptType|Remarks";
      addParam += "&hModel=" + childFile;
      gotoscrn(modName,addParam);

   });
   $("#mBlood").click(function(){
      modName   = "DataManager";
      childFile = "modelBloodType";
      sysData   = "DataLibrary";
      addParam += "&n=blood type";
      addParam += "&hTable=bloodtype";
      addParam += "&hmode=List";
      addParam += "&hSysData=" + sysData;
      addParam += "&hChildFile=" + childFile;
      addParam += "&hGridTblHdr=Code|Name|Remarks";
      addParam += "&hGridTblFld=Code|Name|Remarks";
      addParam += "&hModel=" + childFile;
      gotoscrn(modName,addParam);
   });
   $("#mCitizen").click(function(){
      modName   = "DataManager";
      childFile = "modelCitizenship";
      sysData   = "DataLibrary";
      addParam += "&n=Citizenship";
      addParam += "&hTable=Citizenship";
      addParam += "&hmode=List";
      addParam += "&hSysData=" + sysData;
      addParam += "&hChildFile=" + childFile;
      addParam += "&hGridTblHdr=Code|Name|Remarks";
      addParam += "&hGridTblFld=Code|Name|Remarks";
      addParam += "&hModel=" + childFile;
      gotoscrn(modName,addParam);

   });
   $("#mOrg").click(function(){
      childFile = "modelOrg";
      addParam += "&n=organization";
      addParam += "&hTable=organization";
      addParam += "&hmode=List";
      addParam += "&hSysData=" + sysData;
      addParam += "&hChildFile=" + childFile;
      addParam += "&hGridTblHdr=Code|Name|Address|Remarks";
      addParam += "&hGridTblFld=Code|Name|Address|Remarks";
      addParam += "&hModel=" + childFile;
      gotoscrn(modName,addParam);
   });
   $("#mCourseCat").click(function(){
      childFile = "modelCourseCat";
      addParam += "&n=Course Category";
      addParam += "&hTable=coursecategory";
      addParam += "&hmode=List";
      addParam += "&hSysData=" + sysData;
      addParam += "&hChildFile=" + childFile;
      addParam += "&hGridTblHdr=Code|Name|Remarks";
      addParam += "&hGridTblFld=Code|Name|Remarks";
      addParam += "&hModel=" + childFile;
      gotoscrn(modName,addParam);
   });
   $("#mCourse").click(function(){
      childFile = "modelCourse";
      addParam += "&n=Course";
      addParam += "&hTable=course";
      addParam += "&hmode=List";
      addParam += "&hSysData=" + sysData;
      addParam += "&hChildFile=" + childFile;
      addParam += "&hGridTblHdr=Code|Name|Remarks";
      addParam += "&hGridTblFld=Code|Name|Remarks";
      addParam += "&hModel=" + childFile;
      gotoscrn(modName,addParam);
   });
   $("#mSchool").click(function(){
      childFile = "modelSchools";
      addParam += "&n=Schools";
      addParam += "&hTable=Schools";
      addParam += "&hmode=List";
      addParam += "&hSysData=" + sysData;
      addParam += "&hChildFile=" + childFile;
      addParam += "&hGridTblHdr=Code|Name|Remarks";
      addParam += "&hGridTblFld=Code|Name|Remarks";
      addParam += "&hModel=" + childFile;
      gotoscrn(modName,addParam);

   });
   $("#mCareer").click(function(){
      modName   = "DataManager";
      childFile = "modelCareerService";
      sysData   = "DataLibrary";
      addParam += "&n=career service";
      addParam += "&hTable=careerservice";
      addParam += "&hmode=List";
      addParam += "&hSysData=" + sysData;
      addParam += "&hChildFile=" + childFile;
      addParam += "&hGridTblHdr=Code|Name|Remarks";
      addParam += "&hGridTblFld=Code|Name|Remarks";
      addParam += "&hModel=" + childFile;
      gotoscrn(modName,addParam);
   });
   $("#mSemClass").click(function(){
      modName   = "DataManager";
      childFile = "modelSeminarClassifications";
      sysData   = "DataLibrary";
      addParam += "&n=seminar class";
      addParam += "&hTable=seminarclass";
      addParam += "&hmode=List";
      addParam += "&hSysData=" + sysData;
      addParam += "&hChildFile=" + childFile;
      addParam += "&hGridTblHdr=Code|Name|Remarks";
      addParam += "&hGridTblFld=Code|Name|Remarks";
      addParam += "&hModel=" + childFile;
      gotoscrn(modName,addParam);

   });
   $("#mSemTraining").click(function(){
      modName   = "DataManager";
      childFile = "modelSeminars";
      sysData   = "DataLibrary";
      addParam += "&n=Learning and Development";
      addParam += "&hTable=Seminars";
      addParam += "&hmode=List";
      addParam += "&hSysData=" + sysData;
      addParam += "&hChildFile=" + childFile;
      addParam += "&hGridTblHdr=Code|Name|Remarks";
      addParam += "&hGridTblFld=Code|Name|Remarks";
      addParam += "&hModel=" + childFile;
      gotoscrn(modName,addParam);
   });
   $("#mSalGrade").click(function(){
      modName   = "DataManager";
      childFile = "modelSalaryGrade";
      sysData   = "DataLibrary";
      addParam += "&n=salary grade";
      addParam += "&hTable=salarygrade";
      addParam += "&hmode=List";
      addParam += "&hSysData=" + sysData;
      addParam += "&hChildFile=" + childFile;
      addParam += "&hGridTblHdr=Name|Step 1|Step 2|Step 3|Step 4|Step 5|Step 6|Step 7|Step 8";
      addParam += "&hGridTblFld=Name|Step1|Step2|Step3|Step4|Step5|Step6|Step7|Step8";
      addParam += "&hModel=" + childFile;
      gotoscrn(modName,addParam);
   });
   $("#mJobGrade").click(function(){
      modName   = "DataManager";
      childFile = "modelJobGrade";
      sysData   = "DataLibrary";
      addParam += "&n=job grade";
      addParam += "&hTable=jobgrade";
      addParam += "&hmode=List";
      addParam += "&hSysData=" + sysData;
      addParam += "&hChildFile=" + childFile;
      addParam += "&hGridTblHdr=Name|Step 1|Annual (S1)|Step 2|Annual (S2)|Step 3|Annual (S3)|Step 4|Annual (S4)|Step 5|Annual (S5)|Step 6|Annual (S6)|Step 7|Annual (S7)|Step 8|Annual (S8)";
      addParam += "&hGridTblFld=Name|Step1|AnnualS1|Step2|AnnualS2|Step3|AnnualS3|Step4|AnnualS4|Step5|AnnualS5|Step6|AnnualS6|Step7|AnnualS7|Step8|AnnualS8";
      addParam += "&hModel=" + childFile;
      gotoscrn(modName,addParam);
   });
   $("#mStep").click(function(){
      modName   = "DataManager";
      childFile = "modelStepIncrement";
      sysData   = "DataLibrary";
      addParam += "&n=Step Increment";
      addParam += "&hTable=StepIncrement";
      addParam += "&hmode=List";
      addParam += "&hSysData=" + sysData;
      addParam += "&hChildFile=" + childFile;
      addParam += "&hGridTblHdr=Code|Name|Remarks";
      addParam += "&hGridTblFld=Code|Name|Remarks";
      addParam += "&hModel=" + childFile;
      gotoscrn(modName,addParam);
   });
   $("#mNature").click(function(){
      modName   = "DataManager";
      childFile = "modelCaseNature";
      sysData   = "DataLibrary";
      addParam += "&n=case nature";
      addParam += "&hTable=casenature";
      addParam += "&hmode=List";
      addParam += "&hSysData=" + sysData;
      addParam += "&hChildFile=" + childFile;
      addParam += "&hGridTblHdr=Code|Name|Remarks";
      addParam += "&hGridTblFld=Code|Name|Remarks";
      addParam += "&hModel=" + childFile;
      gotoscrn(modName,addParam);

   });
   $("#mSemPlace").click(function(){
      modName   = "DataManager";
      childFile = "modelSeminarPlace";
      sysData   = "DataLibrary";
      addParam += "&n=Seminar Place";
      addParam += "&hTable=SeminarPlace";
      addParam += "&hmode=List";
      addParam += "&hSysData=" + sysData;
      addParam += "&hChildFile=" + childFile;
      addParam += "&hGridTblHdr=Code|Name|Remarks";
      addParam += "&hGridTblFld=Code|Name|Remarks";
      addParam += "&hModel=" + childFile;
      gotoscrn(modName,addParam);

   });
   $("#mSemSponsor").click(function(){
      modName   = "DataManager";
      childFile = "modelSeminarSponsor";
      sysData   = "DataLibrary";
      addParam += "&n=Seminar Sponsor";
      addParam += "&hTable=SeminarSponsor";
      addParam += "&hmode=List";
      addParam += "&hSysData=" + sysData;
      addParam += "&hChildFile=" + childFile;
      addParam += "&hGridTblHdr=Code|Name|Remarks";
      addParam += "&hGridTblFld=Code|Name|Remarks";
      addParam += "&hModel=" + childFile;
      gotoscrn(modName,addParam);

   });
   $("#mOffice").click(function(){
      modName   = "DataManager";
      childFile = "modelOffice";
      sysData   = "DataLibrary";
      addParam += "&n=Office";
      addParam += "&hTable=Office";
      addParam += "&hmode=List";
      addParam += "&hSysData=" + sysData;
      addParam += "&hChildFile=" + childFile;
      addParam += "&hGridTblHdr=Code|Name|Remarks";
      addParam += "&hGridTblFld=Code|Name|Remarks";
      addParam += "&hModel=" + childFile;
      gotoscrn(modName,addParam);

   });
   $("#mSemType").click(function(){
      modName   = "DataManager";
      childFile = "modelSeminarType";
      sysData   = "DataLibrary";
      addParam += "&n=Seminar Type";
      addParam += "&hTable=SeminarType";
      addParam += "&hmode=List";
      addParam += "&hSysData=" + sysData;
      addParam += "&hChildFile=" + childFile;
      addParam += "&hGridTblHdr=Code|Name|Remarks";
      addParam += "&hGridTblFld=Code|Name|Remarks";
      addParam += "&hModel=" + childFile;
      gotoscrn(modName,addParam);
   });
   $("#mCity").click(function(){
      modName   = "DataManager";
      childFile = "modelCity";
      sysData   = "DataLibrary";
      addParam += "&n=City";
      addParam += "&hTable=City";
      addParam += "&hmode=List";
      addParam += "&hSysData=" + sysData;
      addParam += "&hChildFile=" + childFile;
      addParam += "&hGridTblHdr=Province|Name|Remarks";
      addParam += "&hGridTblFld=ProvinceRefId|Name|Remarks";
      addParam += "&hModel=" + childFile;
      gotoscrn(modName,addParam);
   });
   $("#mProvince").click(function(){
      modName   = "DataManager";
      childFile = "modelProvince";
      sysData   = "DataLibrary";
      addParam += "&n=Province";
      addParam += "&hTable=Province";
      addParam += "&hmode=List";
      addParam += "&hSysData=" + sysData;
      addParam += "&hChildFile=" + childFile;
      addParam += "&hGridTblHdr=Code|Name|Remarks";
      addParam += "&hGridTblFld=Code|Name|Remarks";
      addParam += "&hModel=" + childFile;
      gotoscrn(modName,addParam);
   });
   $("#mSysGroup").click(function(){
      modName   = "DataManager";
      childFile = "modelSysGroup";
      sysData   = "DataLibrary";
      addParam += "&n=System Group";
      addParam += "&hTable=SysGroup";
      addParam += "&hmode=List";
      addParam += "&hSysData=" + sysData;
      addParam += "&hChildFile=" + childFile;
      addParam += "&hGridTblHdr=Code|Name|Remarks";
      addParam += "&hGridTblFld=Code|Name|Remarks";
      addParam += "&hModel=" + childFile;
      gotoscrn(modName,addParam);
   });
   $("#mSysModules").click(function(){
      modName   = "DataManager";
      childFile = "modelSysModules";
      sysData   = "DataLibrary";
      addParam += "&n=System Modules";
      addParam += "&hTable=SysModules";
      addParam += "&hmode=List";
      addParam += "&hSysData=" + sysData;
      addParam += "&hChildFile=" + childFile;
      addParam += "&hGridTblHdr=Code|Name|Remarks";
      addParam += "&hGridTblFld=Code|Name|Remarks";
      addParam += "&hModel=" + childFile;
      gotoscrn(modName,addParam);
   });
   $("#mSysUsers").click(function(){
      modName   = "DataManager";
      childFile = "modelSysUser";
      sysData   = "DataLibrary";
      addParam += "&n=System User";
      addParam += "&hTable=SysUser";
      addParam += "&hmode=List";
      addParam += "&hSysData=" + sysData;
      addParam += "&hChildFile=" + childFile;
      addParam += "&hGridTblHdr=Employees Name|System Group|User Name";
      addParam += "&hGridTblFld=EmployeesRefId|SysGroupRefId|UserName";
      addParam += "&hModel=" + childFile;
      gotoscrn(modName,addParam);
   });
   $("#mEmpStatus").click(function(){
      modName   = "DataManager";
      childFile = "modelEmpStatus";
      sysData   = "DataLibrary";
      addParam += "&n=Employment Status";
      addParam += "&hTable=EmpStatus";
      addParam += "&hmode=List";
      addParam += "&hSysData=" + sysData;
      addParam += "&hChildFile=" + childFile;
      addParam += "&hGridTblHdr=Code|Name|Remarks";
      addParam += "&hGridTblFld=Code|Name|Remarks";
      addParam += "&hModel=" + childFile;
      gotoscrn(modName,addParam);
   });
   $("#mRequirements").click(function(){
      modName   = "DataManager";
      childFile = "modelRequirements";
      sysData   = "DataLibrary";
      addParam += "&n=Requirements";
      addParam += "&hTable=Requirements";
      addParam += "&hmode=List";
      addParam += "&hSysData=" + sysData;
      addParam += "&hChildFile=" + childFile;
      addParam += "&hGridTblHdr=Code|Name|Remarks";
      addParam += "&hGridTblFld=Code|Name|Remarks";
      addParam += "&hModel=" + childFile;
      gotoscrn(modName,addParam);
   });
   $("#mSignatories").click(function(){
      modName   = "DataManager";
      childFile = "modelSignatories";
      sysData   = "DataLibrary";
      addParam += "&n=Signatories";
      addParam += "&hTable=Signatories";
      addParam += "&hmode=List";
      addParam += "&hSysData=" + sysData;
      addParam += "&hChildFile=" + childFile;
      addParam += "&hGridTblHdr=Name|Position|Division";
      addParam += "&hGridTblFld=Name|PositionRefId|DivisionRefId";
      addParam += "&hModel=" + childFile;
      gotoscrn(modName,addParam);
   });
   $("#mDocType").click(function(){
      modName   = "DataManager";
      childFile = "modelDocType";
      sysData   = "DataLibrary";
      addParam += "&n=Document Type Attachment";
      addParam += "&hTable=AttachDocType";
      addParam += "&hmode=List";
      addParam += "&hSysData=" + sysData;
      addParam += "&hChildFile=" + childFile;
      addParam += "&hGridTblHdr=Code|Name|Description";
      addParam += "&hGridTblFld=Code|Name|Description";
      addParam += "&hModel=" + childFile;
      gotoscrn(modName,addParam);
   });
   $("#mAward").click(function(){
      modName   = "DataManager";
      childFile = "modelAwards";
      sysData   = "DataLibrary";
      addParam += "&n=Awards";
      addParam += "&hTable=awards";
      addParam += "&hmode=List";
      addParam += "&hSysData=" + sysData;
      addParam += "&hChildFile=" + childFile;
      addParam += "&hGridTblHdr=Code|Name|Description";
      addParam += "&hGridTblFld=Code|Name|Description";
      addParam += "&hModel=" + childFile;
      gotoscrn(modName,addParam);
   });
   $("#mReports").click(function(){
      modName   = "DataManager";
      childFile = "modelReports";
      addParam += "&n=Reports";
      addParam += "&hTable=reports";
      addParam += "&hmode=List";
      addParam += "&hChildFile=" + childFile;
      addParam += "&hGridTblHdr=Code|Name|System|Filename";
      addParam += "&hGridTblFld=Code|Name|SystemRefId|Filename";
      addParam += "&hModel=" + childFile;
      //addParam += "&globalParam=" + $("#globalParam").val();
      gotoscrn(modName,addParam);
   });
   $("#mCountry").click(function(){
      modName   = "DataManager";
      childFile = "modelCountry";
      sysData   = "DataLibrary";
      addParam += "&n=Country";
      addParam += "&hTable=country";
      addParam += "&hmode=List";
      addParam += "&hSysData=" + sysData;
      addParam += "&hChildFile=" + childFile;
      addParam += "&hGridTblHdr=Code|Name|Remarks";
      addParam += "&hGridTblFld=Code|Name|Remarks";
      addParam += "&hModel=" + childFile;
      gotoscrn(modName,addParam);
   });
   $("#mForms").click(function(){
      modName   = "DataManager";
      childFile = "modelForms";
      sysData   = "DataLibrary";
      addParam += "&n=Forms";
      addParam += "&hTable=Forms";
      addParam += "&hmode=List";
      addParam += "&hSysData=" + sysData;
      addParam += "&hChildFile=" + childFile;
      addParam += "&hGridTblHdr=Code|Name|Description|Remarks";
      addParam += "&hGridTblFld=Code|Name|Description|Remarks";
      addParam += "&hModel=" + childFile;
      gotoscrn(modName,addParam);
   });
   $("#mSystem").click(function(){
      modName   = "DataManager";
      childFile = "modelSystem";
      sysData   = "DataLibrary";
      addParam += "&n=System";
      addParam += "&hTable=System";
      addParam += "&hmode=List";
      addParam += "&hSysData=" + sysData;
      addParam += "&hChildFile=" + childFile;
      addParam += "&hGridTblHdr=Code|Short Name|Name|Remarks";
      addParam += "&hGridTblFld=Code|ShortName|Name|Remarks";
      addParam += "&hModel=" + childFile;
      gotoscrn(modName,addParam);
   });
   $("#mModules").click(function(){
      modName   = "DataManager";
      childFile = "modelModules";
      sysData   = "DataLibrary";
      addParam += "&n=Modules";
      addParam += "&hTable=Modules";
      addParam += "&hmode=List";
      addParam += "&hSysData=" + sysData;
      addParam += "&hChildFile=" + childFile;
      addParam += "&hGridTblHdr=Code|Name|Description|Remarks";
      addParam += "&hGridTblFld=Code|Name|Description|Remarks";
      addParam += "&hModel=" + childFile;
      gotoscrn(modName,addParam);
   });


   /*BTN TRANSACTION*/
   /*$("#btnINSERT").click(function () {
      viewInfo(0,1,"");
   });
   $("#btnREFRESH").click(function () {
      refreshList("");
   });
   $("#btnCANCEL").click(function () {
      pageBehavior($("[name='hRefId']").val(),5);
   });

   $("#btnSAVE").click(function () {

      if ($("[name='char_Code']").val() == "") {
         alert("Code Entry is required");
         $("[name='char_Code']").focus();
         return false;
      }
      if ($("[name='char_Name']").val() == "") {
         alert("Name Entry is required");
         $("[name='char_Name']").focus();
         return false;
      }
      if ($("[name='char_Address']").val() == "") {
         alert("Address Entry is required");
         $("[name='char_Address']").focus();
         return false;
      }
      if ($("[name='deci_SalaryAmount']").val() == "") {
         alert("Salary Amount Entry is required");
         $("[name='deci_SalaryAmount']").focus();
         return false;
      }

      objnval = getElement_ObjnValue();
      var saveSuccess = false;
      if (objnval != "") {
         setCookie("cookies_Object_Value",objnval,1);
         $.post("SystemAjax.e2e.php",
         {
            task:"ajxSaveRecord",
            obj_n_value:objnval,
            refid:$("[name='hRefId']").val(),
            table:$("[name='hTable']").val()
         },
         function(data,status) {
            code = data;
            try {
                eval(code);
            } catch (e) {
                if (e instanceof SyntaxError) {
                    alert(e.message);
                }
            }
         });
      }
   });


   $("#sint_SystemRefId").change(function () {
      $.get("trn.e2e.php",
      {
         fn:"setScrnId",
         systemRefId:$(this).val()
      },
      function(data,status) {
         if (status == "success") {
            try {
               eval(data);
            } catch (e) {
               if (e instanceof SyntaxError) {
                  alert(e.message);
               }
            }
         }
      });
   });*/

   $("#btnCLOSE").click(function () {
      closeSCRN('model');
   });
   $("#btnBACK").click(function () {
      pageBehavior($("[name='hRefId']").val(),5);
   });
   $("button.close").click(function () {
      $(".alert").hide(500);
   });

   $("h5#titleAdmSetUp").click(function(){
      $("#AdmSetUp").slideToggle(300);
   });
   $("h5#titleEmpSetUp").click(function(){
      $("#EmpSetUp").slideToggle(300);
   });
   $("h5#titleCompSetUp").click(function(){
      $("#CompSetUp").slideToggle(300);
   });

   $("#btnDELETE").click(function(){
   });
   if ($("#fmMenu").val() == "Close") {
      $("#AdmSetUp").slideUp(100);
      $("#EmpSetUp").slideUp(100);
      $("#CompSetUp").slideUp(100);
   }
});

function afterEditSave(refid) {
   alert("Record Updated");
   refreshList("");
   //viewInfo(refid,3);

}
function afterNewSave(newRefId) {
   alert("New Record Inserted\nRef. ID: " + newRefId);
   refreshList($("#hParam").val());
}
function afterDelete() {
   alert("Record Deleted");
   refreshList("");
}

function closeSCRN(scrntype) {
   var addParam = "";
   addParam += "&paramTitle=FILE MANAGER";
   addParam += "&hTable=";
   addParam += "&hGridTblHdr=";
   addParam += "&hGridTblFld=";
   gotoscrn("scrnFileManager",addParam);
}
/*
function viewInfo(refid,mode,dum) {
   var url = $("[name='hModel']").val() + ".e2e.php?";
   url += "hRefId=" + refid;
   url += "&hScrnMode=" + mode;
   url += "&uplvl=" + mode;
   url += "&hTable=" + $("[name='hTable']").val();
   $("#scrnModel").html("");
   $("#scrnModel").load(url, function(responseTxt, statusTxt, xhr){
      if(statusTxt == "error")
         alert("Ooops Error: " + xhr.status + ": " + xhr.statusText);
         return false;
   });
   pageBehavior(refid,mode);
}
*/
function pageBehavior(refid,mode) {
   $(".alert").hide();
   $("[name='hRefId']").val(refid);
   switch (mode) {
      case 1:
      case 2:
      case 3:
         $("#divList, #divView, #spBACK, #spSAVECANCEL").hide();
         if (mode == 1) { // ADD
            $(".form-input").val("");
            $(".form-input").attr("disabled",false);
            $("#divView, #spSAVECANCEL").show();
         }
         else if (mode == 2) { // EDIT
            $(".form-input").attr("disabled",true);
            $("#divView, #spSAVECANCEL").show();
            $("#ScreenMode").html("MODIFYING");
         }
         else if (mode == 3) { // VIEW
            $(".form-input").attr("disabled",true);
            $("#divView, #spBACK").show();
            $("#ScreenMode").html("VIEWING");
         }
      break;
      case 4:
         $("#divList").show();
         $("#divView").hide();
      break;
      case 5:
         $("#divList").show();
         $("#divView").hide();
      break;
   }
}

