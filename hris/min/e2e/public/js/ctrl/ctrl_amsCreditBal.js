$(document).ready(function() {
   var credit = $("[name='char_NameCredits']").val();
   if (credit != "") {
      $("#LeaveScreen, #OTScreen").hide();
      if (drpVal == "OT") {
         $("#OTScreen").show(150);
      }
      if (
            drpVal == "SL" ||
            drpVal == "VL"
         ) {
         $("#LeaveScreen").show(150);
         if (drpVal == "SL") {
            $("#ForceLeave").prop("disabled",true);
         } else {
            $("#ForceLeave").prop("disabled",false);
         }
      }
   }
   $("[name='char_NameCredits']").change(function (){
      var drpVal = $(this).val();
      $("#LeaveScreen, #OTScreen").hide();
      if (drpVal == "OT") {
         $("#OTScreen").show(150);
      }
      if (
            drpVal == "SL" ||
            drpVal == "VL" ||
            drpVal == "SPL"
         ) {
         $("#LeaveScreen").show(150);
         if (drpVal == "SL") {
            $("#ForceLeave").prop("disabled",true);
         } else {
            $("#ForceLeave").prop("disabled",false);
         }
      }
   });
   $("#LocSAVE").click(function () {
      $("[name='char_NameCredits']").prop("disabled",false);
      //if (saveProceed() == 0) {
      //alert($("[name='hRefId']").length);
         $.ajax({
            url: "postData.e2e.php",
            type: "POST",
            data: new FormData($("[name='xForm']")[0]),
            success : function(responseTxt){
               if (responseTxt.trim() != "") {
                  alert(responseTxt);
               } else {
                  var param = setAddURL();
                  if ($("[name='txtRefId']").length) {
                     param += "&txtLName=" + $("[name='txtLName']").val();
                     param += "&txtFName=" + $("[name='txtFName']").val();
                     param += "&txtMidName=" + $("[name='txtMidName']").val();
                     param += "&txtRefId=" + $("[name='txtRefId']").val();
                  }
                  gotoscrn ($("#hProg").val(), param);
               }
            },
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            cache: false
         });
      //}
   });
   $(function() {
      $("#LeaveScreen, #OTScreen").hide();
   });
});

function afterNewSave(LastRefId) {
   alert("New Record " + LastRefId + " Succesfully Inserted");
   return false;
}
function afterDelete() {
   alert("Successfully Deleted");
   gotoscrn($("#hProg").val(),"");
}
function afterEditSave(RefId){
   alert("Record " + RefId + " Succesfully Updated");
   //gotoscrn ("amsFileSetup", setAddURL());
   return false;
}
function selectedItems(emprefid,lname,fname) {
   $(".list-group-item").removeClass("active");
   $("[name='txtEmpName'").val(lname + ", " + fname);
   $("#" + emprefid).addClass("active");
   $("[name='sint_EmployeesRefId']").val(emprefid);
   $.post("changeSessionValue.e2e.php",
   {
      hGridTblHdr:"Credits Name|Effectivity Year|Beg. Balance|Bal. As Of Date",
      hGridTblFld:"NameCredits|EffectivityYear|BeginningBalance|BegBalAsOfDate",
      hGridTblId:"gridTable",
      hGridDBTable:"employeescreditbalance",
      sql:"SELECT * FROM `employeescreditbalance` WHERE `EmployeesRefId` = " + emprefid + " ORDER BY RefId Desc LIMIT 100",
      listAction:[true,true,true],
      empselected:emprefid
   },
   function(data,status){
      if (status=='success') {
         refreshTable(emprefid);
         tr_Click(emprefid);
      }
   });
}
function selectMe(emprefid) {
   $.get("EmpQuery.e2e.php",
   {
      emprefid:emprefid,
      hCompanyID:$("#hCompanyID").val(),
      hBranchID:$("#hBranchID").val(),
      hEmpRefId:$("#hEmpRefId").val(),
      hUserRefId:$("#hUserRefId").val()
   },
   function(data,status) {
      $('#selectedEmployees').html("&nbsp;");
      if(status == "error") return false;
      if(status == "success"){
         var data = JSON.parse(data);
         try
         {
            setHTMLById("selectedEmployees",data.LastName + ", " + data.FirstName + " " + data.MiddleName);
            selectedItems(emprefid,
                          data.LastName,
                          data.FirstName);
         }
         catch (e)
         {
            if (e instanceof SyntaxError) {
                alert(e.message);
            }
         }
      }
   });
}
function refreshTable(emprefid) {
   $("#spGridTable").html("");
   $("#spGridTable").load("listRefresh.e2e.php",
   {
      EmployeesRefid : emprefid
   },
   function(responseTxt, statusTxt, xhr){
      if(statusTxt == "error")
         alert("Ooops Error: " + xhr.status + ": " + xhr.statusText);
         return false;
   });
}