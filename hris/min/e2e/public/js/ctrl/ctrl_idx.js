$(document).ready(function () {
   $("#login_div").hide();
   $("#signUp").click(function () {
      loadscrn("signup");
   });
   $("#forgotPW").click(function () {
      loadscrn("setUsername");
   });
   $("#btnNextScrn").click(function () {
      urlParam = "fpw" + "&u=" + $("[name='txtUserName']").val();
      loadscrn(urlParam);
   });
   $(".close").click(function () {
      $("#EntryScrn").html("");
      $("#login").fadeIn(500);
   });
   $("#btnNewPassword").click(function(){

   });
   $("#btnBack").click(function(){
      $("#login_div, #module_div_2").hide();
      $("#module_div").show();
   });
   $("#btnNextModule").click(function(){
      $("#module_div, #login_div").hide();
      $("#module_div_2").show();
   });
   $("#btnBackModule").click(function(){
      $("#module_div_2").hide();
      $("#module_div").show();
   });
   $("#btnSubmitSignUp").click(function(){
      $("[name='char_AnsQues1']").val(calcHash($("[name='AnsQues1']").val()));
      $("[name='char_AnsQues2']").val(calcHash($("[name='AnsQues2']").val()));
      $("[name='AnsQues1']").val("");
      $("[name='AnsQues2']").val("");
      $("[name='hmode']").val("ADD");
      $("[name='hRefId']").val(0);
      var obj = getFieldEntry("SignUpEntry","ADD");
      gSaveRecord(obj,"newsignup");
   });

   $("[id*='user_']").each(function () {
      $(this).click(function () {
         var account = $(this).attr("id");
         var module_selected  = $(this).attr("module");
         account = account.split("_")[1];
         if (module_selected == "rms") {
            //self.location = "//pccrmsst.mye2esolutions.com";
         } else if (module_selected == "pms") {
            self.location = "../../../../../../../../../../payroll/public";
         } else {
            window.open("idx.e2e.php?account="+ account + "&module=" + module_selected,"_blank");
            // $("#hAccount").val(account);
            // $("#hModule").val(module_selected);
            // $("#module_div, #module_div_2").hide();
            // $("#login_div").show();   
         }
      });
   });

   $("[name='btnLogin']").click(function () {
      if (saveProceed() == 0) {
         $.get("trn.e2e.php",
         {
            fn:"ChkUName",
            t:calcHash($("[name='token").val()),
            u:$("[name='txtUser']").val(),
            account:$("#hAccount").val()
         },
         function(data,status) {
            if (status == "success") {
               try {
                  eval(data);
               } catch (e) {
                  if (e instanceof SyntaxError) {
                     alert(e.message);
                  }
               }
            }
            else {
               alert("Ooops Error : " + status + "[005]");
            }
         });
      }  
   });

   $("[name='token']").keypress(function (ev) {
      if (ev.which == 13) {
         if (saveProceed() == 0) {
            $.get("trn.e2e.php",
            {
               fn:"ChkUName",
               t:calcHash($("[name='token").val()),
               u:$("[name='txtUser']").val(),
               account:$("#hAccount").val()
            },
            function(data,status) {
               if (status == "success") {
                  try {
                     eval(data);
                  } catch (e) {
                     if (e instanceof SyntaxError) {
                        alert(e.message);
                     }
                  }
               }
               else {
                  alert("Ooops Error : " + status + "[005]");
               }
            });
         }  
         
      } 
   });   

});

function loadscrn(scrn) {
   var url = "scrnSignUp.e2e.php?scrn=" + scrn;
   $("#EntryScrn").load(url, function(responseTxt, statusTxt, xhr) {
      if(statusTxt == "error") {
         alert("Ooops Error: " + xhr.status + " : " + xhr.statusText);
         return false;
      }
      if(statusTxt == "success") {
         $("#login").hide();
      }
   });
   return false;
}

function afterNewSave(LastRefId) {
   alert("New Record " + LastRefId + " Succesfully Inserted");
   $("#EntryScrn").html("");
   $("#login").fadeIn(500);
   return false;
}