<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script language="JavaScript" src="<?php echo jsCtrl("ctrl_AfterTrn"); ?>"></script>
      <script language="JavaScript" src="<?php echo jsCtrl("ctrl_IPCR"); ?>"></script>
   </head>
   <body>
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"spms"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar(getvalue("paramTitle")); ?>
            <div class="container-fluid margin-top10">
               <div class="row">
                  <div class="col-xs-12" id="divList">
                     <div class="mypanel">
                        <div class="panel-top">LIST</div>
                        <div class="panel-mid">
                           <div class="row">
                              <div class="col-xs-12">
                                 <br>
                                 <div class="row">
                                    <div class="col-xs-2">
                                       <span class="label">Employee Name:</span>
                                    </div>
                                    <div class="col-xs-3">
                                       <input class="form-input" type="text" name="txtEmployeeName" readonly>
                                    </div>
                                    <div class="col-xs-2">
                                       <span class="label">Department Division\Office</span>
                                    </div>
                                    <div class="col-xs-2">
                                       <input class="form-input" type="text" name="txtDept" readonly>
                                    </div>
                                    <div class="col-xs-1">
                                       <span class="label">Date:</span>
                                    </div>
                                    <div class="col-xs-1">
                                       <input class="form-input date--" type="text" name="txtDate" readonly>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <?php
                              spacer(10);
                              bar();
                           ?>
                           <div id="spGridTable">
                              <?php
                                 $table = "empipcr";
                                 $gridTableHdr = "STRATEGIC OBJECTIVES/FUNCTIONS|SUCCESS INDICATOR|ACTUAL ACCOMPLISHMENTS|Q1|E2|T3|A4|REMARKS";
                                 $gridTableFld = "StrategicObjective|SuccessIndicator|ActualAccomplishment|QualityRate|EfficiencyRate|TimelinessRate|AverageRate|Remarks";
                                 $gridTableHdr_arr = explode("|",$gridTableHdr);
                                 $gridTableFld_arr = explode("|",$gridTableFld);
                                 $sql = "SELECT * FROM `$table` WHERE EmployeesRefId = ".getvalue("hEmpRefId")." ORDER BY RefId";
                                 $_SESSION["module_table"] = $table;
                                 $_SESSION["module_gridTableHdr_arr"] = $gridTableHdr_arr;
                                 $_SESSION["module_gridTableFld_arr"] = $gridTableFld_arr;
                                 $_SESSION["module_sql"] = $sql;
                                 $_SESSION["module_gridTable_ID"] = "gridTable";

                                 doGridTable($table,
                                             $gridTableHdr_arr,
                                             $gridTableFld_arr,
                                             $sql,
                                             [true,true,true,false],
                                             $_SESSION["module_gridTable_ID"]);
                              ?>
                           </div>
                           <?php
                              spacer(15);
                           ?>
                           <div class="row margin-top">
                              <div class="col-xs-1"></div>
                              <div class="col-xs-9">
                                 <div class="row margin-top">
                                    <label class="control-label" for="inputs">FINAL AVERAGE RATING</label>
                                 </div>
                                 <div class="row margin-top">
                                    <div class="form-group">
                                       <label class="control-label" for="inputs">Recommendations:</label>
                                       <textarea class="form-input saveFields--" rows="5" name="char_Recommendations"></textarea>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-xs-1">
                              </div>
                              <div class="col-xs-1">
                                 <div class="row margin-top">
                                    <span class="label">Legend</span>
                                 </div>
                                 <div class="row margin-top">
                                    Q1. Quality
                                 </div>
                                 <div class="row margin-top">
                                    E2. Efficiency
                                 </div>
                                 <div class="row margin-top">
                                    T3. Timeliness
                                 </div>
                                 <div class="row margin-top">
                                    A4. Average
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="panel-bottom">
                           <?php
                              btnINRECLO([true,true,false]);
                              echo '<button type="button"
                                            class="btn-cls4-lemon trnbtn"
                                            id="btnPRINT" name="btnPRINT">
                                          <i class="fa fa-print" aria-hidden="true"></i>&nbsp;
                                          PRINT
                                       </button>';
                           ?>
                        </div>
                     </div>
                  </div>
                  <div id="divView">
                     <div class="mypanel panel panel-default">
                        <div class="panel-top">
                           <span id="ScreenMode">INSERTING NEW IPCR</span>
                        </div>
                        <div class="panel-mid-litebg">
                           <div class="container" id="EntryScrn">
                              <div class="row">
                                 <div class="col-xs-6" id="EntryScrn">
                                    <div class="row" id="badgeRefId">
                                       <ul class="nav nav-pills">
                                          <li class="active" style="font-size:12pt;font-weight:600;">
                                             <a>REFID : <span class="badge" style="font-size:12pt;font-weight:600;" id="idRefid">
                                             </span></a>
                                          </li>
                                       </ul>
                                    </div>
                                    <div class="row margin-top10">
                                       <div class="form-group">
                                          <label class="control-label" for="inputs">STRATEGIC OBJECTIVES/FUNCTIONS</label><br>
                                          <textarea class="form-input saveFields--" rows="5" name="char_StrategicObjective"></textarea>
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="form-group">
                                          <label class="control-label" for="inputs">SUCCESS INDICATOR</label><br>
                                          <textarea class="form-input saveFields--" rows="5" name="char_SuccessIndicator"></textarea>
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="form-group">
                                          <label class="control-label" for="inputs">ACTUAL ACCOMPLISHMENTS</label>
                                          <textarea class="form-input saveFields--" rows="5" name="char_ActualAccomplishment"></textarea>

                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="col-xs-6">
                                          <div class="form-group">
                                             <label class="control-label" for="inputs">QUALITY</label>
                                             <?php
                                                createRatingScale("sint_QualityRate","QualityRate",0,"");
                                             ?>
                                          </div>
                                       </div>
                                       <div class="col-xs-6">
                                          <div class="form-group">
                                             <label class="control-label" for="inputs">EFFICIENCY</label>
                                             <?php
                                                createRatingScale("sint_EfficiencyRate","EfficiencyRate",0,"");
                                             ?>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="col-xs-6">
                                          <div class="form-group">
                                             <label class="control-label" for="inputs">TIMELINESS</label>
                                             <?php
                                                createRatingScale("sint_TimelinessRate","TimelinessRate",0,"");
                                             ?>
                                          </div>
                                       </div>
                                       <div class="col-xs-6">
                                          <div class="form-group">
                                             <label class="control-label" for="inputs">AVERAGE</label>
                                             <?php
                                                createRatingScale("sint_AverageRate","AverageRate",0,"");
                                             ?>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="form-group">
                                          <label class="control-label" for="inputs">REMARKS:</label>
                                          <textarea class="form-input saveFields--" rows="5" name="char_Remarks"></textarea>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <?php
                              bar();
                              btnSACABA([true,true,true]);
                           ?>
                        </div>
                        <div class="panel-bottom"></div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- Modal -->
            <div class="modal fade border0" id="prnModal" role="dialog">
               <div class="modal-dialog border0" style="padding:0px;width:97%;height:92%;">
                  <div class="mypanel border0" style="height:100%;">
                     <div class="panel-top bgSilver">
                        <a href="#" data-toggle="tooltip" data-placement="top" title="Print Now" id="btnPRINTNOW">
                           <i class="fa fa-print" aria-hidden="true"></i>
                        </a>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                     </div>
                     <iframe id="rptContent" src="blank.e2e.php" class="iframes"></iframe>
                  </div>
               </div>
            </div>
            <?php
               footer();
               include_once ("varHidden.e2e.php");
            ?>
         </div>
      </form>
   </body>
</html>




