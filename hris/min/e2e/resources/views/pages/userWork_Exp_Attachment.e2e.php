<?php
   require_once "constant.e2e.php";
   require_once pathClass.'0620RptFunctions.e2e.php';
   $table = "employees";
   $emprefid = getvalue("hEmpRefId");
   $whereClause = "WHERE RefId = ".$emprefid." ORDER BY LastName";
   $rsEmployees = SelectEach($table,$whereClause);
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script type="text/javascript">
         $(document).ready(function () {
            $("#workexp_entry").hide();
            $("#cancel_attach").click(function () {
               $("#workexp_entry").slideUp();
               $("#workexp_content").show(500);   
            });
            $("#save_attachment").click(function () {
               var obj = "";
               var value = "";
               var obj_name = "";
               $("[class*='saveFields--']").each(function () {
                  value = $(this).val();
                  obj_name = $(this).attr("name");
                  obj += obj_name + "|" + value + "!";
               });
               saveAttachment(obj);
            });
         });
         function printAttachment(refid){
            $("#rptContent").attr("src","blank.htm");
            var rptFile = "formWorkExperience";
            var url = "ReportCaller.e2e.php?file=" + rptFile;
            url += "&refid=" + refid;
            url += "&" + $("[name='hgParam']").val();
            $("#prnModal").modal();
            $("#rptContent").attr("src",url);
         }
         function saveAttachment(obj){
            $.get("trn.e2e.php",
            {
               fn:"SaveWorkExpAttachment",
               obj: obj
            },
            function(data,status){
               if (status == 'success') {
                  eval(data);
               }
            });
         }
         function addAttachment(refid){
            //$("input").val("");
            $("#workexp_content").slideUp();
            $("#workexp_entry").show();
            $.get("trn.e2e.php",
            {
               fn:"getWorkExp",
               refid:refid
            },
            function(data,status){
               if (status == 'success') {
                  eval(data);
               }
            });
         }
      </script>
   </head>
   <body onload = "indicateActiveModules();">
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"pis"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar ("WORK EXPERIENCE ATTACHMENT"); ?>
            <div class="container-fluid margin-top">
               <div class="row">
                  <div class="col-xs-12" id="div_CONTENT">
                     <div class="container-fluid rptBody">
                        <div class="row" id="workexp_content">
                           <div class="col-xs-12">
                              <div class="panel-top">
                                 MY WORK EXPERIENCE
                              </div>
                              <div class="panel-mid">
                                 <table class="table table-order-column table-striped table-bordered table-hover" id="gridTable">
                                    <thead>
                                       <tr style="color:#fff;font-weight:600;">
                                          <th style="text-align:center; width: 20%;">ACTION</th>
                                          <th style="text-align:center;">POSITION</th>
                                          <th style="text-align:center;">AGENCY</th>
                                          <th style="text-align:center;">SALARY</th>
                                       </tr>
                                    </thead>
                                    <tbody>
                                       <?php
                                          $rs = SelectEach("employeesworkexperience","WHERE EmployeesRefId = $emprefid");
                                          if ($rs) {
                                             while ($row = mysqli_fetch_assoc($rs)) {
                                                $check = FindFirst("employees_work_experience_attachments","WHERE WorkExperienceRefId = ".$row["RefId"],"RefId");
                                                echo '<tr>';
                                                if (is_numeric($check)) {
                                                   echo '
                                                      <td class="text-center">
                                                         <button type="button" class="btn-cls4-sea" onclick="editAttachment('.$check.')" disabled>
                                                            <i class="fa fa-edit"></i>&nbsp;EDIT
                                                         </button>
                                                         <button type="button" class="btn-cls4-lemon" onclick="printAttachment('.$check.')">
                                                            <i class="fa fa-eye"></i>&nbsp;VIEW
                                                         </button>
                                                      </td>
                                                   ';
                                                } else {
                                                   echo '
                                                      <td class="text-center">
                                                         <button type="button" class="btn-cls4-tree" onclick="addAttachment('.$row["RefId"].')">
                                                            INSERT ATTACHMENT
                                                         </button>
                                                      </td>
                                                   ';
                                                }
                                                echo '
                                                      <td>'.getRecord("position",$row["PositionRefId"],"Name").'</td>
                                                      <td>'.getRecord("agency",$row["AgencyRefId"],"Name").'</td>
                                                      <td class="text-right">'.number_format($row["SalaryAmount"],2).'</td>
                                                   </tr>
                                                ';
                                             }
                                          }
                                       ?>
                                    </tbody>
                                 </table>
                              </div>
                              <div class="panel-bottom"></div>
                           </div>
                        </div>
                        <div class="row margin-top" id="workexp_entry">
                           <div class="col-xs-6">
                              <div class="panel-top">
                                 ADD WORK EXPERIENCE ATTACHMENT
                              </div>
                              <div class="panel-mid">
                                 <div class="row">
                                    <div class="col-xs-12">
                                       <input type="hidden" class="saveFields--" name="sint_WorkExperienceRefId" id="sint_WorkExperienceRefId">
                                       <input type="hidden" class="saveFields--" name="sint_EmployeesRefId" id="sint_EmployeesRefId">
                                       <div class="row">
                                          <div class="col-xs-12">
                                             <label>DURATION: </label>
                                             <input type="hidden" class="saveFields--" name="sint_StartDate" id="sint_StartDate" value="">
                                             <input type="hidden" class="saveFields--" name="sint_EndDate" id="sint_EndDate" value="">
                                             <input class="form-input" type="text" name="duration" id="duration" readonly>
                                          </div>
                                       </div>
                                       <div class="row margin-top">
                                          <div class="col-xs-12">
                                             <label>POSITION: </label>
                                             <input type="hidden" class="saveFields--" name="sint_PositionRefId" id="sint_PositionRefId">
                                             <input class="form-input" type="text" name="position" id="position" readonly>
                                          </div>
                                       </div>
                                       <div class="row margin-top">
                                          <div class="col-xs-12">
                                             <label>NAME OF OFFICE/UNIT: </label>
                                             <input type="hidden" class="saveFields--" name="sint_OfficeRefId" id="sint_OfficeRefId">
                                             <input class="form-input" type="text" name="office" id="office" readonly>
                                          </div>
                                       </div>
                                       <div class="row margin-top">
                                          <div class="col-xs-12">
                                             <label>IMMEDIATE SUPERVISOR: </label>
                                             <input class="form-input saveFields--" type="text" name="char_Supervisor" id="supervisor">
                                          </div>
                                       </div>
                                       <div class="row margin-top">
                                          <div class="col-xs-12">
                                             <label>NAME OF AGENCY/ORGANIZATION: </label>
                                             <input type="hidden" class="saveFields--" name="sint_AgencyRefId" id="sint_AgencyRefId">
                                             <input class="form-input" type="text" name="agency" id="agency" readonly>
                                          </div>
                                       </div>
                                       <div class="row margin-top">
                                          <div class="col-xs-12">
                                             <label>LOCATION: </label>
                                             <input class="form-input saveFields--" type="text" name="char_Location" id="location">
                                          </div>
                                       </div>
                                       <div class="row margin-top">
                                          <div class="col-xs-12">
                                             <label>LIST OF ACCOMPLISHMENTS AND CONTRIBUTION (if any): </label>
                                             <br>
                                             <textarea class="form-input saveFields--" rows="5" name="char_Accomplishments"></textarea>
                                          </div>
                                       </div>
                                       <div class="row margin-top">
                                          <div class="col-xs-12">
                                             <label>SUMMARY OF ACTUAL DUTIES: </label>
                                             <br>
                                             <textarea class="form-input saveFields--" rows="5" name="char_Duties"></textarea>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="panel-bottom">
                                 <div class="row">
                                    <div class="col-xs-12">
                                       <button type="button" class="btn-cls4-sea" id="save_attachment">
                                          <i class="fa fa-save"></i>&nbsp;SAVE
                                       </button>
                                       <button type="button" class="btn-cls4-red" id="cancel_attach">CANCEL</button>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- Modal -->
            <div class="modal fade" id="prnModal" role="dialog">
               <div class="modal-dialog" style="height:90%;width:80%">
                  <div class="mypanel" style="height:100%;">
                     <div class="panel-top bgSilver">
                        <a href="#" data-toggle="tooltip" data-placement="top" id="btnPRINTNOW">
                           <i class="fa fa-print" aria-hidden="true"></i>
                        </a>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                     </div>
                     <iframe id="rptContent" src="blank.e2e.php" class="iframes"></iframe>
                  </div>
               </div>
            </div>
            <?php
               footer();
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>
   </body>
</html>



