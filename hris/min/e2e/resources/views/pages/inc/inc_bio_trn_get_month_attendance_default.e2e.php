<?php
	include 'mdbcn.e2e.php';
  	if (isset($connection)) {
     	$query = 'SELECT USERID, Badgenumber, Name FROM USERINFO';
     	$result = $connection->query($query)->fetchAll(PDO::FETCH_ASSOC);
     	if ($result) {
        	foreach($result as $row) {
           		switch ($CompanyID) {
              		case "2":
                 		$mdb_field = "Name";
              		break;
              	default:
                 	$mdb_field = "Badgenumber";
                 	break;
           	}
           	if (strtolower($row[$mdb_field]) == strtolower($biometricsID)) {
              	$userID = $row["USERID"];
              	break;
           	}
        }   
    }
     //echo '$.notify("'.$userID.'");';
    if ($userID == 0) {
       echo '$.notify("Incorrect Biometrics ID '.trim($biometricsID).'");';
       //echo '$("#pop_dtr").prop("disabled",true);';
       //echo '$("#btnProcess").prop("disabled",true);';
    } else {
       echo '$("#pop_dtr").prop("disabled",false);';
       echo '$("#btnProcess").prop("disabled",false);';
    }
     
    $query = "SELECT USERID, CHECKTIME, CHECKTYPE, VERIFYCODE, SENSORID, sn FROM CHECKINOUT WHERE USERID = ".$userID." AND VERIFYCODE = 1";
    $result = $connection->query($query)->fetchAll(PDO::FETCH_ASSOC);
    foreach($result as $row) {
        $utc = strtotime($row["CHECKTIME"]);
        $fulldate = date("Y-m-d",$utc);
        $AttendanceFormatted = date("h:i A",$utc);
        switch ($row["CHECKTYPE"]) {
            case "I":
                $type = $KEntry_json["TimeIn"];
                break;
            case "0":
                $type = $KEntry_json["LunchOut"];
                break;
            case "1":
                $type = $KEntry_json["LunchIn"];
                break;
            case "O":
                $type = $KEntry_json["TimeOut"];
                break;
        }
        $KEntry = $type;
        $where = "WHERE EmployeesRefId = $EmpRefId AND AttendanceDate = '".$fulldate."' AND KindOfEntry = '".$KEntry."'";
        $compare_mdb_empatt = FindFirst("employeesattendance",$where,"RefId");
        if (!$compare_mdb_empatt) {
           	if(isset($arr["ARR"][$fulldate])) {
              	$arr["ARR"][$fulldate]["Has"] = 1;
           	}
           	switch ($KEntry) {
              	case 1:
                 	echo '$("#TI_'.$fulldate.'").html("'.$AttendanceFormatted.'");'."\n";
                 	echo '$("#TI_'.$fulldate.'").attr("checktime","'.$utc.'");'."\n";
                 	break;
              	case 2:
                 	echo '$("#LO_'.$fulldate.'").html("'.$AttendanceFormatted.'");'."\n";
                 	echo '$("#LO_'.$fulldate.'").attr("checktime","'.$utc.'");'."\n";
                 	break;
              	case 3:
                 	echo '$("#LI_'.$fulldate.'").html("'.$AttendanceFormatted.'");'."\n";
                 	echo '$("#LI_'.$fulldate.'").attr("checktime","'.$utc.'");'."\n";
                 	break;
              	case 4:
                 	echo '$("#TO_'.$fulldate.'").html("'.$AttendanceFormatted.'");'."\n";
                 	echo '$("#TO_'.$fulldate.'").attr("checktime","'.$utc.'");'."\n";
                 	break;
           	}
        }
    }
  }
?>