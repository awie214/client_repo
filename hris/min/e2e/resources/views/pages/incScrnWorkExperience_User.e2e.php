<div id="EntryWorkExp_<?php echo $j; ?>" class="entry201">
   <input type="checkbox" id="exp_<?php echo $j; ?>"
            name="chkWorkExp_<?php echo $j; ?>" class="enabler-- workexp_fpreview"
            refid=""
            <?php 
            echo 
            ' fldName= "date_WorkStartDate_'.$j.',
                        date_WorkEndDate_'.$j.',
                        sint_PositionRefId_'.$j.',
                        sint_AgencyRefId_'.$j.',
                        deci_SalaryAmount_'.$j.',
                        sint_JobGradeRefId_'.$j.',
                        sint_SalaryGradeRefId_'.$j.',
                        sint_EmpStatusRefId_'.$j.',
                        sint_isGovtService_'.$j.',
                        char_PayGroup_'.$j.',
                        sint_PayrateRefId_'.$j.',
                        char_LWOP_'.$j.',
                        date_ExtDate_'.$j.',
                        date_SeparatedDate_'.$j.',
                        char_Reason_'.$j.',
                        char_Remarks_'.$j.',
                        sint_isServiceRecord_'.$j.',
                        sint_StepIncrementRefId_'.$j.',
                        sint_Present_'.$j.'" ';
            ?>
            idx="<?php echo $j; ?>"
            unclick="CancelAddRow">
   <input type="hidden" name="workexperienceRefId_<?php echo $j;?>" value="">
   <label for="exp_<?php echo $j; ?>"><b>Experience #<?php echo $j; ?></b></label>

   <div class="row margin-top">
      <div class="col-sm-3">
         <label>Govt. Service:</label><br>
         <select class="form-input saveFields--" idx="<?php echo $j; ?>" id="sint_isGovtService_<?php echo $j; ?>" name="sint_isGovtService_<?php echo $j; ?>" <?php echo $disabled; ?>
         title='Govt. Service' onchange="isGovt(this.name);">
            <option value="">is Govt. Service?</option>
            <option value=0>NO</option>
            <option value=1>YES</option>
         </select>
      </div>
      <div class="col-sm-3">
      </div>
      <div class="col-sm-3">
      </div>
   </div>

   <div class="row margin-top">
      <div class="col-sm-2">
         <select class="form-input saveFields--" idx="<?php echo $j; ?>" id="sint_isGovtService_<?php echo $j; ?>" name="sint_isGovtService_<?php echo $j; ?>" <?php echo $disabled; ?>
         title='Govt. Service' onchange="isGovt(this.name);">
            <option value="">is Govt. Service?</option>
            <option value=0>NO</option>
            <option value=1>YES</option>
         </select>
      </div>
   </div>
   <div class="row margin-top">
      <div class="col-xs-3">
         <div class="row" id="datework">
            <div class="col-xs-4">
               <input type="text" class="form-input date-- saveFields-- mandatory datefrom valDate--" placeholder="Work Start Date"
                  id="WorkStartDate_<?php echo $j; ?>" name="date_WorkStartDate_<?php echo $j; ?>" <?php echo $disabled; ?>
                  title="Work Start Date" readonly>
            </div>
            <div class="col-xs-8">
               <input type="text" class="form-input date-- saveFields-- mandatory dateto valDate--" for="WorkStartDate_<?php echo $j; ?>" placeholder="Work End Date"
               id="WorkEndDate_<?php echo $j; ?>" name="date_WorkEndDate_<?php echo $j; ?>" <?php echo $disabled; ?>
               title="Work End Date" readonly>
               <?php doChkPresent($j,6); ?>
            </div>
         </div>
      </div>
      <div class="col-xs-3">
         <?php
            createSelect2("Position",
                           "sint_PositionRefId_".$j,
                           "",100,"Name","SELECT POSITION",$disabled." title='Position Title'","WHERE IsPrivate = 0 OR IsPrivate IS NULL");
         ?>
      </div>
      <div class="col-xs-3">
         <?php
            createSelect2("Agency",
                           "sint_AgencyRefId_".$j,
                           "",100,"Name","SELECT AGENCY",$disabled." title='Dept / Agency / Office'","WHERE IsPrivate = 0 OR IsPrivate IS NULL");
         ?>
      </div>
      <div class="col-xs-3">
         <input type="text" class="form-input decimal-- saveFields-- number--" placeholder="Salary"
         id="SalaryAmount_<?php echo $j; ?>" name="deci_SalaryAmount_<?php echo $j; ?>" <?php echo $disabled; ?>
         title='Salary Amount'>
      </div>
   </div>
   <div class="row margin-top" id="sg">
      <div class="col-xs-2">
         <?php
            createSelect("SalaryGrade",
                           "sint_SalaryGradeRefId_".$j,
                           "",100,"Name","SELECT SALARY GRADE",$disabled." title='Salary Grade'");
         ?>
      </div>
      <?php 
         if (isset($settings)) {
            $lgShowJG = $settings["show.JobGrade"];
         } else {
            $lgShowJG = $GLOBALS["settings"]["show.JobGrade"];
         } 
      if ($lgShowJG) {
      ?>
      <div class="col-xs-2">
         <?php
            $sql = "SELECT * FROM `jobgrade` ORDER BY Name + 0";
            $result = mysqli_query($conn,$sql) or die(mysqli_error($conn));
            if ($result) {
               echo '<select partner="sint_SalaryGradeRefId_'.$j.'" id="sint_JobGradeRefId_'.$j.'" 
                  name="sint_JobGradeRefId_'.$j.'" 
                  class="form-input saveFields-- rptCriteria-- createSelect--">'."\n";
               echo '<option value="0">SELECT JOB GRADE</option>'."\n";
               while($row = mysqli_fetch_assoc($result)) {
                  if ($row["Name"] != "") {
                     echo '<option value="'.$row["RefId"].'"';
                     echo '>'.$row["Name"].'</option>'."\n";
                  }
               }
               echo '</select>'."\n";
            }
         ?>
      </div>
      <?php 
         }
      ?>

      <div class="col-xs-2">
         <?php
            createSelect("StepIncrement",
                           "sint_StepIncrementRefId_".$j,
                           "",100,"Name","SELECT STEP INCREMENT",$disabled." title='Step Increment'");
         ?>
      </div>
      <div class="col-xs-2" id="appt">
         <?php
            createSelect2("EmpStatus",
                           "sint_EmpStatusRefId_".$j,
                           "",100,"Name","SELECT EMPT. STATUS",$disabled." title='Employement Status'","WHERE IsPrivate = 0 OR IsPrivate IS NULL");
         ?>
      </div>
      <div class="col-xs-2"></div>
      <?php if (getvalue("hCompanyID") != 2) { ?>
         <div class="col-xs-2 txt-center">
            <select title="Service Record" 
               class="form-input saveFields--" 
               id="ServiceRecord_<?php echo $j; ?>" 
               name="sint_isServiceRecord_<?php echo $j; ?>" <?php echo $disabled; ?>>
               <option value="">is Service Record</option>
               <option value="0">NO</option>
               <option value="1">YES</option>
            </select>
         </div>
      <?php } ?>
   </div>
</div>
<div class="panel-bottom bgSilver">
   <a href="javascript:void(0);" class="addRow" id="addRowWorkExp">Add Row</a>
</div>

   
   
   
