<?php 
	include 'FnUpload.php';
   	mysqli_query($conn,"TRUNCATE employees");
  	mysqli_query($conn,"TRUNCATE employeesfamily");
  	
	$emp_personal_array = [
		"LastName","FirstName","MiddleName","Extname","IsFilipino","BirthDate","BirthPlace","Sex",
		"CivilStatus","Weight","Height","BloodTypeRefId","GSIS","PAGIBIG","PHIC","SSS","TIN",
		"AgencyId","TelNo","MobileNo","EmailAdd","ResiHouseNo","ResiStreet","ResiSubd",
		"ResiBrgy","ResiAddCityRefId","ResiAddProvinceRefId","PermanentHouseNo",
		"PermanentStreet","PermanentSubd","PermanentBrgy","PermanentAddCityRefId",
		"PermanentAddProvinceRefId"
	];
	$emp_fam_bg_array = [
		"SpouseLastName","SpouseFirstName","SpouseMiddleName","SpouseExtName","OccupationsRefId",
		"EmployersName","BusinessAddress","SpouseMobileNo","FatherLastName","FatherFirstName",
		"FatherMiddleName","FatherExtName","MotherLastName","MotherFirstName","MotherMiddleName"
	];

	$PersonalInfo = fopen("csv/personal_info_28.csv", "r");
	while(!feof($PersonalInfo)) {
		$row 			= explode(",", fgets($PersonalInfo));
		$save_value 	= "";
		$save_field 	= "";
		$fam_save_value = "";
		$fam_save_field = "";
		$checking   	= "";
		$checking_fam	= "";
		$count 			= 0;
		$err 			= 0;
		if (count($row) > 1) {
			for ($i=0; $i < count($row) - 19; $i++) { 
				$value = clean($row[$i]);
				$field = $emp_personal_array[$i];
				if ($value != "") {
					if ($field != "EmailAdd" && $field != "CivilStatus") {
						$value = strtoupper($value);
					}
					if ($field == "BirthDate") {
						if ($value != "") {
				        	$BirthDate_arr = explode("/",$BirthDate);
				        	$day = $BirthDate_arr[0];
				        	$month = $BirthDate_arr[1];
				        	$year = $BirthDate_arr[2];
				        	$day = intval($day);
				        	$month = intval($month);
				        	if ($month < 13) {
				        		if ($day <= 9) $day = "0".$day;
				        		if ($month <= 9) $month = "0".$month;
				        		$value = $year."-".$month."-".$day;
				        	}
				        }
					}
					if (strpos($field, "RefId") > 1) {
						$table = str_replace("RefId", "", $field);
						if (strpos($field, "esiAdd")) {
							$table = str_replace("ResiAdd", "", $field);
							$table = str_replace("RefId", "", $table);
						}
						if (strpos($field, "entAdd")) {
							$table = str_replace("PermanentAdd", "", $field);
							$table = str_replace("RefId", "", $table);
						}
						
						$value = saveFM($table,"Name, ","'$value', ",$value);
					}
					if ($value != "") {
						$save_value .= "'".$value."', ";
						$save_field .= "`$field`, ";
					}
					
					
				}
			}
			
			$save_value = $save_value." 28, 1, ";
			$save_field = $save_field." CompanyRefId, BranchRefId,";
			$emprefid = save("employees",$save_field,$save_value);
			if (!is_numeric($emprefid)) {
				$err++;
				echo $emprefid;
				return false;
			} else {
				echo $row[0]." Personal Info Successfully Saved<br>";
			}
			if ($err == 0) {
				for ($a=count($row) - 19; $a <count($row)-4; $a++) { 
					if (clean($row[$a]) != "") {
						$count++;
						$fam_value = strtoupper(clean($row[$a]));
						$fam_field = $emp_fam_bg_array[$count];
						if ($fam_value != "") {
							if (strpos($fam_field, "RefId") > 1) {
								$fam_table = str_replace("RefId", "", $fam_field);
								$fam_value = saveFM($fam_table,"Name, ","'$fam_value', ",$fam_value);
							}
							$fam_save_value .= "'".$fam_value."',";
							$fam_save_field .= "`$fam_field`,";			
						}
					}
				}
				$fam_save_field = $fam_save_field." EmployeesRefId, CompanyRefId, BranchRefId, ";
				$fam_save_value = $fam_save_value." $emprefid, 28, 1, ";
				$fam_save = save("employeesfamily",$fam_save_field,$fam_save_value);
				if (!is_numeric($fam_save)) {
					echo $fam_save;
					return false;
				} else {
					echo $row[0]." Family Successfully Saved<br>";
				}
			}
		}
	}


	/*$rs = SelectEach("employees","");
	if ($rs) {
		while ($row = mysqli_fetch_assoc($rs)) {
			$emprefid = $row["RefId"];
			$check_empinfo = FindFirst("empinformation","WHERE EmployeesRefId = $emprefid","RefId",$conn);
			if (!is_numeric($check_empinfo)) {
				$fld = "CompanyRefId, BranchRefId, EmployeesRefId, ";
				$val = "28, 1, $emprefid, ";
				$empinfo = save("empinformation",$fld,$val);
				if (!is_numeric($empinfo)) {
					echo $empinfo;
				}
			}
		}
	}*/
?>