<!DOCTYPE html>
<html>
<body onload="alert('Done');">

<?php
require "constant.e2e.php";
require "conn.e2e.php";
require_once pathClass."0620functions.e2e.php";

            $maintable = "employee_leave_balances";
            $conn->query("TRUNCATE TABLE `employeescreditbalance`;");

            $t = time();
            $date_today    = date("Y-m-d",$t);
            $curr_time     = date("H:i:s",$t);
            $trackingA_fld = "`LastUpdateDate`, `LastUpdateTime`, `LastUpdateBy`, `Data`";
            $trackingA_val = "'$date_today', '$curr_time', 'PHP', 'M'";

            $sql = "SELECT * FROM `$maintable`";
            $rs = mysqli_query($pids_conn,$sql) or die(mysqli_error($pids_conn));
            echo "Number Records : ".mysqli_num_rows($rs)."<br>";
            if ($rs) {
               if (mysqli_num_rows($rs) > 0) {
                  while ($pids = mysqli_fetch_array($rs)) {
                     $empRefId = $pids["employee_personal_information_sheet_id"];

                     $NameCredits = "SL";
                     $EffectivityYear = $pids["date"];
                     $BegBalAsOfDate = $date_today;
                     $BeginningBalance = $pids["sl"];
                     $OutstandingBalance = 0;
                     $ForceLeave = 5;
                     $ExpiryDate = 0;
                     $Remarks = $pids["remarkssl"].";Migrated";

                     $flds = "`NameCredits`, `CompanyRefId`, `BranchRefId`,`EmployeesRefId`,`EffectivityYear`,
                     `BegBalAsOfDate`,`BeginningBalance`,`OutstandingBalance`,`ForceLeave`, `ExpiryDate`, `Remarks`,".$trackingA_fld;
                     $values = "'$NameCredits', '1000', '1','$empRefId','$EffectivityYear',
                     '$BegBalAsOfDate','$BeginningBalance','$OutstandingBalance','$ForceLeave', '$ExpiryDate', '$Remarks',".$trackingA_val;
                     $sql = "INSERT INTO `employeescreditbalance` ($flds) VALUES ($values)";
                     if ($conn->query($sql) === TRUE) {
                        echo "Migrated $maintable SL -->$empRefId<br>";
                     }
                     else {
                        echo "ERROR Migration $maintable -->$empRefId<br>";
                     }

                     $NameCredits = "VL";
                     $EffectivityYear = $pids["date"];
                     $BegBalAsOfDate = $date_today;
                     $BeginningBalance = $pids["vl"];
                     $OutstandingBalance = 0;
                     $ForceLeave = 5;
                     $ExpiryDate = 0;
                     $Remarks = $pids["remarksvl"]."Migrated";


                     $flds = "`NameCredits`, `CompanyRefId`, `BranchRefId`,`EmployeesRefId`,`EffectivityYear`,
                     `BegBalAsOfDate`,`BeginningBalance`,`OutstandingBalance`,`ForceLeave`, `ExpiryDate`, `Remarks`,".$trackingA_fld;
                     $values = "'$NameCredits', '1000', '1','$empRefId','$EffectivityYear',
                     '$BegBalAsOfDate','$BeginningBalance','$OutstandingBalance','$ForceLeave', '$ExpiryDate', '$Remarks',".$trackingA_val;
                     $sql = "INSERT INTO `employeescreditbalance` ($flds) VALUES ($values)";
                     if ($conn->query($sql) === TRUE) {
                        echo "Migrated $maintable VL -->$empRefId<br>";
                     }
                     else {
                        echo "ERROR Migration $maintable -->$empRefId<br>";
                     }
                  }
               }
            }
            $maintable = "employee_coc_balances";
            $sql = "SELECT * FROM `$maintable`";
            $rs = mysqli_query($pids_conn,$sql) or die(mysqli_error($pids_conn));
            echo "Number Records : ".mysqli_num_rows($rs)."<br>";
            if ($rs) {
               if (mysqli_num_rows($rs) > 0) {
                  while ($pids = mysqli_fetch_array($rs)) {
                     $empRefId = $pids["employee_personal_information_sheet_id"];

                     $NameCredits = "COC";
                     $EffectivityYear = $pids["date"];
                     $BegBalAsOfDate = $date_today;
                     $BeginningBalance = $pids["balance"];
                     $OutstandingBalance = 0;
                     $ForceLeave = 0;
                     $ExpiryDate = "2018-01-01";
                     $Remarks = $pids["remarks"].";Migrated";

                     $flds = "`NameCredits`, `CompanyRefId`, `BranchRefId`,`EmployeesRefId`,`EffectivityYear`,
                     `BegBalAsOfDate`,`BeginningBalance`,`OutstandingBalance`,`ForceLeave`, `ExpiryDate`, `Remarks`,".$trackingA_fld;
                     $values = "'$NameCredits', '1000', '1','$empRefId','$EffectivityYear',
                     '$BegBalAsOfDate','$BeginningBalance','$OutstandingBalance','$ForceLeave', '$ExpiryDate', '$Remarks',".$trackingA_val;
                     $sql = "INSERT INTO `employeescreditbalance` ($flds) VALUES ($values)";
                     if ($conn->query($sql) === TRUE) {
                        echo "Migrated $maintable -->$empRefId<br>";
                     }
                     else {
                        echo "ERROR Migration $maintable -->$empRefId<br>";
                     }

                  }
               }
            }
            mysqli_close($conn);
?>
</body>
</html>