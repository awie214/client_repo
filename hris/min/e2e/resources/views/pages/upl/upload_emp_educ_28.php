<?php
	include 'FnUpload.php';
	mysqli_query($conn,"TRUNCATE employeeseduc");
	$EmpEduc = fopen("csv/emp_educ1_28.csv", "r");
	
	while(!feof($EmpEduc)) {
		$Fld = "CompanyRefId, BranchRefId, ";
		$Val = "28, 1, ";
		$educ_row = explode(",", fgets($EmpEduc));
		$EmpAgencyID 	= clean($educ_row[0]);
		$Level 			= clean($educ_row[1]);
		$School 		= clean($educ_row[2]);
		$Course 		= clean($educ_row[3]);
		$DateFrom 		= clean($educ_row[4]);
		$DateTo 		= clean($educ_row[5]);
		$HighestGrade 	= clean($educ_row[6]);
		$YearGraduated 	= clean($educ_row[7]);
		$Honors 		= clean($educ_row[8]);


		if ($Level == 1 || $Level == 2) {
			$Course = "";
		}



		if ($Honors == "N/A" || $Honors == "NONE") {
			$Honors = "";
		}



		if (!is_numeric($DateFrom)) {
			if (strlen($DateFrom) == 10) {
				$DateFrom = explode("/", $DateFrom)[2];
				$Fld .= "DateFrom, ";
				$Val .= "'$DateFrom', ";
			}
		} else {
			$Fld .= "DateFrom, ";
			$Val .= "'$DateFrom', ";
		}



		if (is_numeric($YearGraduated)) {
			$Fld .= "YearGraduated, ";
			$Val .= "'$YearGraduated', ";
		}




		if (!is_numeric($DateTo)) {
			if (strlen($DateTo) == 10) {
				$DateTo = explode("/", $DateTo)[2];
				$Fld .= "DateTo, ";
				$Val .= "'$DateTo', ";
			}
		} else if (strtolower($DateTo) == "present") {
			$DateTo = "PRESENT";
			$Fld .= "Present, ";
			$Val .= "'1', ";
		} else {
			$Fld .= "DateTo, ";
			$Val .= "'$DateTo', ";
		}

		$emprefid = FindFirst("employees","WHERE AgencyId = '$EmpAgencyID'","RefId",$conn);
		$SchoolFld = "Name, Offer1, Offer2, Offer3, Offer4, Offer5, ";
		$SchoolVal = "'$School',1,1,1,1,1, ";
		$SchoolRefId = saveFM("schools",$SchoolFld,$SchoolVal,$School);
		if ($Course != "") {
			$CourseRefId = saveFM("course","Name, ","'$Course', ",$Course);
			$Fld .= "CourseRefId, ";
			$Val .= "'$CourseRefId', ";
		}
		if (is_numeric($emprefid)) {
			$Fld .= "LevelType, SchoolsRefId, HighestGrade, Honors, EmployeesRefId, ";
			$Val .= "$Level, $SchoolRefId, '$HighestGrade', '$Honors', '$emprefid',";
			$save_educ = save("employeeseduc",$Fld,$Val);
			if (is_numeric($save_educ)) {
				echo "$emprefid -> $Level Educ Saved<br>";
			}
		}
	}
?>