<?php
	include 'FnUpload.php';
	include '../conn.e2e.php';
	mysqli_query($conn,"TRUNCATE empinformation");
	$EmpInfo = fopen("csv/empinfo_14.csv", "r");
	while(!feof($EmpInfo)) {
		$empinfo_row 		= explode(",", fgets($EmpInfo));
		$AgencyId 			= clean($empinfo_row[0]);
		$LastName 			= clean($empinfo_row[1]);
		$FirstName 			= clean($empinfo_row[2]);
		$Position 			= clean($empinfo_row[3]);
		$Office 			= clean($empinfo_row[4]);
		$Division 			= clean($empinfo_row[5]);
		$Unit 				= clean($empinfo_row[6]);
		$PlantillaItem 		= clean($empinfo_row[7]);
		$AppointmentDate 	= clean($empinfo_row[8]);
		if ($AppointmentDate != "") {
			$AppointmentDate_arr = explode("/", $AppointmentDate);
			$AppointmentDate 	= $AppointmentDate_arr[2]."-".$AppointmentDate_arr[0]."-".$AppointmentDate_arr[1];	
		}
		
		$SalaryGrade 		= clean($empinfo_row[9]);
		$StepIncrement 		= clean($empinfo_row[10]);
		$MonthlySalary 		= clean($empinfo_row[11]);
		$EmpStatus 			= clean($empinfo_row[12]);
		$sql = "SELECT `RefId` FROM employees WHERE AgencyId = '$AgencyId'";
		$rs = mysqli_query($conn,$sql);
		if (mysqli_num_rows($rs) > 0) {
			$row = mysqli_fetch_assoc($rs);
			$emprefid = $row["RefId"];
			$Flds = "`CompanyRefId`, `BranchRefId`, `EmployeesRefId`,";
			$Vals = "14,1,$emprefid,";
			$plantilla_fldnval = "";
			if ($AppointmentDate != "") {
				if (strtoupper($EmpStatus) == "CONTRACTUAL" || strtoupper($EmpStatus) == "CONTRACT OF SERVICE") {
					$Flds .= "`StartDate`,`AssumptionDate`, ";
					$Vals .= "'$AppointmentDate', '$AppointmentDate',";	
				} else {
					$Flds .= "`HiredDate`,`AssumptionDate`, ";
					$Vals .= "'$AppointmentDate', '$AppointmentDate',";	
				}
			}
			if ($PlantillaItem != "" ) {
				$PlantillaItem = saveFM("positionitem","`Name`, ","'$PlantillaItem', ",$PlantillaItem);
				$Flds .= "`PositionItemRefId`, ";
				$Vals .= "'$PlantillaItem', ";
			}
			if ($Position != "" ) {
				$Position = saveFM("position","`Name`, ","'$Position', ",$Position);
				$Flds .= "`PositionRefId`, ";
				$Vals .= "'$Position', ";
				$plantilla_fldnval .= "`PositionRefId` = '$Position',";
			}

			if ($SalaryGrade != "" ) {
				$SalaryGrade = intval($SalaryGrade);
				$SalaryGrade = saveFM("salarygrade","`Name`, ","'$SalaryGrade', ",$SalaryGrade);
				$Flds .= "`SalaryGradeRefId`, ";
				$Vals .= "'$SalaryGrade', ";
				$plantilla_fldnval .= "`SalaryGradeRefId` = '$SalaryGrade',";
			}

			if ($StepIncrement != "" ) {
				$StepIncrement = intval($StepIncrement);
				$StepIncrement = saveFM("stepincrement","`Name`, ","'$StepIncrement', ",$StepIncrement);
				$Flds .= "`StepIncrementRefId`, ";
				$Vals .= "'$StepIncrement', ";
				$plantilla_fldnval .= "`StepIncrementRefId` = '$StepIncrement',";
			}

			if ($MonthlySalary != "" ) {
				$MonthlySalary = floatval($MonthlySalary);
				$Flds .= "`SalaryAmount`, ";
				$Vals .= "'$MonthlySalary', ";
				$plantilla_fldnval .= "`SalaryAmount` = '$MonthlySalary',";
			}

			if ($Office != "" ) {
				$Office = saveFM("office","`Name`, ","'$Office', ",$Office);
				$Flds .= "`OfficeRefId`, ";
				$Vals .= "'$Office', ";
				$plantilla_fldnval .= "`OfficeRefId` = '$Office',";
			}

			if ($Division != "" ) {
				$Division = saveFM("division","`Name`, ","'$Division', ",$Division);
				$Flds .= "`DivisionRefId`, ";
				$Vals .= "'$Division', ";
				$plantilla_fldnval .= "`DivisionRefId` = '$Division',";
			}

			if ($EmpStatus != "" ) {
				$EmpStatus = saveFM("empstatus","`Name`, ","'$EmpStatus', ",$EmpStatus);
				$Flds .= "`EmpStatusRefId`, ";
				$Vals .= "'$EmpStatus', ";
			}
			if (intval($PlantillaItem) > 0) {
				$update_plantilla = update("positionitem",$plantilla_fldnval,$PlantillaItem);
				if ($update_plantilla != "") {
					echo "Error in updating plantilla.<br>";
				}
			}
			
			$result = save("empinformation",$Flds,$Vals);
			if (is_numeric($result)) {
				echo "$FirstName $LastName Successfully Updated Employee Information.<br>";
			} else {
				echo $Flds." ======> ".$Vals;
			}
		} else {
			echo $AgencyId." -> No Emprefid<br>";
		}
		
		
	}
?>