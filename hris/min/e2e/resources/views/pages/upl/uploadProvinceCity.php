<?php
	include 'conn.e2e.php';
   include "constant.e2e.php";
   include pathClass.'0620functions.e2e.php';
   $table = "province";

   mysqli_query($conn,"truncate table `province`");
   mysqli_query($conn,"truncate table `city`");

	$myfile = fopen(textFile."provinces.csv", "r") or die("Unable to open file!");
	$j = 0;
	while(!feof($myfile)) {
		$j++;
		$a = fgets($myfile);
      $a_arr = explode(",", $a);
      if (count($a_arr) >= 3) {
         $refid = trim($a_arr[0]);
         $code = trim($a_arr[1]);
         $name = trim($a_arr[2]);

         date_default_timezone_set("Asia/Manila");
         $t             = time();
         $date_today    = date("Y-m-d",$t);
         $curr_time     = date("H:i:s",$t);
         $loc_user      = "admin";
         $flds          = "";
         $trackingA_fld = "";
         $trackingA_val = "";
         $values = "";
         $flds   .= "RefId, Code, Name, Remarks,";
         $values .= "'$refid','$code','$name','Uploaded',";
         $trackingA_fld .= "`LastUpdateDate`, `LastUpdateTime`, `LastUpdateBy`, `Data`";
         $trackingA_val .= "'$date_today', '$curr_time', '$loc_user', 'A'";
         $flds   = $flds.$trackingA_fld;
         $values = $values.$trackingA_val;
         $sql = "INSERT INTO `province` ($flds) VALUES ($values)";
         echo $sql."<br>";
         if ($conn->query($sql) === TRUE) {
         } else {
            echo "<br>".$sql." -- Saving Error: ".$conn->error;
         }
      }

	}

   $myfile = fopen(textFile."cities.csv", "r") or die("Unable to open file!");
	$j = 0;
	while(!feof($myfile)) {
		$j++;
		$a = fgets($myfile);
		$a_arr = explode(",", $a);
		$refid = trim($a_arr[0]);
		$provinceRefId = trim($a_arr[1]);
      $cityname = trim(str_replace('"','',$a_arr[2]));
      $zipcode = trim(str_replace('"','',$a_arr[3]));
      if ($zipcode == "") $zipcode = 0;
      //$cityname = mysqli_real_escape_string($conn, $cityname);
      //$zipcode = mysqli_real_escape_string($conn, $zipcode);
      $isCity = trim($a_arr[4]);
      if ($isCity == "") $isCity = 0;

      date_default_timezone_set("Asia/Manila");
      $t             = time();
      $date_today    = date("Y-m-d",$t);
      $curr_time     = date("H:i:s",$t);
      $loc_user      = "admin";
      $flds          = "";
      $trackingA_fld = "";
      $trackingA_val = "";
      $values = "";
      $flds   .= "RefId, ProvinceRefid, Name, isCity, ZipCode ,Remarks,";
      $values .= "'$refid','$provinceRefId','$cityname','$isCity','$zipcode','Uploaded',";
      $trackingA_fld .= "`LastUpdateDate`, `LastUpdateTime`, `LastUpdateBy`, `Data`";
      $trackingA_val .= "'$date_today', '$curr_time', '$loc_user', 'A'";
      $flds   = $flds.$trackingA_fld;
      $values = $values.$trackingA_val;
      $sql = "INSERT INTO `city` ($flds) VALUES ($values)";
      echo $sql."<br>";
      if ($conn->query($sql) === TRUE) {
      } else {

         echo "<br>".$sql." -- Saving Error: ".$conn->error;
      }

	}



?>