<?php
require_once $_SESSION['Classes'].'0620functions.e2e.php';
require "conn.e2e.php";
$target_dir = "uploads/";
$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$uploadOk = 1;
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
$msg = "";

echo $target_file,"<br>";
echo $imageFileType;
// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
    if($check !== false) {
      $uploadOk = 1;
    } else {
        $msg .= "File is not an image.";
        $uploadOk = 0;
    }
}
// Check if file already exists
if (file_exists($target_file)) {
    $msg .= "<br>Sorry, file already exists.";
    $uploadOk = 0;
}
// Check file size
if ($_FILES["fileToUpload"]["size"] > 100000) {
    $msg .= "<br>Sorry, your file is too large.";
    $uploadOk = 0;
}
// Allow certain file formats
if(strtolower($imageFileType) != "jpg" &&
   strtolower($imageFileType) != "png" &&
   strtolower($imageFileType) != "jpeg" &&
   strtolower($imageFileType) != "gif" ) {
    $msg .= "<br>Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOk = 0;
}

// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    $msg .= "<br>Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
        $msg .= "<br>The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
    } else {
        $msg .= "<br>Sorry, there was an error uploading your file.";
    }
}
?>

<!DOCTYPE html>
<html>
   <head>
      <?php include "pageHEAD.e2e.php"; ?>
   </head>
   <body>
      <?php
         if ($msg!=="") {
            alert("Information",$msg);
         }
         mysqli_close($conn);
      ?>
   </body>
</html>
