<?php
	$conn = mysqli_connect("localhost","root","","e2e_ovp");
	mysqli_query($conn,"TRUNCATE employeesattendance");
	function get_today_minute($utc) {
      $baseline = strtotime(date("Y-m-d",$utc));
      return intval(($utc - $baseline) / 60);
   }
   function FindFirst($table,$where,$fld,$conn) {
		$table = strtolower($table);
		if ($fld == "*") {
			$sql	= "SELECT * FROM $table ".$where." LIMIT 1";
		} else {
			$sql	= "SELECT `$fld` FROM $table ".$where." LIMIT 1";
		}
		$rs 		= mysqli_query($conn,$sql) or die(mysqli_error($conn));
		if ($rs) {
			if ($fld == "*") {
				$row = mysqli_fetch_assoc($rs);
				return $row;
			} else {
				$row = mysqli_fetch_assoc($rs);
				return $row[$fld];
			}
		} else {
			echo $sql;
		}
	}
	function save($table,$flds,$vals) {
		$conn = mysqli_connect("localhost","root","","e2e_ovp");
		$table = strtolower($table);
		$date_today    	= date("Y-m-d",time());
        $curr_time     	= date("H:i:s",time());
        $trackingA_fld 	= "`LastUpdateDate`, `LastUpdateTime`, `LastUpdateBy`, `Data`";
        $trackingA_val 	= "'$date_today', '$curr_time', 'Admin', 'A'";
        $flds   	   	= $flds.$trackingA_fld;
        $vals   		= $vals.$trackingA_val;
        $sql 			= "INSERT INTO $table ($flds) VALUES ($vals)";
        $rs 			= mysqli_query($conn,$sql) or die(mysqli_error($conn));
        if ($rs) {
        	return mysqli_insert_id($conn);
        } else {
        	echo $rs;
        }
	}
	$dtr = fopen("csv/150149_28.csv", "r");
	$count = 0;
	$arr = array();
	while(!feof($dtr)) { 
		$count++;						
		
		$dtr_arr 	= explode(",", fgets($dtr));
		$AgencyID 	= $dtr_arr[0];
		
		if ($AgencyID != "") {
			$new_arr    = array();
			$date 		= $dtr_arr[1];
			$date 		= date("Y-m-d",strtotime($date));
			foreach ($dtr_arr as $key => $value) {
				if (!($key == "0" || $key == "1")) {
					if ($value != "") {
						$new_arr[] = $value;
					}
				}
			}
			$arr[$date] = $new_arr;
		}
	}
	foreach ($arr as $key => $value) {
		foreach ($value as $nkey => $nvalue) {
			$utc = strtotime($key." ".$nvalue);
			$minute = get_today_minute($utc);
			  
			$Flds = "CompanyRefId,BranchRefId,EmployeesRefId,AttendanceDate,AttendanceTime,CheckTime,KindOfEntry,";
			$Vals = "28,1,35,'$key','$utc','$utc',";
			
			//echo "<td>".date("h:i A",$utc)."</td>";
			$time  = $minute;
			if ($time < 719) {
				$where_f = "WHERE EmployeesRefId = '35' AND AttendanceDate = '$key' AND KindOfEntry = 1";
				$check_f = FindFirst("employeesattendance",$where_f,"RefId",$conn);
				if (!$check_f) {
					$Vals .= "'1',";
					$result = save("employeesattendance",$Flds,$Vals);
					if (!is_numeric($result)) {
						return false;
					}
				}
			} else if ($time >= 720 && $time <= 780) {
				$where = "WHERE EmployeesRefId = '35' AND AttendanceDate = '$key' AND KindOfEntry = 2";
				$check = FindFirst("employeesattendance",$where,"RefId",$conn);
				if ($check) {
					$Vals .= "'3',";
					$result = save("employeesattendance",$Flds,$Vals);
					if (!is_numeric($result)) {
						return false;
					}	
				} else {
					$Vals .= "'2',";
					$result = save("employeesattendance",$Flds,$Vals);
					if (!is_numeric($result)) {
						return false;
					}
				}
			} else if ($time > 780) {
				$where_l = "WHERE EmployeesRefId = '35' AND AttendanceDate = '$key' AND KindOfEntry = 4";
				$check_l = FindFirst("employeesattendance",$where_l,"RefId",$conn);
				if (!$check_l) {
					$Vals .= "'4',";
					$result = save("employeesattendance",$Flds,$Vals);
					if (!is_numeric($result)) {
						return false;
					}
				}
			}
			
		}
	}
?>