<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script src="<?php echo jsCtrl("ctrl_amsCreditBal"); ?>"></script>
      <style>
      </style>
      <script language="JavaScript">
         $(document).ready(function () {
            <?php
               if (isset($_SESSION["SelectedEMP"])) {
                  echo 'selectMe('.$_SESSION["SelectedEMP"].')';
               }
            ?>
         });
      </script>
   </head>
   <body>
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"ams"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php
               doTitleBar($paramTitle);
            ?>
            <div class="row">
               <div class="col-xs-12" id="bodyContent">
                  <div class="row margin-top">
                     <div class="col-xs-3">
                        <?php employeeSelector(); ?>
                     </div>
                     <div class="col-xs-9">
                        <?php
                           /*$attr = ["empRefId"=>getvalue("txtRefId"),
                                    "empLName"=>getvalue("txtLName"),
                                    "empFName"=>getvalue("txtFName"),
                                    "empMName"=>getvalue("txtMidName")];
                           $EmpRefId = EmployeesSearch($attr);
                           */

                        ?>
                        <?php spacer(5); ?>
                        <div id="divList">
                           <div class="mypanel">
                              <div class="panel-top">
                                 EMPLOYEES CREDIT BALANCE<br>
                                 <span id="selectedEmployees">&nbsp;</span>
                                 <input type="hidden" id="sint_EmployeesRefId" name="sint_EmployeesRefId">
                              </div>
                              <div class="panel-mid">
                                 <span id="spGridTable">
                                    <?php
                                          doGridTable($table,
                                                      $gridTableHdr_arr,
                                                      $gridTableFld_arr,
                                                      $sql,
                                                      [true,true,true,false],
                                                      "gridTable");
                                    ?>
                                 </span>
                              </div>
                              <div class="panel-bottom">
                                 <?php
                                    btnINRECLO([true,false,false]);
                                 ?>
                              </div>
                           </div>
                        </div>
                        <div id="divView">
                           <div class="row">
                              <div class="col-xs-8">
                                 <div class="mypanel">
                                    <div class="panel-top">
                                       <span id="ScreenMode">INSERTING NEW </span> <?php echo strtoupper($ScreenName); ?>
                                    </div>
                                    <div class="panel-mid">
                                       <div id="EntryScrn">
                                          <div class="row margin-top">
                                             <div class="col-xs-8">
                                                   <div class="row" id="badgeRefId">
                                                      <div class="col-xs-6">
                                                         <ul class="nav nav-pills">
                                                            <li class="active" style="font-size:12pt;font-weight:600;">
                                                               <a>REFID : <span class="badge" style="font-size:12pt;font-weight:600;" id="idRefid">
                                                               </span></a>
                                                            </li>
                                                         </ul>
                                                      </div>
                                                   </div>
                                                   <div class="row margin-top">
                                                      <div class="col-xs-9">
                                                         <select class="form-input saveFields--" name="char_NameCredits">
                                                            <option value="">--Select Credits--</option>
                                                            <option value="VL">Vacation Leave</option>
                                                            <option value="SL">Sick Leave</option>
                                                            <option value="OT">Overtime</option>
                                                            <option value="SPL">Special Privelege Leave</option>
                                                         </select>
                                                      </div>
                                                   </div>
                                                   <?php
                                                   echo '<div id="LeaveScreen">';
                                                      echo
                                                      '<h4><u>SL / VL / SPL Credit</u></h4>
                                                      <div class="row margin-top">
                                                         <div class="col-xs-12">
                                                            <div class="row">
                                                               <div class="col-xs-6">
                                                               <label>EffectivityYear</label><br>
                                                               <select name="sint_EffectivityYear" class="form-input saveFields--">';
                                                               $yr = date("Y",time());
                                                               for ($j=0;$j<=5;$j++) {
                                                                  echo '<option value="'.($yr+$j).'">'.($yr+$j).'</option>';
                                                               }
                                                               echo
                                                               '</select>
                                                               </div>';
                                                               $attr = ["br"=>true,"type"=>"text","row"=>false,"name"=>"date_BegBalAsOfDate",
                                                                        "col"=>"6","id"=>"BegBalAsOfDate","label"=>"Beg. Bal. As Of Date","class"=>" saveFields-- mandatory date--",
                                                                        "style"=>"","other"=>""];
                                                               $form->eform($attr);
                                                      echo
                                                      '
                                                            </div>
                                                         </div>
                                                      </div>';
                                                      echo
                                                      '<div class="row margin-top">
                                                         <div class="col-xs-12">
                                                            <div class="row">';
                                                               $attr = ["br"=>true,"type"=>"text","row"=>false,"name"=>"sint_BeginningBalance",
                                                                        "col"=>"6","id"=>"BeginningBalance","label"=>"Beginning Balance",
                                                                        "class"=>"saveFields-- mandatory number--","style"=>"","other"=>""];
                                                               $form->eform($attr);
                                                               $attr = ["br"=>true,"type"=>"text","row"=>false,"name"=>"sint_ForceLeave",
                                                                        "col"=>"6","id"=>"ForceLeave","label"=>"Force Leave",
                                                                        "class"=>"saveFields-- mandatory number--","style"=>"","other"=>""];
                                                               $form->eform($attr);
                                                      echo
                                                      '
                                                            </div>
                                                         </div>
                                                      </div>';
                                                   echo
                                                   '</div>';
                                                   echo
                                                   '<div id="OTScreen">';
                                                      echo
                                                      '<h4><u>OT Credit</u></h4>
                                                      <div class="row margin-top">
                                                         <div class="col-xs-12">
                                                            <div class="row">
                                                               <div class="col-xs-6">
                                                               <label>EffectivityYear</label><br>
                                                               <select name="sint_EffectivityYearOT" class="form-input saveFields--">';
                                                               $yr = date("Y",time());
                                                               for ($j=0;$j<=5;$j++) {
                                                                  echo '<option value="'.($yr+$j).'">'.($yr+$j).'</option>';
                                                               }
                                                               echo
                                                               '</select>
                                                               </div>';
                                                               $attr = ["br"=>true,"type"=>"text","row"=>false,"name"=>"date_BegBalAsOfDateOT",
                                                                        "col"=>"6","id"=>"BegBalAsOfDateOT","label"=>"Beg. Bal. As Of Date","class"=>"saveFields-- mandatory date--",
                                                                        "style"=>"","other"=>""];
                                                               $form->eform($attr);
                                                      echo
                                                      '
                                                            </div>
                                                         </div>
                                                      </div>';
                                                      echo
                                                      '<div class="row margin-top">
                                                         <div class="col-xs-12">
                                                            <div class="row">';
                                                               $attr = ["br"=>true,"type"=>"text","row"=>false,"name"=>"sint_BeginningBalance",
                                                                        "col"=>"6","id"=>"BeginningBalance","label"=>"Beginning Balance",
                                                                        "class"=>"saveFields-- mandatory number--","style"=>"","other"=>""];
                                                               $form->eform($attr);
                                                               $attr = ["br"=>true,"type"=>"text","row"=>false,"name"=>"date_ExpiryDate",
                                                                        "col"=>"6","id"=>"ExpiryDate","label"=>"Expiry Date",
                                                                        "class"=>"saveFields-- mandatory date--","style"=>"","other"=>""];
                                                               $form->eform($attr);
                                                      echo
                                                      '
                                                            </div>
                                                         </div>
                                                      </div>';

                                                   echo
                                                   '</div>';
                                                   ?>
                                                   <div class="row margin-top">
                                                      <div class="col-xs-12">
                                                         <div class="form-group">
                                                            <label class="control-label" for="inputs">Remarks:</label>
                                                            <textarea class="form-input saveFields--" rows="5" name="char_Remarks" placeholder="remarks"></textarea>
                                                         </div>
                                                      </div>
                                                   </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="panel-bottom">
                                       <?php
                                          spacer(10);
                                          echo
                                          '<button type="button" class="btn-cls4-sea"
                                                   name="btnLocSAVE" id="LocSAVE">
                                             <i class="fa fa-floppy-o" aria-hidden="true"></i>
                                             &nbsp;Save
                                          </button>';
                                          btnSACABA([false,true,true]);
                                       ?> 
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <?php
               doHidden("hSaveMethod","POST","");
               doHidden("fn","insertEmpCreditBal","");
               include "varHidden.e2e.php";
               footer();
            ?>
         </div>
      </form>
   </body>
</html>