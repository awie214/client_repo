<?php
	date_default_timezone_set("Asia/Manila");
	/*
	tblAppointment 			- apptstatus
	
	tblAttendanceScheme 	- workschedule
	tblContact 				- signatories
	
	
	tblEmpLeaveBalance 		- employeescreditbalance
	tblEmpOB 				- employeesattendance
	
	tblEmpRequest 			- employeesrequest
	
	
	tblSalarySched 			- stepincrement, salarygrade


	//tblEmpPosition 				- empinformation
	//tblEmpDTR 				- employeesattendance
	//tblAttendanceCode 		- leaves,officaauthority
	//tblPosition 				- position
	//tblServiceRecord 			- employeesworkexperience
	//tblCourse 				- course
	//tblEmpAccount 			- employees
	//tblEmpAccountx 			- employees
	//tblEmpChild 				- employeeschild
	//tblEmpExam 				- employeeseligibility
	//tblEmpTraining 			- employeestraining
	//tblEmpVoluntaryWork 		- employeesvoluntarywork
	//tblExamType 				- employeeseligibility
	//tblEmpReference 			- employeesreference
	//tblEmpPersonal 			- employees
	//tblLeave 					- leaves
	//tblEmpSchool 				- employeeseduc
	tblEmpLeave 				- employeesleave
	*/

	$e2e 		= mysqli_connect("localhost", "root", "", "e2e_mirdc");
	$curr_mirdc = mysqli_connect("localhost", "root", "", "mirdc_old");

	function SelectEach($table,$where,$conn) {
		$sql = "SELECT * FROM $table ".$where;
		$rs = mysqli_query($conn,$sql) or die(mysqli_error($conn));
		if ($rs) {
			return $rs;
		} else {
			echo "No Record Found";
		}
	}

	function save($table,$flds,$vals) {
		$table = strtolower($table);
		$conn 			= mysqli_connect("localhost", "root", "", "e2e_mirdc");
		$date_today    	= date("Y-m-d",time());
        $curr_time     	= date("H:i:s",time());
        $trackingA_fld 	= "`LastUpdateDate`, `LastUpdateTime`, `LastUpdateBy`, `Data`";
        $trackingA_val 	= "'$date_today', '$curr_time', 'Admin', 'A'";
        $flds   	   	= $flds.$trackingA_fld;
        $vals   		= $vals.$trackingA_val;
        $sql 			= "INSERT INTO $table ($flds) VALUES ($vals)";
        $rs 			= mysqli_query($conn,$sql) or die(mysqli_error($conn));
        if ($rs) {
        	return mysqli_insert_id($conn);
        } else {
        	echo $rs;
        }
	}

	function saveFM($table,$flds,$vals,$name,$where = "") {
		$table = strtolower($table);
		$e2e 		= mysqli_connect("localhost", "root", "", "e2e_mirdc");
		$where = "WHERE Name = '$name' ".$where;
		$check_FM 		= FindFirst($table,$where,"RefId",$e2e);
		if (is_numeric($check_FM)) {
			return $check_FM;
		} else {
			$conn 			= mysqli_connect("localhost", "root", "", "e2e_mirdc");
			$date_today    	= date("Y-m-d",time());
	        $curr_time     	= date("H:i:s",time());
	        $trackingA_fld 	= "`LastUpdateDate`, `LastUpdateTime`, `LastUpdateBy`, `Data`";
	        $trackingA_val 	= "'$date_today', '$curr_time', 'Admin', 'A'";
	        $flds   	   	= $flds.$trackingA_fld;
	        $vals   		= $vals.$trackingA_val;
	        $sql 			= "INSERT INTO $table ($flds) VALUES ($vals)";
	        $rs 			= mysqli_query($conn,$sql) or die(mysqli_error($conn));
	        if ($rs) {
	        	return mysqli_insert_id($conn);
	        } else {
	        	echo $rs."<br>$sql";
	        }	
		}
			
	}
	function update($table,$fldnval,$refid) {
		$table = strtolower($table);
		$conn 				= mysqli_connect("localhost", "root", "", "e2e_mirdc");
		$date_today    		= date("Y-m-d",time());
        $curr_time     		= date("H:i:s",time());
        $trackingA_fldnval 	= "LastUpdateBy = 'Admin', LastUpdateTime = '$curr_time',";
        $trackingA_fldnval .= "LastUpdateDate = '$date_today', Data = 'E'";
        $fldnval 		   .= $fldnval.$trackingA_fldnval;
        $sql                = "UPDATE $table SET $fldnval WHERE RefId = $refid";
        $rs 				= mysqli_query($conn,$sql) or die(mysqli_error($conn));
        if ($rs) {
        	return "";
        } else {
        	return $sql;
        }
	}

	function FindFirst($table,$where,$fld,$conn) {
		$table = strtolower($table);
		if ($fld == "*") {
			$sql	= "SELECT * FROM $table ".$where." LIMIT 1";
		} else {
			$sql	= "SELECT `$fld` FROM $table ".$where." LIMIT 1";
		}
		$rs 		= mysqli_query($conn,$sql) or die(mysqli_error($conn));
		if ($rs) {
			if ($fld == "*") {
				$row = mysqli_fetch_assoc($rs);
				return $row;
			} else {
				$row = mysqli_fetch_assoc($rs);
				return $row[$fld];
			}
		} else {
			echo $sql;
		}
	}

	function savePDSQ($pdsq_flds,$pdsq_vals,$emprefid) {
		$e2e 		= mysqli_connect("localhost", "root", "", "e2e_mirdc");
		$check_pdsq = FindFirst("employeespdsq","WHERE EmployeesRefId = $emprefid","RefId",$e2e);
		if (!$check_pdsq) {
			$result = save("employeespdsq",$pdsq_flds,$pdsq_vals);
			if (!is_numeric($result)) {
				echo $result;
			}
		}
	}

	function validateDate($date, $format = 'Y-m-d'){
      	$d = DateTime::createFromFormat($format, $date);
      	return $d && $d->format($format) == $date;
   	}

   	function realEscape($value) {
   		$e2e 		= mysqli_connect("localhost", "root", "", "e2e_mirdc");
   		return mysqli_real_escape_string($e2e,$value);
   	}

   	function getTimeInSeconds($value) {
   		$time_arr = explode(":", $value);
	    $time1 = ($time_arr[0] * 60) * 60;
	    $time2 = ($time_arr[1] * 60);
	    $time = $time1 + $time2 + $time_arr[2];
	    return $time;
   	}
?>
<?php
	/*==================================================================*/
  	//upload tblEmpAccount
  	/*==================================================================*/
	/*
	$rs  = SelectEach("tblEmpAccount","ORDER BY empNumber + 0",$curr_mirdc);
	if ($rs) {
		while ($row = mysqli_fetch_assoc($rs)) {
			$mirdc_emprefid = $row["empNumber"];
			$mirdc_username = $row["userName"];
			$where 			= "WHERE RefId = ".$mirdc_emprefid;
			$refid 			= FindFirst("employees",$where,"RefId",$e2e);
			if (!is_numeric($refid)) {
				$flds = "RefId, CompanyRefId, BranchRefId, UserName, ";
				$vals = "'$mirdc_emprefid', 21, 1, '$mirdc_username', ";
				$save_emp = save("employees",$flds,$vals);
				if (!is_numeric($save_emp)) {
					echo $save_emp;
				}
			}
			echo '
				<tr>
					<td>'.$row["empNumber"].'</td>
					<td>'.$row["userName"].'</td>
				</tr>
			';
		}
	}
	*/
	


	/*==================================================================*/
  	//upload tblEmpAccountx
  	/*==================================================================*/
	/*
	$rs  = SelectEach("tblEmpAccountx","ORDER BY empNumber + 0",$curr_mirdc);
	if ($rs) {
		while ($row = mysqli_fetch_assoc($rs)) {
			$mirdc_emprefid = $row["empNumber"];
			$mirdc_username = $row["userName"];
			$where 			= "WHERE RefId = ".$mirdc_emprefid;
			$refid 			= FindFirst("employees",$where,"RefId",$e2e);
			if (!is_numeric($refid)) {
				$flds = "RefId, CompanyRefId, BranchRefId, UserName, ";
				$vals = "'$mirdc_emprefid', 21, 1, '$mirdc_username', ";
				$save_emp = save("employees",$flds,$vals);
				if (!is_numeric($save_emp)) {
					echo $save_emp;
				}
			}
			echo '
				<tr>
					<td>'.$row["empNumber"].'</td>
					<td>'.$row["userName"].'</td>
				</tr>
			';
		}
	}
	*/


	/*==================================================================*/
  	//upload tblEmpChild
  	/*==================================================================*/
	/*
	$rs  = SelectEach("tblEmpChild","",$curr_mirdc);
	if ($rs) {
		while ($row = mysqli_fetch_assoc($rs)) {
			$mirdc_emprefid = $row["empNumber"];
			$childname 		= $row["childName"];
			$childbday 		= $row["childBirthDate"];
			if ($mirdc_emprefid != "") {
				$where 			= "WHERE EmployeesRefId = $mirdc_emprefid";
				$where 		   .= " AND FullName = '".$childname."'";
				$where         .= " AND BirthDate = '".$childbday."'";
				$refid 			= FindFirst("employeeschild",$where,"RefId",$e2e);
				if (!is_numeric($refid)) {
					$flds = "EmployeesRefId, CompanyRefId, BranchRefId, FullName, BirthDate, ";
					$vals = "'".$mirdc_emprefid."', 21, 1, '".$childname."', '".$childbday."',";
					$save = save("employeeschild",$flds,$vals);
					if (!is_numeric($save)) {
						echo $save;
					}
				}
				echo '
					<tr>
						<td>'.$mirdc_emprefid.'</td>
						<td>'.$childname.'</td>
					</tr>
				';
			}
			
		}
	}
	*/

	

	/*==================================================================*/
  	//upload tblEmpPersonal
  	/*==================================================================*/
	

	/*ARRAYS*/

	
	$pdsq_arr = [
		"Q1a"=>"relatedThird",
		"Q1aexp"=>"relatedDegreeParticularsThird",
		"Q1b"=>"relatedFourth",
		"Q1bexp"=>"relatedDegreeParticulars",
		"Q2a"=>"violateLaw",
		"Q2aexp"=>"violateLawParticulars",
		"Q2b"=>"formallyCharged",
		"Q2bexp"=>"formallyChargedParticulars",
		"Q3a"=>"adminCase",
		"Q3aexp"=>"adminCaseParticulars",
		"Q4a"=>"forcedResign",
		"Q4aexp"=>"forcedResignParticulars",
		"Q5a"=>"candidate",
		"Q5aexp"=>"candidateParticulars",
		"Q7a"=>"indigenous",
		"Q7aexp"=>"indigenousParticulars",
		"Q7b"=>"disabled",
		"Q7bexp"=>"disabledParticulars",
		"Q7c"=>"soloParent",
		"Q7cexp"=>"soloParentParticulars"
	];
	
	/********/



	/*
	$rs  = SelectEach("tblEmpPersonal","",$curr_mirdc);
	if ($rs) {
		while ($row = mysqli_fetch_assoc($rs)) {
			$flds = "";
			$pdsq_flds = "";
			$pdsq_vals = "";
			$mirdc_emprefid 	= mysqli_real_escape_string($e2e,$row["empNumber"]);
			$lastname 			= mysqli_real_escape_string($e2e,$row["surname"]);
			$firstname 			= mysqli_real_escape_string($e2e,$row["firstname"]);
			$middlename 		= mysqli_real_escape_string($e2e,$row["middlename"]);
			$nameExtension		= mysqli_real_escape_string($e2e,$row["nameExtension"]);
			$sex	 			= mysqli_real_escape_string($e2e,$row["sex"]);
			$civilStatus		= mysqli_real_escape_string($e2e,$row["civilStatus"]);
			$tin  	 			= mysqli_real_escape_string($e2e,$row["tin"]);
			$birthday 			= mysqli_real_escape_string($e2e,$row["birthday"]);
			$birthPlace 		= mysqli_real_escape_string($e2e,$row["birthPlace"]);
			$bloodType 			= mysqli_real_escape_string($e2e,$row["bloodType"]);
			$height 			= mysqli_real_escape_string($e2e,$row["height"]);
			$weight 			= mysqli_real_escape_string($e2e,$row["weight"]);
			$residentialAddress	= mysqli_real_escape_string($e2e,$row["residentialAddress"]);
			$telephone1 		= mysqli_real_escape_string($e2e,$row["telephone1"]);
			$permanentAddress 	= mysqli_real_escape_string($e2e,$row["permanentAddress"]);
			$mobile 			= mysqli_real_escape_string($e2e,$row["mobile"]);
			$email 				= mysqli_real_escape_string($e2e,$row["email"]);
			$gsisNumber 		= mysqli_real_escape_string($e2e,$row["gsisNumber"]);
			$philHealthNumber 	= mysqli_real_escape_string($e2e,$row["philHealthNumber"]);
			$sssNumber 			= mysqli_real_escape_string($e2e,$row["sssNumber"]);
			$pagibigNumber 		= mysqli_real_escape_string($e2e,$row["pagibigNumber"]);

			if ($lastname != "") {
				$flds .= "LastName = '$lastname', ";
			}
			if ($firstname != "") {
				$flds .= "FirstName = '$firstname', ";
			}
			if ($middlename != "") {
				$flds .= "MiddleName = '$middlename', ";
			}
			if ($nameExtension != "") {
				$flds .= "ExtName = '$nameExtension', ";
			}
			if ($sex != "") {
				$flds .= "Sex = '$sex', ";
			}
			if ($civilStatus != "") {
				switch ($civilStatus) {
					case 'Single':
						$civilStatus = "Si";
						break;
					case 'Married':
						$civilStatus = "Ma";
						break;
					case 'Separated':
						$civilStatus = "Se";
						break;
					case 'Annulled':
						$civilStatus = "An";
						break;
					case 'Widowed':
						$civilStatus = "Wi";
						break;
					case 'Ot':
						$civilStatus = "Other";
						break;
				}
				$flds .= "CivilStatus = '$civilStatus', ";
			}
			if ($tin != "") {
				$flds .= "TIN = '$tin', ";
			}
			if ($birthday != "" && $birthday != "0000-00-00") {
				$flds .= "BirthDate = '$birthday', ";
			}
			if ($birthPlace != "") {
				$flds .= "BirthPlace = '$birthPlace', ";
			}
			if ($height != "") {
				$flds .= "Height = '$height', ";
			}
			if ($weight != "") {
				$flds .= "Weight = '$weight', ";
			}
			if ($telephone1 != "") {
				$flds .= "TelNo = '$telephone1', ";
			}
			if ($mobile != "") {
				$flds .= "MobileNo = '$mobile', ";
			}
			if ($email != "") {
				$flds .= "EmailAdd = '$email', ";
			}
			if ($gsisNumber!= "") {
				$flds .= "GSIS = '$gsisNumber', ";
			}
			if ($philHealthNumber!= "") {
				$flds .= "PHIC = '$philHealthNumber', ";
			}
			if ($sssNumber!= "") {
				$flds .= "SSS = '$sssNumber', ";
			}
			if ($pagibigNumber != "") {
				$flds .= "PAGIBIG = '$pagibigNumber', ";
			}
			$flds .= "isFilipino = 1, ";
			$flds .= "Inactive = 0, ";
			if (is_numeric($mirdc_emprefid)) {
				if ($mirdc_emprefid != "" && $lastname != "") {
					$where 			= "WHERE RefId = ".$mirdc_emprefid;
					$refid 			= FindFirst("employees",$where,"RefId",$e2e);
					if ($refid) {
						$update_emp = update("employees",$flds,$refid);
						if ($update_emp != "") {
							echo $update_emp;
						} else {
							$pdsq_flds .= "CompanyRefId, BranchRefId, EmployeesRefId, ";
							$pdsq_vals .= "21, 1, '$mirdc_emprefid', ";
							foreach ($pdsq_arr as $key => $value) {
								$values = mysqli_real_escape_string($e2e,$row[$value]);
								if ($values == "Y") {
									$values = "1";
								} else if ($values == "N") {
									$values = "0";
								} else {
									$values = $values;
								}
								if ($values != "") {
									$pdsq_flds .= "$key, ";
									$pdsq_vals .= "'$values',";
								}
							}
							savePDSQ($pdsq_flds,$pdsq_vals,$mirdc_emprefid);
						}
					}
				}
			}
		}
	}
	*/


	/*==================================================================*/
  	//upload tblEmpExam
  	/*==================================================================*/
	/*
	$rs  = SelectEach("tblEmpExam","ORDER BY empNumber + 0",$curr_mirdc);
	if ($rs) {
		while ($row = mysqli_fetch_assoc($rs)) {
			$mirdc_emprefid = $row["empNumber"];
			$examDate 		= $row["examDate"];
			$examRating		= $row["examRating"];
			$examPlace 		= $row["examPlace"];
			$licenseNumber  = $row["licenseNumber"];
			$dateRelease	= $row["dateRelease"];
			$ExamIndex		= $row["ExamIndex"];
			$code 			= $row["examCode"];
			$careerserviceRefId = FindFirst("careerservice","WHERE Code = '".$code."'","RefId",$e2e);

			$eleg_flds 	   = "CompanyRefId, BranchRefId, EmployeesRefId, ";
			$eleg_vals 	   = "21, 1, '$mirdc_emprefid', ";
			$eleg_flds     .= "CareerServiceRefId, Rating, ExamPlace, LicenseNo,";
			$eleg_vals     .= "'$careerserviceRefId', '$examRating', '$examPlace','$licenseNumber',";


			if ($examDate != "" && $examDate != "0000-00-00") {
				$eleg_flds .= "ExamDate, ";
				$eleg_vals .= "'$examDate', ";
			}

			if ($dateRelease != "" && $dateRelease != "0000-00-00") {
				$eleg_flds .= "LicenseReleasedDate, ";
				$eleg_vals .= "'$dateRelease', ";
			}

			if ($careerserviceRefId == "") {
				$flds = "Code, Name, ";
				$vals = "'$code', '$code', ";
				$result = save("careerservice",$flds,$vals);
				if (!is_numeric($result)) {
					echo $result;
				} else {
					$careerserviceRefId = $result;
				}
			} else {
				$result         = save("employeeselegibility",$eleg_flds,$eleg_vals);
				if (!is_numeric($result)) {
					echo $result;
				}	
			}
			
		}
	}
	*/

	/*
	$rs = SelectEach("tblExamType","",$curr_mirdc);
	if ($rs) {
		while ($row = mysqli_fetch_assoc($rs)) {
			$code = $row["examCode"];
			$name = $row["examDesc"];
			$flds = "Code, Name, ";
			$vals = "'$code', '$name', ";
			$result = saveFM("careerservice",$flds,$vals,$name);
			if (!is_numeric($result)) {
				echo $result;
			}
		}
	}
	*/


	/*==================================================================*/
  	//upload tblEmpReference
  	/*==================================================================*/
  	/*
	$rs = SelectEach("tblEmpReference","",$curr_mirdc);
	if ($rs) {
		while ($row = mysqli_fetch_assoc($rs)) {
			$emprefid 	= $row["empNumber"];
			$name 		= $row["refName"];
			$address 	= $row["refAddress"];
			$tel 		= $row["refTelephone"];
			if (is_numeric($emprefid)) {
				$flds = "CompanyRefId, BranchRefId, EmployeesRefId, Name, Address, ContactNo, ";
				$vals = "21, 1, $emprefid, '$name', '$address', '$tel', ";
				$result = save("employeesreference",$flds,$vals);
				if (!is_numeric($result)) {
					echo $result;
				}
			}
			
		}
	}
	*/

	/*==================================================================*/
  	//upload tblEmpTraining
  	/*==================================================================*/
  	/*
	$rs = SelectEach("tblEmpTraining","",$curr_mirdc);
	if ($rs) {
		while ($row = mysqli_fetch_assoc($rs)) {
			$emprefid 	= mysqli_real_escape_string($e2e,$row["empNumber"]);
			$hrs 		= mysqli_real_escape_string($e2e,$row["trainingHours"]);
			$place 		= mysqli_real_escape_string($e2e,$row["trainingVenue"]);
			$seminar    = mysqli_real_escape_string($e2e,$row["trainingTitle"]);
			$sponsor 	= mysqli_real_escape_string($e2e,$row["trainingConductedBy"]);
			$from 		= mysqli_real_escape_string($e2e,$row["trainingStartDate"]);
			$to 		= mysqli_real_escape_string($e2e,$row["trainingEndDate"]);
			if (is_numeric($emprefid)) {
				$flds = "EmployeesRefId, CompanyRefId, BranchRefId, NumofHrs, SeminarPlace, ";
				$vals = "$emprefid, 21, 1, '$hrs', '$place',";
				if ($from != "" && $from != "0000-00-00") {
					$flds .= "StartDate, ";
					$vals .= "'$from', ";
				}
				if ($to != "" && $to != "0000-00-00") {
					$flds .= "EndDate, ";
					$vals .= "'$to', ";
				}
				if ($seminar != "") {
					$sem_fld = "Name, ";
					$sem_val = "'$seminar',";
					$sem_rs = saveFM("seminars",$sem_fld,$sem_val,$seminar);
					if (is_numeric($sem_rs)) {
						$seminar = $sem_rs;
						$flds .= "SeminarsRefId, ";
						$vals .= "'$seminar', ";
					}
				}
				if ($sponsor != "") {
					$sponsor_fld = "Name, ";
					$sponsor_val = "'$sponsor',";
					$sponsor_rs = saveFM("sponsor",$sponsor_fld,$sponsor_val,$sponsor);
					if (is_numeric($sponsor_rs)) {
						$sponsor = $sponsor_rs;
						$flds .= "SponsorRefId, ";
						$vals .= "'$sponsor', ";
					}
				}

				if (is_numeric($seminar)) {
					$result = save("employeestraining",$flds,$vals);
					if (!is_numeric($result)) {
						echo $result;
					}
				}
			}
		}
	}
	*/


	/*==================================================================*/
  	//upload tblServiceRecord
  	/*==================================================================*/
  	/*
	if ($SeparatedDate != "" && $SeparatedDate != "0000-00-00") {
		$flds .= "SeparatedDate, ";
		$vals .= "'$SeparatedDate', ";
	}
	*/

	/*
	$rs = SelectEach("tblServiceRecord","",$curr_mirdc);
	if ($rs) {
		while ($row = mysqli_fetch_assoc($rs)) {
			$CompanyRefId 		= 21;
			$BranchRefId 		= 1;
			$EmployeesRefId 	= mysqli_real_escape_string($e2e,$row["empNumber"]);
			$SalaryAmount 		= mysqli_real_escape_string($e2e,$row["salary"]);
			
			
			$isGovtService 		= mysqli_real_escape_string($e2e,$row["governService"]);
			$Reason 			= mysqli_real_escape_string($e2e,$row["separationCause"]);
			$LWOP 				= mysqli_real_escape_string($e2e,$row["lwop"]);

			$SalaryGradeRefId 	= mysqli_real_escape_string($e2e,$row["salaryGrade"]);
			$AgencyRefId 		= mysqli_real_escape_string($e2e,$row["stationAgency"]);
			$ApptStatusRefId 	= mysqli_real_escape_string($e2e,$row["appointmentCode"]);
			$WorkStartDate 		= mysqli_real_escape_string($e2e,$row["serviceFromDate"]);
			$WorkEndDate 		= mysqli_real_escape_string($e2e,$row["serviceToDate"]);
			$SeparatedDate 		= mysqli_real_escape_string($e2e,$row["separationDate"]);
			$PositionCode 		= mysqli_real_escape_string($e2e,$row["positionCode"]);
			$PositionRefId 		= mysqli_real_escape_string($e2e,$row["positionDesc"]);

			if (strtolower($isGovtService) == "yes") {
				$isGovtService = 1;
			} else {
				$isGovtService = 0;
			}

			if (is_numeric($EmployeesRefId)) {
				$flds = "";
				$vals = "";
				$flds .= "CompanyRefId, BranchRefId, EmployeesRefId, SalaryAmount, Cause, LWOP, isGovtService, ";
				$vals .= "$CompanyRefId, $BranchRefId, $EmployeesRefId, '$SalaryAmount', '$Reason', '$LWOP', '$isGovtService', ";
				if ($PositionRefId != "") {
					$Position_fld = "Code, Name, ";
					$Position_val = "'$PositionCode', '$PositionRefId',";
					$Position_rs = saveFM("position",$Position_fld,$Position_val,$PositionRefId);
					if (is_numeric($Position_rs)) {
						$PositionRefId = $Position_rs;
						$flds .= "PositionRefId, ";
						$vals .= "'$PositionRefId', ";
					}
				}

				if ($ApptStatusRefId != "") {
					$ApptStatus_fld = "Name, ";
					$ApptStatus_val = "'$ApptStatusRefId',";
					$ApptStatus_rs = saveFM("apptstatus",$ApptStatus_fld,$ApptStatus_val,$ApptStatusRefId);
					if (is_numeric($ApptStatus_rs)) {
						$ApptStatusRefId = $ApptStatus_rs;
						$flds .= "ApptStatusRefId, ";
						$vals .= "'$ApptStatusRefId', ";
					}
				}

				if ($AgencyRefId != "") {
					$Agency_fld = "Name, ";
					$Agency_val = "'$AgencyRefId',";
					$Agency_rs = saveFM("agency",$Agency_fld,$Agency_val,$AgencyRefId);
					if (is_numeric($Agency_rs)) {
						$AgencyRefId = $Agency_rs;
						$flds .= "AgencyRefId, ";
						$vals .= "'$AgencyRefId', ";
					}
				}

				if ($WorkStartDate != "" && $WorkStartDate != "0000-00-00") {
					if (validateDate($WorkStartDate)) {
						$flds .= "EffectivityDate, ";
						$vals .= "'$WorkStartDate', ";
					}
				}
				if ($WorkEndDate != "" && $WorkEndDate != "0000-00-00" && $WorkEndDate != "Present") {
					if (validateDate($WorkEndDate)) {
						$flds .= "ExpiryDate, ";
						$vals .= "'$WorkEndDate', ";
					}
				}
				
				
				if (strpos($SalaryGradeRefId, "-")) {
					$sg_arr = explode("-", $SalaryGradeRefId);
					$sg = $sg_arr[0];
					$si = $sg_arr[1];
					if ($sg > 0) {
						$SalaryGrade_fld = "Name, ";
						$SalaryGrade_val = "'$sg',";
						$SalaryGrade_rs = saveFM("salarygrade",$SalaryGrade_fld,$SalaryGrade_val,$sg);
						if (is_numeric($SalaryGrade_rs)) {
							$flds .= "SalaryGradeRefId, ";
							$vals .= "$SalaryGrade_rs, ";
						}
					}
					if ($sg > 0) {
						$StepIncrement_fld = "Name, ";
						$StepIncrement_val = "'$si',";
						$StepIncrement_rs = saveFM("StepIncrement",$StepIncrement_fld,$StepIncrement_val,$si);
						if (is_numeric($StepIncrement_rs)) {
							$flds .= "StepIncrementRefId, ";
							$vals .= "$StepIncrement_rs, ";
						}
					}
				} else {
					if (is_numeric($SalaryGradeRefId)) {
						if ($SalaryGradeRefId > 0) {
							$SalaryGrade_fld = "Name, ";
							$SalaryGrade_val = "'$SalaryGradeRefId',";
							$SalaryGrade_rs = saveFM("salarygrade",$SalaryGrade_fld,$SalaryGrade_val,$SalaryGradeRefId);
							if (is_numeric($SalaryGrade_rs)) {
								$flds .= "SalaryGradeRefId, ";
								$vals .= "$SalaryGrade_rs, ";
							}		
						}
					}
				}

				$rs_empmovement = save("employeesmovement",$flds,$vals);
				if (!is_numeric($rs_empmovement)) {
					echo $rs_empmovement;
				} else {
					echo "Employee $EmployeesRefId added.<br>";
				}
 			}
		}
	}
	*/
	
	/*==================================================================*/
  	//upload tblEmpVoluntaryWork
  	/*==================================================================*/
  	/*
	$rs = SelectEach("tblEmpVoluntaryWork","",$curr_mirdc);
	if ($rs) {
		while ($row = mysqli_fetch_assoc($rs)) {
			$emprefid = $row["empNumber"];
			$org 	  = $row["vwName"];
			$orgAdd   = $row["vwAddress"];
			$from 	  = $row["vwDateFrom"];
			$to 	  = $row["vwDateTo"];
			$NumofHrs = $row["vwHours"];
			if (is_numeric($emprefid)) {
				$flds 	  = "CompanyRefId, BranchRefId, EmployeesRefId, NumofHrs, ";
				$vals     = "21, 1, $emprefid, '$NumofHrs', ";
				if ($from != "" && $from != "0000-00-00") {
					if (validateDate($from)) {
						$flds .= "StartDate, ";
						$vals .= "'$from', ";
					}
				}
				if ($to != "" && $to != "0000-00-00") {
					if (validateDate($to)) {
						$flds .= "EndDate, ";
						$vals .= "'$to', ";
					}
				}
				if ($org != "") {
					$Organization_fld = "Name, Address, ";
					$Organization_val = "'$org', '$orgAdd', ";
					$Organization_rs = saveFM("Organization",$Organization_fld,$Organization_val,$org);
					if (is_numeric($Organization_rs)) {
						$OrganizationRefId = $Organization_rs;
						$flds .= "OrganizationRefId, ";
						$vals .= "'$OrganizationRefId', ";
					}
				}
				$rs_empvw = save("employeesvoluntary",$flds,$vals);
				if (!is_numeric($rs_empvw)) {
					echo $rs_empvw;
				} else {
					echo "Employee $emprefid added.<br>";
				}
			}
		}
	}
	*/
	

	/*==================================================================*/
  	//upload tblCourse
  	/*==================================================================*/
  	/*
	$rs = SelectEach("tblCourse","",$curr_mirdc);
	if ($rs) {
		while ($row = mysqli_fetch_assoc($rs)) {
			$code = mysqli_real_escape_string($e2e,$row["courseCode"]);
			$name = mysqli_real_escape_string($e2e,$row["courseDesc"]);
			$course_flds = "Code, Name, ";
			$course_vals = "'$code', '$name',";
			$result = saveFM("course",$course_flds,$course_vals,$name);
			if (!is_numeric($result)) {
				echo $result;
			} else {
				echo "Course $name added<br>";
			}

		}
	}
	*/


	/*==================================================================*/
  	//upload tblEmpSchool
  	/*==================================================================*/
  	/*
	$rs = SelectEach("tblEmpSchool","",$curr_mirdc);
	if ($rs) {
		while ($row = mysqli_fetch_assoc($rs)) {
			$emprefid = mysqli_real_escape_string($e2e,$row["empNumber"]);
			$lvl 	  =	mysqli_real_escape_string($e2e,$row["levelCode"]);
			$schlname = mysqli_real_escape_string($e2e,$row["schoolName"]);
			$course   = mysqli_real_escape_string($e2e,$row["course"]);
			$yrGrad   = mysqli_real_escape_string($e2e,$row["yearGraduated"]);
			$units    = mysqli_real_escape_string($e2e,$row["units"]);
			$from 	  = mysqli_real_escape_string($e2e,$row["schoolFromDate"]);
			$to 	  = mysqli_real_escape_string($e2e,$row["schoolToDate"]);
			$honors   = mysqli_real_escape_string($e2e,$row["honors"]);
			if (is_numeric($emprefid)) {
				switch ($lvl) {
					case 'ELEM':
					case 'ELM':
						$lvl = 1;
						break;
					case 'HS':
					case 'SECON':
					case 'HSL';
						$lvl = 2;
						break;
					case 'VCL':
						$lvl = 3;
						break;
					case 'CLG':
					case 'COL':
						$lvl = 4;
						break;
					case 'GRADSTUD':
					case 'Ph.D.':
					case 'MAMS':
					case 'MAS':
					case 'MS':
						$lvl = 5;
						break;
				}
				if (strpos($honors, "+")) {
					$honors = str_replace("+", " ", $honors);
				}
				$flds = "CompanyRefId, BranchRefId, EmployeesRefId, Honors, LevelType, ";
				$vals = "21, 1, $emprefid, '$honors', $lvl, ";
				if ($from != "" && $from != "0000-00-00" && $from != "--") {
					$flds .= "DateFrom, ";
					$vals .= "'$from', ";
				}
				if ($to != "" && $to != "0000-00-00" && $to != "--") {
						$flds .= "DateTo, ";
						$vals .= "'$to', ";
				}
				if ($yrGrad != "" && $yrGrad != "0000-00-00") {
					$flds .= "YearGraduated, ";
					$vals .= "'".date("Y",strtotime($yrGrad))."', ";
				}
				if ($schlname != "") {
					$school_flds = "Name, Offer1, Offer2, Offer3, Offer4, Offer5, ";
					$school_vals = "'$schlname', 1, 1, 1, 1, 1, ";
					$school_rs = saveFM("schools",$school_flds,$school_vals,$schlname);
					if (is_numeric($school_rs)) {
						$flds .= "SchoolsRefId, ";
						$vals .= "$school_rs, ";
					}
				}
				if ($course != "") {
					$course_flds = "Name, ";
					$course_vals = "'$course', ";
					$course_rs = saveFM("course",$course_flds,$course_vals,$course);
					if (is_numeric($course_rs)) {
						$flds .= "CourseRefId, ";
						$vals .= "$course_rs, ";
					}
				}
				$rs_empschool = save("employeeseduc",$flds,$vals);
				if (!is_numeric($rs_empschool)) {
					echo $rs_empschool;
				} else {
					echo "Employee $emprefid added $schlname as his school.<br>";
				}
				
			}

		}
	}
	*/

	/*==================================================================*/
  	//upload tblEmpLeave
  	/*==================================================================*/
  	/*
	$rs = SelectEach("tblEmpLeave","",$curr_mirdc);
	if ($rs) {
		while ($row = mysqli_fetch_assoc($rs)) {
			$dateFiled = realEscape($row["dateFiled"]);
			$emprefid  = realEscape($row["empNumber"]);
			$leave     = realEscape($row["leaveCode"]);
			$reason    = realEscape($row["reason"]);
			$from 	   = realEscape($row["leaveFrom"]);
			$to        = realEscape($row["leaveTo"]);
			$leavesrefid = FindFirst("leaves","WHERE Code = '$leave'","RefId",$e2e);
			if (is_numeric($leavesrefid) && is_numeric($emprefid)) {
				$flds = "CompanyRefId, BranchRefId, EmployeesRefId, LeavesRefId, FiledDate, ";
				$flds .= "ApplicationDateFrom, ApplicationDateTo, Status, Remarks, ";
				$vals = "21, 1, $emprefid, $leavesrefid, '$dateFiled', ";
				$vals .= "'$from', '$to', 'Approved', '$reason', ";
				$where = "WHERE EmployeesRefId = $emprefid";
				$where .= " AND LeavesRefId = '$leavesrefid'";
				$where .= " AND ApplicationDateFrom = '$from'";
				$where .= " AND ApplicationDateTo = '$to'";
				$check_leave = FindFirst("employeesleave",$where,"RefId",$e2e);
				if (!is_numeric($check_leave)) {
					$result = save("employeesleave",$flds,$vals);
					if (is_numeric($result)) {
						echo "Employee $emprefid has filed $leave.<br>";
					} else {
						echo $result;
					}
				}
			}

		}
	}
	*/

	/*==================================================================*/
  	//upload tblAttendanceCode
  	/*==================================================================*/
  	/*
	$rs = SelectEach("tblAttendanceCode","",$curr_mirdc);
	if ($rs) {
		while ($row = mysqli_fetch_assoc($rs)) {
			$name = mysqli_real_escape_string($e2e,$row["name"]);
			$code = mysqli_real_escape_string($e2e,$row["code"]);
			if (strpos($name, "Leave")) {
				$flds = "Code, Name, ";
				$vals = "'$code','$name', ";
				$result = saveFM("leaves",$flds,$vals,$name);
				if (!is_numeric($result)) {
					echo $result;
				}
			} else {
				$flds = "Code, Name, ";
				$vals = "'$code','$name', ";
				$result = saveFM("absences",$flds,$vals,$name);
				if (!is_numeric($result)) {
					echo $result;
				}
			}

		}
	}
	*/

	/*==================================================================*/
  	//upload tblLeave
  	/*==================================================================*/
  	/*
	$rs = SelectEach("tblLeave","",$curr_mirdc);
	if ($rs) {
		while ($row = mysqli_fetch_assoc($rs)) {
			$name = mysqli_real_escape_string($e2e,$row["leaveType"]);
			$code = mysqli_real_escape_string($e2e,$row["leaveCode"]);
			$flds = "Code, Name, ";
			$vals = "'$code','$name', ";
			$result = saveFM("leaves",$flds,$vals,$name);
			if (!is_numeric($result)) {
				echo $result;
			}	
		}
	}
	*/

	/*==================================================================*/
  	//upload tblEmpDTR
  	/*==================================================================*/
  	
  	$where = "WHERE dtrDate >= '2018-01-01' AND dtrDate <= '".date("Y-m-d",time())."'";
	$rs = SelectEach("tblEmpDTR",$where,$curr_mirdc);
	if ($rs) {
		$count = 0;
		while ($row = mysqli_fetch_assoc($rs)) {
			$emprefid 	= realEscape($row["empNumber"]);
			$date 		= realEscape($row["dtrDate"]);
			$timein 	= realEscape($row["inAM"]);
			$lunchout 	= realEscape($row["outAM"]);
			$lunchin 	= realEscape($row["inPM"]);
			$timeout 	= realEscape($row["outPM"]);
			if ($date != "" && $date != "0000-00-00") {
				if (validateDate($date)) {
					$timein 	= getTimeInSeconds($timein);
					$lunchout 	= getTimeInSeconds($lunchout);
					$lunchin 	= getTimeInSeconds($lunchin);
					$timeout 	= getTimeInSeconds($timeout);
					$where 		= "WHERE EmployeesRefId = '$emprefid'";
					$flds       = "CompanyRefId, BranchRefId, EmployeesRefId, ";
					$flds      .= "AttendanceDate, AttendanceTime, CheckTime, KindOfEntry, ";
					$vals       = "21, 1, $emprefid, '$date', ";
					if ($timein > 0) {
						$timein 			= strtotime($date) + $timein;
						$timein_vals 		= $vals."'$timein', '$timein', 1, ";
						$timein_where 		= $where." AND CheckTime = '$timein'";
						$timein_check 		= FindFirst("employeesattendance",$timein_where,"RefId",$e2e);
						if (!is_numeric($timein_check)) {
							$timein_result = save("employeesattendance",$flds,$timein_vals);
							if (!is_numeric($timein_result)) {
								echo $timein_result;
							}
						}
					}
					if ($lunchout > 0) {
						$lunchout 			= strtotime($date) + $lunchout;
						$lunchout_vals 		= $vals."'$lunchout', '$lunchout', 2, ";
						$lunchout_where 	= $where." AND CheckTime = '$lunchout'";
						$lunchout_check 	= FindFirst("employeesattendance",$lunchout_where,"RefId",$e2e);
						if (!is_numeric($lunchout_check)) {
							$lunchout_result = save("employeesattendance",$flds,$lunchout_vals);
							if (!is_numeric($lunchout_result)) {
								echo $lunchout_result;
							}
						}
					}
					if ($lunchin > 0) {
						$lunchin 			= strtotime($date) + $lunchin;
						$lunchin_vals 		= $vals."'$lunchin', '$lunchin', 3, ";
						$lunchin_where 		= $where." AND CheckTime = '$lunchin'";
						$lunchin_check 		= FindFirst("employeesattendance",$lunchin_where,"RefId",$e2e);
						if (!is_numeric($lunchin_check)) {
							$lunchin_result = save("employeesattendance",$flds,$lunchin_vals);
							if (!is_numeric($lunchin_result)) {
								echo $lunchin_result;
							}
						}
					}
					if ($timeout > 0) {
						$timeout 			= strtotime($date) + $timeout;
						$timeout_vals 		= $vals."'$timeout', '$timeout', 4, ";
						$timeout_where 		= $where." AND CheckTime = '$timeout'";
						$timeout_check 		= FindFirst("employeesattendance",$timeout_where,"RefId",$e2e);
						if (!is_numeric($timeout_check)) {
							$timeout_result = save("employeesattendance",$flds,$timeout_vals);
							if (!is_numeric($timeout_result)) {
								echo $timeout_result;
							}
						}
					}
				}
			}
		}
	}
	
		
	
	
?>