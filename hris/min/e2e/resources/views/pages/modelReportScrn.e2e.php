<?php
   require_once "incUtilitiesJS.e2e.php";
?>
<script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   <div class="mypanel" id="rptCriteria">
      <div class="panel-mid-litebg">
         <div class="panel-top margin-top">
            Options
         </div>
         <div class="panel-mid-litebg">
            <div class="row margin-top">
               <div class="col-xs-12 txt-center">
                  <span class="label">Sort By:</span>
                  <select class="form-input rptCriteria--" name="drpSortBy" id="drpSortBy" style="width:150px;">
                     <option value="LastName" selected>Last Name</option>
                     <option value="FirstName">First Name</option>
                     <option value="BirthDate">Date of Birth</option>
                     <option value="Height">Height</option>
                     <option value="Weight">Weight</option>
                     <option value="CivilStatus">Civil Status</option>
                  </select>
                  <label style="margin-left:10px;margin-right:10px;">|</label>
                  <input type="checkbox" name="chkRptSummary" class="showCol--">&nbsp;<span class="label">Show Report Summary</span>
               </div>
            </div>
         </div>
      </div>
   </div>

