<?php
   //session_start();
   include 'colors.e2e.php';
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   include 'incRptSortBy.e2e.php';
   $rs = SelectEach($table,$whereClause);
   if ($rs) $rowcount = mysqli_num_rows($rs);
   if ($dbg) { echo "DBG >> ".$whereClause; }

   /*Start Here Date Validation --*/
   $errmsg = "";
   /*End Here - Date Validation*/
   $recordsCount = 0;
?>

<!DOCTYPE html>
<html>
   <head>
      <?php include "pageHEAD.e2e.php"; ?>
      <link rel="stylesheet" href="<?php echo $_SESSION["path"] ?>css/rpt.css">
      <script src="<?php jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            //rptHeader("Notice of Salary Adjustment");

            $empRefid = "";
            while ($row = mysqli_fetch_assoc($rs))
            {
               $childCount = 0;
               $empRefid = $row['RefId'];

               $sql = "SELECT * FROM `employeesmovement` WHERE CompanyRefId = $CompanyId ";
               $sql .= "AND BranchRefId = $BranchId ";
               $sql .= "AND EmployeesRefId = $empRefid ORDER BY RefId DESC Limit 1";

               $rsEMPMOVEMENT = mysqli_query($conn,$sql) or die(mysqli_error($conn));
               $numrow = mysqli_num_rows($rsEMPMOVEMENT);
               if ($numrow) {
                  while ($row_EmpMovement = mysqli_fetch_assoc($rsEMPMOVEMENT)) {


                  $whereClause = "WHERE CompanyRefId = $CompanyId";
                  $whereClause .= " AND BranchRefId = $BranchId";
                  $whereClause .= " AND EmployeesRefId = $empRefid";
                  $whereClause .= " AND RefId < ".$row_EmpMovement['RefId'];
                  $EmpMovement_old = FindLast("EmpMovement",$whereClause,"*");
                  if ($EmpMovement_old) {
                     $salaryAmount_old = $EmpMovement_old["SalaryAmount"];
                     $stepIncrement_old = "[".$EmpMovement_old["StepIncrementRefId"]."] ".getRecord("StepIncrement",$EmpMovement_old["StepIncrementRefId"],"Name");
                     $salaryGradeName_old = "[".$EmpMovement_old["SalaryGradeRefId"]."] ".getRecord("SalaryGrade",$EmpMovement_old["SalaryGradeRefId"],"Name");
                  } else {
                     $whereClause = "WHERE CompanyRefId = $CompanyId";
                     $whereClause .= " AND BranchRefId = $BranchId";
                     $whereClause .= " AND EmployeesRefId = $empRefid";
                     $lastWorkExp = FindLast("EmployeesWorkExperience",$whereClause,"*");
                     $salaryAmount_old = $lastWorkExp["SalaryAmount"];
                     $stepIncrement_old = "[".$lastWorkExp["StepIncrementRefId"]."] ".getRecord("StepIncrement",$lastWorkExp["StepIncrementRefId"],"Name");
                     $salaryGradeName_old = "[".$lastWorkExp["SalaryGradeRefId"]."] ".getRecord("SalaryGrade",$lastWorkExp["SalaryGradeRefId"],"Name");;
                  }

                  $effDate = $row_EmpMovement["EffectivityDate"];
                  $effectivityDate_Arr = explode("-",$row_EmpMovement["EffectivityDate"]);
                  $effectivityDate_name = monthName($effectivityDate_Arr[1],1)." ".$effectivityDate_Arr[2].", ".$effectivityDate_Arr[0];

                  $effectivityDate_add1  = convDate(dateInterval('+1 day',$effDate),1);
                  $effectivityDate_less1 = convDate(dateInterval('-1 day',$effDate),1);

                  $salaryGradeName = "[".$row_EmpMovement["SalaryGradeRefId"]."] ".getRecord("SalaryGrade",$row_EmpMovement["SalaryGradeRefId"],"Name");
                  $positionName = "[".$row_EmpMovement["PositionRefId"]."] ".getRecord("Position",$row_EmpMovement["PositionRefId"],"Name");
                  $positionItemName = "[".$row_EmpMovement["PositionItemRefId"]."] ".getRecord("PositionItem",$row_EmpMovement["PositionItemRefId"],"Name");
                  $stepIncrement_new = getRecord("StepIncrement",$row_EmpMovement["StepIncrementRefId"],"Name");;
                  $salaryAmount_new = $row_EmpMovement["SalaryAmount"];
                  $salaryAmount = $salaryAmount_old + $salaryAmount_new;
                  $EmployeesName = $row["FirstName"]." ".$row["MiddleName"].". ".$row["LastName"];
                  rptHeader("NOTICE OF SALARY ADJUSTMENT");
         ?>
                  <div class="container-fluid nextpage page--">
                     <div>
                        <p>
                           <?php
                              echo "[".$empRefid."] ".$EmployeesName;
                              echo "<br>".getRecord("Office",$row_EmpMovement["OfficeRefId"],"Name");
                           ?>
                        </p>

                        <p>Sir/Madam:</p>
                        <p>
                           Pursuant to National Budget Circular No. 524, dated <?php echo $effectivityDate_name; ?>, implementing OP Executive Order No. 900 dated <?php echo $effectivityDate_name; ?>, your salary is hereby adjusted effective <?php echo $effectivityDate_add1; ?>, as follows:
                        </p>

                        <p>
                           <div class="row">
                              <div class="col-xs-10">
                                 1. Adjusted monthly basic salary effective <?php echo $effectivityDate_name; ?> under the new Salary Schedule: <?php echo $salaryGradeName; ?>, <?php echo $stepIncrement_new; ?>
                              </div>
                              <div class="col-xs-2">P&nbsp;<?php echo number_format($salaryAmount,2); ?></div>
                           </div>
                           <div class="row">
                              <div class="col-xs-10">2. Actual monthly basic salary as of <?php echo $effectivityDate_less1; ?>: <?php echo $salaryGradeName_old; ?>, <?php echo $stepIncrement_old; ?></div>
                              <div class="col-xs-2">P&nbsp;<?php echo number_format($salaryAmount_old,2); ?></div>
                           </div>
                           <div class="row">
                              <div class="col-xs-10">
                                 3. Monthly salary adjustment effective <?php echo $effectivityDate_name; ?> (<?php echo $salaryGradeName; ?> - <?php echo $stepIncrement_new; ?>)
                              </div>
                              <div class="col-xs-2">
                                 P&nbsp;<?php echo number_format($salaryAmount_new,2); ?>
                              </div>
                           </div>
                        </p>

                        <p>It is understood that this salary adjustment is subject to review and post-audit, and to appropriate re-adjustment and refund if found not in order.</p>

                        <div class="row">
                           <div class="col-xs-6">Position Title</div>
                           <div class="col-xs-6">: <?php echo $positionName ?></div>
                        </div>

                        <div class="row">
                           <div class="col-xs-6">Salary Grade</div>
                           <div class="col-xs-6">: <?php echo $salaryGradeName; ?></div>
                        </div>

                        <div class="row">
                           <div class="col-xs-6">Item No./Unique Item No., FY 2009 Personnel</div>
                           <div class="col-xs-6">: <?php echo $positionItemName; ?></div>
                        </div>
                        <div class="row">
                           <div class="col-xs-6">Services Itemization and/or Plantilla of Personnel</div>
                           <div class="col-xs-6">: </div>
                        </div>
                        <br><br><br>
                        <div class="txt-right">
                           <span style="margin-right:1in;">
                              Very truly yours,
                           </span>
                        </div>
                     </div>
                     <hr>
                     <?php rptFooter(); ?>
                  </div>
         <?php

                  }
                  $recordsCount++;
               }
            }
            echo
            '<div class="lastpage">';
            rptHeader("NOTICE OF SALARY ADJUSTMENT");
            echo
            '<span>RECORD COUNT : '.$recordsCount.'</span>
            <div>SEARCH CRITERIA:</div>';
            if ($searchCriteria == "") echo "<li>ALL RECORDS</li>";
            rptFooter();
            echo
            '</div>';
         ?>
      </div>
   </body>
</html>