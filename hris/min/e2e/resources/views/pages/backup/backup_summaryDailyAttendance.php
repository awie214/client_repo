<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   //$whereClause .= " LIMIT 10";
   $table = "employees";
   $rsEmployees = SelectEach($table,$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   $from = getvalue("txtAttendanceDateFrom");
   $to   = getvalue("txtAttendanceDateTo");
   $month = getvalue("txtAttendanceMonth");
   $year  = getvalue("txtAttendanceYear");
   /*
   if ($from == "") {
      $from     = date("Y-m-d",time());
      $from_arr = explode("-", $from);
      $from     = $from_arr[0]."-".$from_arr[1]."-01";
   }
   if ($to == "") {
      $to     = date("Y-m-d",time());
      $to_arr = explode("-", $to);
      
      $to     = $to_arr[0]."-".$to_arr[1]."-".cal_days_in_month(CAL_GREGORIAN,$to_arr[1],$to_arr[0]);
   }
   */
   $from    = $year."-".$month."-01";
   $to      = $year."-".$month."-".cal_days_in_month(CAL_GREGORIAN,$month,$year);
   if ($dbg) {
      echo $whereClause;
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            $errmsg = "";
            rptHeader(getRptName(getvalue("drpReportKind")));
            if ($rsEmployees && $errmsg == "")
            {
         ?>
         <p class="txt-center">
            <?php
               echo "For the Date ";
               echo date("m/d/Y",strtotime($from))." - ".date("m/d/Y",strtotime($to));
            ?>
         </p>
         <table border="1">
            <tr>
               <th rowspan=2>Name of Employee</th>
               <?php for ($j=1;$j<=31;$j++) {?>
                  <th rowspan=2 class="txt-center" style="width:25px;"><?php echo $j ?></th>
               <?php } ?>
               <th colspan=3>Total</th>
               <th colspan=8>Total Used Of</th>
            </tr>
            <tr>
               <th>L</th>
               <th>U</th>
               <th>A</th>
               <th>VL</th>
               <th>FL</th>
               <th>SL</th>
               <th>SPL</th>
               <th>ML</th>
               <th>PL</th>
               <th>MC8</th>
               <th>CTO</th>
            </tr>
            <?php
               $count = 0;
               while ($row_emp = mysqli_fetch_assoc($rsEmployees) ) {
                  $emprefid       = $row_emp["RefId"];
                  $biometricsID   = $row_emp["BiometricsID"];
                  $CompanyID      = $row_emp["CompanyRefId"];
                  $BranchID       = $row_emp["BranchRefId"];
                  $Default_qry    = "WHERE CompanyRefId = ".$CompanyID." AND BranchRefId = ".$BranchID;
                  $workschedrefid = FindFirst("empinformation","WHERE EmployeesRefId = ".$row_emp["RefId"],"WorkscheduleRefId");
                  if (is_numeric($workschedrefid)) {
                     ${"L_".$row_emp["RefId"]}   = 0;
                     ${"U_".$row_emp["RefId"]}   = 0;
                     ${"A_".$row_emp["RefId"]}   = 0;
                     ${"VL_".$row_emp["RefId"]}   = 0;
                     ${"FL_".$row_emp["RefId"]}   = 0;
                     ${"SL_".$row_emp["RefId"]}   = 0;
                     ${"SPL_".$row_emp["RefId"]}  = 0;
                     ${"ML_".$row_emp["RefId"]}   = 0;
                     ${"PL_".$row_emp["RefId"]}   = 0;
                     ${"MC8_".$row_emp["RefId"]}  = 0;
                     ${"CTO_".$row_emp["RefId"]}  = 0;
                     for ($v=1;$v<=31;$v++) {
                        if ($v <= 9) $v = "0".$v; 
                        ${"d_".$row_emp["RefId"]."_".$v} = "&nbsp;";
                     } 
                     $count++;
                     $curr_date   = date("Y-m-d",time());
                     $month_start = $from;
                     $month_end   = $to;
                     include 'mdbcn.e2e.php';
                     include 'DailyAttendance.e2e.php';
            ?>
               <tr>
                  <td class="pad-left">
                     <?php 
                        echo $row_emp['LastName'].', '.$row_emp['FirstName'].', '.$row_emp['MiddleName'];
                     ?>
                  </td>
                  <?php
                     for ($v=1;$v<=31;$v++) {
                        if ($v <= 9) $v = "0".$v; 
                        echo '<td class="txt-center">'.${"d_".$row_emp["RefId"]."_".$v}.'</td>';
                     } 
                  ?>
                  <td class="txt-center">
                     <?php echo ${"L_".$row_emp["RefId"]}; ?>
                  </td>
                  <td class="txt-center">
                     <?php echo ${"U_".$row_emp["RefId"]}; ?>
                  </td>
                  <td class="txt-center">
                     <?php echo ${"A_".$row_emp["RefId"]}; ?>
                  </td>
                  <?php
                     if (
                        ${"L_".$row_emp["RefId"]}     == 0 &&
                        ${"U_".$row_emp["RefId"]}     == 0 &&
                        ${"A_".$row_emp["RefId"]}     == 0
                     ) {
                        echo '<td class="text-center" colspan=8>COMPLETE ATTENDANCE</td>';
                     } else {
                  ?>
                  <td class="txt-center">
                     <?php echo ${"VL_".$row_emp["RefId"]}; ?>
                  </td>
                  <td class="txt-center">
                     <?php echo ${"FL_".$row_emp["RefId"]}; ?>
                  </td>
                  <td class="txt-center">
                     <?php echo ${"SL_".$row_emp["RefId"]}; ?>
                  </td>
                  <td class="txt-center">
                     <?php echo ${"SPL_".$row_emp["RefId"]}; ?>
                  </td>
                  <td class="txt-center">
                     <?php echo ${"ML_".$row_emp["RefId"]}; ?>
                  </td>
                  <td class="txt-center">
                     <?php echo ${"PL_".$row_emp["RefId"]}; ?>
                  </td>
                  <td class="txt-center">
                     <?php echo ${"MC8_".$row_emp["RefId"]}; ?>
                  </td>
                  <td class="txt-center">
                     <?php echo ${"CTO_".$row_emp["RefId"]}; ?>
                  </td>
                  <?php } ?>
               </tr>
            <?php
                  }
               }
               echo "RECORD COUNT : ".$count;
            }else {
               echo '<div>NO RECORD QUERIED base on your criteria!!!</div>';
               echo '<div>'.$errmsg.'</div>';
            }
            ?>
         </table>
         <p>
            <div class="row">
               <div class="col-xs-2 txt-right">Prepared By:</div>
               <div class="col-xs-4"></div>
               <div class="col-xs-2 txt-right">Approved By:</div>
               <div class="col-xs-4"></div>
            </div>
            <div class="row">
               <div class="col-xs-2"></div>
               <div class="col-xs-4">________________________</div>
               <div class="col-xs-2"></div>
               <div class="col-xs-3">________________________</div>
               <div class="col-xs-1"></div>
            </div>
         </p>

      </div>
   </body>
</html>