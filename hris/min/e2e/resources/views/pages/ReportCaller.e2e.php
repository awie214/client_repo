<?php
   session_start();
   require "conn.e2e.php";
   require 'constant.e2e.php';
   require_once pathClass.'0620functions.e2e.php';
   $fContent = file_get_contents(json."file.json");
   $files = json_decode($fContent, true);

   $CompanyId = 0;
   $BranchId = 0;
   $CompanyId = getvalue("hCompanyID");
   $BranchId = getvalue("hBranchID");
   $CompanyCode = getvalue("ccode");
   $Owner = getvalue("hOwner");
   //$SmallLogo = "PIDSLogo.png";
   if (getvalue("ext") == "") {
      $ext = "php";
   }
   else {
      $ext = getvalue("ext");
   }

   $agencyCode = FindFirst("company","","Code");
   $rptFile = "./rpt/".getvalue("file");

   if ($Owner == "E2E" || $Owner == "") {
      $rptFile = "./rpt/".getvalue("file");
      require $rptFile.".e2e.".$ext;
   } else {
      $rptFile = "./rpt/$CompanyId/".getvalue("file");
      require $rptFile.".e2e.".$ext;
   }

   $conn->close();
?>