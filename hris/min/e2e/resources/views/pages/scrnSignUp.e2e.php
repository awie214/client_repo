<script language="JavaScript">
   $(document).ready(function () {

   });
</script>
<?php
   session_start();
   require_once "conn.e2e.php";
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'SysFunctions.e2e.php';
   require_once $_SESSION['Classes'].'Forms.e2e.php';
   $form = new SysForm();
   function LcreateSelect($table,$objname,$val,$w,$name,$initValue,$disabled) {
      include "conn.e2e.php";
      $w = 70;
      $sql = "SELECT * FROM ".strtolower($table)." ORDER BY RefId";
      $result = mysqli_query($conn,$sql) or die(mysqli_error($conn));
      if ($result) {
         echo '<select name="'.$objname.'" '.$disabled.' class="form-input saveFields-- rptCriteria--" style="width:'.$w.'%">'."\n";
         echo '<option value="0">'.$initValue.'</option>'."\n";
         while($row = mysqli_fetch_assoc($result)) {

            echo '<option value="'.$row["RefId"].'"';
            if ($val==$row["RefId"]) echo 'selected'; else true;
            echo '>['.$row["RefId"].'] '.$row[$name].'</option>'."\n";

         }
         echo '</select>'."\n";
      }
      mysqli_close($conn);
   }
?>
                  <?php
                     if (getvalue("scrn") == "fpw") {
                        $username = getvalue("u");
                        $criteria  = " WHERE UserName = '$username'";
                        $criteria .= " LIMIT 1";
                        $recordSet = f_Find("sysuser",$criteria);
                        if ($recordSet) {
                           $rowcount = mysqli_num_rows($recordSet);
                           $sysuser = array();
                           $sysuser = mysqli_fetch_assoc($recordSet);
                           $question1 = $sysuser["SecurityQuestion1"];
                           $question2 = $sysuser["SecurityQuestion2"];
                  ?>
                           <script type="text/javascript" src="<?php echo jsCtrl("ctrl_idx"); ?>"></script>
                           <div class="row scrns" id="ForgotPWEntry">
                              <div class="col-xs-4"></div>
                              <div class="col-xs-4">
                                 <div class="mypanel">
                                    <div class="panel-top" style="padding:10px;">
                                       <span>FORGOT / CHANGE PASSWORD</span>
                                       <a href="javascript:void(0);" class="close" id="closePWScrn">
                                          <i class="fa fa-times" aria-hidden="true"></i>
                                       </a>
                                    </div>
                                    <div class="panel-mid">
                                       <div class="row">
                                          <div class="col-xs-1"></div>
                                          <div class="col-xs-10 txt-left">
                                                   <?php
                                                      spacer(30);
                                                      echo '<div>Security Question #1</div>';
                                                      echo '<div class="form-disp margin-top">'.$question1.'</div>';
                                                      spacer(10);
                                                      $attr = ["br"=>true,
                                                               "type"=>"password",
                                                               "row"=>true,
                                                               "name"=>"fpw_AnswerQuestion1",
                                                               "col"=>"12",
                                                               "id"=>"fpwAnsQue1",
                                                               "label"=>"Answer Question #1",
                                                               "class"=>"saveFields-- mandatory",
                                                               "style"=>"width:70%",
                                                               "other"=>""];
                                                      $form->eform($attr);
                                                      spacer(10);
                                                      echo '<div>Security Question #2</div>';
                                                      echo '<div class="form-disp margin-top">'.$question2.'</div>';
                                                      spacer(10);
                                                      $attr = ["br"=>true,
                                                               "type"=>"password",
                                                               "row"=>true,
                                                               "name"=>"char_AnswerQuestion2",
                                                               "col"=>"12",
                                                               "id"=>"AnsQue2",
                                                               "label"=>"Answer Question #2",
                                                               "class"=>"saveFields-- mandatory",
                                                               "style"=>"width:70%",
                                                               "other"=>""];
                                                      $form->eform($attr);
                                                      spacer(30);
                                                   ?>
                                          </div>
                                          <div class="col-xs-1"></div>
                                       </div>
                                    </div>
                                    <div class="panel-bottom txt-right">
                                       <?php
                                          createButton("Request New Password","btnNewPassword","btn-cls4-sea","fa-unlock","");
                                       ?>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-xs-4"></div>
                           </div>
                  <?php
                        } else {
                           echo '<script type="text/javascript">
                              alert("User Name : '.$username.' Not Found");
                              loadscrn("setUsername");
                           </script>';
                           return;
                        }
                     }
                     else if (getvalue("scrn") == "setUsername") {
                  ?>
                        <script type="text/javascript" src="<?php echo jsCtrl("ctrl_idx"); ?>"></script>
                        <div class="row" id="">
                           <div class="col-xs-4"></div>
                           <div class="col-xs-4">
                              <div class="mypanel">
                                 <div class="panel-top" style="padding:10px;">
                                    <span>FORGOT / CHANGE PASSWORD</span>
                                    <a href="javascript:void(0);" class="close">
                                       <i class="fa fa-times" aria-hidden="true"></i>
                                    </a>
                                 </div>
                                 <div class="panel-mid">
                                    <div class="row">
                                       <div class="col-xs-1"></div>
                                       <div class="col-xs-10 txt-left">
                                                <?php
                                                   spacer(10);
                                                   $attr = ["br"=>true,
                                                            "type"=>"text",
                                                            "row"=>true,
                                                            "name"=>"txtUserName",
                                                            "col"=>"12",
                                                            "id"=>"",
                                                            "label"=>"What is your User Name?",
                                                            "class"=>"",
                                                            "style"=>"",
                                                            "other"=>""];
                                                   $form->eform($attr);
                                                   spacer(10);
                                                ?>
                                       </div>
                                       <div class="col-xs-1"></div>
                                    </div>
                                 </div>
                                 <div class="panel-bottom txt-right">
                                    <?php
                                       createButton("Next","btnNextScrn","btn-cls4-sea","fa-forward","");
                                    ?>
                                 </div>
                              </div>
                           </div>
                           <div class="col-xs-4"></div>
                        </div>
                  <?php
                     } else if (getvalue("scrn") == "signup") {
                  ?>
                        <script type="text/javascript" src="<?php echo jsCtrl("ctrl_idx"); ?>"></script>
                        <div class="row scrns" id="SignUpEntry">
                              <div class="col-xs-2"></div>
                              <div class="col-xs-8">
                                 <div class="mypanel">
                                    <div class="panel-top" style="padding:10px;">
                                       <span>SIGN UP</span>
                                       <a href="javascript:void(0);" class="close" id="closePWScrn">
                                          <i class="fa fa-times" aria-hidden="true"></i>
                                       </a>
                                    </div>
                                    <div class="panel-mid">
                                       <div class="row">
                                          <div class="col-xs-1"></div>
                                          <div class="col-xs-5 txt-left">
                                          <?php
                                             $attr = ["br"=>true,
                                                      "type"=>"text",
                                                      "row"=>true,
                                                      "name"=>"sint_CompanyId",
                                                      "col"=>"12",
                                                      "id"=>"CompId",
                                                      "label"=>"Company Id",
                                                      "class"=>"saveFields-- mandatory",
                                                      "style"=>"",
                                                      "other"=>""];
                                             $form->eform($attr);
                                             spacer(10);
                                             $attr = ["br"=>true,
                                                      "type"=>"text",
                                                      "row"=>true,
                                                      "name"=>"char_LastName",
                                                      "col"=>"12",
                                                      "id"=>"LName",
                                                      "label"=>"Last Name",
                                                      "class"=>"saveFields-- mandatory",
                                                      "style"=>"",
                                                      "other"=>""];
                                             $form->eform($attr);
                                             spacer(10);
                                             $attr = ["br"=>true,
                                                      "type"=>"text",
                                                      "row"=>true,
                                                      "name"=>"char_FirstName",
                                                      "col"=>"12",
                                                      "id"=>"FName",
                                                      "label"=>"First Name",
                                                      "class"=>"saveFields-- mandatory",
                                                      "style"=>"",
                                                      "other"=>""];
                                             $form->eform($attr);
                                             spacer (10);
                                             echo '
                                                <div class="row">
                                                   <div class="col-xs-12">
                                                      <label class="control-label">Department:</label><br>
                                                         ';
                                                         LcreateSelect("department","sint_DepartmentRefId","",100,"Name","","");
                                             echo '
                                                   </div>
                                                </div>
                                             ';
                                             spacer (10);
                                             echo '
                                                <div class="row">
                                                   <div class="col-xs-12">
                                                      <label class="control-label">Division:</label><br>
                                                         ';
                                                         LcreateSelect("division","sint_DivisionRefId","",100,"Name","","");
                                             echo '
                                                   </div>
                                                </div>
                                             ';
                                             spacer(20);

                                          ?>
                                          </div>
                                          <div class="col-xs-5 txt-left">
                                             <?php
                                                $attr = ["br"=>true,
                                                         "type"=>"text",
                                                         "row"=>true,
                                                         "name"=>"char_SecQues1",
                                                         "col"=>"12",
                                                         "id"=>"SecQue1",
                                                         "label"=>"Security Question #1",
                                                         "class"=>"saveFields-- mandatory",
                                                         "style"=>"",
                                                         "other"=>""];
                                                $form->eform($attr);
                                                spacer(10);
                                                $attr = ["br"=>true,
                                                         "type"=>"password",
                                                         "row"=>true,
                                                         "name"=>"AnsQues1",
                                                         "col"=>"12",
                                                         "id"=>"AnsQue1",
                                                         "label"=>"Answer Question #1",
                                                         "class"=>"mandatory",
                                                         "style"=>"width:50%",
                                                         "other"=>""];
                                                $form->eform($attr);
                                                spacer(10);
                                                $attr = ["br"=>true,
                                                         "type"=>"text",
                                                         "row"=>true,
                                                         "name"=>"char_SecQues2",
                                                         "col"=>"12",
                                                         "id"=>"SecQue2",
                                                         "label"=>"Security Question #2",
                                                         "class"=>"saveFields-- mandatory",
                                                         "style"=>"",
                                                         "other"=>""];
                                                $form->eform($attr);
                                                spacer(10);
                                                $attr = ["br"=>true,
                                                         "type"=>"password",
                                                         "row"=>true,
                                                         "name"=>"AnsQues2",
                                                         "col"=>"12",
                                                         "id"=>"AnsQue2",
                                                         "label"=>"Answer Question #2",
                                                         "class"=>"mandatory",
                                                         "style"=>"width:50%",
                                                         "other"=>""];
                                                $form->eform($attr);
                                             ?>
                                          </div>
                                          <div class="col-xs-1">
                                             <input type="hidden" name="char_AnsQues1" class="saveFields--">
                                             <input type="hidden" name="char_AnsQues2" class="saveFields--">
                                          </div>
                                       </div>
                                    </div>
                                    <div class="panel-bottom txt-right">
                                       <?php
                                          createButton("SUBMIT","btnSubmitSignUp","btn-cls4-sea","fa-file-text-o","");
                                       ?>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-xs-2"></div>
                        </div>
                  <?php
                     }
                  ?>
