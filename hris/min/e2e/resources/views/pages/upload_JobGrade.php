<?php
	include 'conn.e2e.php';
	include 'constant.e2e.php';
	include pathClass.'0620functions.e2e.php';

	$sql = "SELECT * FROM pms_jobgrade";
	$rs  = mysqli_query($conn,$sql);
	if ($rs) {
		while ($pms_row = mysqli_fetch_assoc($rs)) {
			$Name = $pms_row["job_grade"];
			$Code = "JG".$Name;
			$Step1 = floatval($pms_row["step1"]);
			$Step2 = floatval($pms_row["step2"]);
			$Step3 = floatval($pms_row["step3"]);
			$Step4 = floatval($pms_row["step4"]);
			$Step5 = floatval($pms_row["step5"]);
			$Step6 = floatval($pms_row["step6"]);
			$Step7 = floatval($pms_row["step7"]);
			$Step8 = floatval($pms_row["step8"]);
			$AnnualS1 = ($Step1 * 12);
			$AnnualS2 = ($Step2 * 12);
			$AnnualS3 = ($Step3 * 12);
			$AnnualS4 = ($Step4 * 12);
			$AnnualS5 = ($Step5 * 12);
			$AnnualS6 = ($Step6 * 12);
			$AnnualS7 = ($Step7 * 12);
			$AnnualS8 = ($Step8 * 12);
			$EffectivityDateS1 = "2018-01-01";
			$EffectivityDateS2 = "2018-01-01";
			$EffectivityDateS3 = "2018-01-01";
			$EffectivityDateS4 = "2018-01-01";
			$EffectivityDateS5 = "2018-01-01";
			$EffectivityDateS6 = "2018-01-01";
			$EffectivityDateS7 = "2018-01-01";
			$EffectivityDateS8 = "2018-01-01";
			$Flds = "Code, Name, Step1, Step2, Step3, Step4, Step5, Step6, Step7, Step8, ";
			$Flds .= "EffectivityDateS1, EffectivityDateS2, EffectivityDateS3, EffectivityDateS4, ";
			$Flds .= "EffectivityDateS5, EffectivityDateS6, EffectivityDateS7, EffectivityDateS8, ";
			$Flds .= "AnnualS1, AnnualS2, AnnualS3, AnnualS4, AnnualS5, AnnualS6, AnnualS7, AnnualS8,";
			$Vals = "'$Code', '$Name', '$Step1', '$Step2', '$Step3', '$Step4', '$Step5', '$Step6', '$Step7', '$Step8',";
			$Vals .= "'$EffectivityDateS1', '$EffectivityDateS2', '$EffectivityDateS3', '$EffectivityDateS4', ";
			$Vals .= "'$EffectivityDateS5', '$EffectivityDateS6', '$EffectivityDateS7', '$EffectivityDateS8', ";
			$Vals .= "'$AnnualS1', '$AnnualS2', '$AnnualS3', '$AnnualS4', '$AnnualS5', '$AnnualS6', '$AnnualS7', '$AnnualS8',";
			$result = f_SaveRecord("NEWSAVE","jobgrade",$Flds,$Vals,"ADMIN");
			if (is_numeric($result)) {
				echo "Successfully Save $Code.<br>";
			}

		}
	}

?>
