<?php 
   	$dbName = $_SERVER["DOCUMENT_ROOT"] . "/repo/hris/min/e2e/mdb/att2000.mdb";
   	// $driver = 'MDBTools';
   	// $dataSourceName = "odbc:Driver=$driver;DBQ=$dbName;Uid=;Pwd=;";
   	if (!file_exists($dbName)) {
   		echo "File Not Found.";
   		return false;
   	}
   	$dataSourceName = "odbc:Driver={Microsoft Access Driver (*.mdb)}; DBQ=".realpath($dbName);
   	$connection = new PDO($dataSourceName);
   	$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
?>
