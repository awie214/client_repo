<?php
   function select_school($table,$objname,$val,$w,$name,$initValue,$disabled,$where,$add) {
      include "conn.e2e.php";
      $w = 80;
      $sql = "SELECT * FROM ".strtolower($table)." ".$where." ORDER BY Name LIMIT 100";
      $result = mysqli_query($conn,$sql) or die(mysqli_error($conn));
      if ($result) {
         echo '<select id="'.$objname.'" name="'.$objname.'" '.$disabled.' class="form-input saveFields-- rptCriteria-- createSelect--" style="width:'.$w.'%">'."\n";
         echo '<option style="padding:2px;" value="0">'.$initValue.'</option>'."\n";
         while($row = mysqli_fetch_assoc($result)) {
            if ($row[$name] != "") {
               echo '<option style="padding:2px;" value="'.$row["RefId"].'"';
               if ($val==$row["RefId"]) echo 'selected'; else true;
               echo '>'.$row[$name].'</option>'."\n";
            }

         }
         echo '</select>'."\n";
         if ($add) {
            echo
            '<span class="newDataLibrary">
               <a href="javascript:void(0);"
                  onclick="popAddDataLibrary(\''.$objname.'\',\''.$table.'\');"
                  id="FM_'.$table.'" class="FMModalNewRecord--" for="'.$objname.'">
                  <i class="fa fa-plus-square" aria-hidden="true"></i>
               </a>
               <a href="javascript:void(0)"
                  onclick="refreshFMDrop(\''.$objname.'\',\''.$table.'\');">
                  <i class="fa fa-refresh" aria-hidden="true"></i>
               </a>
            </span>';
         }
      }
   }

   function doChkPresent($idx,$levelType) {

      if ($levelType >= 1 && $levelType <= 5) {
         echo
         '<span class="tip--"><input type="checkbox"
            class="form-input saveFields-- presentbox--"
            currValue="" id="sint_Present_'.$levelType.'_'.$idx.'"
            style="width:20px;"
            for="sint_DateTo_'.$levelType.'_'.$idx.'"
            yrGrad="sint_YearGraduated_'.$levelType.'_'.$idx.'"
            name="sint_Present_'.$levelType.'_'.$idx.'" disabled
            value="0">
            <span class="tipText">Check this if Present</span>
         </span>';
         /*<label class="btn-cls2-sea" for="sint_Present_'.$levelType.'_'.$idx.'">Present?</label>';*/
      }
      if ($levelType == 6) { // Work Experience
         echo
         '<span class="tip--"><input type="checkbox"
            class="form-input saveFields-- presentbox--"
            currValue="" id="sint_Present_'.$idx.'"
            style="width:20px;"
            for="date_WorkEndDate_'.$idx.'"
            name="sint_Present_'.$idx.'" disabled
            value="0">
            <span class="tipText">Check this if Present</span>
         </span>   ';
         //<label class="btn-cls2-sea" for="sint_Present_'.$idx.'">Present?</label>';
      }
      if ($levelType == 7) { // Voluntary Work
         echo
         '<span class="tip--"><input type="checkbox"
            class="form-input saveFields-- presentbox--"
            currValue="" id="sint_Present_vw_'.$idx.'"
            style="width:20px;"
            for="date_EndDate_'.$idx.'"
            name="sint_Present_vw_'.$idx.'" disabled
            value="0">
            <span class="tipText">Check this if Present</span>
         </span>';
         //<label class="btn-cls2-sea" for="sint_Present_vw_'.$idx.'">Present?</label>';
      }
      if ($levelType == 8) { //
         echo
         '<span class="tip--"><input type="checkbox"
            class="form-input saveFields-- presentbox--"
            currValue="" id="sint_Present_tp_'.$idx.'"
            style="width:20px;"
            for="date_EndDate_'.$idx.'_TrainProg"
            name="sint_Present_tp_'.$idx.'" disabled
            value="0">
            <span class="tipText">Check this if Present</span>
         </span>';
         //<label class="btn-cls2-sea" for="sint_Present_tp_'.$idx.'">Present?</label>';
      }

   }

?>
<div class="mypanel">
   <div class="panel-top">
      <div class="row txt-center">
         <div class="col-sm-3">
            NAME OF SCHOOL (Write in full)
         </div>
         <div class="col-sm-3">
            DEGREE COURSE (Write in full)
         </div>
         <div class="col-sm-2">
            YEAR GRADUATED (if graduated)
         </div>
         <div class="col-sm-4">
            HIGHEST GRADE/<br>LEVEL/<br>UNITS EARNED(if not graduated)
         </div>
      </div>
      <div class="row txt-center">   
         <div class="col-sm-4">
            <div class="row">
               INCLUSIVE DATES OF ATTENDANCE
            </div>
            <div class="row">
               <div class="col-sm-6">
                  FROM
               </div>
               <div class="col-sm-6">
                  TO
               </div>
            </div>
         </div>
         <div class="col-sm-6">
            SCHOLARSHIP/<br>ACADEMIC HONORS RECEIVED
         </div>
      </div>
   </div>
</div>
<div id="educ">
   <div class="mypanel">
      <?php spacer(10); ?>
      <div class="mypanel margin-top">
         <div class="panel-top" for="Elementary">ELEMENTARY</div>
            <div class="panel-mid-litebg" id="Elementary">
               <?php for($j=1;$j<=1;$j++)
               { ?>
               <div id="EntryElementary_<?php echo $j; ?>" class="entry201">
                  <input class="enabler--" type="checkbox" id="Elementary_<?php echo $j; ?>" name="chkElementary_<?php echo $j; ?>"
                  refid="" fldName="sint_SchoolsRefId_1_<?php echo $j; ?>,
                  sint_YearGraduated_1_<?php echo $j; ?>,
                  char_HighestGrade_1_<?php echo $j; ?>,
                  sint_DateFrom_1_<?php echo $j; ?>,
                  sint_DateTo_1_<?php echo $j; ?>,
                  char_Honors_1_<?php echo $j; ?>">
                  <label class="btn-cls2-sea" for="Elementary_<?php echo $j; ?>"><b>EDIT ELEMENTARY #<?php echo $j; ?></b></label>
                  <div class="row margin-top">
                     <div class="col-sm-3 tip--">
                        <span class="tipText">Name of School</span>
                        <input type="hidden" class="form-input saveFields--" value="1" name="sint_LevelType_1_<?php echo $j; ?>" disabled>
                        <input type="hidden" class="form-input saveFields--" value="" name="elemRefId_<?php echo $j; ?>">
                        <input type="text" class="form-input" id="sint_SchoolsRefId_1_<?php echo $j; ?>" style="width: 80%;" placeholder="MANDATORY" readonly>
                        <input type="hidden" class="form-input saveFields-- mandatory" name="sint_SchoolsRefId_1_<?php echo $j; ?>" for="sint_SchoolsRefId_1_<?php echo $j; ?>">
                        <span>
                           <a href="javascript:void(0);"
                              onclick="selectSchool('Offer1','sint_SchoolsRefId_1_<?php echo $j; ?>',1,'Elementary_<?php echo $j; ?>','nav','');"
                              title="School Lookup">
                              <i class="fa fa-search" aria-hidden="true"></i>
                           </a>
                        </span>
                        <span class="newDataLibrary">
                           <a href="javascript:void(0);"
                              onclick="popAddSchools(1,'chkElementary_<?php echo $j; ?>','sint_SchoolsRefId_1_<?php echo $j; ?>');"
                              id="FM_schools" class="FMModalNewRecord--" for="" title="Add New School for Elementary">
                              <i class="fa fa-plus-square" aria-hidden="true"></i>
                           </a>
                        </span>
                     </div>
                     <div class="col-sm-3">
                     </div>
                     <div class="col-sm-2 tip--">
                        <span class="tipText">Year Graduated (if Graduated)</span>
                        <select class="form-input number-- saveFields-- mandatory" hgradeval="" style="width:100px;" name="sint_YearGraduated_1_<?php echo $j; ?>" disabled onChange="validateYGDT(1,<?php echo $j; ?>,1);">
                           <option value="0">YEAR</option>
                           <?php yearPicker(date("Y",time())); ?>
                        </select>
                     </div>
                     <div class="col-sm-4 tip--">
                        <span class="tipText">HIGHEST GRADE/LEVEL/UNITS EARNED (if not Graduated)</span>
                        <input type="text" class="form-input saveFields-- number--" placeholder="Highest Grade" name="char_HighestGrade_1_<?php echo $j; ?>" disabled>
                     </div>
                  </div>
                  <div class="row margin-top">
                     <div class="col-sm-4">
                        <div class="row margin-top">
                           <div class="col-sm-6 tip--">
                              <span class="tipText">Inclusive Date (From)</span>
                              <select class="form-input number-- saveFields-- mandatory" id="DateFrom_1_<?php echo $j; ?>" name="sint_DateFrom_1_<?php echo $j; ?>" disabled style="width:100px;">
                                 <option value="0">YEAR</option>
                                 <?php yearPicker(date("Y",time())); ?>
                              </select>
                              <!-- <input type="text" class="form-input date-- saveFields-- valDate--" placeholder="From"
                              id="DateFrom_1_<?php echo $j; ?>" name="sint_DateFrom_1_<?php echo $j; ?>" disabled>
                              -->
                           </div>
                           <div class="col-sm-6 tip--">
                              <!-- <input type="text" class="form-input date-- saveFields-- valDate--" placeholder="To"
                              id="DateTo_1_<?php echo $j; ?>" name="sint_DateTo_1_<?php echo $j; ?>" disabled>
                              -->
                              <span class="tipText">Inclusive Date (To)</span>
                              <select class="form-input number-- saveFields-- mandatory" id="DateTo_1_<?php echo $j; ?>" name="sint_DateTo_1_<?php echo $j; ?>" disabled style="width:100px;" onChange="validateYGDT(1,<?php echo $j; ?>,2);">
                                 <option value="0">YEAR</option>
                                 <option value="9999">On Going</option>
                                 <?php yearPicker(date("Y",time())); ?>
                              </select>
                           </div>
                        </div>   
                     </div>
                     <div class="col-sm-6 tip--">
                        <span class="tipText">SCHOLARSHIP/ACADEMIC HONORS RECEIVED</span>
                        <input type="text" class="form-input saveFields-- uCase--" placeholder="Honors" name="char_Honors_1_<?php echo $j; ?>" disabled>
                     </div>
                  </div>
               </div>
               <?php
               } ?>
            </div>
            <div class="panel-bottom"></div>
         <!-- <div class="panel-bottom bgSilver"><a href="#" id="addRowElementary" class="addRow">Add Row</a></div> -->
      </div>
      <?php spacer(10); ?>
      <div class="mypanel margin-top">
         <div class="panel-top" for="Secondary">SECONDARY</div>
            <div class="panel-mid-litebg" id="Secondary">
               <?php for($j=1;$j<=1;$j++)
               { ?>
               <div id="EntrySecondary_<?php echo $j; ?>" class="entry201">
                  <input class="enabler--" type="checkbox" id="Secondary_<?php echo $j; ?>" name="chkSecondary_<?php echo $j; ?>" refid="" 
                  fldName="sint_SchoolsRefId_2_<?php echo $j; ?>,
                  sint_YearGraduated_2_<?php echo $j; ?>,
                  char_HighestGrade_2_<?php echo $j; ?>,
                  sint_DateFrom_2_<?php echo $j; ?>,
                  sint_DateTo_2_<?php echo $j; ?>,
                  char_Honors_2_<?php echo $j; ?>">
                  <label class="btn-cls2-sea" for="Secondary_<?php echo $j; ?>"><b>EDIT SECONDARY #<?php echo $j; ?></b></label>
                  <div class="row margin-top">
                     <div class="col-sm-3 tip--">
                        <span class="tipText">Name of School</span>
                        <input type="hidden" class="form-input saveFields--" value="2" name="sint_LevelType_2_<?php echo $j; ?>" disabled>
                        <input type="hidden" class="form-input saveFields--" value="" name="secondaryRefId_<?php echo $j; ?>">
                        <input type="text" title="Name of School" class="form-input" id="sint_SchoolsRefId_2_<?php echo $j; ?>" style="width: 80%;" placeholder="MANDATORY" readonly>
                        <input type="hidden" class="form-input saveFields-- mandatory" name="sint_SchoolsRefId_2_<?php echo $j; ?>">
                        <a href="javascript:void(0);"
                            onclick="selectSchool('Offer2','sint_SchoolsRefId_2_<?php echo $j; ?>',1,'Secondary_<?php echo $j; ?>','nav','');">
                           <i class="fa fa-search" aria-hidden="true"></i>
                        </a>
                        <span class="newDataLibrary">
                           <a href="javascript:void(0);"
                              onclick="popAddSchools(2,'chkSecondary_<?php echo $j; ?>','sint_SchoolsRefId_2_<?php echo $j; ?>');"
                              id="FM_schools" class="FMModalNewRecord--" for="" title="Add New School for Secondary">
                              <i class="fa fa-plus-square" aria-hidden="true"></i>
                           </a>
                        </span>
                     </div>
                     <div class="col-sm-3">
                     </div>
                     <div class="col-sm-2 tip--">
                        <span class="tipText">Year Graduated (if Graduated)</span>
                        <select class="form-input number-- saveFields-- mandatory" hgradeval="" style="width:100px;" name="sint_YearGraduated_2_<?php echo $j; ?>" disabled onChange="validateYGDT(2,<?php echo $j; ?>,1);">
                           <option value="0">YEAR</option>
                           <?php yearPicker(date("Y",time())); ?>
                        </select>
                     </div>
                     <div class="col-sm-4 tip--">
                        <span class="tipText">HIGHEST GRADE/LEVEL/UNITS EARNED (if not graduated)</span>
                        <input type="text" class="form-input saveFields-- number--" placeholder="Highest Grade" name="char_HighestGrade_2_<?php echo $j; ?>" disabled>
                     </div>
                  </div>   
                  <div class="row margin-top">
                     <div class="col-sm-4">
                        <div class="row margin-top">
                           <div class="col-sm-6 tip--">
                              <span class="tipText">Inclusive Date (From)</span>
                              <select class="form-input number-- saveFields-- mandatory" id="DateFrom_2_<?php echo $j; ?>" name="sint_DateFrom_2_<?php echo $j; ?>" disabled style="width:100px;">
                                 <option value="0">YEAR</option>
                                 <?php yearPicker(date("Y",time())); ?>
                              </select>
                           </div>
                           <div class="col-sm-6 tip--">
                              <span class="tipText">Inclusive Date (To)</span>
                              <select title="Inclusive Date (To)" class="form-input number-- saveFields-- mandatory" id="DateTo_2_<?php echo $j; ?>" name="sint_DateTo_2_<?php echo $j; ?>" disabled style="width:100px;" onChange="validateYGDT(2,<?php echo $j; ?>,2);">
                                 <option value="0">YEAR</option>
                                 <option value="9999">On Going</option>
                                 <?php yearPicker(date("Y",time())); ?>
                              </select>
                           </div>
                        </div>
                     </div> 
                     <div class="col-sm-6 tip--">
                        <span class="tipText">SCHOLARSHIP/ACADEMIC HONORS RECEIVED</span>
                        <input type="text" class="form-input saveFields-- uCase--" placeholder="Honors" name="char_Honors_2_<?php echo $j; ?>" disabled>
                     </div>
                  </div>
               </div>
               <?php
               } ?>
            </div>
            <div class="panel-bottom"></div>
         <!-- <div class="panel-bottom bgSilver"><a href="#" id="addRowSecondary" class="addRow">Add Row</a></div> -->
      </div>
   </div>
   <?php spacer(10); ?>
   <div class="mypanel margin-top">
      <div class="panel-top" for="VocCourse">VOCATIONAL - TRADE COURSE</div>
      <div class="panel-mid-litebg" id="VocCourse">

         <?php
            $createEntry = 0;
            if (getvalue("hUserGroup") == "COMPEMP") {
               $rs = SelectEach("employeeseduc","WHERE CompanyRefId = $CompanyId AND BranchRefId = $BranchId AND EmployeesRefId = $EmployeesRefId AND LevelType = 3");
               $j = 0;
               if ($rs) {
                  $createEntry = mysqli_num_rows($rs);
               } else {
                  $createEntry = 1;
               }
            } else {
               $createEntry = 1;
            }

            for($j=1;$j<=$createEntry;$j++) {
         ?>
               <div id="EntryVocCourse_<?php echo $j; ?>" class="entry201 voc_fpreview" idx="<?php echo $j; ?>">
                  <input class="enabler--" type="checkbox" id="VocCourse_3_<?php echo $j; ?>" name="chkVocCourse_3_<?php echo $j; ?>" refid="" rowid="VocationalRefId_3_<?php echo $j; ?>"
                  fldName="sint_SchoolsRefId_3_<?php echo $j; ?>,
                  sint_CourseRefId_3_<?php echo $j; ?>,
                  sint_YearGraduated_3_<?php echo $j; ?>,
                  char_HighestGrade_3_<?php echo $j; ?>,
                  sint_DateTo_3_<?php echo $j; ?>,
                  sint_DateFrom_3_<?php echo $j; ?>,
                  char_Honors_3_<?php echo $j; ?>" unclick="CancelAddRow">
                  <label class="btn-cls2-sea" for="VocCourse_3_<?php echo $j; ?>"><b>EDIT VOCATIONAL COURSE #<?php echo $j; ?></b></label>
                  <div class="row margin-top">
                     <div class="col-sm-3 tip--">
                        <span class="tipText">Name of School</span>                     
                        <input type="hidden" class="form-input saveFields--" value="3" name="sint_LevelType_3_<?php echo $j; ?>">
                        <input type="hidden" class="form-input saveFields--" value="" name="VocationalRefId_3_<?php echo $j; ?>">
                        <input type="text" class="form-input" id="sint_SchoolsRefId_3_<?php echo $j; ?>" style="width: 80%;" placeholder="MANDATORY" readonly>
                        <input type="hidden" class="form-input saveFields-- mandatory" name="sint_SchoolsRefId_3_<?php echo $j; ?>">
                        <a href="javascript:void(0);"
                            onclick="selectSchool('Offer3','sint_SchoolsRefId_3_<?php echo $j; ?>',1,'VocCourse_3_<?php echo $j; ?>','nav','');">
                           <i class="fa fa-search" aria-hidden="true"></i>
                        </a>
                        <span class="newDataLibrary">
                           <a href="javascript:void(0);"
                              onclick="popAddSchools(3,'chkVocCourse_3_<?php echo $j; ?>','sint_SchoolsRefId_3_<?php echo $j; ?>');"
                              id="FM_schools" class="FMModalNewRecord--" for="" title="Add New School for Vocational">
                              <i class="fa fa-plus-square" aria-hidden="true"></i>
                           </a>
                        </span>
                     </div>
                     <div class="col-sm-3 tip--">
                        <span class="tipText">Name of the Course</span>
                        <?php
                           createSelect2("Course",
                                        "sint_CourseRefId_3_".$j,
                                        0,100,"Name","Select Course","disabled","WHERE Level = 3");
                        ?>
                     </div>
                     <div class="col-sm-2 tip--">
                        <span class="tipText">Year Graduated (if Graduated)</span>
                        <select class="form-input number-- saveFields--" hgradeval="" style="width:100px;" name="sint_YearGraduated_3_<?php echo $j; ?>" disabled onChange="validateYGDT(3,<?php echo $j; ?>,1);">
                           <option value="0">YEAR</option>
                           <?php yearPicker(date("Y",time())); ?>
                        </select>
                     </div>
                     <div class="col-sm-4 tip--">
                        <span class="tipText">HIGHEST GRADE/LEVEL/UNITS EARNED (if not graduated)</span> 
                        <input type="text" class="form-input saveFields-- number--" placeholder="Highest Grade"
                        name="char_HighestGrade_3_<?php echo $j; ?>" disabled click="HighestGrade_validation">
                     </div>
                  </div>
                  <div class="row margin-top">
                     <div class="col-sm-4">
                        <div class="row margin-top">   
                           <div class="col-sm-6 tip--">
                              <span class="tipText">Inclusive Date (From)</span>
                              <select class="form-input number-- saveFields-- mandatory" id="DateFrom_3_<?php echo $j; ?>" name="sint_DateFrom_3_<?php echo $j; ?>" disabled style="width:100px;">
                                 <option value="0">YEAR</option>
                                 <?php yearPicker(date("Y",time())); ?>
                              </select>
                           </div>
                           <div class="col-sm-6 tip--">
                              <span class="tipText">Inclusive Date (To)</span>
                              <select class="form-input number-- saveFields-- mandatory" id="DateTo_3_<?php echo $j; ?>" name="sint_DateTo_3_<?php echo $j; ?>" disabled style="width:100px;" onChange="validateYGDT(3,<?php echo $j; ?>,2);">
                                 <option value="0">YEAR</option>
                                 <option value="9999">On Going</option>
                                 <?php yearPicker(date("Y",time())); ?>
                              </select>
                           </div>
                        </div>
                     </div>      
                     <div class="col-sm-6">
                        <input title="SCHOLARSHIP/ACADEMIC HONORS RECEIVED" 
                               type="text" class="form-input saveFields-- uCase--" placeholder="Honors" name="char_Honors_3_<?php echo $j; ?>" disabled>
                     </div>
                  </div>
               </div>
         <?php
            }
         ?>
      </div>
      <div class="panel-bottom bgSilver"><a href="javascript:void(0);" id="addRowVocCourse" class="addRow">Add Row</a></div>
   </div>
   <?php spacer(10); ?>
   <div class="mypanel margin-top">
      <div class="panel-top" for="College">COLLEGE</div>
      <div class="panel-mid-litebg" id="College">
         <?php
            $createEntry = 0;
            if (getvalue("hUserGroup") == "COMPEMP") {
               $rs = SelectEach("employeeseduc","WHERE CompanyRefId = $CompanyId AND BranchRefId = $BranchId AND EmployeesRefId = $EmployeesRefId AND LevelType = 4");
               $j = 0;
               if ($rs) {
                  $createEntry = mysqli_num_rows($rs);
               } else {
                  $createEntry = 1;
               }
            } else {
               $createEntry = 1;
            }


            for($j=1;$j<=$createEntry;$j++)
               {
         ?>
            <div id="EntryCollege_<?php echo $j; ?>" class="entry201">
               <input unclick="CancelAddRow" class="enabler--" type="checkbox" id="College_4_<?php echo $j; ?>" name="chkCollege_4_<?php echo $j; ?>" refid=""
               rowid="collegeRefId_<?php echo $j; ?>" 
               fldName="sint_SchoolsRefId_4_<?php echo $j; ?>,
               sint_CourseRefId_4_<?php echo $j; ?>,
               sint_YearGraduated_4_<?php echo $j; ?>,
               char_HighestGrade_4_<?php echo $j; ?>,
               sint_DateTo_4_<?php echo $j; ?>,
               sint_DateFrom_4_<?php echo $j; ?>,
               char_Honors_4_<?php echo $j; ?>">
               <label class="btn-cls2-sea" for="College_<?php echo $j; ?>"><b>EDIT COLLEGE #<?php echo $j; ?></b></label>
               <div class="row">
                  <div class="col-sm-3 tip--">
                     <span class="tipText">Name of School</span>
                     <input type="hidden" class="form-input saveFields--" value="4" name="sint_LevelType_4_<?php echo $j; ?>" disabled>
                     <input type="hidden" class="form-input saveFields--" value="" name="collegeRefId_<?php echo $j; ?>">
                     <input type="text" title="School Lookup" class="form-input" id="sint_SchoolsRefId_4_<?php echo $j; ?>" style="width: 80%;" placeholder="MANDATORY" readonly>
                        <input type="hidden" class="form-input saveFields--" name="sint_SchoolsRefId_4_<?php echo $j; ?>">
                        <a href="javascript:void(0);"
                            onclick="selectSchool('Offer4','sint_SchoolsRefId_4_<?php echo $j; ?>',1,'College_4_<?php echo $j; ?>','nav','');">
                           <i class="fa fa-search" aria-hidden="true"></i>
                        </a>
                        <span class="newDataLibrary">
                           <a href="javascript:void(0);"
                              onclick="popAddSchools(4,'chkCollege_4_<?php echo $j; ?>','sint_SchoolsRefId_4_<?php echo $j; ?>');"
                              id="FM_schools" class="FMModalNewRecord--" for="" title="Add New School for College">
                              <i class="fa fa-plus-square" aria-hidden="true"></i>
                           </a>
                        </span>
                  </div>
                  <div class="col-sm-3 tip--">
                     <span class="tipText">Course Name</span>
                     <?php
                        createSelect2("Course",
                                     "sint_CourseRefId_4_".$j,
                                     0,100,"Name","Select Course","disabled","WHERE Level = 4");
                     ?>
                  </div>
                  <div class="col-sm-2 tip--">
                        <span class="tipText">Year Graduated (if Graduated)</span>  
                        <select class="form-input number-- saveFields--" hgradeval="" style="width:100px;" name="sint_YearGraduated_4_<?php echo $j; ?>" disabled onChange="validateYGDT(4,<?php echo $j; ?>,1);">
                           <option value="0">YEAR</option>
                           <?php yearPicker(date("Y",time())); ?>
                        </select>
                  </div>
                  <div class="col-sm-4 tip--">
                     <span class="tipText">HIGHEST GRADE/LEVEL/UNITS EARNED (if not graduated)</span>
                     <input type="text" class="form-input saveFields-- number--"
                     placeholder="Highest Grade" name="char_HighestGrade_4_<?php echo $j; ?>" disabled click="HighestGrade_validation">
                  </div>
               </div>
               <div class="row margin-top">
                     <div class="col-sm-4">
                        <div class="row margin-top">
                           <div class="col-sm-6 tip--">
                              <span class="tipText">Inclusive Date (From)</span> 
                              <select class="form-input number-- saveFields-- mandatory" id="DateFrom_4_<?php echo $j; ?>" name="sint_DateFrom_4_<?php echo $j; ?>" disabled style="width:100px;">
                                 <option value="0">YEAR</option>
                                 <?php yearPicker(date("Y",time())); ?>
                              </select>
                           </div>
                           <div class="col-sm-6">
                              <span class="tipText">Inclusive Date (To)</span> 
                              <select class="form-input number-- saveFields-- mandatory" id="DateTo_4_<?php echo $j; ?>" name="sint_DateTo_4_<?php echo $j; ?>" disabled style="width:100px;" onChange="validateYGDT(4,<?php echo $j; ?>,2);">
                                 <option value="0">YEAR</option>
                                 <option value="9999">On Going</option>
                                 <?php yearPicker(date("Y",time())); ?>
                              </select>
                           </div>
                        </div>
                     </div>      
                     <div class="col-sm-6 tip--">
                        <span class="tipText">SCHOLARSHIP/ACADEMIC HONORS RECEIVED</span>
                        <input type="text" class="form-input saveFields-- uCase--" placeholder="Honors" name="char_Honors_4_<?php echo $j; ?>" disabled>
                     </div>
               </div>
            </div>
         <?php
         } ?>
      </div>
      <div class="panel-bottom bgSilver"><a href="javascript:void(0);" id="addRowCollege" class="addRow">Add Row</a></div>
   </div>
   <?php spacer(10); ?>
   <div class="mypanel margin-top">
      <div class="panel-top" for="GradStudies">GRADUATE STUDIES</div>
      <div class="panel-mid-litebg" id="GradStudies">
         <?php

            $createEntry = 0;
            if (getvalue("hUserGroup") == "COMPEMP") {
               $rs = SelectEach("employeeseduc","WHERE CompanyRefId = $CompanyId AND BranchRefId = $BranchId AND EmployeesRefId = $EmployeesRefId AND LevelType = 5");
               $j = 0;
               if ($rs) {
                  $createEntry = mysqli_num_rows($rs);
               } else {
                  $createEntry = 1;
               }
            } else {
               $createEntry = 1;
            }

            for($j=1;$j<=$createEntry;$j++) {
         ?>
               <div id="EntryGradStudies_<?php echo $j; ?>" class="entry201 grad_fpreview" idx="<?php echo $j; ?>">
                  <input unclick="CancelAddRow" class="enabler--" type="checkbox" id="GradStudies_5_<?php echo $j; ?>" name="chkGradStudies_5_<?php echo $j; ?>" refid="" rowid="GradStudiesRefId_5_<?php echo $j; ?>" 
                  fldName="sint_SchoolsRefId_5_<?php echo $j; ?>,
                  sint_CourseRefId_5_<?php echo $j; ?>,
                  sint_YearGraduated_5_<?php echo $j; ?>,
                  char_HighestGrade_5_<?php echo $j; ?>,
                  sint_DateTo_5_<?php echo $j; ?>,
                  sint_DateFrom_5_<?php echo $j; ?>,
                  char_Honors_5_<?php echo $j; ?>">
                  <label class="btn-cls2-sea" for="GradStudies_5_<?php echo $j; ?>"><b>EDIT GRAD STUDIES #<?php echo $j; ?></b></label>
                  <div class="row margin-top">
                     <div class="col-sm-3 tip--">
                        <span class="tipText">Name of School</span>
                        <input type="hidden" class="form-input saveFields--" value="5" name="sint_LevelType_5_<?php echo $j; ?>">
                        <input type="hidden" class="form-input saveFields--" value="" name="GradStudiesRefId_5_<?php echo $j; ?>">
                        <input type="text" class="form-input" id="sint_SchoolsRefId_5_<?php echo $j; ?>" style="width: 80%;" placeholder="MANDATORY" readonly>
                        <input type="hidden" class="form-input saveFields-- mandatory" name="sint_SchoolsRefId_5_<?php echo $j; ?>">
                        <a href="javascript:void(0);"
                            onclick="selectSchool('Offer5','sint_SchoolsRefId_5_<?php echo $j; ?>',1,'GradStudies_5_<?php echo $j; ?>','nav','');">
                           <i class="fa fa-search" aria-hidden="true"></i>
                        </a>
                        <span class="newDataLibrary">
                           <a href="javascript:void(0);"
                              onclick="popAddSchools(5,'chkGradStudies_5_<?php echo $j; ?>','sint_SchoolsRefId_5_<?php echo $j; ?>');"
                              id="FM_schools" class="FMModalNewRecord--" for="" title="Add New School for Grad. Studies">
                              <i class="fa fa-plus-square" aria-hidden="true"></i>
                           </a>
                        </span>
                     </div>
                     <div class="col-sm-3 tip--">
                        <span class="tipText">Name of the Course</span>
                        <?php
                           createSelect2("Course",
                                        "sint_CourseRefId_5_".$j,
                                        0,100,"Name","Select Course","disabled","WHERE Level = 5");
                        ?>
                     </div>
                     <div class="col-sm-2 tip--">
                        <span class="tipText">Year Graduated (if Graduated)</span>
                        <select class="form-input number-- saveFields--" hgradeval="" style="width:100px;" name="sint_YearGraduated_5_<?php echo $j; ?>" disabled onChange="validateYGDT(5,<?php echo $j; ?>,1);">
                           <option value="0">YEAR</option>
                           <?php yearPicker(date("Y",time())); ?>
                        </select>
                     </div>
                     <div class="col-sm-4 tip--">
                        <span class="tipText">HIGHEST GRADE/LEVEL/UNITS EARNED (if not graduated)</span>
                        <input type="text" class="form-input saveFields-- number--"
                        placeholder="Highest Grade" name="char_HighestGrade_5_<?php echo $j; ?>" disabled click="HighestGrade_validation">
                     </div>
                  </div>   
                  <div class="row margin-top">
                     <div class="col-sm-4">
                        <div class="row margin-top">
                           <div class="col-sm-6 tip--">
                              <span class="tipText">Inclusive Date (From)</span>
                              <select class="form-input number-- saveFields-- mandatory" id="DateFrom_5_<?php echo $j; ?>" name="sint_DateFrom_5_<?php echo $j; ?>" disabled style="width:100px;">
                                 <option value="0">YEAR</option>
                                 <?php yearPicker(date("Y",time())); ?>
                              </select>
                           </div>
                           <div class="col-sm-6 tip--">
                              <span class="tipText">Inclusive Date (To)</span>
                              <select title="Inclusive Date (To)" class="form-input number-- saveFields-- mandatory" id="DateTo_5_<?php echo $j; ?>" name="sint_DateTo_5_<?php echo $j; ?>" disabled style="width:100px;" onChange="validateYGDT(5,<?php echo $j; ?>,2);">
                                 <option value="0">YEAR</option>
                                 <option value="9999">On Going</option>
                                 <?php yearPicker(date("Y",time())); ?>
                              </select>
                           </div>
                        </div>
                     </div>      
                     <div class="col-sm-6 tip--">
                        <span class="tipText">SCHOLARSHIP/ACADEMIC HONORS RECEIVED</span>
                        <input type="text" class="form-input saveFields-- uCase--"
                        placeholder="Honors" name="char_Honors_5_<?php echo $j; ?>" disabled>
                     </div>
                  </div>
               </div>
         <?php
            }
         ?>
      </div>
      <div class="panel-bottom bgSilver"><a href="javascript:void(0);" id="addRowGradStudies" class="addRow">Add Row</a></div>
   </div>
</div>
<script language="javascript">
   $("[name*='sint_SchoolsRefId_'], [name*='sint_CourseRefId_']").addClass("mandatory");
   $("#educ .saveFields-- ").attr("tabname","Education");
   function selectSchool(offer,objname,batch,objID,mode,srchString) {
      if ($("#"+objID).is(":checked")) {
         $.get("getSchool.e2e.php",{
            offer:offer,
            objname:objname,
            drpSchoolpage:$("[name='drpSchoolpage']").val(),
            chkEnablerID:objID,
            mode:mode,
            srch:srchString,
            batch:batch
         },
         function(data,status) {
            if (status == "success") {
               try {
                  $("#selectSchool").modal();
                  $("#tbl_school").html(data);
               } catch (e) {
                  if (e instanceof SyntaxError) {
                     alert(e.message);
                  }
               }
            }
         });
      }
   }
   function insertSchool(refid,objname,name){
      $("#"+objname).val(name);
      $("[name='"+objname+"']").val(refid);
      $("#selectSchool").modal("hide");
   }
   function validateYGDT(lvl,idx,unique){
      
         /*if (unique == 1) {
            $("[name='sint_DateTo_"+lvl+"_"+idx+"']").val($("[name='sint_YearGraduated_"+lvl+"_"+idx+"']").val());      
         } else {
            if ($("[name='sint_DateTo_"+lvl+"_"+idx+"']").val() == 9999) {
               $("[name='sint_YearGraduated_"+lvl+"_"+idx+"']").val("0");
            } else {
               $("[name='sint_YearGraduated_"+lvl+"_"+idx+"']").val($("[name='sint_DateTo_"+lvl+"_"+idx+"']").val());
            }
         }   */
      
      
      
   }
</script>


