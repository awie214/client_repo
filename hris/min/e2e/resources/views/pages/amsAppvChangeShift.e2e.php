<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script language="JavaScript" src="<?php echo jsCtrl("ctrl_approval") ?>"></script>
   </head>
   <body onload = "indicateActiveModules();">
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"ams"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php
               doTitleBar("For Aprroval > Change Shift");
               $EmpRefId = getvalue("txtRefId");
               $attr = ["empRefId"=>getvalue("txtRefId"),
                        "empLName"=>getvalue("txtLName"),
                        "empFName"=>getvalue("txtFName"),
                        "empMName"=>getvalue("txtMidName")];
               $EmpRefId = EmployeesSearch($attr);
               bar();
               /*$sql = "SELECT *, changeshift_request.RefId as asRefId FROM employees
                       INNER JOIN changeshift_request
                       WHERE employees.CompanyRefId = ".$CompanyId."
                         AND employees.BranchRefId = ".$BranchId;

               if (getvalue("txtLName") != "") {
                  $sql .= " AND employees.LastName LIKE '".getvalue("txtLName")."%'";
               }
               if (getvalue("txtFName") != "") {
                  $sql .= " AND employees.FirstName LIKE '".getvalue("txtFName")."%'";
               }
               if (getvalue("txtMidName") != "") {
                  $sql .= " AND employees.MiddleName LIKE '".getvalue("txtMidName")."%'";
               }
               if (getvalue("txtRefId") != "") {
                  $sql .= " AND changeshift_request.EmployeesRefId = '".getvalue("txtRefId")."'";
               } else {
                  $sql .= " AND employees.RefId = changeshift_request.EmployeesRefId";
               }
               $sql .= " and changeshift_request.Status IS NULL ORDER BY StartDate LIMIT 20";*/
               $sql = "SELECT *,changeshift_request.RefId as asRefId FROM changeshift_request
               INNER JOIN employees 
               ON changeshift_request.CompanyRefId = employees.CompanyRefId
               AND changeshift_request.BranchRefId = employees.BranchRefId
               AND changeshift_request.EmployeesRefId = employees.RefId";

               if (getvalue("txtLName") != "") {
                  $sql .= " AND employees.LastName LIKE '".getvalue("txtLName")."%'";
               }
               if (getvalue("txtFName") != "") {
                  $sql .= " AND employees.FirstName LIKE '".getvalue("txtFName")."%'";
               }
               if (getvalue("txtMidName") != "") {
                  $sql .= " AND employees.MiddleName LIKE '".getvalue("txtMidName")."%'";
               }
               if (getvalue("txtRefId") != "") {
                  $sql .= " AND changeshift_request.EmployeesRefId = '".getvalue("txtRefId")."'";
               } 
               $sql .= " AND changeshift_request.Status IS NULL ORDER BY StartDate LIMIT 20";
               $rs = mysqli_query($conn,$sql) or die(mysqli_error($conn));
            ?>
            <div class="row">
               <div class="col-xs-1"></div>
               <div class="col-xs-10 padd5">
                  <?php
                     if ($rs) {
                        $recordNum = mysqli_num_rows($rs);
                        if ($recordNum) {
                           while ($row = mysqli_fetch_array($rs)) {
                              $refid = $row["asRefId"];
                              $rsEmp = FFirstRefId("employees",$row["EmployeesRefId"],"*");
                              $where = "WHERE CompanyRefId = $CompanyId AND BranchRefId = $BranchId AND EmployeesRefId = ".$rsEmp["RefId"];
                              $empinformation = FindFirst('empinformation',$where,"*");
                              if ($empinformation) {
                                 $info = array_merge($rsEmp,$empinformation);
                  ?>
                              <div class="mypanel pull-left padd5" style="margin:5px;width:45%;">
                                 <div class="panel-top">REF. ID:&nbsp;<?php echo $refid; ?></div>
                                 <div class="panel-mid">
                                    <div class="row txt-right" style="margin-right:10px;">
                                       <label>DATE FILE:</label><span style="margin-left:15px;"><?php echo $row["FiledDate"];?></span>
                                    </div>
                                    <?php
                                       
                                       echo '
                                       <div class="row margin-top padd5">
                                          <div class="row margin-top">
                                             <div class="col-sm-4 txt-center">
                                                <div class="border" style="height:1.5in;width:1.3in;">
                                                   <img src="'.img("EmployeesPhoto/".$rsEmp['PicFilename']).'" style="width:100%;height:100%;">
                                                </div>
                                             </div>
                                             <div class="col-sm-8 txt-center padd5">';
                                                $templ->btn_apprvReject(2,$refid);
                                             echo    
                                             '</div>
                                          </div>
                                          <div class="row margin-top">   
                                             <div class="col-sm-12">';
                                                $templ->doEmployeeInfo($info);
                                       echo       
                                             '</div>   
                                          </div>   
                                       </div>';
                                       bar();  
                                    ?>
                                    <div class="row">
                                       <div class="col-xs-6">
                                          <label>START DATE:</label><span style="margin-left:15px;"><?php echo $row["StartDate"];?></span>
                                       </div>
                                       <div class="col-xs-6">
                                          <label>END DATE:</label><span style="margin-left:15px;"><?php echo $row["EndDate"];?></span>
                                       </div>
                                    </div>
                                    <div class="row margin-top">
                                       <div class="col-xs-6">
                                          <label>WORK SCHEDULE</label>
                                          <a href="#" class="workSched" style="margin-left:15px;">
                                             <?php echo getRecord("workschedule",$row["WorkScheduleRefId"],"Name");?>
                                          </a>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="panel-bottom"></div>
                              </div>
                  <?php
                              }
                           }
                        }
                        else {
                           alert("Information","No Record For Approval");
                        }
                     } else {
                        alert("Information","RS ERROR");
                     }
                  ?>
               </div>
               <div class="col-xs-1"></div>
            </div>
            <div class="modal fade border0" id="workSched_modal" role="dialog">
               <div class="modal-dialog border0" style="padding:10px;width:40%;">
                  <div class="mypanel border0" style="height:100%;">
                     <div class="panel-top bgTree">
                        WORK SCHEDULE
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                     </div>
                     <div class="panel-mid" id="workSched_detail">

                     </div>
                  </div>
               </div>
            </div>
            <?php
               footer();
               $table = "changeshift_request";
               include "varHidden.e2e.php";
               modalReject();
            ?>
         </div>
      </form>
   </body>
</html>