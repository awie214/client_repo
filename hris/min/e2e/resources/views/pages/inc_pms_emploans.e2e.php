<script>
   $(document).ready(function () {
      remIconDL()
   });
</script>
<div class="mypanel" id="emploans">
   <div class="row margin-top" style="height:200px;overflow:auto; padding: 5px;">
      <table border="1" width="100%">
         <tr  class="txt-center" style="padding:5px;">
            <td><label>Deduction Name</label></td>
            <td><label>Amount</label></td>
            <td><label>Date Start</label></td>
            <td><label>Date End</label></td>
            <td><label>Terminated</label></td>
         </tr>
      <?php
         $rs = SelectEach("employeeschild","");
         if ($rs) {
            $recordNum = mysqli_num_rows($rs);
            while ($row = mysqli_fetch_array($rs)) {
      ?>
         <tr>
            <td><?php echo $row["FullName"]?></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
         </tr>
      <?php 
            }
         }
      ?>
      </table>
   </div>
   <?php spacer(20);?>
   <div class="mypanel">
      <div class="panel-top">Detail</div>
      <div class="panel-mid">
         <div class="row">
            <div class="col-xs-12">
               <input type="checkbox" class="enabler--" name="chkEnabledloans" id="chkEnabledloans"  for="emploans">
               <label id="enable" for="chkEnabledloans">Enable Fields</label>
            </div> 
         </div>
         <?php bar(); ?>
         <div class="row margin-top">
            <div class="col-xs-12">
               <div class="row margin-top">
                  <div class="col-xs-2 label" class="label">
                     <label>Loan Name:</label>
                  </div>
                  <div class="col-xs-4">
                     <input type="text" name="LoanName" class="saveFields-- form-input">
                  </div>
                  <div class="col-xs-2 label" class="label">
                     <label>Pay Rate:</label>
                  </div>
                  <div class="col-xs-2 label">
                     <select class="saveFields-- form-input" name="LIPayRate">
                        <option></option>
                        <option>Monthly Rate</option>
                        <option>Weekly Rate</option>
                        <option>Daily Rate</option>
                        <option>Annual Rate</option>
                     </select>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-2 label">
                     <label>Date Granted:</label class="label">
                  </div>
                  <div class="col-xs-2 label">
                     <input type="text" name="DateGranted" class="saveFields-- form-input date--">
                  </div>
                  <div class="col-xs-2"></div>
                  <div class="col-xs-2 label" class="label">
                     <label>Pay Period:</label>
                  </div>
                  <div class="col-xs-2 label">
                     <select class="saveFields-- form-input" name="LIPayPeriod">
                        <option></option>
                        <option>Both</option>
                        <option>Monthly</option>
                        <option>Weekly</option>
                        <option>First Half</option>
                        <option>Second Half</option>
                     </select>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-2 label" class="label">
                     <label>Total Loan Amount:</label>
                  </div>
                  <div class="col-xs-2 label">
                     <input type="text" name="TotalLoanAmount" class="saveFields-- form-input">
                  </div>
                  <div class="col-xs-2"></div>
                  <div class="col-xs-2 label" class="label">
                     <label>Date Start:</label>
                  </div>
                  <div class="col-xs-2">
                     <input type="text" name="LIDateStart" class="saveFields-- form-input date--">
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-2" class="label">
                     <label>Total Loan Balance:</label>
                  </div>
                  <div class="col-xs-2">
                     <input type="text" name="TotalLoanBalance" class="saveFields-- form-input">
                  </div>
                  <div class="col-xs-2"></div>
                  <div class="col-xs-2 label" class="label">
                     <label>Date End:</label>
                  </div>
                  <div class="col-xs-2">
                     <input type="text" name="DataEnd" class="saveFields-- form-input date--">
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-2 label" class="label">
                     <label>Amortization:</label>
                  </div>
                  <div class="col-xs-2">
                     <input type="text" name="Amortization" class="saveFields-- form-input">
                  </div>
                  <div class="col-xs-2 txt-center">
                     <input type="checkbox" name="LIterminate">&nbsp;<label>Terminate</label>
                  </div>
                  <div class="col-xs-2 label" class="label">
                     <label>Date Terminated:</label>
                  </div>
                  <div class="col-xs-2">
                     <input type="text" name="LIDateTerminated" class="saveFields-- form-input date--">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="panel-bottom"></div>
   </div>
</div>