<?php
   $moduleContent = file_get_contents(json."EmpMovement.json");
   $module        = json_decode($moduleContent, true);

?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script type="text/javascript" src="<?php echo jsCtrl("ctrl_EmpMovement"); ?>"></script>
      <script type="text/javascript">
         $(document).ready(function () {
            <?php if (isset($_GET["savesuccess"])) {
               if (getvalue("savesuccess") == "yes") {
            ?>
               $.notify("Record Successfully Saved.","info");
            <?php
               }
            }
            ?>
            <?php if (isset($_GET["updatesuccess"])) {
               if (getvalue("updatesuccess") == "yes") {
            ?>
               $.notify("Record Successfully Updated.","info");
            <?php
               }
            }
            ?>
            
         });
         function getEmpID() {
            $.get("trn.e2e.php",
            {
               fn:"getEmpID"
            },
            function(data,status){
               if (status == 'success') {
                  eval(data);
               }
            });
         }   
      </script>
      <style type="text/css">
      </style>
   </head>
   <body onload = "indicateActiveModules();">
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"pis"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar($module[$module["Module"]]["Title"]); ?>
            <div class="container-fluid margin-top">
               <div class="row">
                  <div class="col-xs-12" id="div_CONTENT">
                        <div class="row" id="divList">
                           <div class="col-xs-3">
                              <?php employeeSelector(); ?>
                           </div>
                           <div class="col-xs-9">
                              <div class="mypanel">
                                 <div class="panel-top">
                                    EMPLOYEES MOVEMENT RECORD <br>
                                    <span id="selectedEmployees">&nbsp;</span>
                                 </div>
                                 <div class="panel-mid-litebg">
                                    <div class="row">
                                       <div class="col-xs-12">
                                          <div style="overflow:auto;max-height:500px;">
                                             <div id="spGridTable">
                                                <?php
                                                /*
                                                   $dbField = $module["Module.PIS.EmployeesMovement"]["dbField"];
                                                   $table = $module["Module.PIS.EmployeesMovement"]["dbTable"];
                                                   $gridTableHdr_arr = [];
                                                   $gridTableFld_arr = [];
                                                   $j = 0;
                                                   foreach ($dbField as $key => $value) {
                                                      if ($value[5]) {
                                                         $j++;
                                                         $gridTableFld_arr[$j] = $value[0];
                                                         $gridTableHdr_arr[$j] = $value[4];
                                                      }
                                                   }
                                                   $sql = "SELECT * FROM `$table` WHERE RefId = 0 ORDER BY RefId Desc LIMIT 100";
                                                   $_SESSION["module_sql"] = $sql;
                                                   $_SESSION["module_gridTable_ID"] = "gridTable";
                                                   echo "test";
                                                   doGridTable($table,
                                                               $gridTableHdr_arr,
                                                               $gridTableFld_arr,
                                                               $sql,
                                                               $module["Module.PIS.EmployeesMovement"]["Action"],
                                                               $_SESSION["module_gridTable_ID"]);
                                                               */
                                                ?>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="panel-bottom">
                                    <span id="spADDPRINT">
                                       <button type="button"
                                            class="btn-cls4-sea trnbtn"
                                            id="btnADD" name="btnADD">
                                            <i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;
                                             ADD
                                       </button>
                                       <button type="button"
                                            class="btn-cls4-lemon trnbtn"
                                            id="btnPRINT" name="btnPRINT">
                                            <i class="fa fa-print" aria-hidden="true"></i>&nbsp;
                                             PREVIEW SERVICE RECORD
                                       </button>
                                    </span>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="row" id="divView">
                           <div class="col-xs-12">
                              <div class="row" id="idSAVECANCEL">
                                 <div class="col-xs-12">
                                    <button type="button" value="SAVE"
                                         class="btn-cls4-sea trnbtn"
                                         id="btnSAVE" name="btnSAVE">
                                         <i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;
                                         SAVE
                                    </button>
                                    <button type="button"
                                         class="btn-cls4-red trnbtn"
                                         id="btnCANCEL" name="btnCANCEL">
                                         <i class="fa fa-undo" aria-hidden="true"></i>
                                         CLOSE
                                    </button>
                                 </div>
                              </div>
                              <?php bar(); ?>
                              <div class="mypanel margin-top">
                                 <div class="panel-top">
                                    <div class="row">
                                       <div class="col-xs-2 txt-right">
                                          <span><b>EMPLOYEE NAME:</b></span>
                                       </div>
                                       <div class="col-xs-4">
                                          <input type="text" class="form-disp" name="txtEmpName" readonly/>
                                       </div>
                                       <div class="col-xs-2 txt-right">
                                          <span><b>EMPLOYEE ID:</b></span>
                                       </div>
                                       <div class="col-xs-2">
                                          <input type="text" class="form-disp" name="txtEmpId" readonly/>
                                       </div>
                                       <div class="col-xs-2 txt-right" id="EmpMovementRefId"></div>
                                    </div>
                                 </div>
                                 <div class="panel-mid-litebg" id="EntryEmpMovement">
                                    <input type="hidden" class="saveFields--" name="sint_EmployeesRefId">
                                    <div class="row">
                                       <div class="col-xs-6">
                                          <div class="" style="padding:10px;">
                                             <!--<ul class="nav nav-pills">
                                                <li class="active"><a data-toggle="pill" href="#movetaken">Movement Taken</a></li>
                                                <li><a data-toggle="pill" href="#formerincumbent">Former Incumbent</a></li>
                                             </ul>-->
                                             <div class="tab-content">
                                                <div id="movetaken" class="tab-pane fade in active">
                                                   <h4>Movement Taken</h4>
                                                   <?php bar(); ?>
                                                   <div class="row">
                                                      <div class="col-xs-12">
                                                         <div class="row margin-top">
                                                            <div class="col-xs-4 mylabel">
                                                               Appointment Type
                                                            </div>
                                                            <div class="col-xs-7">
                                                               <?php createSelectApptType("sint_ApptType","",""); ?>
                                                            </div>
                                                         </div>
                                                         <div class="row margin-top">
                                                            <div class="col-xs-4 mylabel">
                                                               Appointment Status:
                                                            </div>
                                                            <div class="col-xs-7">
                                                               <?php
                                                                  createSelect("ApptStatus",
                                                                               "sint_ApptStatusRefId",
                                                                               "",100,"Name","--Select Appt. Status--","");
                                                               ?>
                                                            </div>
                                                         </div>
                                                         <div class="row margin-top">
                                                            <div class="col-xs-4 mylabel">
                                                               Effectivity Date:
                                                            </div>
                                                            <div class="col-xs-7">
                                                               <input type="text" class="form-input saveFields-- date--" name="date_EffectivityDate">
                                                            </div>
                                                         </div>
                                                         <div class="row margin-top">
                                                            <div class="col-xs-4 mylabel">
                                                               End Date:
                                                            </div>
                                                            <div class="col-xs-5">
                                                               <input type="text" class="form-input saveFields-- date--" name="date_ExpiryDate">
                                                               <label class="mylabel" for="sint_Present">PRESENT&nbsp;&nbsp;
                                                                  <input type="checkbox" 
                                                                         class="saveFields--"
                                                                         name="sint_Present" 
                                                                         id="sint_Present" 
                                                                         value="0" 
                                                                         >
                                                               </label>
                                                            </div>
                                                         </div>
                                                         <div class="row margin-top">
                                                            <div class="col-xs-4 mylabel">
                                                               Details/Remarks:
                                                            </div>
                                                            <div class="col-xs-7">
                                                               <textarea class="form-input saveFields--" rows="5" name="char_Remarks"
                                                               placeholder="Remarks"></textarea>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="row">
                                                   <?php 
                                                      spacer(30);
                                                   ?>
                                                   <!-- <h4>Former Incumbent</h4>
                                                   <?php bar(); ?>
                                                   <div class="row">
                                                      <div class="col-xs-12">
                                                         <button type="button" name="btnEmpLkUp" id="btnEmpLkUp" class="btn btn-cls4-sea btn-xs">
                                                               Employees Look Up
                                                         </button>
                                                         <?php spacer(10); ?>
                                                         <div>
                                                            Employee Name: <br>
                                                            <input type="hidden" class="form-input saveFields--" name="sint_IncumbentEmployeesRefId">
                                                            <input type="text" class="form-input" name="txtFullName">
                                                         </div>
                                                         <?php spacer(20) ?>
                                                         <div>
                                                            Reason of Separation:<br>
                                                            <textarea class="form-input saveFields--" name="char_IncumbentSeparationReason" row=20 col=5 ></textarea>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   -->
                                                   <div class="row margin-top">
                                                      <div class="col-xs-4 mylabel">NEW EMPLOYEES ID:</div>
                                                      <div class="col-xs-4">
                                                         <input type="text" class="form-input saveFields--" name="char_NewEmployeesId" id="char_NewEmployeesId">
                                                      </div>
                                                      <?php
                                                         if (getvalue("hCompanyID") == "2") {
                                                            echo '
                                                               <button type="button" class="btn-cls4-tree" id="generate_empid" onclick="getEmpID();">
                                                                  GENERATE EMP ID
                                                               </button>
                                                            ';
                                                         }
                                                      ?>
                                                   </div>
                                                   <div class="row margin-top">
                                                      <div class="col-xs-4 mylabel">AGENCY:</div>
                                                      <div class="col-xs-6">
                                                         <?php
                                                            $table = "Agency";
                                                            createSelect($table,
                                                                        "sint_".$table."RefId",
                                                                        0,100,"Name","Select ".$table,"");
                                                         ?>
                                                      </div>
                                                   </div>
                                                   <div class="row margin-top">
                                                      <div class="col-xs-4 mylabel">
                                                         Position Item No.:
                                                      </div>
                                                      <div class="col-xs-6">
                                                         <?php
                                                            $table = "PositionItem";
                                                            createSelect($table,
                                                                        "sint_".$table."RefId",
                                                                        0,100,"Name","Select Position Item","");
                                                         ?>
                                                      </div>
                                                   </div>
                                                   <div class="row margin-top">
                                                      <div class="col-xs-4 mylabel">
                                                         Position:
                                                      </div>
                                                      <div class="col-xs-6">
                                                         <?php
                                                            $table = "Position";
                                                            createSelect($table,
                                                                        "sint_".$table."RefId",
                                                                        0,100,"Name","Select ".$table,"");
                                                         ?>
                                                      </div>
                                                   </div>
                                                </div>
                                                <!-- <div id="formerincumbent" class="tab-pane fade">
                                                   
                                                </div> -->
                                             </div>
                                          </div>
                                       </div>
                                       <div class="col-xs-6">
                                          <div>
                                             <h4>&nbsp;</h4>
                                             <div class="row margin-top20">
                                                <div class="col-xs-4 mylabel">
                                                   Department:
                                                </div>
                                                <div class="col-xs-6">
                                                   <?php
                                                      $table = "Department";
                                                      createSelect($table,
                                                                  "sint_".$table."RefId",
                                                                  0,100,"Name","Select ".$table,"");
                                                   ?>
                                                </div>
                                             </div>
                                             <div class="row margin-top">
                                                <div class="col-xs-4 mylabel">
                                                   Office(CO/RO):
                                                </div>
                                                <div class="col-xs-6">
                                                   <?php
                                                      $table = "Office";
                                                      createSelect($table,
                                                                  "sint_".$table."RefId",
                                                                  0,100,"Name","Select ".$table,"");
                                                   ?>
                                                </div>
                                             </div>
                                             <div class="row margin-top">
                                                <div class="col-xs-4 mylabel">
                                                   Division:
                                                </div>
                                                <div class="col-xs-6">
                                                   <?php
                                                      $table = "Division";
                                                      createSelect($table,
                                                                  "sint_".$table."RefId",
                                                                  0,100,"Name","Select ".$table,"");
                                                   ?>
                                                </div>
                                             </div>
                                             <div class="row margin-top">
                                                <div class="col-xs-4 mylabel">
                                                   Employment Status:
                                                </div>
                                                <div class="col-xs-6">
                                                   <?php
                                                      $table = "EmpStatus";
                                                      createSelect($table,
                                                                  "sint_".$table."RefId",
                                                                  0,100,"Name","Select Emp. Status","");
                                                   ?>
                                                </div>
                                             </div>

                                             <div class="row margin-top">
                                                <div class="col-xs-4 mylabel">
                                                   Designation:
                                                </div>
                                                <div class="col-xs-6">
                                                   <?php
                                                      $table = "Designation";
                                                      createSelect($table,
                                                                  "sint_".$table."RefId",
                                                                  "",100,"Name","Select ".$table,"");
                                                   ?>
                                                </div>
                                             </div>
                                             <div class="row margin-top">
                                                <?php
                                                   if (getvalue("hCompanyID") != "2") {
                                                ?>
                                                <div class="col-xs-4 mylabel">
                                                   Salary Grade:
                                                </div>
                                                <div class="col-xs-6">
                                                   <?php
                                                      $table = "SalaryGrade";
                                                      createSelect($table,
                                                                  "sint_".$table."RefId",
                                                                  0,100,"Name","Select Salary Grade","");
                                                   ?>
                                                </div>
                                                <?php
                                                   } else {
                                                ?>
                                                <div class="col-xs-4 mylabel">
                                                   Job Grade:
                                                </div>
                                                <div class="col-xs-6">
                                                   <?php
                                                      $table = "JobGrade";
                                                      createSelect($table,
                                                                  "sint_".$table."RefId",
                                                                  0,100,"Name","Select Job Grade","");
                                                   ?>
                                                </div>
                                                <?php
                                                   }
                                                ?>
                                             </div>
                                             <div class="row margin-top">
                                                <div class="col-xs-4 mylabel">
                                                   Salary Step:
                                                </div>
                                                <div class="col-xs-6">
                                                   <?php
                                                      $table = "StepIncrement";
                                                      createSelect($table,
                                                                  "sint_".$table."RefId",
                                                                  0,100,"Name","Select Step Increment","");
                                                   ?>
                                                </div>
                                             </div>
                                             <div class="row margin-top">
                                                <div class="col-xs-4 mylabel">
                                                   Salary Amount:
                                                </div>
                                                <div class="col-xs-6">
                                                   <input type="text" class="form-input saveFields-- number--" name="deci_SalaryAmount"/>
                                                </div>
                                             </div>
                                             <!-- 
                                             <div class="row margin-top">
                                                <div class="col-xs-4 mylabel">
                                                   Government Service:
                                                </div>
                                                <div class="col-xs-6">
                                                   <div class="row margin-top">
                                                      <input type="hidden" class="saveFields--" name="sint_isGovtService" value="1" >
                                                      <div class="col-xs-4">
                                                         <input id="GovtServiceYes" type="radio" name="GovernmentService" checked value="1">&nbsp;
                                                         <label for="GovtServiceYes">Yes</label>
                                                      </div>
                                                      <div class="col-xs-4">
                                                         <input id="GovtServiceNo" type="radio" name="GovernmentService" value="0">&nbsp;
                                                         <label for="GovtServiceNo">No</label>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="row margin-top">
                                                <div class="col-xs-4 mylabel">
                                                   Is Service Record?
                                                </div>
                                                <div class="col-xs-6">
                                                   <div class="row margin-top">
                                                      <input type="hidden" class="saveFields--" name="sint_isServiceRecord" value="1" >
                                                      <div class="col-xs-4">
                                                         <input id="IsServiceRecordYes" type="radio" name="IsServiceRecord" checked value="1">&nbsp;
                                                         <label for="IsServiceRecordYes">Yes</label>
                                                      </div>
                                                      <div class="col-xs-4">
                                                         <input id="IsServiceRecordNo" type="radio" name="IsServiceRecord" value="0">&nbsp;
                                                         <label for="IsServiceRecordNo">No</label>
                                                      </div>
                                                      <div class="col-xs-4">
                                                         <input id="IsServiceRecordBoth" type="radio" name="IsServiceRecord" value="2">&nbsp;
                                                         <label for="IsServiceRecordBoth">Both</label>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             -->
                                          </div>   
                                          <div>
                                             <?php spacer(30); ?>
                                             <div class="row margin-top">
                                                <div class="col-xs-4 mylabel">
                                                   LWOP:
                                                </div>
                                                <div class="col-xs-6">
                                                   <input type="number" class="form-input saveFields-- number--" name="sint_LWOP"/>
                                                </div>
                                             </div>
                                             <div class="row margin-top">
                                                <div class="col-xs-4 mylabel">
                                                   Date:
                                                </div>
                                                <div class="col-xs-6">
                                                   <input type="text" class="form-input saveFields-- date--" name="date_LWOPDate"/>
                                                </div>
                                             </div>
                                             <div class="row margin-top">
                                                <div class="col-xs-4 mylabel">
                                                   Cause:
                                                </div>
                                                <div class="col-xs-6">
                                                   <textarea class="form-input saveFields--" rows="5" name="char_Cause"
                                                   placeholder="Cause"></textarea>
                                                </div>
                                             </div>
                                                   
                                          </div>            


                                       </div>
                                    </div>
                                 </div>
                                 <div class="panel-bottom"></div>
                              </div>
                           </div>
                        </div>
                  </div>
               </div>
            </div>

            <div class="modal fade" id="modalEmpLookUp" role="dialog">
               <div class="modal-dialog modal-lg">
                  <div class="mypanel">
                     <div class="panel-top">
                        EMPLOYEES LOOK UP
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                     </div>
                     <div class="panel-mid-litebg"
                          id="EmpListLkUp" style="max-height:500px;overflow:auto;">
                     </div>
                  </div>
               </div>
            </div>
            <div class="modal fade border0" id="modalEmpMovement" role="dialog">
               <div class="modal-dialog border0" style="padding:0px;width:90%;height:92%;">
                  <div class="mypanel border0" style="height:100%;">
                     <div class="panel-top bgSilver">
                        <a href="#" title="Print Now" id="" onclick="self.print();">
                           <i class="fa fa-print" aria-hidden="true"></i>
                        </a>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                     </div>
                     <iframe id="empIFrame" src="blank.e2e.php" class="iframes"></iframe>
                  </div>
               </div>
            </div>
            <?php
               footer();
               $table         = "employeesmovement";
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>
   </body>
</html>



