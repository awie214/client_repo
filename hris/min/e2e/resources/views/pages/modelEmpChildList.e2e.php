<?php
   require_once "incUtilitiesJS.e2e.php";
?>
<script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   <div class="mypanel" id="rptCriteria">
      <div class="panel-top">
         Options
      </div>
      <div class="panel-mid-litebg">
         <div class="row margin-top">
            <div class="col-xs-12 txt-center">
               <span class="label">Sort By:</span>
               <select class="form-input rptCriteria--" name="drpSortBy" id="drpSortBy" style="width:150px;">
                  <option value="LastName" selected>Last Name</option>
                  <option value="FirstName">First Name</option>
                  <option value="BirthDate">Date of Birth</option>
                  <option value="Height">Height</option>
                  <option value="Weight">Weight</option>
                  <option value="CivilStatus">Civil Status</option>
               </select>
               <label style="margin-left:10px;margin-right:10px;">|</label>
               <input type="checkbox" name="chkRptSummary" class="showCol--">&nbsp;<span class="label">Show Report Summary</span>
            </div>
         </div>
      </div>
      <div id="column">
         <div class="panel-top">
            Show Column
         </div>
         <div class="panel-mid-litebg padd10">
            <div class="row">
               <div class="col-xs-3">
                  <input type="checkbox" id="checkAll"/>&nbsp;<span class="label">Check All</span>
                  <?php bar();?>
               </div>
            </div>
            <?php
               $ObjList =
               [
                  [
                     "chkEmpRefId|EmpRefId|checked|Employees Ref. Id",
                     "chkEmpName|EmpName|checked disabled|Employees Name",
                     "chkChildFullName|ChildFullName||Child Full Name",
                  ],
                  [
                     "chkChildBDate|ChildBDate|checked|Child Birth Date",
                     "chkChildGender|ChildGender||Child Gender"
                  ],
               ];

               for ($j=0;$j<count($ObjList);$j++) {
                  $RowElement = $ObjList[$j];
                  echo
                  '<div class="row margin-top">';
                  for ($d=0;$d<count($RowElement);$d++) {
                     $obj = explode("|",$RowElement[$d]);

                     if ($obj[0] != "") {
                        echo
                        '<div class="col-xs-3 colopt">
                           <input type="checkbox" id="'.$obj[1].'" name="'.$obj[0].'" class="showCol--" '.$obj[2].'>
                           <label class="label" for="'.$obj[1].'">'.$obj[3].'</label>
                        </div>';
                     }

                  }
                  echo
                  '</div>'."\n";
               }
            ?>
         </div>
      </div>
   </div>
