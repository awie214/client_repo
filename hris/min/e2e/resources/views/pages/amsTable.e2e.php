<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script src="<?php echo jsCtrl("ctrl_amsFileSetUp"); ?>"></script>
      <style>
      </style>
      <script language="JavaScript">
      </script>
   </head>
   <body onload = "indicateActiveModules();">
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"ams"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php
               doTitleBar ($paramTitle);
            ?>
            <div class="container-fluid margin-top">
               <div class="row">
                  <div class="col-xs-12">
                        <div id="divList">
                           <div class="row" class="amsTabsHolder">

                              <div class="col-xs-2 bgTab padd5">
                                 <div class="dropdown">
                                    <div class="dropdown-toggle" data-toggle="dropdown">
                                       <i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;ATTENDANCE RELATED
                                    </div>
                                    <ul class="dropdown-menu">
                                       <li><a href="javascript:void();" class="subMenu" memof="ams" pre="ams" route="FileSetup" id="mCutOff">
                                          <i class="fa fa-arrow-right" aria-hidden="true"></i>&nbsp;Cut-Off</a></li>
                                       <li><a href="javascript:void();" class="subMenu" memof="ams" pre="ams" route="FileSetup" id="mLeaves">
                                          <i class="fa fa-arrow-right" aria-hidden="true"></i>&nbsp;Leaves</a></li>
                                       <li><a href="javascript:void();" class="subMenu" memof="ams" pre="ams" route="FileSetup" id="mAbsences">
                                          <i class="fa fa-arrow-right" aria-hidden="true"></i>&nbsp;Absences</a></li>
                                       <li><a href="javascript:void();" class="subMenu" memof="ams" pre="ams" route="FileSetup" id="mHolidays">
                                          <i class="fa fa-arrow-right" aria-hidden="true"></i>&nbsp;Holidays</a></li>
                                    </ul>
                                 </div>
                              </div>
                              <div class="col-xs-2 bgTab padd5">
                                 <div class="dropdown">
                                    <div class="dropdown-toggle" data-toggle="dropdown">
                                       <i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;TABLE
                                    </div>
                                    <ul class="dropdown-menu margin-top10">
                                       <li><a href="javascript:void();" class="tabMenu" id="mTable1">
                                          <i class="fa fa-arrow-right" aria-hidden="true"></i>&nbsp;Table I</a></li>
                                       <li><a href="javascript:void();" class="tabMenu" id="mTable2">
                                          <i class="fa fa-arrow-right" aria-hidden="true"></i>&nbsp;Table II</a></li>
                                       <li><a href="javascript:void();" class="tabMenu" id="mTable3">
                                          <i class="fa fa-arrow-right" aria-hidden="true"></i>&nbsp;Table III</a></li>
                                       <li><a href="javascript:void();" class="tabMenu" id="mTable4">
                                          <i class="fa fa-arrow-right" aria-hidden="true"></i>&nbsp;Table IV</a></li>
                                    </ul>
                                 </div>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-xs-12 padd5" id="tbContent">
                                 <?php
                                 if (isset($childFile)) {
                                 ?>
                                 <div class="mypanel">
                                    <div class="panel-top"><span><?php echo strtoupper($ScreenName); ?></span> LIST</div>
                                    <div class="panel-mid">
                                             <span id="spGridTable">
                                                <?php
                                                   doGridTable($table,
                                                               $gridTableHdr_arr,
                                                               $gridTableFld_arr,
                                                               $sql,
                                                               $Action,
                                                               $gridtable);
                                                ?>
                                             </span>
                                    </div>
                                    <div class="panel-bottom">
                                       <?php
                                          btnINRECLO([true,true,false]);
                                       ?>
                                    </div>
                                 </div>
                                 <?php
                                 }
                                 ?>
                              </div>
                           </div>
                        </div>
                        <div id="divView">
                           <?php
                              if (isset($childFile)) {
                           ?>
                              <div class="mypanel">
                                 <div class="panel-top">
                                    <span id="ScreenMode">INSERTING NEW</span> <?php echo strtoupper($ScreenName); ?>
                                 </div>
                                 <div class="panel-mid-litebg">
                                    <?php require_once $childFile.".e2e.php"; ?>
                                 </div>
                                 <div class="panel-bottom">
                                    <?php btnSACABA([true,true,true]); ?>
                                 </div>
                              </div>
                           <?php
                              }
                           ?>

                        </div>
                  </div>
               </div>
            </div>
            <?php
               footer();
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>

   </body>
</html>