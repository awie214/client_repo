<?php
   //session_start();
   //$_SESSION["Classes"] = "../app/Classes/";
   //$_SESSION["path"] = "../public/";
   require_once $_SESSION["Classes"]."0620functions.e2e.php";
   require_once $_SESSION["Classes"]."SysFunctions.e2e.php";
   $_SESSION["cssFile"] = "CSS_SPMS".f_encode(date("Ymd",time())).".css";
   $sys = new SysFunctions();
   $sys->destroy_css("CSS_SPMS");
   $css = ["insigniaRed1","insigniaRed2","bgSPMS"];
   $_SESSION["cssFilePath"] = $sys->css_create($css);
?>
<!doctype html>
<html lang="{{ config('app.locale') }}">
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
   </head>
    <body class="spmsBody">
        <div class="flex-center position-ref full-height">
            <div class="content">
                <div class="flex-center">
                    <h1>WELCOME TO <br>STRATEGIC PERFORMANCE MANAGEMENT SYSTEM</h1>
                </div>
                <div class="links">
                   <div class="flex-center">
                     <span id="dashboard" class="spmsMENU">Dashboard</span>&nbsp;|&nbsp;
                     <span id="IPCR" class="spmsMENU">IPCR</span>&nbsp;|&nbsp;
                     <span id="OPCR" class="spmsMENU">OPCR</span>&nbsp;|&nbsp;
                     <span id="Reports" class="spmsMENU">Reports</span>&nbsp;|&nbsp;
                     <span id="Signatories" class="spmsMENU">Signatories</span>&nbsp;|&nbsp;
                     <span id="Logout" class="spmsMENU">Logout</span>
                   </div>
                </div>
            </div>
            <?php
            footer();
            include_once ("varHidden.e2e.php");
         ?>
        </div>
    </body>
</html>
