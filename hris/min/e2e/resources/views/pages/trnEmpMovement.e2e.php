<?php
   require_once "constant.e2e.php";
   require_once "conn.e2e.php";
   require_once pathClass."0620functions.e2e.php";

   $moduleContent = file_get_contents(json.$post["json"].".json");
   $module = json_decode($moduleContent, true);

   $CompanyId = $post["hCompanyID"];
   $BranchId = $post["hBranchID"];

    

   if (strtoupper($post["hBtnValue"]) == "SAVE") {
      $fv = parseFieldnValue($module[$post["elem1"]]["dbField"],$post);
      $empRefId = $post["sint_EmployeesRefId"];
      if ($empRefId) {
         $fv["Fields"] .= "EmployeesRefId, ";
         $fv["Values"] .= "$empRefId, ";
         if (isset($post["sint_ApptType"])) {
            $fv["Fields"] .= "ApptType, ";
            $fv["Values"] .= "'".realEscape($post["sint_ApptType"])."', ";   
            $fv["FieldsValues"] .= "`ApptType` = '".realEscape($post["sint_ApptType"])."',";
         }
         if (intval($post["hRefId"]) > 0) {
            $emp_movement_refid = $post["hRefId"];
            $EmpMovement_update = f_SaveRecord("EDITSAVE","employeesmovement",$fv["FieldsValues"],$emp_movement_refid);
            if ($EmpMovement_update != "") {
               echo "Error in updating";
            } else {
               echo "Update Success";
            }
         } else {
            $EmpMovement = f_SaveRecord("NEWSAVE","employeesmovement",$fv["Fields"],$fv["Values"]);
            if (is_numeric($EmpMovement)) {
               $where = "WHERE CompanyRefId = ".$CompanyId." AND BranchRefId = ".$BranchId;
               $where .= " AND EmployeesRefId = $empRefId";
               $rowEmpInfo = FindLast("empinformation",$where,"*");
               if ($rowEmpInfo) {
                  $fldnval = ""; 
                  $fldnval .= "`EmployeesRefId` = '".realEscape($post["sint_EmployeesRefId"])."', ";
                  if (isset($post["sint_AgencyRefId"])) {
                     $fldnval .= "`AgencyRefId` = '".realEscape($post["sint_AgencyRefId"])."', ";   
                  }
                  if (isset($post["sint_PositionItemRefId"])) {
                     $fldnval .= "`PositionItemRefId` = '".realEscape($post["sint_PositionItemRefId"])."', ";   
                  }
                  if (isset($post["sint_PositionRefId"])) {
                     $fldnval .= "`PositionRefId` = '".realEscape($post["sint_PositionRefId"])."', ";   
                  }
                  if (isset($post["sint_SalaryGradeRefId"])) {
                     $fldnval .= "`SalaryGradeRefId` = '".realEscape($post["sint_SalaryGradeRefId"])."', ";   
                  }
                  if (isset($post["sint_JobGradeRefId"])) {
                     $fldnval .= "`JobGradeRefId` = '".realEscape($post["sint_JobGradeRefId"])."', ";   
                  }
                  if (isset($post["sint_StepIncrementRefId"])) {
                     $fldnval .= "`StepIncrementRefId` = '".realEscape($post["sint_StepIncrementRefId"])."', ";   
                  }
                  if (isset($post["deci_SalaryAmount"])) {
                     $fldnval .= "`SalaryAmount` = '".realEscape($post["deci_SalaryAmount"])."', ";
                  }
                  if (isset($post["sint_OfficeRefId"])) {
                     $fldnval .= "`OfficeRefId` = '".realEscape($post["sint_OfficeRefId"])."', ";   
                  }
                  if (isset($post["sint_DepartmentRefId"])) {
                     $fldnval .= "`DepartmentRefId` = '".realEscape($post["sint_DepartmentRefId"])."', ";   
                  }
                  if (isset($post["sint_DivisionRefId"])) {
                     $fldnval .= "`DivisionRefId` = '".realEscape($post["sint_DivisionRefId"])."', ";   
                  }
                  if (isset($post["sint_EmpStatusRefId"])) {
                     $fldnval .= "`EmpStatusRefId` = '".realEscape($post["sint_EmpStatusRefId"])."', ";   
                  }
                  if (isset($post["sint_ApptStatusRefId"])) {
                     $fldnval .= "`ApptStatusRefId` = '".realEscape($post["sint_ApptStatusRefId"])."', ";   
                  }
                  if (isset($post["sint_DesignationRefId"])) {
                     $fldnval .= "`DesignationRefId` = '".realEscape($post["sint_DesignationRefId"])."', ";   
                  }
                  $newAgencyID = "`AgencyId` = '".realEscape($post["char_NewEmployeesId"])."', ";
                  $update_empinfo = f_SaveRecord("EDITSAVE","empinformation",$fldnval,$rowEmpInfo["RefId"]);
                  if ($update_empinfo == "") {
                     if ($post["char_NewEmployeesId"] != "") {
                        $update_employees = f_SaveRecord("EDITSAVE","employees",$newAgencyID,$empRefId);
                        if ($update_employees == "") {
                           echo "Save Success";
                        } else {
                           "Error on Updating (Employee Agency ID)\n";
                        }   
                     } else {
                        echo "Save Success";
                     }
                  } else {
                     echo "Error on Saving (Emp. Information)\n";
                  }
               } else {
                  echo "NO EMP. INFORMATION RECORD\nYou need update Employment Information Module";
               }
            } else {
               echo "Error on Saving (Emp. Movement)\n";
            }
         }
      } else {
         echo "No Employees Selected!!!";
      }
   }
?>

