<div class="mypanel">
   <div class="row">
      <div class="col-sm-12">
         <div class="row">
            <div class="col-sm-3">
            </div>
            <div class="col-sm-3 txt-center">
               <label>Employee Share</label>
            </div>
            <div class="col-sm-3 txt-center">
               <label>Employer Share</label>
            </div>
            <div class="col-sm-3 txt-center">
               <label>ECC</label>
            </div>
         </div>
         <div class="row margin-top">
            <div class="col-sm-3 label">
               <label>GSIS:</label>
            </div>
            <div class="col-sm-3">
               <input type="text" name="EmployeeShareGSIS" class="form-input number--">
            </div>
            <div class="col-sm-3">
               <input type="text" name="EmployerShareGSIS" class="form-input number--">
            </div>
            <div class="col-sm-3">
               <input type="text" name="ECCGSIS" class="form-input number--">
            </div>
         </div>
         <div class="row margin-top">
            <div class="col-sm-3 label">
               <label>SSS:</label>
            </div>
            <div class="col-sm-3">
               <input type="text" name="EmployeeShareSSS" class="form-input number--">
            </div>
            <div class="col-sm-3">
               <input type="text" name="EmployerShareSSS" class="form-input number--">
            </div>
            <div class="col-sm-3">
               <input type="text" name="ECCSSS" class="form-input number--">
            </div>
         </div>
         <div class="row margin-top">
            <div class="col-sm-3 label">
               <label>Philhealth:</label>
            </div>
            <div class="col-sm-3">
               <input type="text" name="EmployeeSharePhilhealth" class="form-input number--">
            </div>
            <div class="col-sm-3">
               <input type="text" name="EmployerSharePhilhealth" class="form-input number--">
            </div>
         </div>
         <div class="row margin-top">
            <div class="col-sm-3 label">
               <label>Pag-ibig:</label>
            </div>
            <div class="col-sm-3">
               <input type="text" name="EmployeeSharePagibig" class="form-input number--">
            </div>
            <div class="col-sm-3">
               <input type="text" name="EmployerSharePagibig" class="form-input number--">
            </div>
         </div>
         <div class="row margin-top">
            <div class="col-sm-3 label">
               <label>Withholding Tax:</label>
            </div>
            <div class="col-sm-3">
               <input type="text" name="EmployeeShareWithoutHoldingTax" class="form-input number--">
            </div>
         </div>
         <div class="row margin-top">
            <div class="col-sm-3 label">
               <label>Provident Fund:</label>
            </div>
            <div class="col-sm-3">
               <input type="text" name="EmployeeShareProvidentFund" class="form-input number--">
            </div>
            <div class="col-sm-3">
               <input type="text" name="EmployerShareProvidentFund" class="form-input number--">
            </div>
         </div>
         <div class="row margin-top">
            <div class="col-sm-3 label">
               <label>Association Dues:</label>
            </div>
            <div class="col-sm-3">
               <input type="text" name="EmployeeShareAssociationDues" class="form-input number--">
            </div>
         </div>
         <div class="row margin-top">
            <div class="col-sm-3 label">
               <label>Pag-Ibig Fund II:</label>
            </div>
            <div class="col-sm-3">
               <input type="text" name="EmployeeSharePagibigFundII" class="form-input number--">
            </div>
         </div>
         <div class="row margin-top">
            <div class="col-sm-3 label">
               <label>GSIS UOLI Premium I:</label>
            </div>
            <div class="col-sm-3">
               <input type="text" name="EmployeeShareGSISUOLIPremiumI" class="form-input number--">
            </div>
         </div>
         <div class="row margin-top">
            <div class="col-sm-3 label">
               <label>GSIS UOLI Premium II:</label>
            </div>
            <div class="col-sm-3">
               <input type="text" name="EmployeeShareGSISUOLIPremiumII" class="form-input number--">
            </div>
         </div>
         <div class="row margin-top">
            <div class="col-sm-3 label">
               <label>GSIS UOLI Premium III:</label>
            </div>
            <div class="col-sm-3">
               <input type="text" name="EmployeeShareGSISUOLIPremiumIII" class="form-input number--">
            </div>
         </div>
         <div class="row margin-top">
            <div class="col-sm-3 label">
               <label>GSIS UOLI Premium IV:</label>
            </div>
            <div class="col-sm-3">
               <input type="text" name="EmployeeShareGSISUOLIPremiumIV" class="form-input number--">
            </div>
         </div>
         <div class="row margin-top">
            <div class="col-sm-3 label">
               <label>GSIS UOLI Premium V:</label>
            </div>
            <div class="col-sm-3">
               <input type="text" name="EmployeeShareGSISUOLIPremiumV" class="form-input number--">
            </div>
         </div>
      </div>
   </div>
   <div class="row margin-top txt-right">
      <?php createButton("ADJUSTMENTS","btnAdjust","btn-cls4-sea","",""); ?>
   </div>
</div>