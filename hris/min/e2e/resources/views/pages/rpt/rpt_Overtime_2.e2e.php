<?php
   $file = "Request for Authority To Render Overtime Work";
   
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   $refid = getvalue("refid");
   $row = FindFirst("overtime_request","WHERE RefId = '$refid'","*");
   $FiledDate = date("F d, Y",strtotime($row["FiledDate"]));
?>
<!DOCTYPE html>
<html>
   <head>
      <?php
         include_once 'pageHEAD.e2e.php';
      ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
      <style>
         /*.bottomline {border-bottom:2px solid black;}*/
         .bold {font-size:600;}
      </style>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <button type="button" class="btn-cls4-sea" onclick="self.location = 'rpt/docs_OT.e2e.php?refid=<?php echo $refid; ?>';">
            DOWNLOAD AS WORD
         </button>
         <br><br>
         <?php
            rptHeader("");
         ?>
         <div class="txt-center bold">
            <b>
               <h5>ANNEX A</h5>
               <h4>AUTHORITY TO RENDER OVERTIME SERVICES</h4>
            </b>
         </div>
         <div class="row">
            <div class="col-xs-8"></div>
            <div class="col-xs-4 text-center">
               <u>
                  <?php
                     echo $FiledDate;
                  ?>
               </u>
               <br>
               Date
            </div>
         </div>
         <br>
         <table border="1">
            <thead>
               <tr>
                  <th>
                     Name
                  </th>
                  <th>
                     Office
                  </th>
                  <th>
                     Position
                  </th>
                  <th>
                     Preferred<br>Remuneration<br>(OT Pay or CTO)
                  </th>
               </tr>
            </thead>
            <tbody>
               <?php
                  if ($row) {
                     $emprefid = $row["EmployeesRefId"];
                     $emp_row = FindFirst("employees","WHERE RefId = '$emprefid'","`FirstName`,`LastName`,`MiddleName`,`ExtName`");
                     if ($emp_row) {
                        $LastName = $emp_row["LastName"];
                        $FirstName = $emp_row["FirstName"];
                        $MiddleName = $emp_row["MiddleName"];
                        $ExtName = $emp_row["ExtName"];
                        $FullName = $LastName.", ".$FirstName." $ExtName ".$MiddleName;
                     } else {
                        $FullName = "";
                     }
                     $empinfo_row = FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'","*");
                     if ($empinfo_row) {
                        $Office = getRecord("office",$empinfo_row["OfficeRefId"],"Name");
                        $Position = getRecord("position",$empinfo_row["PositionRefId"],"Name");
                     } else {
                        $Office = "";
                        $Position = "";
                     }
                     $stat = $row["WithPay"];
                     if (intval($stat) > 0) {
                        $stat = "OT Pay";
                     } else {
                        $stat = "CTO";
                     }
                     echo '
                        <tr>
                           <td>'.$FullName.'</td>
                           <td class="text-center">'.$Office.'</td>
                           <td class="text-center">'.$Position.'</td>
                           <td class="text-center">'.$stat.'</td>
                        </tr>
                     ';
                  }
               ?>
            </tbody>
         </table>
         <br>
         <div class="row margin-top">
            <div class="col-xs-8">
               To render overtime job/services on:
            </div>
            <div class="col-xs-4 text-center">
               ______________________
               <br>
               Date
            </div>
         </div>
         <br>
         <div class="row margin-top">
            <div class="col-xs-12">
               Specific output to be done/expected output:
            </div>
         </div>
         <div class="row margin-top">
            <div class="col-xs-12" style="border-bottom: 1px solid black;">
               1.
            </div>
         </div>
         <div class="row margin-top">
            <div class="col-xs-12" style="border-bottom: 1px solid black;">
               2.
            </div>
         </div>
         <div class="row margin-top">
            <div class="col-xs-12" style="border-bottom: 1px solid black;">
               3.
            </div>
         </div>
         <div class="row margin-top">
            <div class="col-xs-6">
               Recommeding Approval:
               <?php
                  spacer(40);
               ?>
               <b>
                  Immediate Supervisor
               </b>
            </div>
            <div class="col-xs-6">
               Approved:
               <?php
                  spacer(40);
               ?>
               <b>
                  Head of Office
               </b>
            </div>
         </div>
         <div class="row margin-top">
            <div class="col-xs-12">
               <b>Note</b>:This should be accomplished in two (2) copies prior to rendering overtime work. Please submit the duplicate copy to the Human Resource Development Division at the end of the month along with the Daily Time Record (DTR) and Accomplishment Report.
            </div>
         </div>
      </div>
   </body>
</html>