<?php
   //session_start();
   include 'colors.e2e.php';
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;

   $empRefid = getvalue('txtRefId');
   $p_TrnFrom = getvalue('txtTrnFrom');
   $p_TrnTo = getvalue('txtTrnTo');
   $searchCriteria = "";

   if ($empRefid != "") {
      if ($searchCriteria == "")
         $searchCriteria .= "Employees RefId $empRefid";
      else
         $searchCriteria .= "|Employees RefId $empRefid";
   }
   if ($p_TrnFrom != "" && $p_TrnTo != "") {
      if ($searchCriteria == "")
         $searchCriteria .= "Trn Date BETWEEN '$p_TrnFrom' AND '$p_TrnTo'";
      else
         $searchCriteria .= "|Trn Date BETWEEN '$p_TrnFrom' AND '$p_TrnTo'";
   } else if ($p_TrnFrom != "" && $p_TrnTo == "") {
      if ($searchCriteria == "")
         $searchCriteria .= "Trn Date on or after $p_TrnFrom";
      else
         $searchCriteria .= "|Trn Date on or after $p_TrnFrom";
   } else if ($p_TrnFrom == "" && $p_TrnTo != "") {
      if ($searchCriteria == "")
         $searchCriteria .= "Trn Date on or before $p_TrnTo";
      else
         $searchCriteria .= "|Trn Date on or before $p_TrnTo";
   }
?>

<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            rptHeader("Audit Trails");
         ?>
         <div id="Trails">
         <?php
            $sql = "";
            $isDisp = false;
            $sql = "SELECT * FROM `updates201` where CompanyRefId >= 0";
            if ($empRefid != "") {
               $sql .= " and EmployeesRefId = $empRefid ";
               $isDisp = true;
            }
            if ($p_TrnFrom != "") {
               $sql .= " and TrnDate >= '$p_TrnFrom'";
               $isDisp = true;
            }
            if ($p_TrnTo != "") {
               $sql .= " and TrnDate <= '$p_TrnTo'";
               $isDisp = true;
            }
            $sql .= " ORDER BY TrnDate DESC, TrnTime DESC LIMIT 10";
            $resultUpdates201 = mysqli_query($conn,$sql) or die(mysqli_error($conn));
            $numrow = mysqli_num_rows($resultUpdates201);
            $recordsCount=0;
             
            spacer(5);
            
            echo
            '<div class="margin-top10;">';
            if ($numrow) {
               echo '<table class="tblNormal" border=1 style="width:100%;">';
                  echo '<tr class="tbl_trHdr">';
                  echo '<td>&nbsp;</td>';
                  echo '<td>Employees Name</td>';
                  echo '<td>Table Name</td>';
                  echo '<td>Tabs Name</td>';
                  echo '<td>Fields To Edit</td>';
                  echo '<td>Old Value</td>';
                  echo '<td>New Value</td>';
                  echo '<td>Trn Date.</td>';
                  //echo '<td>Remarks</td>';
                  echo '<td>Update By</td>';
                  echo '</tr>';

               while ($row = mysqli_fetch_assoc($resultUpdates201)) {
                  $rowEmployees = FindFirst("employees","where RefId = ".$row['EmployeesRefId'],"*");
                  $empName = "";
                  if ($rowEmployees) {
                     $empName = "[".$rowEmployees["RefId"]."] - ".$rowEmployees["LastName"].", ".$rowEmployees["FirstName"];
                  }
                  $recordsCount++;
                  echo '<tr class="tbl_trBody">';
                  echo '<td class="txt-center">'.$recordsCount.'</td>';
                  echo '<td>'.$empName.'</td>';
                  echo '<td>'.$row["TableName"].'</td>';
                  echo '<td>'.$row["TabsName"].'</td>';
                  echo '<td>'.$row["FieldsEdit"].'</td>';
                  echo '<td>'.$row["OldValue"].'</td>';
                  echo '<td>'.$row["NewValue"].'</td>';
                  echo '<td class="txt-center">'.$row["TrnDate"].' '.$row["TrnTime"].'</td>';
                  //echo '<td>'.$row["Remarks"].'</td>';
                  echo '<td class="txt-center">'.$row["LastUpdateBy"].'</td>';
                  echo '</tr>';
               }
               echo '</table>';
               $recordsCount++;
            }
            else {
               echo "<h4>NO RECORDS BASE ON YOUR CRITERIA !!!</h4>";
            }
            echo '</div>';
            
            /*$sql = "SELECT * FROM `trnlog`";
            $res = mysqli_query($conn,$sql) or die(mysqli_error($conn));
            $numrow = mysqli_num_rows($res);
            if ($numrow) {
               while($row = mysqli_fetch_assoc($res)) {
                  echo $row['MACAddress'].'<br>';
                  echo $row['FldnVal'].'<br>';
               }
            }*/
            
         ?>
         </div>
         <?php
            echo
            '<div>SEARCH CRITERIA:</div>';
            if (!empty($searchCriteria)) {
               echo '<div>';
               $crit_Arr = explode("|",$searchCriteria);
               for ($j=0;$j<count($crit_Arr);$j++) {
                  echo "<li>".$crit_Arr[$j]."</li>";
               }
               echo '</div>';
            } else {
               echo "<li>ALL RECORDS</li>";
            }
            rptFooter();
         ?>
      </div>
   </body>
</html>