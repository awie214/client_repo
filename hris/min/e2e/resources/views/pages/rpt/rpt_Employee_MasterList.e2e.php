<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   $table = "employees";
   $whereClause .= " ORDER BY LastName";
   $rsEmployees = SelectEach($table,$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   if ($dbg) {
      echo $whereClause;
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
      <style type="text/css">
         @media print {
            table {
               font-size: 6pt !important;
            }
         }
      </style>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <div class="row" style="page-break-after: always;">
            <div class="col-xs-12">
               <div class="row margin-top">
                  <div class="col-xs-12">
                     <?php
                        rptHeader(getvalue("RptName"));
                     ?>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     <table width="100%">
                        <thead>
                           <tr class="colHEADER">
                              <th>Division</th>
                              <th>Fullname<sup>3</sup></th>
                              <th>Employee<br>No.</th>
                              <th>Employee<br>Status</th>
                              <th>Date of Original<br>Appointment</th>
                              <th>Date of Last<br>Promotion</th>
                              <th>Civil<br>Service<br>Eligibility</th>
                              <th>Sex</th>
                              <th>Residential<br>Address</th>
                              <th>Mobile<br>No.</th>
                              <th>Email<br>Address</th>
                              <th>Date<br>Of<br>Birth</th>
                              <th>Civil<br>Status</th>
                              <th>TIN</th>
                              <th>GSIS</th>
                              <th>Pag-<br>ibig</th>
                              <th>PHIC</th>
                              <th>Salary</th>
                           </tr>
                        </thead>
                        <tbody>
                     <?php
                        if ($rsEmployees) {
                           while ($row = mysqli_fetch_assoc($rsEmployees)) {
                              $emprefid   = $row["RefId"];
                              $LastName   = $row["LastName"];
                              $FirstName  = $row["FirstName"];
                              $MiddleName = $row["MiddleName"];
                              $ExtName    = $row["ExtName"];
                              $FullName   = $LastName.", ".$FirstName." ".$ExtName." ".$MiddleName;
                              $AgencyId   = $row["AgencyId"];
                              $BirthDate  = $row["BirthDate"];
                              $Sex        = $row["Sex"];
                              $CivilStatus= $row["CivilStatus"];
                              $PAGIBIG    = $row["PAGIBIG"];
                              $GSIS       = $row["GSIS"];
                              $TIN        = $row["TIN"];
                              $PHIC       = $row["PHIC"];
                              $EmailAdd   = $row["EmailAdd"];
                              $MobileNo   = $row["MobileNo"];
                              if ($BirthDate != "") {
                                 $BirthDate = date("F d, Y",strtotime($BirthDate));
                              }
                              $empinfo    = FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'","*");
                              if ($empinfo) {
                                 $Division = getRecord("division",$empinfo["DivisionRefId"],"Name");
                                 $EmpStatus = getRecord("empstatus",$empinfo["EmpStatusRefId"],"Name");
                                 $HiredDate = $empinfo["HiredDate"];
                                 if ($HiredDate != "") {
                                    $HiredDate = date("F d, Y",strtotime($HiredDate));
                                 }
                                 $Salary = $empinfo["SalaryAmount"];
                                 if($Salary != "") {
                                    $Salary = number_format($Salary,2);
                                 }
                              } else {
                                 $Division = $EmpStatus = $HiredDate = $Salary = "";
                              }
                              echo '
                                 <tr>
                                    <td>'.$Division.'</td>
                                    <td>'.$FullName.'</td>
                                    <td class="text-center">'.$AgencyId.'</td>
                                    <td class="text-center">'.$EmpStatus.'</td>
                                    <td class="text-center">'.$HiredDate.'</td>
                                    <td></td>
                                    <td></td>
                                    <td class="text-center">'.$Sex.'</td>
                                    <td></td>
                                    <td class="text-center">'.$MobileNo.'</td>
                                    <td class="text-center">'.$EmailAdd.'</td>
                                    <td class="text-center">'.$BirthDate.'</td>
                                    <td class="text-center">'.$CivilStatus.'</td>
                                    <td class="text-center">'.$TIN.'</td>
                                    <td class="text-center">'.$GSIS.'</td>
                                    <td class="text-center">'.$PAGIBIG.'</td>
                                    <td class="text-center">'.$PHIC.'</td>
                                    <td class="text-center">'.$Salary.'</td>
                                 </tr>
                              ';
                     ?>
                     <?php
                           }
                        }
                     ?>
                        </tbody>
                     </table>
                  </div>
               </div>
               <br>
               <br>
               <br>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     <sup>1</sup> Source of data: Personal Data Sheet and CSC Form No. 33-A
                     <br>
                     <sup>2</sup> Adjustable date range for the reporting period
                     <br>
                     <sup>3</sup> Name should be in the format: Surname, Firstname, Middlename
                  </div>
               </div>
            </div>
         </div>
      </div>
   </body>
</html>