<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include "pageHEAD.e2e.php"; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            rptHeader(getRptName(getvalue("drpReportKind")));
         ?>
         <p class="txt-right">Date:&nbsp;&nbsp;<u><?php echo date("F j, Y") ?> </u></p>
         <p class="txt-left">MEMORANDUM</p>
         <blockquote><p>To: Dela Cruz, Juan</p></blockquote>
         <p>Division: <u> MSID </u></p>
         <p>&nbsp;&nbsp;&nbsp;&nbsp;Pursuant to Section 8, Rule XVII of the Omnibus Rules Implementing Book V of Executive Order 292 and April 14, 2016 other Pertinent Civil Service Law, as a amended by CSC Memonrandum Circular No. 23,s 19998. officers or employees who have incurred tardniness and undertime, regardless of the number of minutes per day,ten(10) times a monthfor at least two(2) consecutive months during the year or for at least(2) months in a semester shall be subject to disciplinary action.</p>
         <p>Our recordsshow that you incurred tardiness more than the prescribed limit as follows:</p>
         <table border="1">
            <tr>
               <th>MONTH</th>
               <th>No. Of Late</th>
               <th>No. Of Undertime</th>
               <th>Total</th>
            </tr>
            <?php for ($j=1;$j<=4;$j++) {?>
               <tr>
                  <td class="txt-center">&nbsp;</td>
                  <td class="txt-center">&nbsp;</td>
                  <td class="txt-center">&nbsp;</td>
                  <td class="txt-center">&nbsp;</td>
               </tr>
            <?php } ?>
         </table>
         <br>
         <p>&nbsp;&nbsp;&nbsp;&nbsp;Item C, Number 4, Section 52 of Rule IV of the Uniform Rules on Administrative Cases in the Civil Service (URACCS) imposses the following penalties on your administrative offense, to wit:<br>
         "4. Frequent unauthorized tardiness (Habitual Tardiness)
         </p>
         <p style="padding-left:1in">1st Offense - Reprimand</p>
         <p style="padding-left:1in">2nd Offense - Suspension 1 - 30 days</p>
         <p style="padding-left:1in">3rd Offense - Dismissal"</p>
         <p>In view thereof, you are strictly advisedto imporove on your attendance, otherwise repetition on the same shall be deal with accordingly</p>
         <br><br><br>
         <div class="row">
            <div class="col-xs-9"></div>
            <div class="col-xs-3 txt-center">
               <p>(Name)</p>
               <p>______________________________________________________</p>
               <p>(Position)</p>
               <p>(Divisition)</p>
            </div>
         </div>
      </div>
      <?php rptFooter(); ?>
   </body>
</html>