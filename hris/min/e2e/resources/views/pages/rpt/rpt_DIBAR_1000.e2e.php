<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   $table = "employees";
   $whereClause .= " ORDER BY LastName";
   $rsEmployees = SelectEach($table,$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   if ($dbg) {
      echo $whereClause;
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
      <style type="text/css">
         @media print {
            body {
               font-size: 9pt;
            }
            thead {
               font-size: 9pt;  
            }
            tbody {
               font-size: 8pt !important;
            }
         }
      </style>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <div class="col-xs-12">
            <div class="row">
               <div class="col-xs-12">
                  <b><i>CS Form No. 8</i></b>
                  <br>
                  <i>Revised 2017</i>
               </div>
            </div>
            <br>
            <div class="row margin-top">
               <div class="col-xs-12 text-center" style="font-size: 12pt;">
                  <b>
                     REPORT ON  DATABASE OF INDIVIDUALS BARRED FROM ENTERING SERVICE<br>
                     AND TAKING CIVIL SERVICE EXAMINATIONS (DIBAR)
                     <br>
                     Philippine Institute for Development Studies (PIDS)
                  </b>
               </div>
            </div>
            <br>
            <div class="row margin-top">
               <div class="col-xs-12">
                  Report for the Month of: _________________
               </div>
            </div>
            <div class="row margin-top">
               <div class="col-xs-12">
                  <table width="100%">
                     <thead>
                        <tr class="colHEADER">
                           <th colspan="12">INDIVIDUAL INFORMATION</th>
                           <th colspan="7">DESIGNATION INFORMATION*</th>
                        </tr>
                        <tr class="colHEADER">
                           <th colspan="5">NAME</th>
                           <th rowspan="2">SEX</th>
                           <th rowspan="2">
                              DATE AND<br>PLACE OF<br>BIRTH
                           </th>
                           <th rowspan="2">
                              OCCUPATION<br>CATEGORY
                           </th>
                           <th rowspan="2">
                              POSITION
                           </th>
                           <th rowspan="2">
                              AGENCY
                           </th>
                           <th rowspan="2">
                              DATE & <br> PLACE OF<br>EXAM
                           </th>
                           <th rowspan="2">
                              ELIGIBILITY/<br>RATING
                           </th>
                           <th rowspan="2">
                              DOCUMENT<br>TYPE/ NO./<br>DATE
                           </th>
                           <th rowspan="2">
                              SIGNATORY
                           </th>
                           <th rowspan="2">
                              POSITION
                           </th>
                           <th rowspan="2">
                              AGENCY
                           </th>
                           <th rowspan="2">
                              OFFENSE
                           </th>
                           <th rowspan="2">
                              PENALTY
                           </th>
                           <th rowspan="2">
                              REMARKS
                           </th>
                        </tr>
                        <tr class="colHEADER">
                           <th>LAST NAME</th>
                           <th>FIRST NAME</th>
                           <th>NAME<br>EXTENSION</th>
                           <th>MIDDLE<br>NAME</th>
                           <th>MAIDEN<br>NAME</th>
                        </tr>
                     </thead>
                     <tbody>
                        <?php
                           for ($i=1; $i <= 10 ; $i++) { 
                              echo '
                                 <tr>
                                    <td>&nbsp;</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                 </tr>
                              ';
                           }
                        ?>
                     </tbody>
                  </table>
               </div>
            </div>
            <div class="row margin-top">
               <div class="col-xs-12">
                  <b>*DECISION INFORMATION</b> - Decision/Resolution/Order issued to bar certain individuals from entering government service and taking civil service examinations
               </div>
            </div>
            <div class="row margin-top">
               <div class="col-xs-1"></div>
               <div class="col-xs-11">
                  I hereby certify that the Decisions/Resolutions/Orders enumerated are executory.
               </div>
            </div>
            <div class="row margin-top">
               <div class="col-xs-6">
                  Prepared by:
               </div>
               <div class="col-xs-6">
                  Approved by:
               </div>
            </div>
            <br>
            <br>
            <div class="row margin-top">
               <div class="col-xs-6 text-center">
                  <u>JOHN ZERNAN B. LUNA</u>
                  <br>
                  HRMO
               </div>
               <div class="col-xs-6 text-center">
                  <u>ANDREA S. AGCAOILI</u>
                  <br>
                  Department Manager III - Administrative and Finance Department
               </div>
            </div>
            <br>
            <br>
            <div class="row margin-top">
               <div class="col-xs-6">
                  Date: _____________________________
               </div>
            </div>
         </div>
      </div>
   </body>
</html>