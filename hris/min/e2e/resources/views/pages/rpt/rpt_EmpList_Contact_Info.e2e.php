<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   $table = "employees";
   $whereClause .= " ORDER BY LastName";
   $rsEmployees = SelectEach($table,$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   if ($dbg) {
      echo $whereClause;
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            $count = 0;
            if ($rsEmployees) {
         ?>
         
         <table style="width: 100%;">
            <thead>
               <tr>
                  <td colspan="6">
                     <?php
                        rptHeader(getvalue("RptName"));
                     ?>
                  </td>
               </tr>
               <tr class="colHEADER">
                  <th rowspan="2">Sequence</th>
                  <th rowspan="2">Employee Name</th>
                  <th rowspan="2">Employee ID</th>
                  <th>Residential</th>
                  <th>Permanent</th>
                  <th rowspan="2">Mobile No. / Email Address</th>
               </tr>
               <tr class="colHEADER">
                  <th>Address / Tel No.</th>
                  <th>Address / Tel No.</th>
               </tr>
            </thead>
            <tbody>
               <?php
                  while ($row_emp = mysqli_fetch_assoc($rsEmployees)) {
                     $count++;
                     $FullName   = $row_emp["LastName"].", ".$row_emp["FirstName"]." ".$row_emp["MiddleName"];
                     $TelNo      = $row_emp["TelNo"];
                     $ResiStreet = $row_emp["ResiStreet"];
                     $ResiSubd   = $row_emp["ResiSubd"];
                     $ResiBrgy   = $row_emp["ResiBrgy"];
                     $MobileNo   = $row_emp["MobileNo"];
                     $EmailAdd   = $row_emp["EmailAdd"];
                     $ResiAddCityRefId             = getRecord("city",$row_emp["ResiAddCityRefId"],"Name");
                     $ResiAddProvinceRefId         = getRecord("province",$row_emp["ResiAddProvinceRefId"],"Name");
                     $ResidentTelNo                = $row_emp["ResidentTelNo"];
                     $PermanentStreet              = $row_emp["PermanentStreet"];
                     $PermanentSubd                = $row_emp["PermanentSubd"];
                     $PermanentBrgy                = $row_emp["PermanentBrgy"];
                     $PermanentAddCityRefId        = getRecord("city",$row_emp["PermanentAddCityRefId"],"Name");
                     $PermanentAddProvinceRefId    = getRecord("province",$row_emp["PermanentAddProvinceRefId"],"Name");
                     $ResiAddress = "";
                     $PermanentAddress = "";
                     if ($ResiStreet != "") $ResiAddress .= "$ResiStreet, ";
                     if ($ResiSubd != "") $ResiAddress .= "$ResiSubd, ";
                     if ($ResiBrgy != "") $ResiAddress .= "$ResiBrgy, ";
                     if ($ResiAddCityRefId != "") $ResiAddress .= "$ResiAddCityRefId, ";
                     if ($ResiAddProvinceRefId != "") $ResiAddress .= "$ResiAddProvinceRefId";

                     if ($PermanentStreet != "") $PermanentAddress .= "$PermanentStreet, ";
                     if ($PermanentSubd != "") $PermanentAddress .= "$PermanentSubd, ";
                     if ($PermanentBrgy != "") $PermanentAddress .= "$PermanentBrgy, ";
                     if ($PermanentAddCityRefId != "") $PermanentAddress .= "$PermanentAddCityRefId, ";
                     if ($PermanentAddProvinceRefId != "") $PermanentAddress .= "$PermanentAddProvinceRefId";
                     echo '
                        <tr>
                           <td class="text-center">'.$count.'</td>
                           <td>'.strtoupper($FullName).'</td>
                           <td>'.$row_emp["AgencyId"].'</td>
                           <td>'.$ResiAddress."<br>".$ResidentTelNo.'</td>
                           <td>'.$PermanentAddress."<br>".$TelNo.'</td>
                           <td>'.$MobileNo."<br>".$EmailAdd.'</td>
                        </tr>
                     ';
                  }
               ?>
               
            </tbody>
         </table>
         <?php
            } else {
         ?>
         <?php
            rptHeader(getvalue("RptName"));
         ?>
         <table border="1" style="width: 100%;">
            <thead>
               <tr class="colHEADER">
                  <th rowspan="2">Sequence</th>
                  <th rowspan="2">Employee Name</th>
                  <th rowspan="2">Employee ID</th>
                  <th>Residential</th>
                  <th>Permanent</th>
                  <th rowspan="2">Mobile No. / Email Address</th>
               </tr>
               <tr class="colHEADER">
                  <th>Address / Tel No.</th>
                  <th>Address / Tel No.</th>
               </tr>
            </thead>
            <tbody>
               <tr>
                  <td colspan="6"> No Record Found.</td>
               </tr>
            </tbody>
         </table>
         <?php
            }
         ?>
      </div>
   </body>
</html>