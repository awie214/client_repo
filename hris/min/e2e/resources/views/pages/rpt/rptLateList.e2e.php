<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   //$whereClause .= " LIMIT 10";
   $table = "employees";
   $rsEmployees = SelectEach($table,$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   $from = getvalue("txtAttendanceDateFrom");
   $to   = getvalue("txtAttendanceDateTo");
   $month = getvalue("txtAttendanceMonth");
   $year  = getvalue("txtAttendanceYear");
   /*
   if ($from == "") {
      $from     = date("Y-m-d",time());
      $from_arr = explode("-", $from);
      $from     = $from_arr[0]."-".$from_arr[1]."-01";
   }
   if ($to == "") {
      $to     = date("Y-m-d",time());
      $to_arr = explode("-", $to);
      
      $to     = $to_arr[0]."-".$to_arr[1]."-".cal_days_in_month(CAL_GREGORIAN,$to_arr[1],$to_arr[0]);
   }
   */
   $from    = $year."-".$month."-01";
   $to      = $year."-".$month."-".cal_days_in_month(CAL_GREGORIAN,$month,$year);
   if ($dbg) {
      echo $whereClause;
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            $errmsg = "";
            rptHeader(getRptName(getvalue("drpReportKind")));
            if ($rsEmployees && $errmsg == "")
            {
         ?>
         <p class="txt-center">
            <?php
               echo "For the Date ";
               echo date("m/d/Y",strtotime($from))." - ".date("m/d/Y",strtotime($to));
            ?>
         </p>
         <table border="1">
            <tr>
               <th>Name of Employee</th>
               <?php for ($j=1;$j<=31;$j++) {?>
                  <th class="txt-center" style="width:30px;"><?php echo $j ?></th>
               <?php } ?>
               <th>Total</th>
               <th>Remarks</th>
            </tr>
            <?php
               $count = 0;
               while ($row_emp = mysqli_fetch_assoc($rsEmployees) ) {
                  $emprefid       = $row_emp["RefId"];
                  $biometricsID   = $row_emp["BiometricsID"];
                  $CompanyID      = $row_emp["CompanyRefId"];
                  $BranchID       = $row_emp["BranchRefId"];
                  $Default_qry    = "WHERE CompanyRefId = ".$CompanyID." AND BranchRefId = ".$BranchID;
                  $workschedrefid = FindFirst("empinformation","WHERE EmployeesRefId = ".$row_emp["RefId"],"WorkscheduleRefId");
                  $late_count     = 0;
                  if (is_numeric($workschedrefid)) {
                     
                     for ($v=1;$v<=31;$v++) {
                        if ($v <= 9) $v = "0".$v; 
                        ${"l_".$row_emp["RefId"]."_".$v} = "&nbsp;";
                     } 
                     $count++;
                     $curr_date   = date("Y-m-d",time());
                     $month_start = $from;
                     $month_end   = $to;
                     include 'mdbcn.e2e.php';
                     include 'incDTRSummary.e2e.php';
            ?>
               <tr>
                  <td class="pad-left">
                     <?php 
                        echo $row_emp['LastName'].', '.$row_emp['FirstName'].', '.$row_emp['MiddleName'];
                     ?>
                  </td>
                  <?php
                     for ($v=1;$v<=31;$v++) {
                        if ($v <= 9) $v = "0".$v; 
                        echo '<td class="txt-center">'.${"l_".$row_emp["RefId"]."_".$v}.'</td>';
                     } 
                  ?>
                  <td class="txt-center">
                     <?php echo $late_count; ?>
                  </td>
                  <?php
                     if ($late_count == 0 || $late_count == "") {
                        echo '<td class="txt-center">&nbsp;</td>';
                     } else {
                        echo '<td class="txt-center">No Application</td>';
                     }
                  ?>
                  
               </tr>
            <?php
                  }
               }
               echo "RECORD COUNT : ".$count;
            }else {
               echo '<div>NO RECORD QUERIED base on your criteria!!!</div>';
               echo '<div>'.$errmsg.'</div>';
            }
            ?>
         </table>
         <p>
            <div class="row">
               <div class="col-xs-2 txt-right">Prepared By:</div>
               <div class="col-xs-4"></div>
               <div class="col-xs-2 txt-right">Approved By:</div>
               <div class="col-xs-4"></div>
            </div>
            <div class="row">
               <div class="col-xs-2"></div>
               <div class="col-xs-4">________________________</div>
               <div class="col-xs-2"></div>
               <div class="col-xs-3">________________________</div>
               <div class="col-xs-1"></div>
            </div>
         </p>

      </div>
   </body>
</html>