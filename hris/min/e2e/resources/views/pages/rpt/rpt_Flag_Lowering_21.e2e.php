<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   $table = "employees";
   $whereClause .= " ORDER BY LastName";
   $rsEmployees = SelectEach($table,$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   if ($dbg) {
      echo $whereClause;
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            $count = 0;
            if ($rsEmployees) {
         ?>
            <table style="width: 100%;">
               <thead>
                  <tr>
                     <td colspan="9">
                        <?php
                           rptHeader(getvalue("RptName"));
                        ?>
                     </td>
                  </tr>
                  <tr class="colHEADER">
                     <th rowspan="2" style="width: 20%;">EMPLOYEE NAME</th>
                     <?php
                        $month = date("m",time());
                        $year  = date("Y",time());
                        $days  = cal_days_in_month(CAL_GREGORIAN,$month,$year);
                        $week_count = 0;
                        for ($i=1; $i <= $days; $i++) {
                           if ($i <= 9) $i = "0".$i;
                           $new_date = $year."-".$month."-".$i; 
                           if (date("D",strtotime($new_date)) == "Fri") {
                              echo '<th colspan="2">'.$i.' - '.date("F",strtotime($new_date)).'</th>';
                              $week_count++;
                           }
                        }
                     ?>
                  </tr>
                  <tr class="colHEADER">
                     <?php
                        for ($a=1; $a <= $week_count ; $a++) { 
                           echo '<th>SIGNATURE</th>';
                           echo '<th>REASON FOR NON-ATTENDANCE</th>';
                        }
                     ?>
                  </tr>
               </thead>
               <tbody>
                  <?php
                     while ($row_emp = mysqli_fetch_assoc($rsEmployees)) {
                        $FullName   = $row_emp["LastName"].", ".$row_emp["FirstName"]." ".$row_emp["MiddleName"];
                        echo '<tr>';
                           echo '<td>'.$FullName.'</td>';
                        for ($b=1; $b <= $week_count ; $b++) { 
                           echo '<td>&nbsp;</td>';
                           echo '<td>&nbsp;</td>';
                        }
                        echo '</tr>';
                     }
                  ?>
               </tbody>
            </table>
         
         <?php
            }
         ?>
      </div>
   </body>
</html>