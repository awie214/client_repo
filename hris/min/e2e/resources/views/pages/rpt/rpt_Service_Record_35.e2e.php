<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   $table = "employees";
   $whereClause .= " ORDER BY LastName";
   $rsEmployees = SelectEach($table,$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   if ($dbg) {
      echo $whereClause;
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
      <style type="text/css">
         @media print {
            table {
               font-size: 6pt !important;
            }
         }
      </style>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <div class="row" style="page-break-after: always;">
            <div class="col-xs-12">
               <?php
                  if ($rsEmployees) {
                     while ($row = mysqli_fetch_assoc($rsEmployees)) {
                        $emprefid   = $row["RefId"];
                        $LastName   = $row["LastName"];
                        $FirstName  = $row["FirstName"];
                        $MiddleName = $row["MiddleName"];
                        $ExtName    = $row["ExtName"];
                        $BirthDate  = $row["BirthDate"];
                        $BirthPlace = $row["BirthPlace"];
                        if ($BirthDate != "") {
                           $BirthDate = date("F d, Y",strtotime($BirthDate));
                        }
               ?>
               <div class="row margin-top">
                  <div class="col-xs-4 text-right">
                     <img src="../../../public/images/35/report_logo.png" style="width:40%;">
                  </div>
                  <div class="col-xs-4 text-center">
                     Republic of the Philippines
                     <br>
                     Philippine Commission on Women
                     <br>
                     1145 J.P. Laurel St., San Miguel Manila
                     <br>
                     Tel. No. (02) 735-4767 | Fax No. (02) 736-4449
                     <br>
                     Email: oed@pcw.gov.ph | website: www.pcw.gov.ph
                     <br>
                     <br>
                     <h4><b>SERVICE RECORD</b></h4>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-1">
                     NAME
                  </div>
                  <div class="col-xs-2">
                     <u><?php echo $LastName; ?></u>
                     <br>
                     (Surname)
                  </div>
                  <div class="col-xs-3">
                     <u><?php echo $FirstName; ?></u>
                     <br>
                     (Given Name)
                  </div>
                  <div class="col-xs-2">
                     <u><?php echo $MiddleName; ?></u>
                     <br>
                     (Middle Name)
                  </div>
                  <div class="col-xs-3">
                     (If married woman, give also full maiden name)
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-1">
                     BIRTH
                  </div>
                  <div class="col-xs-2">
                     <u><?php echo $BirthDate; ?></u>
                     <br>
                     (Date)
                  </div>
                  <div class="col-xs-3">
                     <u><?php echo $BirthPlace; ?></u>
                     <br>
                     (Place)
                  </div>
                  <div class="col-xs-2">
                     <u><?php echo $MiddleName; ?></u>
                     <br>
                     (Middle Name)
                  </div>
                  <div class="col-xs-3">
                     Data herein should be checked from birth or
                     baptismal certificate or some other reliable
                     documents
                  </div>
               </div>
               <br>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     This is to certify that the above named employee actually rendered services in this Office as shown by the "SERVICE RECORD" below. Each line of which is supported by appointments and other papers actually issued and
                     approved by the authorities concerned
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     <table width="100%">
                        <thead>
                           <tr class="colHEADER">
                              <th colspan="2">Service<br>(Inclusive Dates)</th>
                              <th colspan="3">Record of Appointment</th>
                              <th rowspan="2">Remarks</th>
                              <th rowspan="2">Office<br>Entity/Division/Station/Place<br>of Assignment</th>
                              <th rowspan="2">Branch<br>(3)</th>
                              <th rowspan="2">L/V ABS<br>W/O Pay</th>
                              <th colspan="2">Separation(4)</th>
                           </tr>
                           <tr class="colHEADER">
                              <th>From</th>
                              <th>To</th>
                              <th>Designation</th>
                              <th>Status(1)</th>
                              <th>Annual Salary(2)</th>
                              <th>Date</th>
                              <th>Cause</th>
                           </tr>
                        </thead>
                        <tbody>
                           <?php
                              for ($i=1; $i <= 10 ; $i++) { 
                                 echo '
                                 <tr>
                                    <td>&nbsp;</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                 </tr>
                                 ';
                              }
                           ?>
                        </tbody>
                     </table>
                  </div>
               </div>
               <br>
               <div class="row margin-top">
                  <div class="col-xs-1"></div>
                  <div class="col-xs-11">
                     Issued in compliance with Executive Order No. 54 dated August 10, 1954 and accordance with the Circular No. 58 dated August 10, 1954 of the system
                  </div>
               </div>
               <br>
               <div class="row margin-top">
                  <div class="col-xs-6">
                     <u><?php echo date("F d, Y",time()); ?></u>
                     <br>
                     Date
                  </div>
                  <div class="col-xs-6">
                     Certified Correct:
                     <br>
                     <br>
                     <b><u>JUANA DELA CRUZ</u></b>
                     <br>
                     Head of Office
                     <br>
                     <b><u>Executive Director</u></b>
                     <br>
                     Designation
                  </div>
               </div>
               <?php
                     }
                  }
               ?>
            </div>
         </div>
      </div>
   </body>
</html>