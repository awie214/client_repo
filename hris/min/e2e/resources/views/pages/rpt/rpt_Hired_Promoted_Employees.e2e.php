<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   $table = "employees";
   $whereClause .= " ORDER BY LastName";
   $rsEmployees = SelectEach($table,$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   if ($dbg) {
      echo $whereClause;
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
      <style type="text/css">
         @media print {
            table {
               font-size: 6pt !important;
            }
         }
      </style>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <div class="row" style="page-break-after: always;">
            <div class="col-xs-12">
               <div class="row margin-top">
                  <div class="col-xs-12">
                     <?php
                        rptHeader(getvalue("RptName"));
                     ?>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     <table width="100%">
                        <thead>
                           <tr class="colHEADER">
                              <th>Division</th>
                              <th>Fullname<sup>3</sup></th>
                              <th>Employee<br>No.</th>
                              <th>Nature of<br>Appoinment<sup>2</sup></th>
                              <th>Date<br>Hired/Promoted</th>
                              <th>Position<br>Title</th>
                              <th>Plantilla Item<br>No.</th>
                              <th>Salary</th>
                              <th>Step</th>
                           </tr>
                        </thead>
                        <tbody>
                     <?php
                        if ($rsEmployees) {
                           while ($row = mysqli_fetch_assoc($rsEmployees)) {
                              $emprefid   = $row["RefId"];
                              $LastName   = $row["LastName"];
                              $FirstName  = $row["FirstName"];
                              $MiddleName = $row["MiddleName"];
                              $ExtName    = $row["ExtName"];
                              $FullName   = $LastName.", ".$FirstName." ".$ExtName." ".$MiddleName;
                              $AgencyId   = $row["AgencyId"];
                              $empinfo    = FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'","*");
                              if ($empinfo) {
                                 $Division      = getRecord("division",$empinfo["DivisionRefId"],"Name");
                                 $EmpStatus     = getRecord("empstatus",$empinfo["EmpStatusRefId"],"Name");
                                 $StepIncrement = getRecord("stepincrement",$empinfo["StepIncrementRefId"],"Name");
                                 $PositionItem  = getRecord("PositionItem",$empinfo["PositionItemRefId"],"Name");
                                 $Position      = getRecord("Position",$empinfo["PositionRefId"],"Name");
                                 $HiredDate     = $empinfo["HiredDate"];
                                 if ($HiredDate != "") {
                                    $HiredDate = date("F d, Y",strtotime($HiredDate));
                                 }
                                 $Salary = $empinfo["SalaryAmount"];
                                 if($Salary != "") {
                                    $Salary = number_format($Salary,2);
                                 }
                              } else {
                                 $Division = $EmpStatus = $HiredDate = $Salary = $StepIncrement = $Position = "";
                              }
                              echo '
                                 <tr>
                                    <td>'.$Division.'</td>
                                    <td>'.$FullName.'</td>
                                    <td class="text-center">'.$AgencyId.'</td>
                                    <td class="text-center">'.$EmpStatus.'</td>
                                    <td class="text-center">'.$HiredDate.'</td>
                                    <td class="text-center">'.$Position.'</td>
                                    <td class="text-center">'.$PositionItem.'</td>
                                    <td class="text-center">'.$Salary.'</td>
                                    <td class="text-center">'.$StepIncrement.'</td>
                                 </tr>
                              ';
                     ?>
                     <?php
                           }
                        }
                     ?>
                        </tbody>
                     </table>
                  </div>
               </div>
               <br>
               <br>
               <br>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     <sup>1</sup> Name should be in the format: Surname, Firstname, Middlename
                     <br>
                     <sup>2</sup> Probationary, Transferred, Promoted
                  </div>
               </div>
            </div>
         </div>
      </div>
   </body>
</html>