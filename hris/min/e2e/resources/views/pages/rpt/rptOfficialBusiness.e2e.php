<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   //$whereClause .= " LIMIT 10";
   $table = "employees";
   $rsEmployees = SelectEach($table,$whereClause);
   //echo mysqli_num_rows($rsEmployees);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);

   if ($dbg) {
      echo $whereClause;
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <?php while ($row = mysqli_fetch_assoc($rsEmployees) ) {
         $EmployeesRefId = $row["RefId"];
         $CompanyRefId   = $row["CompanyRefId"];
         $BranchRefId    = $row["BranchRefId"];
         $where  = "WHERE CompanyRefId = $CompanyRefId";
         $where .= " AND BranchRefId = $BranchRefId";
         $where .= " AND EmployeesRefId = $EmployeesRefId";
         $empauth_row = FindFirst("employeesauthority",$where,"*");
         if ($empauth_row) {
            $FiledDate           = $empauth_row["FiledDate"];
            $ApplicationDateFrom = $empauth_row["ApplicationDateFrom"];
            $ApplicationDateTo   = $empauth_row["ApplicationDateTo"];
            $RefId               = $empauth_row["RefId"];
            $Absences            = getRecord("Absences",$empauth_row["AbsencesRefId"],"Name");
            //$ApprovedBy          = getRecord("ApprovedBy",$empauth_row["ApprovedByRefId"],"Name");
            $row_emp             = FindFirst("employees","WHERE RefId = ".$empauth_row["ApprovedByRefId"],"*");
            $ApprovedBy          = $row_emp["FirstName"]." ".$row_emp["LastName"];
         } else {
            $FiledDate = "";
            $ApplicationDateFrom = "";
            $ApplicationDateTo = "";
            $RefId = "";
            $Absences = "";
            $ApprovedBy = "";
         }
      ?>
      <div class="container-fluid rptBody">
         <?php
            rptHeader(getRptName(getvalue("drpReportKind")));
         ?>
         <p class="txt-center">For the Month of : <u><?php echo monthName(date("m",time()),1).", ".date("Y",time()) ?></u></p>
         <p>Division : _____________________________</p>

         <table border="1">
            <tr>
               <th rowspan="2">Employees Name</th>
               <th rowspan="2">Date Filed</th>
               <th colspan="2">Application Date</th>
               <th colspan="2">Covered Time</th>
               <th rowspan="2">Encoded By:</th>
               <th rowspan="2">Updated By:</th>
            </tr>
            <tr>
               <th>From</th>
               <th>To</th>
               <th>From</th>
               <th>To</th>
            </tr>
            <tr>
               <td class="pad-left"><?php echo $row['LastName'].', '.$row['FirstName'].', '.$row['MiddleName'];?></td>
               <td class="txt-center"><?php echo $FiledDate; ?></td>
               <td class="txt-center"><?php echo $ApplicationDateFrom; ?></td>
               <td class="txt-center"><?php echo $ApplicationDateTo; ?></td>
               <td class="txt-center"></td>
               <td class="txt-center"></td>
               <td class="txt-center"><?php echo $Absences; ?></td>
               <td class="txt-center"><?php echo $ApprovedBy; ?></td>
            </tr>
         </table>
         <p>
            <div class="row">
               <div class="col-xs-2 txt-right">Prepared By:</div>
               <div class="col-xs-4"></div>
               <div class="col-xs-2 txt-right">Approved By:</div>
               <div class="col-xs-4"></div>
            </div>
            <div class="row">
               <div class="col-xs-2"></div>
               <div class="col-xs-4">________________________</div>
               <div class="col-xs-2"></div>
               <div class="col-xs-3">________________________</div>
               <div class="col-xs-1"></div>
            </div>
         </p>
         <?php 
            }
         ?>
      </div>
   </body>
</html>