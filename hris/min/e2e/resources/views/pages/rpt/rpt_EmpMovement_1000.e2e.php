<?php
   $emprefid = getvalue("ServiceRecordEmpRefId");
   $whereClause = "WHERE RefId = '$emprefid'";
   $rsEmployees = SelectEach("employees",$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
      <style type="text/css">
         @media print {
            body {
               font-size: 9pt;
            }
            thead {
               font-size: 9pt;  
            }
            tbody {
               font-size: 8pt !important;
            }
         }
      </style>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            $count = 0;
            if ($rsEmployees) {
               while ($row_emp = mysqli_fetch_assoc($rsEmployees)) {
                  $empid = $row_emp["RefId"];
                  $MiddleInitial = substr($row_emp["MiddleName"], 0,1);
                  $FullName = $row_emp["LastName"].", ".$row_emp["FirstName"]." ".$MiddleInitial;
                  if ($row_emp["Sex"] == "F") {
                     if ($row_emp["CivilStatus"] == "Ma") {
                        $MaidenName = $row_emp["MiddleName"];
                     } else {
                        $MaidenName = "";
                     }
                  } else {
                     $MaidenName = "";
                  }
         ?>
         <div style="page-break-after: always;">
            <table style="width: 100%;">
               <thead>
                  <tr>
                     <td colspan="8" class="text-center">
                        <div class="row">
                           <div class="col-xs-3 txt-left">
                           </div>
                           <div class="col-xs-6 txt-center">
                              <div class="fontB11">REPUBLIC OF THE PHILIPPINES</div>
                              <div class="fontB11" style="vertical-align:bottom;">
                                 PHILIPPINE INSTITUTE FOR DEVELOPMENT STUDIES
                              </div>
                              <div class="font10">
                                 18F Three Cyberpod Centris - North Tower <br>
                                 EDSA corner Quezon Avenue, Quezon City
                              </div>
                           </div>
                           <div class="col-xs-3"></div>
                        </div>
                        <br>
                        <div class="txt-center">
                           <div class="fontB10 txt-center">S E R V I C E  R E C O R D</div>
                        </div>
                     </td>
                  </tr>
                  <tr>
                     <td colspan="8">&nbsp;</td>
                  </tr>
                  <tr>
                     <td colspan="8">
                        <div class="row">
                           <div class="col-xs-5" style="border-bottom: 1px solid black;">
                              <?php echo rptDefaultValue($FullName); ?>
                           </div>
                           <div class="col-xs-1"></div>
                           <div class="col-xs-6">
                              If married woman, give also full maiden name
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-xs-5">
                              Surname, Given Name, Middle Name
                           </div>
                           <div class="col-xs-1">
                           </div>
                           <div class="col-xs-6" style="border-bottom: 1px solid black;">
                              <?php echo $MaidenName; ?>
                           </div>
                        </div>
                        <div class="row margin-top">
                           <div class="col-xs-5">BIRTH:</div>
                           <div class="col-xs-1">
                           </div>
                           <div class="col-xs-6">
                              (Date herein should be checked from birth or baptismal certificate or some other reliable documents)
                           </div>
                        </div>
                        <div class="row margin-top">
                           <div class="col-xs-3 text-center">
                              <u>
                                 <?php echo rptDefaultValue(date("d M Y",strtotime($row_emp["BirthDate"]))); ?>
                              </u>
                              <br>
                              Date
                           </div>
                           <div class="col-xs-3 text-center">
                              <u>
                                 <?php echo rptDefaultValue($row_emp["BirthPlace"]); ?>
                              </u>
                              <br>
                              Place
                           </div>

                        </div>
                        
                     </td>
                  </tr>
                  <tr>
                     <td colspan="8">&nbsp;</td>
                  </tr>
                  <tr>
                     <td colspan="8">
                        <p>
                           This is to certify that the employee named herein above actually rendered services in this Office as shown by the service record below, each line of which is supported by appointment and other papers actually issued by this Office and approved by the authorities otherwise indicated.
                        </p>
                     </td>
                  </tr>
                  <tr>
                     <td colspan="8">&nbsp;</td>
                  </tr>
                  <tr class="colHEADER">
                     <th colspan="2">SERVICES<br>(Inclusive Dates)</th>
                     <th colspan="3">RECORD OF APPOINTMENT</th>
                     <th rowspan="2" style="width: 15%;">Station/Place<br>of Assignment</th>
                     <th rowspan="2" style="width: 10%;">Leave of<br>Absence<br>Without Pay</th>
                     <th rowspan="2" style="width: 15%;">Remarks<br>Separation<br>(Reference)</th>
                  </tr>
                  <tr class="colHEADER">
                     <th style="width: 10%;">FROM</th>
                     <th style="width: 10%;">TO</th>
                     <th style="width: 20%;">DESIGNATION</th>
                     <th style="width: 10%;">STATUS</th>
                     <th style="width: 10%;">RATE/YEAR</th>
                  </tr>
               </thead>
               <tbody>
                  <?php
                     $emprefid = $row_emp["RefId"];
                     $emp_movement = SelectEach("employeesmovement","WHERE EmployeesRefId = $emprefid ORDER BY EffectivityDate");
                     if ($emp_movement) {
                        while ($emp_movement_row = mysqli_fetch_assoc($emp_movement)) {
                  ?>
                     <tr>
                        <td class="text-center" valign="top">
                           <?php 
                              if ($emp_movement_row["EffectivityDate"] != "") {
                                 echo rptDefaultValue(date("m/d/Y",strtotime($emp_movement_row["EffectivityDate"])));   
                              }
                           ?>
                        </td>
                        <td class="text-center" valign="top">
                           <?php 
                              if ($emp_movement_row["ExpiryDate"] != "") {
                                 echo rptDefaultValue(date("m/d/Y",strtotime($emp_movement_row["ExpiryDate"]))); 
                              } else if ($emp_movement_row["ExpiryDate"] == "") {
                                 echo "PRESENT";
                              }
                           ?>
                        </td>
                        <td valign="top">
                           <?php echo rptDefaultValue($emp_movement_row["PositionRefId"],"position"); ?>
                        </td>
                        <td class="text-center" valign="top">
                           <?php echo rptDefaultValue($emp_movement_row["ApptStatusRefId"],"apptstatus"); ?>
                        </td>
                        <td class="text-right" valign="top">
                           <?php
                              $SalaryAmount = intval($emp_movement_row["SalaryAmount"] * 12);
                              if ($SalaryAmount != "") {
                                 echo "P ".number_format($SalaryAmount,2);
                              } else {
                                 echo "P ".number_format(0,2);
                              }
                           ?>
                        </td>
                        <td valign="top">
                           <?php echo rptDefaultValue($emp_movement_row["AgencyRefId"],"agency"); ?>
                        </td>
                        <td class="text-center" valign="top">
                           <?php
                              $LWOP = intval($emp_movement_row["LWOP"]);
                              if ($LWOP > 0) {
                                 echo $LWOP;   
                              }
                           ?>
                        </td>
                        <td valign="top">
                           <?php 
                              if ($emp_movement_row["Remarks"] != "") {
                                 echo rptDefaultValue($emp_movement_row["Remarks"]);    
                              } else {
                                 echo rptDefaultValue($emp_movement_row["Cause"]); 
                              }
                              
                           ?>
                        </td>
                     </tr>

                  <?php
                        }
                        echo '
                           <tr>
                              <td class="text-center" colspan="8"><b>* * *NOTHING FOLLOWS* * *</b></td>
                           </tr>
                        ';
                     } else {
                        echo '
                           <tr>
                              <td class="text-center" colspan="8"><b>* * *NO SERVICE RECORD* * *</b></td>
                           </tr>
                        ';
                     }
                  ?>
               </tbody>
            </table>
            <div class="row margin-top">
               <div class="col-xs-12">
                  <br>
                  Issued in compliance with Executive Order No. 54 dated August 10, 1954, and in accordance with Circular No. 58, dated August 10,1954 of the System.
               </div>
            </div>
            <br><br>
            <div class="row margin-top">
               <div class="col-xs-6">
                  DATE: <b><?php echo date("m/d/Y",time()); ?></b>
               </div>
               <div class="col-xs-3">
                  CERTIFIED CORRECT:
               </div>
               <div class="col-xs-3 text-center">
                  <u>
                     <?php echo $Signatory_FullName; ?>
                  </u>
                  <br>
                  <?php echo $Signatory_Position; ?>
               </div>
            </div>
         </div>
         <?php
               }
            } else {
               echo "No Result For Criteria $searchCriteria";
            }
         ?>
      </div>
   </body>
</html>