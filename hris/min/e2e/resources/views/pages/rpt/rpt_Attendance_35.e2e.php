<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   $table = "employees";
   $whereClause .= " ORDER BY LastName";
   $rsEmployees = SelectEach($table,$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   if ($dbg) {
      echo $whereClause;
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
      <style type="text/css">
         @media print {
            table {
               font-size: 6pt !important;
            }
         }
      </style>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <div class="row" style="page-break-after: always;">
            <div class="col-xs-12">
               <div class="row margin-top">
                  <div class="col-xs-12">
                     <?php
                        rptHeader(getvalue("RptName"));
                     ?>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     <table width="100%">
                        <thead>
                           <tr class="colHEADER">
                              <th>Division</th>
                              <th>Fullname<sup>3</sup></th>
                              <th>Total No. of<br>Working Days<sup>3</sup></th>
                              <th>Total Days<br>Present<sup>4</sup></th>
                              <th>%Attendance<sup>56</sup></th>
                           </tr>
                        </thead>
                        <tbody>
                     <?php
                        if ($rsEmployees) {
                           while ($row = mysqli_fetch_assoc($rsEmployees)) {
                              $emprefid   = $row["RefId"];
                              $LastName   = $row["LastName"];
                              $FirstName  = $row["FirstName"];
                              $MiddleName = $row["MiddleName"];
                              $ExtName    = $row["ExtName"];
                              $FullName   = $LastName.", ".$FirstName." ".$ExtName." ".$MiddleName;
                              $AgencyId   = $row["AgencyId"];
                              $empinfo    = FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'","*");
                              if ($empinfo) {
                                 $Division      = getRecord("division",$empinfo["DivisionRefId"],"Name");
                              } else {
                                 $Division = "";
                              }
                              echo '
                                 <tr>
                                    <td>'.$Division.'</td>
                                    <td>'.$FullName.'</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                 </tr>
                              ';
                     ?>
                     <?php
                           }
                        }
                     ?>
                        </tbody>
                     </table>
                  </div>
               </div>
               <br>
               <br>
               <br>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     <sup>1</sup> Adjustable date range for the reporting period
                     <br>
                     <sup>2</sup> Name should be in the format: Surname, Firstname, Middlename
                     <br>
                     <sup>3</sup> Based on actual no. of workdays
                     <br>
                     <sup>4</sup> Based on DTR (assuming an employee reports for at least 4 hours
                     <br>
                     <sup>5</sup> Formula: [Total days present / Total working days] * 100%
                     <br>
                     <sup>6</sup> Table arranged from highest to lowest percentage
                  </div>
               </div>
            </div>
         </div>
      </div>
   </body>
</html>