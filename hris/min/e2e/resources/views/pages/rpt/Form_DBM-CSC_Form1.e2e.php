<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   $table = "employees";
   $whereClause .= " ORDER BY LastName";
   $rsEmployees = SelectEach($table,$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   if ($dbg) {
      echo $whereClause;
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
      <style type="text/css">
         .td-title {
            background: gray;
            font-weight: 600;
         }
      </style>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <div class="row" style="page-break-after: always;">
            <div class="col-xs-12">
               <table style="width: 100%">
                  <tr>
                     <td class="text-center" colspan="3" rowspan="2">
                        <b>
                           Republic of the Philippines
                           <br>
                           POSITION DESCRIPTION FORM
                           <br>
                           DBM-CSC Form No. 1
                           <br>
                        </b>
                        (Revised Version No. 1, s. 2017)
                     </td>
                     <td colspan="3" class="td-title" valign="top">
                        1.  POSITION TITLE (as approved by authorized agency) with parenthetical title
                     </td>
                  </tr>
                  <tr>
                     <td colspan="3">
                        <?php spacer(60); ?>
                     </td>
                  </tr>
                  <tr>
                     <td colspan="3" class="td-title">2.  ITEM NUMBER</td>
                     <td colspan="3" class="td-title">3.  SALARY GRADE</td>
                  </tr>
                  <tr>
                     <td colspan="3">
                        <?php spacer(60); ?>
                     </td>
                     <td colspan="3">
                        <?php spacer(60); ?>
                     </td>
                  </tr>
                  <tr>
                     <td colspan="6" class="td-title">4.  FOR LOCAL GOVERNMENT POSITION, ENUMERATE GOVERNMENTAL UNIT AND CLASS</td>
                  </tr>
                  <tr>
                     <td colspan="6" valign="top">
                        <div class="row" style="padding-left: 10px; padding-right: 10px;">
                           <div class="col-xs-4">
                              <input type="checkbox">&nbsp;Province
                              <br>
                              <input type="checkbox">&nbsp;City
                              <br>
                              <input type="checkbox">&nbsp;Municipality
                           </div>
                           <div class="col-xs-4">
                              <input type="checkbox">&nbsp;1st Class
                              <br>
                              <input type="checkbox">&nbsp;2nd Class
                              <br>
                              <input type="checkbox">&nbsp;3rd Class
                              <br>
                              <input type="checkbox">&nbsp;5th Class
                           </div>
                           <div class="col-xs-4">
                              <input type="checkbox">&nbsp;5th Class
                              <br>
                              <input type="checkbox">&nbsp;6th Class
                              <br>
                              <input type="checkbox">&nbsp;Special
                           </div>

                        </div>
                     </td>
                  </tr>
                  <tr>
                     <td colspan="3" class="td-title">5.  DEPARTMENT, CORPORATION OR AGENCY/LOCAL GOVERNMENT</td>
                     <td colspan="3" class="td-title">6.  BUREAU OR OFFICE</td>
                  </tr>
                  <tr>
                     <td colspan="3">
                        <?php spacer(60); ?>
                     </td>
                     <td colspan="3">
                        <?php spacer(60); ?>
                     </td>
                  </tr>
                  <tr>
                     <td colspan="3" class="td-title">7.  DEPARTMENT / BRANCH / DIVISION</td>
                     <td colspan="3" class="td-title">8.  WORKSTATION / PLACE OF WORK</td>
                  </tr>
                  <tr>
                     <td colspan="3">
                        <?php spacer(60); ?>
                     </td>
                     <td colspan="3">
                        <?php spacer(60); ?>
                     </td>
                  </tr>
                  <tr>
                     <td class="td-title">
                        9.  PRESENT APPROP ACT
                     </td>
                     <td colspan="2" class="td-title">
                        10.  PREVIOUS APPROP ACT
                     </td>
                     <td class="td-title">
                        11.  SALARY AUTHORIZED
                     </td>
                     <td colspan="2" class="td-title">
                        12.  OTHER COMPENSATION
                     </td>
                  </tr>
                  <tr>
                     <td>
                        <?php spacer(60); ?>
                     </td>
                     <td colspan="2">
                        <?php spacer(60); ?>
                     </td>
                     <td>
                        <?php spacer(60); ?>
                     </td>
                     <td colspan="2">
                        <?php spacer(60); ?>
                     </td>
                  </tr>
                  <tr>
                     <td colspan="3" class="td-title">13.  POSITION TITLE OF IMMEDIATE SUPERVISOR</td>
                     <td colspan="3" class="td-title">14.  POSITION TITLE OF NEXT HIGHER SUPERVISOR</td>
                  </tr>
                  <tr>
                     <td colspan="3">
                        <?php spacer(60); ?>
                     </td>
                     <td colspan="3">
                        <?php spacer(60); ?>
                     </td>
                  </tr>
                  <tr>
                     <td colspan="6" class="td-title">15.  POSITION TITLE, AND ITEM OF THOSE DIRECTLY SUPERVISED</td>
                  </tr>
                  <tr>
                     <td colspan="6" class="text-center"><i>(if more than seven (7) list only by their item numbers and titles)</i></td>
                  </tr>
                  <tr>
                     <td colspan="3" class="td-title">POSITION TITLE</td>
                     <td colspan="3" class="td-title">ITEM NUMBER</td>
                  </tr>
                  <tr>
                     <td colspan="3">
                        <?php spacer(60); ?>
                     </td>
                     <td colspan="3">
                        <?php spacer(60); ?>
                     </td>
                  </tr>
                  <tr>
                     <td colspan="6" class="td-title">16.  MACHINE, EQUIPMENT, TOOLS, ETC., USED REGULARLY IN PERFORMANCE OF WORK</td>
                  </tr>
                  <tr>
                     <td colspan="6">
                        <?php spacer(60); ?>
                     </td>
                  </tr>
                  <tr>
                     <td colspan="6" class="td-title">17.  CONTACTS / CLIENTS / STAKEHOLDERS</td>
                  </tr>
                  <tr>
                     <td class="td-title" style="width: 24.14%; text-align: center;">17a. Internal</td>
                     <td class="td-title" style="width: 13%; text-align: center;">Occasional</td>
                     <td class="td-title" style="width: 12.86%; text-align: center;">Frequent</td>
                     <td class="td-title" style="width: 24.14%; text-align: center;">17b. External</td>
                     <td class="td-title" style="width: 13%; text-align: center;">Occasional</td>
                     <td class="td-title" style="width: 12.86%; text-align: center;">Frequent</td>
                  </tr>
                  <tr>
                     <td>Executive / Managerial</td>
                     <td class="text-center"><input type="checkbox"></td>
                     <td class="text-center"><input type="checkbox"></td>
                     <td>General Public</td>
                     <td class="text-center"><input type="checkbox"></td>
                     <td class="text-center"><input type="checkbox"></td>
                  </tr>
                  <tr>
                     <td>Supervisors</td>
                     <td class="text-center"><input type="checkbox"></td>
                     <td class="text-center"><input type="checkbox"></td>
                     <td>Other Agencies</td>
                     <td class="text-center"><input type="checkbox"></td>
                     <td class="text-center"><input type="checkbox"></td>
                  </tr>
                  <tr>
                     <td>Non-Supervisors</td>
                     <td class="text-center"><input type="checkbox"></td>
                     <td class="text-center"><input type="checkbox"></td>
                     <td>Others (Please Specify):</td>
                     <td colspan="2"></td>
                  </tr>
                  <tr>
                     <td>Staff</td>
                     <td class="text-center"><input type="checkbox"></td>
                     <td class="text-center"><input type="checkbox"></td>
                     <td colspan="3"></td>
                  </tr>
                  <tr>
                     <td colspan="6" class="td-title">18.  WORKING CONDITION</td>
                  </tr>
                  <tr>
                     <td>Office Work</td>
                     <td class="text-center"><input type="checkbox"></td>
                     <td class="text-center"><input type="checkbox"></td>
                     <td>Others (Please Specify):</td>
                     <td colspan="2"></td>
                  </tr>
                  <tr>
                     <td>Field Work</td>
                     <td class="text-center"><input type="checkbox"></td>
                     <td class="text-center"><input type="checkbox"></td>
                     <td colspan="3"></td>
                  </tr>
                  <tr>
                     <td colspan="6" class="td-title">
                        19.  BRIEF DESCRIPTION OF THE GENERAL FUNCTION OF THE UNIT OR SECTION
                     </td>
                  </tr>
                  <tr>
                     <td colspan="6">
                        <?php spacer(60); ?>
                     </td>
                  </tr>
                  <tr>
                     <td colspan="6" class="td-title">
                        20.  BRIEF DESCRIPTION OF THE GENERAL FUNCTION OF THE POSITION (Job Summary)
                     </td>
                  </tr>
                  <tr>
                     <td colspan="6">
                        <?php spacer(120); ?>
                     </td>
                  </tr>
                  <tr>
                     <td colspan="6" class="td-title">
                        21.  QUALIFICATION STANDARDS
                     </td>
                  </tr>
                  <tr>
                     <td class="td-title">
                        21a.  Education
                     </td>
                     <td colspan="2" class="td-title">
                        21b.  Experience  
                     </td>
                     <td class="td-title">
                        21c.  Training
                     </td>
                     <td colspan="2" class="td-title">
                        21d.  Eligibility 
                     </td>
                  </tr>
                  <tr>
                     <td>
                        <?php spacer(60); ?>
                     </td>
                     <td colspan="2">
                        <?php spacer(60); ?>
                     </td>
                     <td>
                        <?php spacer(60); ?>
                     </td>
                     <td colspan="2">
                        <?php spacer(60); ?>
                     </td>
                  </tr>
                  <tr>
                     <td colspan="4" class="td-title">21e.  Core Competencies</td>
                     <td colspan="2" class="td-title">Competency Level</td>
                  </tr>
                  <tr>
                     <td colspan="4" style="padding-top: 30px; padding-bottom: 30px;" class="text-center">(Indicate the required Leadership Competencies here)</td>
                     <td colspan="2" style="padding-top: 30px; padding-bottom: 30px;" class="text-center">(Indicate the required Competency Level here)</td>
                  </tr>
                  <tr>
                     <td colspan="4" class="td-title">21f.  Leadership Competencies</td>
                     <td colspan="2" class="td-title">Competency Level</td>
                  </tr>
                  <tr>
                     <td colspan="4" style="padding-top: 30px; padding-bottom: 30px;" class="text-center">(Indicate the required Leadership Competencies here)</td>
                     <td colspan="2" style="padding-top: 30px; padding-bottom: 30px;" class="text-center">(Indicate the required Competency Level here)</td>
                  </tr>
                  <tr>
                     <td colspan="4" class="td-title">22.  STATEMENT OF DUTIES AND RESPONSIBILITIES (Technical Competencies)</td>
                     <td colspan="2" class="td-title">Competency Level</td>
                  </tr>
                  <tr>
                     <td class="text-center">Percentage of Working Time</td>
                     <td colspan="3" class="text-center">(State the duties and responsibilities here:)</td>
                     <td colspan="2" rowspan="2" style="padding-top: 30px; padding-bottom: 30px;" class="text-center">
                        (Indicate the required Competency Level here)
                     </td>
                  </tr>
                  <tr>
                     <td>
                        <?php spacer(40); ?>
                     </td>
                     <td colspan="3">
                        <?php spacer(40); ?>
                     </td>
                  </tr>
                  <tr>
                     <td colspan="6" class="td-title">23.  ACKNOWLEDGMENT AND ACCEPTANCE:</td>
                  </tr>
                  <tr>
                     <td colspan="6">
                        <div class="row">
                           <div class="col-xs-12 text-center" style="text-indent: 5px;">
                              <br>
                              I have received a copy of this position description. It has been discussed with me and I have freely chosen to comply with the performance and behavior/conduct expectations contained herein.
                           </div>
                        </div>
                        <br>
                        <br>
                        <br>
                        <div class="row margin-top">
                           <div class="col-xs-6 text-center">
                              _________________________________
                              <br>
                              <b>Employee's Name, Date and Signature</b>
                           </div>
                           <div class="col-xs-6 text-center">
                              _________________________________
                              <br>
                              <b>Supervisor's Name, Date and Signature</b>
                           </div>
                        </div>
                     </td>
                  </tr>
               </table>
            </div>
         </div>
      </div>
   </body>
</html>