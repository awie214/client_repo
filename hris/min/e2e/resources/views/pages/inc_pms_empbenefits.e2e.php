<script>
   $(document).ready(function () {
      $(".newDataLibrary").hide();
      $(".createSelect--").css("width","100%");
   });
</script>
<div class="mypanel">
   <div class="row" style="height:200px;overflow:auto; padding: 5px;">
      <table border="1" width="100%">
         <tr  class="txt-center" style="padding:5px;">
            <td><label>Deduction Name</label></td>
            <td><label>Amount</label></td>
            <td><label>Date Start</label></td>
            <td><label>Date End</label></td>
            <td><label>Terminated</label></td>
         </tr>
      <?php
         $rs = SelectEach("pms_employeesbenefits","");
         if ($rs) {
            $recordNum = mysqli_num_rows($rs);
            while ($row = mysqli_fetch_array($rs)) {
      ?>
            <tr>
               <td><?php echo $row["FullName"]?></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
            </tr>
      <?php 
            }
         }
      ?>
      </table>
   </div>
   <?php spacer(20);?>
   <div class="mypanel">
      <div class="panel-top">Detail</div>
      <div class="panel-mid">
         <div class="row">
            <div class="col-xs-12">
               <input type="checkbox" class="enabler--" name="chkBenefitsInfo" id="chkBenefitsInfo" for="empbenefits">
               <label id="enable" for="chkBenefitsInfo">Enable Fields</label>
            </div> 
         </div>
         <?php bar(); ?>
         <div class="row margin-top" id="empbenefits">
            <div class="col-xs-12">
               <div class="row margin-top">
                  <div class="col-xs-2 label">
                     <label>Effectivity Date:</label>
                  </div>
                  <div class="col-xs-2">
                     <input type="text" name="Effectivity" class="saveFields-- form-input date--">
                  </div>
                  <div class="col-xs-2">
                     <input type="checkbox" name="chkWithEndDate" class="saveFields--">&nbsp;<label>With End Date</label>
                  </div>
                  <div class="col-xs-2">
                     <input type="text" name="WithEndDate" class="saveFields-- form-input date--">
                  </div>
                  <div class="col-xs-2 label">
                     <label>Tax Type:</label>
                  </div>
                  <div class="col-xs-2">
                     <select class="form-input saveFields--" name="TaxType" >
                        <option></option>
                     </select>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-2 label">
                     <label>Allowance:</label>
                  </div>
                  <div class="col-xs-6">
                     <select class="form-input saveFields--" name="Allowance" >
                        <option></option>
                     </select>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-2 label">
                     <label>Description:</label>
                  </div>
                  <div class="col-xs-6">
                     <input type="text" name="Description" class="saveFields-- form-input date--">
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-2 label">
                     <label>Pay Period:</label>
                  </div>
                  <div class="col-xs-2">
                     <select class="form-input saveFields--" name="BAPayPeriod">
                        <option></option>
                        <option>Monthly</option>
                        <option>Weekly</option>
                     </select>
                  </div>
                  <div class="col-xs-2 label">
                     <label>Amount:</label>
                  </div>
                  <div class="col-xs-2">
                     <input type="text" name="Amount" class="saveFields-- form-input date--">
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-2 label">
                     <label>Pay Rate:</label>
                  </div>
                  <div class="col-xs-2">
                     <input type="text" name="BAPayRate" class="saveFields-- form-input date--">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="panel-bottom"></div>
   </div>
</div>