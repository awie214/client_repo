<?php

   $code = "";
   
   require_once "inc_model.e2e.php";
   require_once "incUtilitiesJS.e2e.php";
?>
<script type="text/javascript">
</script>
   <div class="container" id="EntryScrn">
      <input class="form-input saveFields--" type="hidden" name="char_CompanyCode" value="<?php echo getvalue("ccode"); ?>">
      <div class="row">
         <div class="col-xs-9">
            <?php if ($ScrnMode != 1) { ?>
               <div class="row">
                  <ul class="nav nav-pills">
                     <li class="active" style="font-size:12pt;font-weight:600;">
                        <a>REFID : <span class="badge" style="font-size:12pt;font-weight:600;" id="idRefid">
                        <?php echo $refid; ?>
                        </span></a>
                     </li>
                  </ul>
               </div>
            <?php } ?>
            <div class="row margin-top10">
               <div class="form-group">
                  <div class="col-xs-12">
                     <label class="control-label" >CODE:</label><br>
                     <input class="form-input saveFields-- uCase-- mandatory" type="text" 
                     placeholder="Code" <?php if ($ScrnMode == 2) { echo "disabled"; } else { echo $disabled; }  ?> 
                     name="char_Code" style='width:50%' value="<?php echo $code; ?>" autofocus>   
                  </div>
               </div>
            </div>
            <div class="row margin-top">
               <div class="form-group">
                  <div class="col-xs-12">
                     <label class="control-label" for="inputs">NAME:</label><br>
                     <input class="form-input saveFields-- mandatory" type="text" placeholder="Name" <?php echo $disabled; ?> 
                     name="char_Name" value="">
                  </div>
               </div>
            </div>
            <div class="row margin-top">
               <div class="form-group">
                  <div class="col-xs-6">
                     <label class="control-label" for="inputs">FILE NAME:</label><br>
                     <input class="form-input saveFields-- mandatory" type="text" placeholder="File Name" <?php echo $disabled; ?> 
                     name="char_Filename" value="">
                  </div>
                  <div class="col-xs-6">
                     <label class="control-label">System:</label><br>
                     <select class="form-input saveFields-- mandatory" name="sint_SystemRefId">
                        <option value="">SELECT SYSTEM</option>
                        <?php
                           $rs = SelectEach("system","");
                           if ($rs) {
                              while ($sys = mysqli_fetch_assoc($rs)) {
                                 echo '<option value="'.$sys["RefId"].'">'.($sys["Code"]." - ".$sys["Name"]).'</option>';
                              }
                           }
                        ?>
                     </select>
                  </div>
               </div>
            </div>
            <div class="row margin-top">
               <div class="form-group">
                  <div class="col-xs-4">
                     <label class="control-label" for="inputs">Owner:</label><br>
                     <select class="form-input saveFields-- mandatory" name="char_Owner">
                        <option value="E2E">E2E</option>
                        <option value="Agency">Agency</option>
                     </select>
                  </div>
                  <div class="col-xs-4">
                     <label class="control-label" for="inputs">Ext. Type:</label><br>
                     <select class="form-input saveFields-- mandatory" name="char_ExtType">
                        <option value="php">php</option>
                        <option value="htm">htm</option>
                     </select>
                  </div>
                  <div class="col-xs-4">
                     <label class="control-label" for="inputs">For:</label><br>
                     <input class="form-input saveFields--" type="text" name="char_For" value="">
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="form-group">
                  <div class="col-xs-6">
                     <label class="control-label" for="inputs">REMARKS:</label>
                     <textarea class="form-input saveFields--" rows="5" name="char_Remarks" <?php echo $disabled; ?>
                     placeholder="remarks"><?php echo $remarks; ?></textarea>   
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>