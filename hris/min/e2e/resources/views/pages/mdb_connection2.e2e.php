<?php

    $dbName = $_SERVER["DOCUMENT_ROOT"] . "/PCC/hris/min/e2e/database/biometrics/att2000_2.mdb"; /*local*/

    include 'conn.e2e.php';
    include 'constant.e2e.php';
    include pathClass.'0620functions.e2e.php';

    if (!file_exists($dbName)) {
       die("Could not find database file.");
    }

    $driver = 'MDBTools';
    $dataSourceName = "odbc:Driver=$driver;DBQ=$dbName;Uid=;Pwd=;";
    $connection = new PDO($dataSourceName);
    $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $today = date("j/n/Y",time());

    $query = 'SELECT USERID, CHECKTIME, CHECKTYPE, VERIFYCODE, SENSORID, sn FROM CHECKINOUT WHERE USERID = 2 AND VERIFYCODE = 1';
    $result = $connection->query($query)->fetchAll(PDO::FETCH_ASSOC);
    echo
    '<TABLE border=1>
        <tr>
            <td>CHECK TIME</td>
            <td>CHECK TYPE</td>
            <td>DATE</td>
            <td>TIME</td>
            <td>TERMINAL</td>
        </tr>';
        foreach($result as $row) {


            $utc = strtotime($row["CHECKTIME"]);
            $d = date("Y-m-d",$utc);
            $t = date("h:i:s A",$utc);
            switch ($row["CHECKTYPE"]) {
                case "I":
                    $type = 1;
                    break;
                case "0":
                    $type = 2;
                    break;
                case "1":
                    $type = 3;
                    break;
                case "O":
                    $type = 4;
                    break;
            }
            $terminal = $row["SENSORID"];

            if ($d == "2017-12-19") {
               echo
               '<tr>
                   <td>'.$row["CHECKTIME"]." - ".$utc.'</td>
                   <td>'.$type.'</td>
                   <td>'.$d.'</td>
                   <td>'.$t.'</td>
                   <td>'.$terminal.'</td>
               </tr>';
            }


        /*$curr_d = date("Y-m-d",time());
        $curr_t = date("h:i:s",time());
        $flds = "`CompanyRefId`, `BranchRefId`, `EmployeesRefId`, `AttendanceDate`, `AttendanceTime`, `KindOfEntry`, `Terminal`,";
        $flds .= "`LastUpdateDate`, `LastUpdateTime`, `LastUpdateBy`, `Data`";
        $vals = "'2', '1', '116', '$d', '$utc', '$type', '$terminal',";
        $vals .= "'$curr_d', '$curr_t', 'ADMIN', 'A'";
        $sql_att = "INSERT INTO `employeesattendance` ($flds) VALUES ($vals)";
        $rs = mysqli_query($conn,$sql_att);
        if (!$rs) {
            echo "Error in Saving.";
        }*/
    }

    echo
    '</table>';

    $result = null;
    $connection = null;

?>