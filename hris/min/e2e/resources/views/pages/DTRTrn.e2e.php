<?php
   error_reporting(E_ALL);
   ini_set('display_errors', 1);
   session_start();
   include_once "constant.e2e.php";
   include_once pathClass.'0620functions.e2e.php';
   include_once pathClass.'DTRFunction.e2e.php';
   include_once pathClass.'SysFunctions.e2e.php';
   include_once pathClass.'0620TrnData.e2e.php';
   $trn = new Transaction();
   $user = getvalue("hUser");


   function fndelete_logs() {
      include_once 'conn.e2e.php';
      $emprefid = getvalue("emprefid");
      $selected_date = getvalue("date");
      $kentry = getvalue("kentry");
      $objname = getvalue("obj");

      $str = $emprefid." -> ".$selected_date." -> ".$kentry." -> ".$objname;
      
      $where = "WHERE EmployeesRefId = '$emprefid' AND AttendanceDate = '$selected_date'";
      $where .= " AND KindOfEntry = '$kentry'";
      $refid = FindFirst("employeesattendance",$where,"RefId");
      if ($refid) {
         $check_remark = FindFirst("dtr_remarks","WHERE EmployeesRefId = '$emprefid' AND AttendanceDate = '$selected_date'","RefId");
         if ($check_remark) {
            $delete_sql_remarks  = "DELETE FROM dtr_remarks WHERE RefId = '$check_remark'";
            $delete_rs_remarks   = mysqli_query($conn,$delete_sql);   
         }
         $delete_sql = "DELETE FROM employeesattendance WHERE RefId = '$refid'";
         $delete_rs = mysqli_query($conn,$delete_sql);
         if ($delete_rs) {
            echo '$.notify("Succesfully Deleted.");'."\n";   
            echo 'setValueByName("'.$objname.'","");';
         }
      } else {
         echo '$.notify("Biometrics Data Cannot Be Deleted.");';
      }
   }
   function fndtr_process() {
      $emprefid = getvalue("emprefid");
      $month    = getvalue("month");
      $workschedule = getvalue("workschedule");
      $dtr_array     = DTR_Summary ($emprefid,$month,date("Y",time()),$workschedule);
      $flds          = "";
      $vals          = ""; 
      $table         = "dtr_process";
      $fldnval       = "";
      foreach ($dtr_array as $key => $value) {
         $flds .= "`$key`,";
         $vals .= "'$value', ";
         $fldnval .= "`$key` = '$value', ";
      }
      $check = FindFirst("dtr_process","WHERE EmployeesRefId = '$emprefid' AND Month = '$month' AND Year = '".date("Y",time())."' ","RefId");
      if ($check) {
         $update_dtr_process = f_SaveRecord("EDITSAVE",$table,$fldnval,$check);   
         if ($update_dtr_process == "") {
            echo 2;
         } else {
            echo 0;
         }
      } else {
         $save_dtr_process = f_SaveRecord("NEWSAVE",$table,$flds,$vals);
         if (is_numeric($save_dtr_process)) {
            echo 1;
         } else {
            echo 0;
         }   
      }
      
   }
   function fnGetMonthAttendance() {
      include 'conn.e2e.php';
      $curr_month       = date("m",time());
      $userID           = 0;
      $CompanyID        = getvalue("hCompanyID");
      $BranchID         = getvalue("hBranchID");
      $Month            = getvalue("Month") + 1;
      $Year             = getvalue("Year");
      $LastDayofMonth   = getvalue("LastDay");
      $WorkSchedule     = "";

      if ($CompanyID == "2") {
         if ($Year == "2018" && $Month <= 8) {
            echo '$("#pop_dtr").prop("disabled",true);';
            return false;
         }
      }
      if ((intval($Month) > intval($curr_month)) || $Year != date("Y",time())) {
         echo '$("#pop_dtr").prop("disabled",true);';
         return false;
      } else {
         echo '$("#pop_dtr").prop("disabled",false);';
      }
      $KEntryContent = file_get_contents(json."Settings_".$CompanyID.".json");
      $KEntry_json   = json_decode($KEntryContent, true);
      
      if ($Month <= 9) {
         $Month = "0".$Month;
      }
      $start_this_month = $Year."-".$Month."-01";
      $end_this_month = $Year."-".$Month."-".$LastDayofMonth;

      $EmpRefId = getvalue("EmpRefId");
      if ($EmpRefId == "") {
         $EmpRefId = getvalue("hEmpRefId");
      }

      //=============================================================//
      $arr = array();
      for ($i=1; $i <= $LastDayofMonth; $i++) { 
         if ($i <= 9) {
            $i = "0".$i;
         }
         $curr_date = $Year."-".$Month."-".$i;
         //echo $curr_date."\n";
         $dtr = [
            "AttendanceDate" => "$curr_date",
            "Has" => "",
            "Holiday" => "",
            "OffSus"=>"",
            "OffSusTime"=>""
         ];
         $arr["ARR"][$curr_date] = $dtr;
      }
      //=============================================================//
      
      $biometricsID = FindFirst("employees","WHERE RefId = '$EmpRefId'","BiometricsID");
      if (!$biometricsID) {
         //echo '$.notify("Employee No '.$EmpRefId.' is invalid.");'; 
         echo '$.notify("No Biometrics ID");'; 
         return false;
      }
      /*****************/
      
      $has_worksched = FindFirst("empinformation","WHERE EmployeesRefId = '$EmpRefId'","*");
      if ($has_worksched) {
         if ($has_worksched["WorkScheduleRefId"] == "") {
            echo '$.notify("Employee '.$EmpRefId.' has no workschedule yet");';
            echo '$("#pop_dtr").prop("disabled",true);';
         } else {
            $WorkSchedule = $has_worksched["WorkScheduleRefId"];
         }
      } else {
         echo '$.notify("Employee '.$EmpRefId.' has no workschedule yet");';
         echo '$("#pop_dtr").prop("disabled",true);';
      }
      

      $where = "WHERE CompanyRefId = $CompanyID AND BranchRefId = $BranchID AND EmployeesRefId = '$EmpRefId'";
      $where .= " AND AttendanceDate BETWEEN '".$start_this_month."' AND '".$end_this_month."'";
      $rs_empAttendance = SelectEach("employeesattendance",$where);
      if ($rs_empAttendance) {
         while ($row = mysqli_fetch_assoc($rs_empAttendance)) {
            $fulldate = date("Y-m-d",$row["AttendanceTime"]);
            $utc = $row["CheckTime"];
            $AttendanceFormatted = date("h:i A",$utc);
            if(isset($arr["ARR"][$fulldate])) {
               $arr["ARR"][$fulldate]["Has"] = 1;
            }
            switch ($row["KindOfEntry"]) {
               case 1:
                  echo '$("#TI_'.$fulldate.'").html("'.$AttendanceFormatted.'");'."\n";
                  echo '$("#TI_'.$fulldate.'").attr("checktime","'.$utc.'");'."\n";
                  break;
               case 2:
                  echo '$("#LO_'.$fulldate.'").html("'.$AttendanceFormatted.'");'."\n";
                  echo '$("#LO_'.$fulldate.'").attr("checktime","'.$utc.'");'."\n";
                  break;
               case 3:
                  echo '$("#LI_'.$fulldate.'").html("'.$AttendanceFormatted.'");'."\n";
                  echo '$("#LI_'.$fulldate.'").attr("checktime","'.$utc.'");'."\n";
                  break;
               case 4:
                  echo '$("#TO_'.$fulldate.'").html("'.$AttendanceFormatted.'");'."\n";
                  echo '$("#TO_'.$fulldate.'").attr("checktime","'.$utc.'");'."\n";
                  break;
               case 7:
                  echo '$("#OBO_'.$fulldate.'").html("'.$AttendanceFormatted.'");'."\n";
                  echo '$("#OBO_'.$fulldate.'").attr("checktime","'.$utc.'");'."\n";
                  break;
               case 8:
                  echo '$("#OBI_'.$fulldate.'").html("'.$AttendanceFormatted.'");'."\n";
                  echo '$("#OBI_'.$fulldate.'").attr("checktime","'.$utc.'");'."\n";
                  break;
            }
         }
      }


      switch ($CompanyID) {
         case '2':
            include 'inc/inc_bio_trn_get_month_attendance_2.e2e.php';
            break;
         case '35':
            include 'inc/inc_bio_trn_get_month_attendance_2.e2e.php';
            break;
         case '28':
            include 'inc/inc_bio_trn_get_month_attendance_28.e2e.php';
            break;
         case '14':
            include 'inc/inc_bio_trn_get_month_attendance_14.e2e.php';
            break;
         default:
            include 'inc/inc_bio_trn_get_month_attendance_default.e2e.php';
            break;
      }
      $holiday = SelectEach("holiday","");
      if ($holiday) {
         while ($row = mysqli_fetch_assoc($holiday)) {
            $StartDate        = $row["StartDate"];
            $EndDate          = $row["EndDate"];
            $holiday_date_diff   = dateDifference($StartDate,$EndDate);
            $Name             = $row["Name"];
            $EveryYr          = $row["isApplyEveryYr"];
            $Legal            = $row["isLegal"];
            $temp_arr         = explode("-", $StartDate);
            $temp_date        = $Year."-".$temp_arr[1]."-".$temp_arr[2];
            if ($holiday_date_diff > 1) {
               for ($H=0; $H <= $holiday_date_diff-1; $H++) { 
                  $holiday_NewDate = date('Y-m-d', strtotime($temp_date. ' + '.$H.' days'));
                  if ($EveryYr == 1) {
                     if(isset($arr["ARR"][$holiday_NewDate])) {
                        $arr["ARR"][$holiday_NewDate]["Holiday"] = $Name;
                     }   
                  } else {
                     if(isset($arr["ARR"][$holiday_NewDate])) {
                        $arr["ARR"][$holiday_NewDate]["Holiday"] = $Name;
                     }
                  }
               }
            } else {
               if ($EveryYr == 1) {
                  if(isset($arr["ARR"][$temp_date])) {
                     $arr["ARR"][$temp_date]["Holiday"] = $Name;
                  }   
               } else {
                  if(isset($arr["ARR"][$StartDate])) {
                     $arr["ARR"][$StartDate]["Holiday"] = $Name;
                  }
               }
            }               
         }
      }
      $officesuspension = SelectEach("officesuspension","");
      if ($officesuspension) {
         while ($row = mysqli_fetch_assoc($officesuspension)) {
            $StartDate        = $row["StartDate"];
            $EndDate          = $row["EndDate"];
            $OffSus_date_diff = dateDifference($StartDate,$EndDate);
            $Name             = $row["Name"];
            $OffSusTime       = $row["StartTime"];
            if ($OffSus_date_diff > 1) {
               for ($OS=0; $OS <= $OffSus_date_diff-1 ; $OS++) { 
                  $OffSus_NewDate = date('Y-m-d', strtotime($StartDate. ' + '.$OS.' days'));
                  if(isset($arr["ARR"][$OffSus_NewDate])) {
                     $arr["ARR"][$OffSus_NewDate]["OffSus"]      = $Name;
                     $arr["ARR"][$OffSus_NewDate]["OffSusTime"] = $OffSusTime;
                  }   
               }   
            } else {
               if(isset($arr["ARR"][$StartDate])) {
                  $arr["ARR"][$StartDate]["OffSus"]      = $Name;
                  $arr["ARR"][$StartDate]["OffSusTime"]  = $OffSusTime;
               }
            }
         }
      }
      foreach ($arr as $value) {
         foreach ($value as $key => $row) {
            if (isset($row["AttendanceDate"])) {
               $fulldate = $row["AttendanceDate"];
               $Has = $row["Has"];
               if ($Has != 1) {
                  $where_leave = "WHERE CompanyRefId = $CompanyID AND BranchRefId = $BranchID AND EmployeesRefId = '$EmpRefId'";
                  $where_leave .= " AND ApplicationDateFrom <= '".$fulldate."' AND ApplicationDateTo >= '".$fulldate."'";
                  $where_leave .= " AND Status = 'Approved'";
                  $check_leave = FindFirst("employeesleave",$where_leave,"`LeavesRefId`,`isForceLeave`");
                  if ($check_leave) {
                     $where_worksched = "WHERE CompanyRefId = $CompanyID";
                     $where_worksched .= " AND BranchRefId = $BranchID";
                     $where_worksched .= " AND EmployeesRefId = '$EmpRefId'";
                     $WorkScheduleRefId = FindFirst("empinformation",$where_worksched,"WorkScheduleRefId");
                     $row_worksched = FindFirst("workschedule","WHERE RefId = $WorkScheduleRefId","*");
                     $leave_day = date("D",strtotime($fulldate));
                     switch ($leave_day) {
                        case 'Mon':
                           $leave_day = "Monday";
                           break;
                        case 'Tue':
                           $leave_day = "Tuesday";
                           break;
                        case 'Wed':
                           $leave_day = "Wednesday";
                           break;
                        case 'Thu':
                           $leave_day = "Thursday";
                           break;
                        case 'Fri':
                           $leave_day = "Friday";
                           break;
                        case 'Sat':
                           $leave_day = "Saturday";
                           break;
                        case 'Sun':
                           $leave_day = "Sunday";
                           break;
                     }
                     if ($row_worksched[$leave_day."isRestDay"] != 1) {
                        $leave = getRecord("leaves",$check_leave["LeavesRefId"],"Name");
                        $fl = $check_leave["isForceLeave"];
                        if ($fl == 1) {
                           echo '$("#content_'.$fulldate.'").html("Force Leave");'."\n";   
                        } else {
                           echo '$("#content_'.$fulldate.'").html("'.$leave.'");'."\n";      
                        }   
                     }
                  }


                  /*=========================================================================================================*/
                  //GET CTO FILED
                  /*=========================================================================================================*/
                  $where_cto = "WHERE CompanyRefId = $CompanyID AND BranchRefId = $BranchID AND EmployeesRefId = '$EmpRefId'";
                  $where_cto .= " AND ApplicationDateFrom <= '".$fulldate."' AND ApplicationDateTo >= '".$fulldate."'";
                  $where_cto .= " AND Status = 'Approved'";
                  $row_cto = FindFirst("employeescto",$where_cto,"*");
                  if ($row_cto) {
                     $day_name = date("D",strtotime($fulldate));
                     if ($day_name != "") {
                        switch ($day_name) {
                           case 'Mon':
                              $day_name = "Monday";
                              break;
                           case 'Tue':
                              $day_name = "Tuesday";
                              break;
                           case 'Wed':
                              $day_name = "Wednesday";
                              break;
                           case 'Thu':
                              $day_name = "Thursday";
                              break;
                           case 'Fri':
                              $day_name = "Friday";
                              break;
                           case 'Sat':
                              $day_name = "Saturday";
                              break;
                           case 'Sun':
                              $day_name = "Sunday";
                              break;
                        }
                     }
                     $where_worksched = "WHERE CompanyRefId = $CompanyID";
                     $where_worksched .= " AND BranchRefId = $BranchID";
                     $where_worksched .= " AND EmployeesRefId = '$EmpRefId'";
                     $WorkScheduleRefId = FindFirst("empinformation",$where_worksched,"WorkScheduleRefId");
                     $rs_worksched = FindFirst("workschedule","WHERE RefId = $WorkScheduleRefId","*");
                     $day_in                    = $day_name."In";
                     $day_out                   = $day_name."Out";
                     $day_LBOut                 = $day_name."LBOut";
                     $day_LBIn                  = $day_name."LBIn";
                     $data_timein               = $rs_worksched[$day_in];
                     $data_timeout              = $rs_worksched[$day_out];
                     $data_LunchOut             = $rs_worksched[$day_LBOut];
                     $data_LunchIn              = $rs_worksched[$day_LBIn];
                     $coreTime                  = ($data_timeout - $data_timein) - ($data_LunchIn - $data_LunchOut);
                     $coreTime                  = $coreTime / 60;
                     if ($rs_worksched[$day_name."isRestDay"] != 1) {
                        if ($coreTime <= $row_cto["Hours"]) {
                           echo '$("#content_'.$fulldate.'").html("CTO Filed");'."\n";   
                        }
                     }
                  }
               } else {
                  $where_leave = "WHERE CompanyRefId = $CompanyID AND BranchRefId = $BranchID AND EmployeesRefId = '$EmpRefId'";
                  $where_leave .= " AND ApplicationDateFrom <= '".$fulldate."' AND ApplicationDateTo >= '".$fulldate."'";
                  $where_leave .= " AND Status = 'Approved'";
                  //echo $where_leave."\n";
                  $check_leave = FindFirst("employeesleave",$where_leave,"RefId");
                  if ($check_leave) {
                     $refid   = $check_leave;
                     $reason  = "Cancel Leave";
                     $row = FindFirst("employeesleave","WHERE RefId = $refid","*");
                     if ($row) {
                        $creditName    = getRecord("leaves",$row["LeavesRefId"],"Code");
                        $DateFrom      = $row["ApplicationDateFrom"];
                        $DateTo        = $row["ApplicationDateTo"];
                        $empLeaveRefId = $row["RefId"];
                        $emprefid      = $row["EmployeesRefId"];
                        $fld           = "EmployeesLeaveRefId, Reason, FiledDate, EmployeesRefId, LeavesRefId, ";
                        $vals          = "$empLeaveRefId, '$reason', '".date("Y-m-d",time())."', '".$emprefid."', '".$row["LeavesRefId"]."',";
                        $save          = f_SaveRecord("NEWSAVE","fl_cancellation_request",$fld,$vals);
                        $fldnval       = "Status = 'Request For Cancellation',";
                        $update_empleave = f_SaveRecord("EDITSAVE","employeesleave",$fldnval,$empLeaveRefId);
                        if ($update_empleave == "") {
                           if (!is_numeric($save)) {
                              echo "Error ".$save;
                           }
                        } else {
                           echo "Error ".$update_empleave;
                        }
                     } else {
                        echo 'alert("No Record Found");';
                     }
                  }
               }
               /*=========================================================================================================*/
               //GET OB FILED
               /*=========================================================================================================*/
               $where_ob = "WHERE CompanyRefId = $CompanyID AND BranchRefId = $BranchID AND EmployeesRefId = '$EmpRefId'";
               $where_ob .= " AND ApplicationDateFrom <= '".$fulldate."' AND ApplicationDateTo >= '".$fulldate."'";
               $where_ob .= " AND Status = 'Approved'";
               $row_ob = FindFirst("employeesauthority",$where_ob,"*");
               if ($row_ob) {
                  $day_name = date("D",strtotime($fulldate));
                  if ($day_name != "") {
                     switch ($day_name) {
                        case 'Mon':
                           $day_name = "Monday";
                           break;
                        case 'Tue':
                           $day_name = "Tuesday";
                           break;
                        case 'Wed':
                           $day_name = "Wednesday";
                           break;
                        case 'Thu':
                           $day_name = "Thursday";
                           break;
                        case 'Fri':
                           $day_name = "Friday";
                           break;
                        case 'Sat':
                           $day_name = "Saturday";
                           break;
                        case 'Sun':
                           $day_name = "Sunday";
                           break;
                     }
                  }
                  $where_worksched = "WHERE CompanyRefId = $CompanyID";
                  $where_worksched .= " AND BranchRefId = $BranchID";
                  $where_worksched .= " AND EmployeesRefId = '$EmpRefId'";
                  $WorkScheduleRefId = FindFirst("empinformation",$where_worksched,"WorkScheduleRefId");
                  $rs_worksched = FindFirst("workschedule","WHERE RefId = $WorkScheduleRefId","*");
                  $day_in                    = $day_name."In";
                  $day_out                   = $day_name."Out";
                  $day_LBOut                 = $day_name."LBOut";
                  $day_LBIn                  = $day_name."LBIn";
                  $data_timein               = $rs_worksched[$day_in];
                  $data_timeout              = $rs_worksched[$day_out];
                  $data_LunchOut             = $rs_worksched[$day_LBOut];
                  $data_LunchIn              = $rs_worksched[$day_LBIn];
                  $coreTime                  = ($data_timeout - $data_timein) - ($data_LunchIn - $data_LunchOut);
                  $coreTime                  = $coreTime / 60;
                  $from                      = $row_ob["FromTime"];
                  $to                        = $row_ob["ToTime"];
                  $time_diff                 = ($to - $from) / 60;
                  if ($rs_worksched[$day_name."isRestDay"] != 1) {
                     if (($from == "" && $to == "") || ($coreTime <= $time_diff)) {
                        echo '$("#content_'.$fulldate.'").html("'.getRecord("absences",$row_ob["AbsencesRefId"],"Name").'");'."\n";
                     }
                  }
                  
               }
               if (isset($row["Holiday"])) {
                  if ($row["Holiday"] != "") {
                     $num_date = explode("-",$fulldate);
                     $content = "<span class='dayNum'>".$num_date[2]."<br><br></span>";
                     $content .= "<b class='NOENTRY'>".$row["Holiday"]."<br><br><br></b>";
                     echo '$("#content_'.$fulldate.'").html("'.$content.'");'."\n";   
                  }   
               }
               
               if ($WorkSchedule != "") {
                  if (isset($row["OffSus"])) {
                     if ($row["OffSus"] != "") {
                        $day_name = date("D",strtotime($fulldate));
                        if ($day_name != "") {
                           switch ($day_name) {
                              case 'Mon':
                                 $day_name = "Monday";
                                 break;
                              case 'Tue':
                                 $day_name = "Tuesday";
                                 break;
                              case 'Wed':
                                 $day_name = "Wednesday";
                                 break;
                              case 'Thu':
                                 $day_name = "Thursday";
                                 break;
                              case 'Fri':
                                 $day_name = "Friday";
                                 break;
                              case 'Sat':
                                 $day_name = "Saturday";
                                 break;
                              case 'Sun':
                                 $day_name = "Sunday";
                                 break;
                           }
                        }
                        $where_worksched = "WHERE CompanyRefId = $CompanyID";
                        $where_worksched .= " AND BranchRefId = $BranchID";
                        $where_worksched .= " AND EmployeesRefId = '$EmpRefId'";
                        $rs_worksched = FindFirst("workschedule","WHERE RefId = $WorkSchedule","*");
                        $day_in                    = $day_name."In";
                        $day_out                   = $day_name."Out";
                        $day_LBOut                 = $day_name."LBOut";
                        $day_LBIn                  = $day_name."LBIn";
                        $data_timein               = $rs_worksched[$day_in];
                        $data_timeout              = $rs_worksched[$day_out];
                        $data_LunchOut             = $rs_worksched[$day_LBOut];
                        $data_LunchIn              = $rs_worksched[$day_LBIn];
                        $coreTime                  = $data_timein;
                        if ($coreTime >= $row["OffSusTime"]) {
                           echo '$("#content_'.$fulldate.'").html("'.$row['OffSus'].'");'."\n";   
                        }
                     }   
                  }
               }
            }
         }
      }   
   }
   

   function fnFillDetails(){
      $date = getvalue("date");
      $EmpRefId = getvalue("EmpRefId");
      $userID = 0;
      $CompanyID = getvalue("hCompanyID");
      $BranchID = getvalue("hBranchID");
      if ($EmpRefId) {
         if (strtotime($date) > time()) {
            echo '$("#btnProcess").prop("disabled",true);';
            echo '$.notify("Cannot Process Future Date.");';
         } else {
            echo '$("#btnProcess").prop("disabled",false);';
         }
         $where = "WHERE CompanyRefId = $CompanyID AND BranchRefId = $BranchID AND EmployeesRefId = '$EmpRefId' AND AttendanceDate = '".$date."'";
         $rs_empAttendance = SelectEach("employeesattendance",$where);
         if ($rs_empAttendance) {
            while ($row = mysqli_fetch_assoc($rs_empAttendance)) {
               switch ($row["KindOfEntry"]) {
                  case 1:
                     echo 'setValueByName("entry_TI","'.date("H:i",$row["CheckTime"]).'");'."\n";
                     echo '$("[name=\'entry_TI\']").attr("checktime","'.$row["CheckTime"].'");'."\n";
                     break;
                  case 2:
                     echo 'setValueByName("entry_LO","'.date("H:i",$row["CheckTime"]).'");'."\n";
                     echo '$("[name=\'entry_LO\']").attr("checktime","'.$row["CheckTime"].'");'."\n";
                     break;
                  case 3:
                     echo 'setValueByName("entry_LI","'.date("H:i",$row["CheckTime"]).'");'."\n";
                     echo '$("[name=\'entry_LI\']").attr("checktime","'.$row["CheckTime"].'");'."\n";
                     break;
                  case 4:
                     echo 'setValueByName("entry_TO","'.date("H:i",$row["CheckTime"]).'");'."\n";
                     echo '$("[name=\'entry_TO\']").attr("checktime","'.$row["CheckTime"].'");'."\n";
                     break;
                  case 5:
                     echo 'setValueByName("entry_OTI",'.date("H:i",$row["CheckTime"]).'");'."\n";
                     echo '$("[name=\'entry_OTI\']").attr("checktime","'.$row["CheckTime"].'");'."\n";
                     break;
                  case 6:
                     echo 'setValueByName("entry_OTO","'.date("H:i",$row["CheckTime"]).'");'."\n";
                     echo '$("[name=\'entry_OTO\']").attr("checktime","'.$row["CheckTime"].'");'."\n";
                     break;
                  case 7:
                     echo 'setValueByName("entry_OBO","'.date("H:i",$row["CheckTime"]).'");'."\n";
                     echo '$("[name=\'entry_OBO\']").attr("checktime","'.$row["CheckTime"].'");'."\n";
                     break;
                  case 8:
                     echo 'setValueByName("entry_OBI","'.date("H:i",$row["CheckTime"]).'");'."\n";
                     echo '$("[name=\'entry_OBI\']").attr("checktime","'.$row["CheckTime"].'");'."\n";
                     break;
               }
            }
         }
         switch ($CompanyID) {
            case '2':
               include 'inc/inc_bio_fill_details_2.e2e.php';
               break;
            
            default:
               include 'inc/inc_bio_fill_details_default.e2e.php';
               break;
         }
         $remarks_where = "WHERE EmployeesRefId = '$EmpRefId' AND AttendanceDate = '$date'";
         $check_remark = FindFirst("dtr_remarks",$remarks_where,"Remarks");
         if ($check_remark) {
            echo '$("#DTRRemarks").val("'.$check_remark.'");';   
         }
         
      } else {
         echo "alert('NO REFID ASSIGNED');";
         return;
      }
   }

   function fnDTRSave(){
      //include 'mdbcn.e2e.php';
      $date       = getvalue("date");
      $punch      = getvalue("punch");
      $new_utc    = strtotime($date." ".$punch);
      $EmpRefId   = getvalue("EmpRefId");
      $kEntry     = getvalue("kEntry");
      $date_arr   = explode("-", $date);
      $checktime  = getvalue("checktime");
      $remarks    = getvalue("remarks");
      $year       = $date_arr[0];
      $month      = $date_arr[1];
      $day        = $date_arr[2];
      $js         = "";
      //$Terminal = getMAC();
      $time_arr = explode(":", $punch);
      $time1 = ($time_arr[0] * 60) * 60;
      $time2 = ($time_arr[1] * 60);
      $time = $time1 + $time2;
      $utc_date = strtotime($date);
      $utc = $time + $utc_date;
      $clause = "";
      if ($checktime == "") {
         $checktime = 0;
      }
      
      /*echo date("Y-m-d H:i s",1519908240)."\n";
      echo "Time: ".$time."\n";
      echo "Date: ".$date."\n";
      echo "Punch: ".$punch."\n";
      echo "UTC: ".$utc."\n";
      echo "KEntry: ".$kEntry."\n";
      echo "Checktime: ".$checktime."\n";*/

      $remarks_fld = "EmployeesRefId , Remarks, AttendanceDate,";
      $remarks_val = "'$EmpRefId', '$remarks', '$date', ";
      $remarks_fldnval = "Remarks = '$remarks', ";
      $remarks_where = "WHERE EmployeesRefId = '$EmpRefId' AND AttendanceDate = '$date'";
      $check_remark = FindFirst("dtr_remarks",$remarks_where,"RefId");
      if ($check_remark) {
         $UpdateRemarks = f_SaveRecord("EDITSAVE","dtr_remarks",$remarks_fldnval,$check_remark);
      } else {
         $SaveRemarks = f_SaveRecord("NEWSAVE","dtr_remarks",$remarks_fld,$remarks_val);
      }
      //$find_attendance = "WHERE EmployeesRefId = '$EmpRefId' AND KindOfEntry = $kEntry AND Checktime = $checktime";
      $find_attendance = "WHERE EmployeesRefId = '$EmpRefId' AND KindOfEntry = $kEntry AND AttendanceDate = '$date'";
      /*echo $find_attendance."\n";
      return false;*/
      $find_attendance_rs = FindFirst("employeesattendance",$find_attendance,"*");
      if ($find_attendance_rs) {
         $row = $find_attendance_rs;
         $RefId = $row["RefId"];
         //echo $RefId;
         $Fields = "`Checktime` = $utc, `AttendanceTime` = $utc,";
         $SaveSuccessfull = f_SaveRecord("EDITSAVE","employeesattendance",$Fields,$RefId);
         if (!(strpos($SaveSuccessfull,"Error") > 0)) {
            $js .= "fillAttendance('".$year."','".$month."','".$day."');";
            $js .= "afterEditSave(".$RefId.");";
         }
      } else {
         $Fields = "`EmployeesRefId`, `AttendanceDate`, `AttendanceTime`, `KindOfEntry`, `CheckTime`, ";
         $Values = "'$EmpRefId', '$date', '$utc', '$kEntry','$utc',";
         //echo $Fields."<br>".$Values;
         $SaveSuccessfull = f_SaveRecord("NEWSAVE","employeesattendance",$Fields,$Values);
         if (is_numeric($SaveSuccessfull)) {
            $js .= "fillAttendance('".$year."','".$month."','".$day."');";
            $js .= "afterNewSave(".$SaveSuccessfull.");";
         }
      }
      $day = explode("-",$date)[2];
      $timein = date("h:i A",$new_utc);
      if ($kEntry == 1) {
         //echo '$("#TI_'.$day.'").html("Hello '.$timein.'");'."\n";
         echo '$("#TI_'.$date.'").html("'.$timein.'");'."\n";
         echo '$("#TI_'.$date.'").attr("checktime","'.$new_utc.'");'."\n";
      }
      if ($kEntry == 2) {
         //echo '$("#LO_'.$day.'").html("Hello '.$timein.'");'."\n";
         echo '$("#LO_'.$date.'").html("'.$timein.'");'."\n";
         echo '$("#LO_'.$date.'").attr("checktime","'.$new_utc.'");'."\n";
      }
      if ($kEntry == 3) {
         //echo '$("#LI_'.$day.'").html("Hello '.$timein.'");'."\n";
         echo '$("#LI_'.$date.'").html("'.$timein.'");'."\n";
         echo '$("#LI_'.$date.'").attr("checktime","'.$new_utc.'");'."\n";
      }
      if ($kEntry == 4) {
         //echo '$("#TO_'.$day.'").html("Hello '.$timein.'");'."\n";
         echo '$("#TO_'.$date.'").html("'.$timein.'");'."\n";
         echo '$("#TO_'.$date.'").attr("checktime","'.$new_utc.'");'."\n";
      }
      echo $js;
   }

   function fnPostStatus($params) {
      $Fields = "";
      $counter = 0;
      $thisYear = date("Y",time());
      if (getvalue("status") == "Rejected")  {
         $Fields  = "`Reason` = '".getvalue("reason")."', ";
         $Fields .= "`Status` = '".getvalue("status")."', ";
      } else {
         $Fields  = "`ApprovedByRefId` = '".getvalue("apprvBy")."', ";
         $Fields .= "`Status` = '".getvalue("status")."', ";
      }
      $SaveSuccessfull = f_SaveRecord("EDITSAVE",getvalue("tbl"),$Fields,getvalue("refid"));
      //$SaveSuccessfull = "";
      if ($SaveSuccessfull == "") {
         switch (getvalue("tbl")) {
            /*case 'employeesleavemonetization':
               $rowMonetization = FindFirst(getvalue("tbl"),"WHERE RefId = ".getvalue("refid"),"*");
               if ($rowMonetization) {
                  $slVal = $rowMonetization["SLValue"];
                  $vlVal = $rowMonetization["VLValue"];
                  $emprefid = $rowMonetization["EmployeesRefId"];
                  $company = $rowMonetization["CompanyRefId"];
                  $branch = $rowMonetization["BranchRefId"];
                  $where = "WHERE CompanyRefId = $company AND BranchRefId = $branch AND EmployeesRefId = '$emprefid'";
                  $where .= " AND EffectivityYear = '".$thisYear."'";
                  if ($slVal > 0) {
                     $CBrow = FindFirst("employeescreditbalance",$where." AND NameCredits = 'SL'","*");
                     $cbrefid = $CBrow["RefId"];
                     if ($CBrow["OutstandingBalance"] != "") {
                        $newsl = $CBrow["OutstandingBalance"] - $slVal;
                     } else {
                        $newsl = $CBrow["BeginningBalance"] - $slVal;
                     }
                     $fldnval = "OutstandingBalance = '".$newsl."', BegBalAsOfDate = '".date("Y-m-d",time())."',";
                     $update = f_SaveRecord("EDITSAVE","employeescreditbalance",$fldnval,$cbrefid);
                     if ($update != "") {
                        $counter++;
                     }
                  }
                  if ($vlVal > 0) {
                     $CBrow = FindFirst("employeescreditbalance",$where." AND NameCredits = 'VL'","*");
                     $cbrefid = $CBrow["RefId"];
                     if ($CBrow["OutstandingBalance"] != "") {
                        $newvl = $CBrow["OutstandingBalance"] - $vlVal;
                     } else {
                        $newvl = $CBrow["BeginningBalance"] - $vlVal;
                     }
                     $fldnval = "OutstandingBalance = '".$newsl."', BegBalAsOfDate = '".date("Y-m-d",time())."',";
                     $update = f_SaveRecord("EDITSAVE","employeescreditbalance",$fldnval,$cbrefid);
                     if ($update != "") {
                        $counter++;
                     }
                  }
               }
               break;*/
            /*case 'employeesauthority':
               $rowAuthority = FindFirst(getvalue("tbl"),"WHERE RefId = ".getvalue("refid"),"*");
               if ($rowAuthority) {
                  $emprefid = $rowAuthority["EmployeesRefId"];
                  $workschedule = FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'","WorkScheduleRefId");
                  $rowWorkSched = FindFirst("workschedule","WHERE RefId = $workschedule","*");
                  $AttendanceDate = $rowAuthority["ApplicationDateFrom"];
                  $OB_Fld       = "EmployeesRefId, AttendanceDate, AttendanceTime, CheckTime, KindOfEntry,";
                  if ($rowAuthority["FromTime"] != "" || $rowAuthority["ToTime"] != "") {
                     $OBOUT = $rowAuthority["FromTime"] * 60;   
                     $OBIN = $rowAuthority["ToTime"] * 60;
                     $OBOUT_Val  = "$emprefid, '$AttendanceDate', $OBOUT, $OBOUT, 7,";
                     $OBIN_Val  = "$emprefid, '$AttendanceDate', $OBIN, $OBIN, 8,";
                     $save_obout = f_SaveRecord("NEWSAVE","employeesattendance",$OB_Fld,$OBOUT_Val);
                     if (is_numeric($save_obout)) {
                        $save_obin = f_SaveRecord("NEWSAVE","employeesattendance",$OB_Fld,$OBIN_Val);
                        if (!is_numeric($save_obin)) {
                           echo $save_obin;
                           $counter++;
                        }
                     } else {
                        echo $save_obout;
                     }
                  } else {
                     $DateFrom = $rowAuthority["ApplicationDateFrom"];
                     $DateTo = $rowAuthority["ApplicationDateTo"];
                     $date_diff = dateDifference($DateFrom,$DateTo);
                     $counter = 0;
                     for ($i=0; $i <= $date_diff; $i++) { 
                        $AttendanceDate = date('Y-m-d', strtotime( "$DateFrom + $i day" ));
                        $day_name = date("D",strtotime( "$DateFrom + $i day" ));
                        switch ($day_name) {
                           case 'Mon':
                              $day_name = "Monday";
                              break;
                           case 'Tue':
                              $day_name = "Tuesday";
                              break;
                           case 'Wed':
                              $day_name = "Wednesday";
                              break;
                           case 'Thu':
                              $day_name = "Thursday";
                              break;
                           case 'Fri':
                              $day_name = "Friday";
                              break;
                           case 'Sat':
                              $day_name = "Saturday";
                              break;
                           case 'Sun':
                              $day_name = "Sunday";
                              break;
                        }
                        $OBOUT = $rowWorkSched[$day_name."In"] * 60;
                        $OBIN  = $rowWorkSched[$day_name."Out"] * 60;
                        $OBOUT = strtotime($AttendanceDate) + $OBOUT;
                        $OBIN  = strtotime($AttendanceDate) + $OBIN; 
                        $OBOUT_Val  = "$emprefid, '$AttendanceDate', $OBOUT, $OBOUT, 7,";
                        $OBIN_Val  = "$emprefid, '$AttendanceDate', $OBIN, $OBIN, 8,";
                        $save_obout = f_SaveRecord("NEWSAVE","employeesattendance",$OB_Fld,$OBOUT_Val);
                        if (is_numeric($save_obout)) {
                           $save_obin = f_SaveRecord("NEWSAVE","employeesattendance",$OB_Fld,$OBIN_Val);
                           if (!is_numeric($save_obin)) {
                              echo $save_obin;
                              $counter++;
                           }
                        } else {
                           echo $save_obout;
                           $counter++;
                        }
                     }
                  }
               } else {
                  echo "alert('No Record Found');";   
               }
            break;*/
            /*case "employeescto":
               $row = FindFirst(getvalue("tbl"),"WHERE RefId = ".getvalue("refid"),"*");
               if ($row) {
                  $time       = $row["Hours"] * 60;
                  $emprefid   = $row["EmployeesRefId"];
                  $company    = $row["CompanyRefId"];
                  $branch     = $row["BranchRefId"];
                  $where      = "WHERE CompanyRefId = $company AND BranchRefId = $branch AND EmployeesRefId = '$emprefid'";
                  $where      .= " AND EffectivityYear = '".$thisYear."'";
                  $CBrow = FindFirst("employeescreditbalance",$where." AND NameCredits = 'OT'","*");
                  $cbrefid = $CBrow["RefId"];
                  if ($CBrow["OutstandingBalance"] != "") {
                     $newcoc = $CBrow["OutstandingBalance"] - $time;
                  } else {
                     $newcoc = $CBrow["BeginningBalance"] - $time;
                  }
                  $fldnval = "OutstandingBalance = '".$newcoc."', BegBalAsOfDate = '".date("Y-m-d",time())."',";
                  $update = f_SaveRecord("EDITSAVE","employeescreditbalance",$fldnval,$cbrefid);
                  if ($update != "") {
                     $counter++;
                  }
               }
               break;*/
            /*case 'employeesleave':     
               $row = FindFirst(getvalue("tbl"),"WHERE RefId = ".getvalue("refid"),"*");
               if ($row) {
                  $emprefid   = $row["EmployeesRefId"];
                  $company    = $row["CompanyRefId"];
                  $branch     = $row["BranchRefId"];
                  $DateFrom   = $row["ApplicationDateFrom"];
                  $DateTo     = $row["ApplicationDateTo"];
                  $date_diff  = dateDifference($DateFrom,$DateTo);
                  $credit     = getRecord("leaves",$row["LeavesRefId"],"Code");
                  $where      = "WHERE CompanyRefId = $company";
                  $where      .= " AND BranchRefId = $branch";
                  $where      .= " AND EmployeesRefId = '$emprefid'";
                  $where      .= " AND EffectivityYear = '".$thisYear."'";
                  $where      .= " AND NameCredits = '".$credit."'";
                  if ($credit == "VL") {
                     $CBrow = FindFirst("employeescreditbalance",$where,"*");
                     if ($CBrow) {
                        $cbrefid = $CBrow["RefId"];
                        if ($CBrow["OutstandingBalance"] != "") {
                           $newleave = $CBrow["OutstandingBalance"] - $date_diff;
                        } else {
                           $newleave = $CBrow["BeginningBalance"] - $date_diff;
                        }
                        if ($newleave <= 0) {
                           $newleave = 0;
                        }
                        $fldnval = "OutstandingBalance = '".$newleave."', BegBalAsOfDate = '".date("Y-m-d",time())."',";
                        $update = f_SaveRecord("EDITSAVE","employeescreditbalance",$fldnval,$cbrefid);
                        if ($update != "") {
                           $counter++;
                        }
                     }   
                  } else if ($credit == "SL") {
                     $CBrow = FindFirst("employeescreditbalance",$where,"*");
                     if ($CBrow) {
                        $cbrefid = $CBrow["RefId"];
                        if ($CBrow["OutstandingBalance"] != "") {
                           $newleave = $CBrow["OutstandingBalance"] - $date_diff;
                        } else {
                           $newleave = $CBrow["BeginningBalance"] - $date_diff;
                        }
                        if ($newleave < 0) {
                           $where      = "WHERE CompanyRefId = $company";
                           $where      .= " AND BranchRefId = $branch";
                           $where      .= " AND EmployeesRefId = '$emprefid'";
                           $where      .= " AND EffectivityYear = '".$thisYear."'";
                           $where      .= " AND NameCredits = 'VL'";
                           $new_CBrow  = FindFirst("employeescreditbalance",$where,"*");
                           if ($new_CBrow) {
                              $borrow_refid = $new_CBrow["RefId"];
                              if ($CBrow["OutstandingBalance"] != "") {
                                 $borrow_leave = $CBrow["OutstandingBalance"];
                              } else {
                                 $borrow_leave = $CBrow["BeginningBalance"];
                              }
                              if ($borrow_leave > $newleave) {
                                 $borrow_left = $borrow_leave + $newleave;   
                              } else {
                                 $borrow_left = 0;
                              }
                              if ($borrow_left > 0) {
                                 $remarks = "You have borrowed '".$borrow_left."' from your VL";
                                 $update_leave = "WithPay = 1, ";
                                 $borrow_result = f_SaveRecord("EDITSAVE","employeesleave",$update_leave,getvalue("refid"));
                                 if ($borrow_result != "") {
                                    $counter++;
                                 }
                              } else {
                                 $remarks = "You have borrowed all your VL Earnings (".$borrow_leave."). ";
                                 $update_leave = "WithPay = 0, ";
                                 $borrow_result = f_SaveRecord("EDITSAVE","employeesleave",$update_leave,getvalue("refid"));
                                 if ($borrow_result != "") {
                                    $counter++;
                                 }
                              }
                              $borrowed = $borrow_left - $borrow_leave;
                              $new_fldnval   = "OutstandingBalance = '".$borrow_left."',";
                              $new_fldnval   .= "BegBalAsOfDate = '".date("Y-m-d",time())."',";
                              $new_fldnval   .= "Remarks = '$remarks', ";
                              $new_update    = f_SaveRecord("EDITSAVE","employeescreditbalance",$new_fldnval,$borrow_refid);
                              if ($new_update != "") {
                                 $counter++;
                              } else {
                                 $newleave = 0;
                              }
                           }
                        }
                        $fldnval = "OutstandingBalance = '".$newleave."', BegBalAsOfDate = '".date("Y-m-d",time())."',";
                        $update = f_SaveRecord("EDITSAVE","employeescreditbalance",$fldnval,$cbrefid);
                        if ($update != "") {
                           $counter++;
                        }
                     }
                  }
                  
               }
               break;*/
            case 'fl_cancellation_request':
               $row = FindFirst(getvalue("tbl"),"WHERE RefId = ".getvalue("refid"),"*");
               if ($row) {
                  $leaverefid    = $row["EmployeesLeaveRefId"];
                  $row_leave     = FindFirst("employeesleave","WHERE RefId = $leaverefid","*");
                  if (getvalue("status") == "Rejected")  {
                     $leave_fldnval = "Status = 'Approved', ";
                  } else {
                     $leave_fldnval = "Status = 'Cancelled', ";
                  }
                  
                  $update_leave  = f_SaveRecord("EDITSAVE","employeesleave",$leave_fldnval,$row_leave["RefId"]);
                  if ($update_leave != "") {
                     $counter++;
                  }
               }
               /*$row = FindFirst(getvalue("tbl"),"WHERE RefId = ".getvalue("refid"),"*");
               if ($row) {
                  $leaverefid    = $row["EmployeesLeaveRefId"];
                  $row_leave     = FindFirst("employeesleave","WHERE RefId = $leaverefid","*");
                  if ($row_leave) {
                     $creditName    = getRecord("leaves",$row_leave["LeavesRefId"],"Code");
                     $DateFrom      = $row_leave["ApplicationDateFrom"];
                     $DateTo        = $row_leave["ApplicationDateTo"];
                     $emprefid      = $row_leave["EmployeesRefId"];
                     $date_diff     = dateDifference($DateFrom,$DateTo);
                     if ($date_diff == 0) $date_diff = 1;
                     if ($creditName == "VL" || $creditName == "SL") {
                        $wherecreditbal = "WHERE EmployeesRefId = ".$emprefid." AND NameCredits = '$creditName'";
                        $EmpCreditBal = FindFirst("employeescreditbalance",$wherecreditbal,"*");
                        if ($EmpCreditBal) {
                           $fldnval = "BegBalAsOfDate = '".date("Y-m-d",time())."',";
                           if ($EmpCreditBal["OutstandingBalance"] != "") {
                              $fldnval .= "OutstandingBalance = '".($EmpCreditBal["OutstandingBalance"] + $date_diff)."',";
                           } else {
                              $fldnval .= "OutstandingBalance = '".($EmpCreditBal["BeginningBalance"] + $date_diff)."',";
                           }
                           $update = f_SaveRecord("EDITSAVE","employeescreditbalance",$fldnval,$EmpCreditBal["RefId"]);
                           if ($update != "") {
                              $counter++;
                           } else {
                              $leave_fldnval = "Status = 'Cancelled', ";
                              $update_leave = f_SaveRecord("EDITSAVE","employeesleave",$leave_fldnval,$row_leave["RefId"]);
                              if ($update_leave != "") {
                                 $counter++;
                              }
                           }   
                        }
                     }   
                  }
                  
               }*/
               break;
            default:
               $counter = 0;
               break;
         }
         if ($counter == 0) {
            echo "alert('Record Succesfully Updated');";         
         }
      }
   }

   function fnDTRProcess() {
      $emprefid      = getvalue("emprefid");
      $month         = getvalue("month");
      $year          = getvalue("year");
      if ($month < 10) $month = "0".$month;
      $check_dtr     = FindFirst("dtr_process","WHERE EmployeesRefId = $emprefid AND Month = '$month' AND Year = '$year'","RefId");
      if ($check_dtr) {
         $result        = dtr_process($emprefid,$month,$year);   
      } else {
         $flds = "Month, Year, EmployeesRefId, ";
         $vals = "'$month', '$year', '$emprefid', ";
         $save_new_dtr = f_SaveRecord("NEWSAVE","dtr_process",$flds,$vals); 
         if (is_numeric($save_new_dtr)) {
            $result = $save_new_dtr;
         }
      }
      $worksched     = "";
      $DTRRemarks    = "";
      $tdyHours      = "";
      $tdyAbsent     = "";
      $tdyLate1      = "";
      $tdyUT1        = "";
      $tdyLate2      = "";
      $tdyUT2        = "";
      $tdyOT         = "";
      $TOTDays       = "";
      $TOTHours      = "";
      $TOTAbsent     = "";
      $TOTLate       = "";
      $TOTUT         = "";
      $TOTOT         = "";
      $VLEarned      = "";
      $SLEarned      = "";
      $VLBal         = "";
      $SLBal         = "";
      $COC           = "";
      $DayEQ         = 0;
      $HoursEQ       = 0;
      $AbsentEQ      = 0;
      $LateEQ        = 0;
      $UTEQ          = 0;
      $OTEQ          = 0;


      $WorkDayConversion   = file_get_contents(json."WorkDayConversion.json");
      $WorkDayEQ           = json_decode($WorkDayConversion, true);
      $CompanyID           = FindFirst("employees","WHERE RefId = $emprefid","CompanyRefId");
      $Setting_json        = file_get_contents(json."Settings_".$CompanyID.".json");
      $Setting_arr         = json_decode($Setting_json, true);
      if (isset($Setting_arr["WorkingHrs"])) {
         $PerDayHours         = $Setting_arr["WorkingHrs"];   
      } else {
         $PerDayHours = 8;
      }
      
      if (!isset($PerDayHours)) $PerDayHours = 8;
      if (isset($PerDayHours)) {
         for ($i=1; $i <= $TOTDays; $i++) { 
            $DayEQ = $WorkDayEQ["WorkDayConversion"][$PerDayHours] + $DayEQ;
         }
      }
      if ($PerDayHours != "") {
         for ($i=1; $i <= $TOTAbsent; $i++) { 
            $AbsentEQ = $WorkDayEQ["WorkDayConversion"][$PerDayHours] + $AbsentEQ;
         }
      }
      if (is_numeric($result)) {
         $row = FindFirst("dtr_process","WHERE RefId = $result","*");
         $worksched     = getRecord("workschedule",$row["WorkScheduleRefId"],"Name");
         $TOTDays       = $row["Total_Present_Count"];
         $TOTAbsent     = $row["Total_Absent_Count"];
         $TOTHours      = ToHours($row["Total_Regular_Hr"],"0");
         $COC           = ToHours($row["Total_COC_Hr"],"0");
         $TOTLate       = ToHours($row["Total_Tardy_Hr"],"0");
         $TOTUT         = ToHours($row["Total_Undertime_Hr"],"0");
         $TOTOT         = ToHours($row["Total_OT_Hr"],"0");
         $LateEQ        = $row["Tardy_Deduction_EQ"];
         $UTEQ          = $row["Undertime_Deduction_EQ"];
         $HoursEQ       = $row["Regular_Hr_EQ"];
         $VLEarned      = $row["VL_Earned"];
         $SLEarned      = $row["SL_Earned"];
         $OTEQ          = getEquivalent($row["Total_OT_Hr"],"workinghrsconversion");
         if (isset($PerDayHours)) {
            for ($i=1; $i <= $TOTDays; $i++) { 
               $DayEQ = $WorkDayEQ["WorkDayConversion"][$PerDayHours] + $DayEQ;
            }
         }
         if ($PerDayHours != "") {
            for ($i=1; $i <= $TOTAbsent; $i++) { 
               $AbsentEQ = $WorkDayEQ["WorkDayConversion"][$PerDayHours] + $AbsentEQ;
            }
         }
         
      }
      echo 'setValueByName("worksched","'.$worksched.'");'."\n";
      echo 'setValueByName("tdyHours","'.$tdyHours.'");'."\n";
      echo 'setValueByName("tdyAbsent","'.$tdyAbsent.'");'."\n";
      echo 'setValueByName("tdyLate1","'.$tdyLate1.'");'."\n";
      echo 'setValueByName("tdyUT1","'.$tdyUT1.'");'."\n";
      echo 'setValueByName("tdyLate2","'.$tdyLate2.'");'."\n";
      echo 'setValueByName("tdyUT2","'.$tdyUT2.'");'."\n";
      echo 'setValueByName("tdyOT","'.$tdyOT.'");'."\n";
      echo 'setValueByName("TOTDays","'.$TOTDays.'");'."\n";
      echo 'setValueByName("TOTHours","'.$TOTHours.'");'."\n";
      echo 'setValueByName("TOTAbsent","'.$TOTAbsent.'");'."\n";
      echo 'setValueByName("TOTLate","'.$TOTLate.'");'."\n";
      echo 'setValueByName("TOTUT","'.$TOTUT.'");'."\n";
      echo 'setValueByName("TOTOT","'.$TOTOT.'");'."\n";
      echo 'setValueByName("HoursEQ","'.$HoursEQ.'");'."\n";
      echo 'setValueByName("LateEQ","'.$LateEQ.'");'."\n";
      echo 'setValueByName("UTEQ","'.$UTEQ.'");'."\n";
      echo 'setValueByName("OTEQ","'.$OTEQ.'");'."\n";
      echo 'setValueByName("VLEarned","'.$VLEarned.'");'."\n";
      echo 'setValueByName("SLEarned","'.$SLEarned.'");'."\n";
      echo 'setValueByName("VLBal","'.$VLBal.'");'."\n";
      echo 'setValueByName("SLBal","'.$SLBal.'");'."\n";
      echo 'setValueByName("COC","'.$COC.'");'."\n";
      echo 'setValueByName("DayEQ","'.$DayEQ.'");'."\n";
      echo 'setValueByName("AbsentEQ","'.$AbsentEQ.'");'."\n";
      echo 'document.getElementById("overlay").style.display = "none";';
   }
   function fnGetDTRSummary() {
      $emprefid      = getvalue("emprefid");
      $month         = getvalue("month");
      $year          = getvalue("year");
      if ($month < 10) $month = "0".$month;
      
      $worksched     = "";
      $DTRRemarks    = "";
      $tdyHours      = "";
      $tdyAbsent     = "";
      $tdyLate1      = "";
      $tdyUT1        = "";
      $tdyLate2      = "";
      $tdyUT2        = "";
      $tdyOT         = "";
      $TOTDays       = "";
      $TOTHours      = "";
      $TOTAbsent     = "";
      $TOTLate       = "";
      $TOTUT         = "";
      $TOTOT         = "";
      $VLEarned      = "";
      $SLEarned      = "";
      $VLBal         = "";
      $SLBal         = "";
      $COC           = "";
      $DayEQ         = 0;
      $HoursEQ       = 0;
      $AbsentEQ      = 0;
      $LateEQ        = 0;
      $UTEQ          = 0;
      $OTEQ          = 0;


      $WorkDayConversion   = file_get_contents(json."WorkDayConversion.json");
      $WorkDayEQ           = json_decode($WorkDayConversion, true);
      $CompanyID           = FindFirst("employees","WHERE RefId = $emprefid","CompanyRefId");
      $Setting_json        = file_get_contents(json."Settings_".$CompanyID.".json");
      $Setting_arr         = json_decode($Setting_json, true);
      if (isset($Setting_arr["WorkingHrs"])) {
         $PerDayHours         = $Setting_arr["WorkingHrs"];   
      } else {
         $PerDayHours = 8;
      }
      
      if (!isset($PerDayHours)) $PerDayHours = 8;
      $empinfo_row = FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'","WorkScheduleRefId");
      if ($empinfo_row) {
         $worksched     = getRecord("workschedule",$empinfo_row,"Name");
      }
      $where_dtr = "WHERE EmployeesRefId = '$emprefid' AND Month = '$month' AND Year = '$year'";
      $row = FindFirst("dtr_process",$where_dtr,"*");
      if ($row) {
         //$worksched     = getRecord("workschedule",$row["WorkScheduleRefId"],"Name");
         $TOTDays       = $row["Total_Present_Count"];
         $TOTAbsent     = $row["Total_Absent_Count"];
         $TOTHours      = ToHours($row["Total_Regular_Hr"],"0");
         $COC           = ToHours($row["Total_COC_Hr"],"0");
         $TOTLate       = ToHours($row["Total_Tardy_Hr"],"0");
         $TOTUT         = ToHours($row["Total_Undertime_Hr"],"0");
         $TOTOT         = ToHours($row["Total_OT_Hr"],"0");
         $LateEQ        = $row["Tardy_Deduction_EQ"];
         $UTEQ          = $row["Undertime_Deduction_EQ"];
         $HoursEQ       = $row["Regular_Hr_EQ"];
         $VLEarned      = $row["VL_Earned"];
         $SLEarned      = $row["SL_Earned"];
         $OTEQ          = getEquivalent($row["Total_OT_Hr"],"workinghrsconversion");   
      }
      
      if (isset($PerDayHours)) {
         for ($i=1; $i <= $TOTDays; $i++) { 
            $DayEQ = $WorkDayEQ["WorkDayConversion"][$PerDayHours] + $DayEQ;
         }
      }
      if ($PerDayHours != "") {
         for ($i=1; $i <= $TOTAbsent; $i++) { 
            $AbsentEQ = $WorkDayEQ["WorkDayConversion"][$PerDayHours] + $AbsentEQ;
         }
      }
      echo 'setValueByName("worksched","'.$worksched.'");'."\n";
      echo 'setValueByName("tdyHours","'.$tdyHours.'");'."\n";
      echo 'setValueByName("tdyAbsent","'.$tdyAbsent.'");'."\n";
      echo 'setValueByName("tdyLate1","'.$tdyLate1.'");'."\n";
      echo 'setValueByName("tdyUT1","'.$tdyUT1.'");'."\n";
      echo 'setValueByName("tdyLate2","'.$tdyLate2.'");'."\n";
      echo 'setValueByName("tdyUT2","'.$tdyUT2.'");'."\n";
      echo 'setValueByName("tdyOT","'.$tdyOT.'");'."\n";
      echo 'setValueByName("TOTDays","'.$TOTDays.'");'."\n";
      echo 'setValueByName("TOTHours","'.$TOTHours.'");'."\n";
      echo 'setValueByName("TOTAbsent","'.$TOTAbsent.'");'."\n";
      echo 'setValueByName("TOTLate","'.$TOTLate.'");'."\n";
      echo 'setValueByName("TOTUT","'.$TOTUT.'");'."\n";
      echo 'setValueByName("TOTOT","'.$TOTOT.'");'."\n";
      echo 'setValueByName("HoursEQ","'.$HoursEQ.'");'."\n";
      echo 'setValueByName("LateEQ","'.$LateEQ.'");'."\n";
      echo 'setValueByName("UTEQ","'.$UTEQ.'");'."\n";
      echo 'setValueByName("OTEQ","'.$OTEQ.'");'."\n";
      echo 'setValueByName("VLEarned","'.$VLEarned.'");'."\n";
      echo 'setValueByName("SLEarned","'.$SLEarned.'");'."\n";
      echo 'setValueByName("VLBal","'.$VLBal.'");'."\n";
      echo 'setValueByName("SLBal","'.$SLBal.'");'."\n";
      echo 'setValueByName("COC","'.$COC.'");'."\n";
      echo 'setValueByName("DayEQ","'.$DayEQ.'");'."\n";
      echo 'setValueByName("AbsentEQ","'.$AbsentEQ.'");'."\n";
   }

   function fncancelDTRSave() {
      $date     = getvalue("date");
      $EmpRefId = getvalue("EmpRefId");
      $kEntry   = getvalue("kEntry");
      $row      = FindFirst("employeesattendance","where AttendanceDate = '$date' AND `KindOfEntry` = '$kEntry' ","*");

   }

   /*DONT MODIFY HERE*/
   $funcname = "fn".getvalue("fn");
   $params   = getvalue("params");
   if (!empty($funcname)) {
      $funcname($params);
   } else {
      echo 'alert("Error... No Function defined");';
   }
?>