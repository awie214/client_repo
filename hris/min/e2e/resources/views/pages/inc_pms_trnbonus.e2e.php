<?php
   function insertIconBonus() {
      echo
      '<a href="javascript:void(0);">
         <i class="fa fa-plus-square" aria-hidden="true" id="bonusInsert" title="INSERT NEW" style="color:white;"></i>
      </a>';
   }
   function dobtnBonus() {
      echo
      '<hr>
      <div class="row">
         <div class="col-xs-12 txt-center">';
               createButton("Save","btnLocSaveItem","btn-cls4-sea","fa-floppy-o","");
               createButton("Cancel","btnLocCancelItem","btn-cls4-red","fa-undo","");
         echo
         '</div>
      </div>';
   }

?>
<div class="mypanel">
   <div class="row margin-top" id="newBenefits">
      <div class="col-xs-12">
         <div class="panel-top">
            <?php insertIconBonus(); ?> <label>INSERT BONUS</label>
         </div> 
         <div class="panel-mid">
            <?php
               $table = "employees";
               $tableHdr = ["Covered Period","Bonus/Benefits","Amount","Tax Type"];
               $tableFld = ["","","","",""];
               $sql = "SELECT * FROM `$table` WHERE RefId = 0 ORDER BY RefId Desc LIMIT 100";
               $action = ["true","true","true"];
               doGridTable($table,
                           $tableHdr,
                           $tableFld,
                           $sql,
                           $action,
                           "Deduction");
            ?>
         </div>
         <div class="panel-bottom"></div>
      </div>
   </div>
</div>
