<?php
    $dbName = $_SERVER["DOCUMENT_ROOT"] . "/HRISDEV/hris/min/e2e/database/biometrics/att2000_2.mdb";

    include 'conn.e2e.php';
    include 'constant.e2e.php';
    include pathClass.'0620functions.e2e.php';

    if (!file_exists($dbName)) {
       die("Could not find database file.");
    }

    $driver = 'MDBTools';
    $dataSourceName = "odbc:Driver=$driver;DBQ=$dbName;Uid=;Pwd=;";
    //$connection = new PDO("odbc:DRIVER={Microsoft Access Driver (*.mdb)}; DBQ=$dbName; Uid=; Pwd=;");
    $connection = new PDO($dataSourceName);
    $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $result = null;
    $USERID = 0;
    $LastPunchInOut = 0;


    $query = 'SELECT USERID, Badgenumber, Name FROM USERINFO';
    $result = $connection->query($query)->fetchAll(PDO::FETCH_ASSOC);

    if ($result) {
        echo "SUCCESS CONNECTIONS<br>";
        foreach($result as $row) {
            echo $row["Name"]." - ".$row["USERID"]." - ".$row["Badgenumber"]."<br>";

        }
    }
    $result = null;
    $connection = null;

?>