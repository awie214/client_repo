<?php
	require_once "constant.e2e.php";
    require_once "colors.e2e.php";
    require_once "conn.e2e.php";
    $CompanyID = 2;
    require_once pathClass.'0620functions.e2e.php';
    require_once "mdbcn.e2e.php";
    function get_today_minute($utc) {
		$baseline = strtotime(date("Y-m-d",$utc));
		return intval(($utc - $baseline) / 60);
	}

	function get_minute($time_in,$time_out) {
		return ((($time_in - $time_out) / 60) - 60);
	}


    $arr = array();
    

    $KEntryContent = file_get_contents(json."bioKindEntry_".$CompanyID.".json");
  	$KEntry_json   = json_decode($KEntryContent, true);
	$new_week = "";
	$week_count = "0";
   	$num_of_days = "31";
   	$NewYear = "2018";
   	$NewMonth = "03"; 
   	$month_start = "2018-03-01";
   	$month_end = "2018-03-31";
   	$emprefid = "36";
   	for($i=1;$i<=$num_of_days;$i++) {
		if ($i <= 9) {
			$i = "0".$i;
		}
		$y = $NewYear."-".$NewMonth."-".$i;
		$day = date("D",strtotime($y));	
		$week = date("W",strtotime($y));
		if ($week != $new_week) {
			$week_count++;
		}
		$flds = "AttendanceDate,Day,Week";
		$vals = "'".$y."','".$i."','$week_count'";
		$dtr = [
	   		"AttendanceDate" => "$y",
	   		"AttendanceTime" => "0",
	   		"UTC" => "0",
	   		"TimeIn" => "0",
	   		"LunchOut" => "0",
	   		"LunchIn" => "0",
	   		"TimeOut" => "0",
	   		"Day" => "$i",
	   		"Week" => "$week_count",
	   		"KEntry" => "0",
	   	];
	   	$arr["ARR"][$y] = $dtr;
		$new_week = $week;
	}
	
	$where_empAtt = "WHERE EmployeesRefId = '".$emprefid."' AND AttendanceDate BETWEEN '".$month_start."' AND '".$month_end."'";
	$rs_empAtt = SelectEach("employeesattendance",$where_empAtt);
	if ($rs_empAtt) {
		while ($row = mysqli_fetch_assoc($rs_empAtt)) {
			$AttendanceDate 	= $row["AttendanceDate"];
			$AttendanceTime 	= $row["AttendanceTime"];
			$UTC 				= $row["CheckTime"];
			$KEntry         	= $row["KindOfEntry"];
			$fldnval        	= "AttendanceTime = '".$UTC."',";
			$fldnval        	.= "UTC = '".$UTC."',";
			$fld 				= "";
			$val 				= "";
			switch ($KEntry) {
				case 1:
					$fld 		= "TimeIn";
					$val     	= get_today_minute($UTC);
					$fldnval	.= "TimeIn = '".get_today_minute($UTC)."',";
					break;
				case 2:
					$fld 		= "LunchOut";
					$val     	= get_today_minute($UTC);
					$fldnval	.= "LunchOut = '".get_today_minute($UTC)."',";
					break;
				case 3:
					$fld 		= "LunchIn";
					$val     	= get_today_minute($UTC);
					$fldnval	.= "LunchIn = '".get_today_minute($UTC)."',";
					break;
				case 4:
					$fld 		= "TimeOut";
					$val     	= get_today_minute($UTC);
					$fldnval	.= "TimeOut = '".get_today_minute($UTC)."',";
					break;
			}
			$arr["ARR"][$AttendanceDate][$fld] = $val;
			$arr["ARR"][$AttendanceDate]["UTC"] = $UTC;
		}
	}
	$biometricsID = FindFirst("employees","WHERE RefId = $emprefid","BiometricsID");
    $query = "SELECT USERID, Badgenumber, Name FROM USERINFO";
    $result = $connection->query($query)->fetchAll(PDO::FETCH_ASSOC);
    if ($result) {
        foreach($result as $row) {
           	switch ($CompanyID) {
              	case "1000":
                	$mdb_field = "Badgenumber";
              		break;
              	case "2":
                	$mdb_field = "Name";
              		break;
           	}

           	if ($row[$mdb_field] == $biometricsID) {
              	$userID = $row["USERID"];
              	break;
           	}
        }
    }
    $query = 'SELECT USERID, CHECKTIME, CHECKTYPE, VERIFYCODE, SENSORID, sn FROM CHECKINOUT WHERE USERID = '.$userID.' AND VERIFYCODE = 1';
    $result = $connection->query($query)->fetchAll(PDO::FETCH_ASSOC);
    foreach($result as $row) {
       	$utc 				= strtotime($row["CHECKTIME"]);
       	$d 					= date("Y-m-d",$utc);
    	$mdbTime 			= date("H:i",$utc);
    	$mdbDate 			= date("Y-m-d",$utc);
    	$fldnval        	= "AttendanceTime = '".$utc."',";
		$fldnval        	.= "UTC = '".$utc."',";
		$where 				= "WHERE AttendanceDate = '".$mdbDate."'";
		$fld 				= "";
		$val 				= "";
    	switch ($row["CHECKTYPE"]) {
          	case "I":
          		$fld 		= $KEntry_json["FieldTimeIn"];
          		$val 		= get_today_minute($utc);
             	$fldnval	.= $KEntry_json["FieldTimeIn"]." = '".get_today_minute($utc)."'";
             	$where 		.= " AND TimeIn IS NULL";
             	break;
          	case "0":
          		$fld 		= $KEntry_json["FieldLunchOut"];
          		$val 		= get_today_minute($utc);
             	$fldnval	.= $KEntry_json["FieldLunchOut"]." = '".get_today_minute($utc)."'";
             	$where 		.= " AND LunchOut IS NULL";
             	break;
          	case "1":
          		$fld 		= $KEntry_json["FieldLunchIn"];
          		$val 		= get_today_minute($utc);
             	$fldnval	.= $KEntry_json["FieldLunchIn"]." = '".get_today_minute($utc)."'";
             	$where 		.= " AND LunchIn IS NULL";
             	break;
          	case "O":
          		$fld 		= $KEntry_json["FieldTimeOut"];
          		$val 		= get_today_minute($utc);
            	$fldnval	.= $KEntry_json["FieldTimeOut"]." = '".get_today_minute($utc)."'";
            	$where 		.= " AND TimeOut IS NULL";
            	break;
    	}
    	if (isset($arr["ARR"][$d]["AttendanceDate"])) {

    		if ($arr["ARR"][$d]["AttendanceDate"] == $d) {
	    		if ($arr["ARR"][$d][$fld] == 0) {
		    		$arr["ARR"][$d][$fld] = $val;
		    		$arr["ARR"][$d]["UTC"] = $utc;
		    	}	
	    	}	
    	}
	}
	$dbg = true;

	if ($dbg) {
		print_r(json_encode($arr));
	
		echo '
			<table border=1 style="width:100%;">
				<tr>
					<td>AttendanceDate</td>
					<td>AttendanceTime</td>
					<td>UTC</td>
					<td>TimeIn</td>
					<td>LunchOut</td>
					<td>LunchIn</td>
					<td>TimeOut</td>
					<td>Day</td>
					<td>Week</td>
					<td>KEntry</td>
				</tr>
		';
		foreach ($arr as $value) {
			foreach ($value as $key => $new_val) {
				//print_r(json_encode($new_val));
				
				echo '
					<tr>
						<td>'.$new_val["AttendanceDate"].'</td>
						<td>'.$new_val["AttendanceTime"].'</td>
						<td>'.$new_val["UTC"].'</td>
						<td>'.$new_val["TimeIn"].'</td>
						<td>'.$new_val["LunchOut"].'</td>
						<td>'.$new_val["LunchIn"].'</td>
						<td>'.$new_val["TimeOut"].'</td>
						<td>'.$new_val["Day"].'</td>
						<td>'.$new_val["Week"].'</td>
						<td>'.$new_val["KEntry"].'</td>
					</tr>
				';
				
			}
			
		}
		echo '</table>';

	}














































































	/*$tt_empdoc_arr = array();
	$emp_count = 0;
	$emp_count = mysqli_num_rows(SelectEach("employees",""));

	$sql = "SELECT * FROM attachdoctype";
	$rs = mysqli_query($conn,$sql);
	if (mysqli_num_rows($rs) > 0) {
		while ($row = mysqli_fetch_assoc($rs)) {	
			$c = $row["Code"];
			$n = $row["Name"];
			$ttcount_arr["CODE"][$c] = [$n,$emp_count];
		}
	}
	$dir = path."images/EmpDocument";
  	if (is_dir($dir)){
     	if ($dh = opendir($dir)){
	        while ($file = readdir($dh))
        	{
	           	if (stripos($file,".png") > 0 ||
	               	stripos($file,".jpg") > 0 ||
	               	stripos($file,".gif") > 0 ||
	               	stripos($file,".jpeg") > 0 ||
	               	stripos($file,".pdf") > 0
	              	) {

	              	$arr = explode("_", $file);
	              	$type = $arr[0];
	              	$emprefid = explode(".", $arr[1])[0];
	              	$count = $ttcount_arr["CODE"][$type][1];
	              	$rs = FindFirst("employees","WHERE RefId = ".$emprefid,"*");
	              	if ($rs) {
	              		$count--;
	              		echo $emprefid."====".$type."====".$count."<br>";
	              		$ttcount_arr["CODE"][$type][1] = $count;
	              	}
	              	

	              	
	           	}	
        	}
        	closedir($dh);
     	}
  	}
  	echo '<br><br>';
	print_r(json_encode($tt_empdoc_arr));
	echo '<br><br>';
	print_r(json_encode($ttcount_arr));
	echo '<br><br>';
	echo "<br><br><table border=1 style='width:25%;'>";
	echo '
		<tr>
			<td colspan=3>checking</td>
		</tr>
		<tr>
			<td>Code</td>
			<td>Name</td>
			<td>Count</td>
		</tr>
		';
	$rs = SelectEach("attachdoctype","");
	while ($row = mysqli_fetch_assoc($rs)) {
		echo '
			<tr>
				<td>'.$row["Code"].'</td>
				<td>'.$ttcount_arr["CODE"][$row["Code"]][0].'</td>
				<td>'.$ttcount_arr["CODE"][$row["Code"]][1].'</td>
			</tr>
		';
	}
	echo "</table><br><br><br><br>";*/

?>