<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'SysFunctions.e2e.php';
   $sys = new SysFunctions();
?>

<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script language="JavaScript" src="<?php //echo jsCtrl("ctrl_OPCR"); ?>"></script>
   </head>
   <body onload = "indicateActiveModules();">
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"rms"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar(getvalue("paramTitle")); ?>
            <div class="container-fluid margin-top10">
               <div class="row">
                  <div class="col-xs-12" id="divList">
                     <div class="mypanel">
                        <div class="panel-top">EMPLOYEES LIST</div>
                        <div class="panel-mid">
                           <div class="mypanel">
                              <div class="panel-top">HEADER</div>
                              <div class="panel-mid">
                                 <div class="row margin-top">
                                    <div class="col-xs-12">
                                       <div id="spGridTable">
                                          <?php
                                             $sql = "SELECT * FROM `".strtolower($table)."` ORDER BY RefId Desc LIMIT 500";
                                             doGridTable($table,
                                                         $gridTableHdr_arr,
                                                         $gridTableFld_arr,
                                                         $sql,
                                                         [true,true,true],
                                                         $_SESSION["module_gridTable_ID"]);
                                          ?>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="panel-bottom">
                                 <?php
                                    btnINRECLO([true,true,false]);
                                 ?>
                              </div>
                           </div>
                           <div class="mypanel" style="padding-top: 5px;">
                           <div class="mypanel">
                              <div class="panel-top">DETAILS</div>
                              <div class="panel-mid">
                                 <div class="row margin-top">
                                    <div class="col-xs-12">
                                       <div id="spGridTable2">
                                          <?php
                                             $table = "employeesrequirementsdetail";
                                             $gridTableHdr = "Date Submitted|Requirements";
                                             $gridTableFld = "RefId|EmpRequirmentsRefId|DateSubmit|RequirmentsRefId";
                                             $gridTableHdr_arr = explode("|",$gridTableHdr);
                                             $gridTableFld_arr = explode("|",$gridTableFld);
                                             $_SESSION["module_table"] = $table;
                                             $_SESSION["module_gridTableHdr_arr"] = $gridTableHdr_arr;
                                             $_SESSION["module_gridTableFld_arr"] = $gridTableFld_arr;
                                             $_SESSION["module_sql"] = $sql;
                                             $_SESSION["module_gridTable_ID"] = "gridTable";
                                             $sql = "SELECT * FROM `$table` WHERE EmpRequirementsRefId = 0 ORDER BY RefId Desc LIMIT 100";
                                             doGridTable($table,
                                                         $gridTableHdr_arr,
                                                         $gridTableFld_arr,
                                                         $sql,
                                                         [false,false,false],
                                                         "gridTable_Requirements");
                                          ?>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="panel-bottom"></div>
                           </div>
                        </div>
                     </div>
                     <div class="panel-bottom"></div>
                  </div>
                  <div id="divView">
                     <div class="mypanel">
                        <div class="panel-top">
                           INSERTING NEW EMPLOYMENT REQUIREMENTS
                        </div>
                        <div class="panel-mid">
                           <div id="EntryScrn">
                              <div class="row" id="badgeRefId">
                                 <div class="col-xs-5">
                                    <ul class="nav nav-pills">
                                       <li class="active" style="font-size:12pt;font-weight:600;">
                                          <a>REFID : <span class="badge" style="font-size:12pt;font-weight:600;" id="idRefid">
                                          </span></a>
                                       </li>
                                    </ul>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="panel-bottom">
                           <?php
                              btnSACABA([true,true,true]);
                           ?>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <?php
               footer();
               include_once ("varHidden.e2e.php");
            ?>
         </div>
      </form>
   </body>
</html>




