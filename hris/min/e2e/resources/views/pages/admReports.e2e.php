<?php
   $moduleContent = file_get_contents(json.'reports.json');
   $module = json_decode($moduleContent, true); 
   $objs = $module["Fields"];
   $label = $module["Label"];
   $inputType = $module["InputType"];
   $class = $module["Class"];
   $defvalue = $module["DefaultValue"];
   $css = $module["css"];
   $count = count($objs);
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"];?>
      <script language="JavaScript">
         $(document).ready(function () {
            $("#bAdd").click(function () {
               $("#DataEntry").modal();
            });
         }); 
      </script>
      <link rel="stylesheet" href="<?php echo $path."/css/sideBar.css"; ?>">
      <link rel="stylesheet" href="<?php echo $path."/css/table.css"; ?>">
   </head>
   <body>
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <nav class="navbar navbar-fixed-top">
            <div class="sysNameHolder sysBG" style="border-bottom:3px solid #fff;">
               <?php
                  $TRNBTN = 0;
                  $title = "";
                  $Logout = true;
                  include $files["inc"]["hdr"];
               ?>
               <span class="sysName">Human Resources And Information System</span>
            </div>
         </nav>
         <div style="margin-top:60px; padding: 5px;">
            <?php doSideBarMain(); ?>
            <div class="container-fluid" id="mainScreen">
               <?php doTitleBar($module["Title"]); ?>
               <?php spacer(5) ?>
               <?php
                  include "inc/inc_gridTable.e2e.php";
                  include "inc/inc_ModalDataEntry.e2e.php";
                  footer();
                  include "varHidden.e2e.php";
               ?>
            </div>
         </div> 
      </form>
   </body>
</html>



