<?php
   include "params.e2e.php";
 
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script src="<?php echo jsCtrl("ctrl_amsFileSetup"); ?>"></script>
      <style>
      </style>
      <script language="JavaScript">
      </script>
   </head>
   <body onload = "indicateActiveModules();">
      <form name="xForm" method="post" id="xForm" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"ams"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php
               doTitleBar ($paramTitle);
            ?>
               <div class="row">
                  <div class="col-xs-12" id="bodyContent">
                  <?php spacer(5); ?>
                        <div class="row" class="amsTabsHolder">
                           <div class="col-xs-3 bgTab padd5">
                              <div class="dropdown">
                                 <div class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;ATTENDANCE RELATED/AMS DATA
                                 </div>
                                 <ul class="dropdown-menu">
                                    <li><a href="javascript:void(0);" class="subMenu" memof="ams" pre="ams" route="FileSetup" id="mCutOff">
                                       <i class="fa fa-arrow-right" aria-hidden="true"></i>&nbsp;Cut-Off</a></li>
                                    <li><a href="javascript:void(0);" class="subMenu" memof="ams" pre="ams" route="FileSetup" id="mLeaves">
                                       <i class="fa fa-arrow-right" aria-hidden="true"></i>&nbsp;Leaves</a></li>
                                    <li><a href="javascript:void(0);" class="subMenu" memof="ams" pre="ams" route="FileSetup" id="mAbsences">
                                       <i class="fa fa-arrow-right" aria-hidden="true"></i>&nbsp;Office Authority</a></li>
                                    <li><a href="javascript:void(0);" class="subMenu" memof="ams" pre="ams" route="FileSetup" id="mHolidays">
                                       <i class="fa fa-arrow-right" aria-hidden="true"></i>&nbsp;Holidays</a></li>
                                    <li>
                                       <a href="javascript:void(0);" class="subMenu" pre="ams" route="FileSetup" id="mWorkSched">
                                          <i class="fa fa-arrow-right" aria-hidden="true"></i>&nbsp;Work Schedule
                                       </a>
                                    </li>
                                    <li>
                                       <a href="javascript:void(0);" class="subMenu" memof="ams" pre="ams" route="FileSetup" id="mOfficeSuspension">
                                          <i class="fa fa-arrow-right" aria-hidden="true"></i>&nbsp;Office Suspension
                                       </a>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                           <div class="col-xs-2 bgTab padd5">
                              <div class="dropdown">
                                 <div class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;TABLE
                                 </div>
                                 <ul class="dropdown-menu margin-top10">
                                    <li><a href="javascript:void(0);" class="subMenu" id="mTable1" pre="ams" route="FileSetup">
                                       <i class="fa fa-arrow-right" aria-hidden="true"></i>&nbsp;Table I</a></li>
                                    <li><a href="javascript:void(0);" class="subMenu" id="mTable2" pre="ams" route="FileSetup">
                                       <i class="fa fa-arrow-right" aria-hidden="true"></i>&nbsp;Table II</a></li>
                                    <li><a href="javascript:void(0);" class="subMenu" id="mTable3" pre="ams" route="FileSetup">
                                       <i class="fa fa-arrow-right" aria-hidden="true"></i>&nbsp;Table III</a></li>
                                    <li><a href="javascript:void(0);" class="subMenu" id="mTable4" pre="ams" route="FileSetup">
                                       <i class="fa fa-arrow-right" aria-hidden="true"></i>&nbsp;Table IV</a></li>
                                 </ul>
                              </div>
                           </div>
                        </div>

                        <?php

                        if (!empty($ScreenName)) {
                           if ($sql) {
                        ?>
                              <div>
                                 <div id="divList">
                                    <div class="row">
                                       <div class="col-xs-12 padd5" id="tbContent">
                                          <?php
                                          if (isset($childFile)) {
                                          ?>
                                          <div class="mypanel">
                                             <div class="panel-top"><span><?php echo strtoupper($ScreenName); ?></span> LIST</div>
                                             <div class="panel-mid">
                                                      <span id="spGridTable">
                                                         <?php
                                                            doGridTable($table,
                                                                        $gridTableHdr_arr,
                                                                        $gridTableFld_arr,
                                                                        $sql,
                                                                        $Action,
                                                                        $gridtable);
                                                         ?>
                                                      </span>
                                             </div>
                                             <div class="panel-bottom">
                                                <?php
                                                   btnINRECLO([true,true,false]);
                                                ?>
                                             </div>
                                          </div>
                                          <?php
                                          }
                                          ?>
                                       </div>
                                    </div>
                                 </div>
                                 <div id="divView">
                                    <?php
                                       if (isset($childFile)) {
                                       spacer(10);
                                    ?>
                                       <div class="mypanel">
                                          <div class="panel-top">
                                             <span id="ScreenMode">INSERTING NEW</span> <?php echo strtoupper($ScreenName); ?>
                                          </div>
                                          <div class="panel-mid-litebg">
                                             <?php require_once $childFile.".e2e.php"; ?>
                                          </div>
                                          <div class="panel-bottom">
                                             <?php btnSACABA([true,true,true]); ?>
                                          </div>
                                       </div>
                                    <?php
                                       }
                                    ?>
                                 </div>
                              </div>
                        <?php
                           } else {
                           // Tables
                           spacer(5);
                           require_once $childFile.".e2e.php";
                           }
                        }
                        ?>

                  </div>
               </div>
            <?php
               footer();
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>

   </body>
</html>