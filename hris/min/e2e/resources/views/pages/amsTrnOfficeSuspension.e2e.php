<?php 
   $module = module("OfficeSuspension"); 
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script language="JavaScript">
         $(document).ready(function () {
            $("#LocSAVE").click(function () {
               if (saveProceed() == 0) {
                  $("[name='xForm']").submit();
               }
            });
            $("[name='EndTime']").addClass("mandatory");
            $("[name='EndTime_mode']").addClass("mandatory");
            $("[name='StartTime']").addClass("mandatory");
            $("[name='StartTime_mode']").addClass("mandatory");
         });
         function afterDelete(refid){
            alert("Record "+refid+" Successfully Deleted!!!");
            gotoscrn("amsTrnOfficeSuspension","");
            
         }
      </script>
   </head>
   <body onload = "indicateActiveModules();">
      <form name="xForm" method="POST" action="postMultiple.e2e.php">
         <?php $sys->SysHdr($sys,"ams"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar($paramTitle); ?>
            <div class="container-fluid margin-top">
               <div class="row">
                  <div class="col-xs-12" id="div_CONTENT">
                        <div id="divList">
                           <div class="mypanel">
                              <div class="panel-top">LIST</div>
                              <div class="panel-mid">
                                 <span id="spGridTable">
                                    <?php
                                       doGridTable($table,
                                                   $gridTableHdr_arr,
                                                   $gridTableFld_arr,
                                                   $sql,
                                                   $Action,
                                                   "gridTable");
                                    ?>
                                 </span>
                                 <?php
                                       btnINRECLO([true,true,false]);
                                 ?>
                              </div>
                              <div class="panel-bottom"></div>
                           </div>
                        </div>
                        <div id="divView">
                           <div class="row">
                              <div class="col-xs-8">
                                 <div class="mypanel">
                                    <div class="panel-top">
                                       <span id="ScreenMode">&nbsp;</span> <?php echo strtoupper($ScreenName); ?>
                                    </div>
                                    <div class="panel-mid">
                                       <div id="EntryScrn">
                                          <div class="row margin-top">
                                             <div class="col-xs-8">
                                                <div class="row" id="badgeRefId">
                                                   <div class="col-xs-6">
                                                      <ul class="nav nav-pills">
                                                         <li class="active" style="font-size:12pt;font-weight:600;">
                                                            <a>REFID : <span class="badge" style="font-size:12pt;font-weight:600;" id="idRefid">
                                                            </span></a>
                                                         </li>
                                                      </ul>
                                                   </div>
                                                </div>
                                                <?php
                                                   $attr = ["br"=>true,
                                                            "type"=>"text",
                                                            "row"=>true,
                                                            "name"=>"char_Name",
                                                            "col"=>"12",
                                                            "id"=>"SuspensionName",
                                                            "label"=>"Suspension Name",
                                                            "class"=>"saveFields-- mandatory",
                                                            "style"=>"",
                                                            "other"=>""];
                                                   $form->eform($attr);
                                                   echo
                                                   '<div class="row margin-top">
                                                      <div class="col-xs-12">';
                                                         $attr = ["br"=>true,
                                                                  "type"=>"text",
                                                                  "row"=>false,
                                                                  "name"=>"date_StartDate",
                                                                  "col"=>"",
                                                                  "id"=>"StartDate",
                                                                  "label"=>"Start Date",
                                                                  "class"=>"saveFields-- mandatory date--",
                                                                  "style"=>"width:100px;margin-right:5px",
                                                                  "other"=>""];
                                                         $form->eform($attr);
                                                         timePicker("StartTime","Start Time");
                                                   echo
                                                   '  </div>
                                                   </div>

                                                   
                                                   <div class="row margin-top">
                                                      <div class="col-xs-12">';
                                                         $attr = ["br"=>true,
                                                                  "type"=>"text",
                                                                  "row"=>false,
                                                                  "name"=>"date_EndDate",
                                                                  "col"=>"",
                                                                  "id"=>"EndDate",
                                                                  "label"=>"End Date",
                                                                  "class"=>"saveFields-- mandatory date--",
                                                                  "style"=>"width:100px;margin-right:5px",
                                                                  "other"=>""];
                                                         $form->eform($attr);
                                                         //timePicker("EndTime","End Time");
                                                   echo
                                                   '  </div>
                                                   </div>';
                                                   
                                                ?>
                                                <div class="row margin-top">
                                                   <div class="col-xs-12">
                                                      <div class="form-group">
                                                         <label class="control-label" for="inputs">Remarks:</label>
                                                         <textarea class="form-input saveFields--" rows="5" name="char_Remarks" placeholder="remarks"></textarea>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="col-xs-4 padd5">
                                                <label>Employees Selected</label>
                                                <select multiple id="EmpSelected" name="EmpSelected" class="form-input" style="height:250px;">
                                                </select>
                                                <p>Double Click the items to remove in the List</p>
                                                <input type="hidden" class="saveFields-- mandatory" name="hEmpSelected" for="EmpSelected">
                                             </div>
                                          </div>
                                       </div>
                                       <?php
                                          spacer(10);
                                          echo
                                          '<button type="button" class="btn-cls4-sea"
                                                   name="btnLocSAVE" id="LocSAVE">
                                             <i class="fa fa-floppy-o" aria-hidden="true"></i>
                                             &nbsp;Save
                                          </button>';
                                          btnSACABA([false,true,true]);
                                       ?>
                                    </div>
                                    <div class="panel-bottom"></div>
                                 </div>
                              </div>
                              <div class="col-xs-4">
                                 <div class="mypanel">
                                    <div class="panel-top">Employees List</div>
                                    <div class="panel-mid">
                                       <?php include_once "incEmpListSearchCriteria.e2e.php" ?>
                                       <div id="empList" style="max-height:300px;overflow:auto;">
                                          <h4>No Employees Searched</h4>
                                       </div>
                                    </div>
                                    <div class="panel-bottom">
                                       <a href="javascript:void(0);" id="clearEmpSelected">Clear Employees Selected</a>&nbsp;
                                    </div>

                                 </div>
                              </div>
                           </div>
                        </div>

                  </div>
               </div>
            </div>
            <?php
               footer();
               include "varJSON.e2e.php";
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>

   </body>
</html>